DELIMITER $$

CREATE FUNCTION first_day(dt DATETIME) RETURNS date
BEGIN
    RETURN DATE_ADD(DATE_ADD(LAST_DAY(dt),
                INTERVAL 1 DAY),
            INTERVAL - 1 MONTH);
END;

CREATE OR REPLACE VIEW v_customers_ledger AS
-- Creditr Invoices

SELECT
	`customer`.`customer_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
    `customer`.`customer_id` AS `recepientId`,
	CONCAT('Opening Balance from',' ',`customer`.`start_date`) AS `transactionDescription`,
  	`customer`.`opening_balance` AS `dr_amount`,
	'0' AS `cr_amount`,
	`customer`.`start_date` AS `transactionDate`,
	`customer`.`start_date` AS `createdAt`,
	`customer`.`start_date` AS `referenceDate`,
	`customer`.`customer_status` AS `status`,
	'Opening Balance' AS `transactionCategory`,
	'Customer Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'customer' AS `referenceTable`
FROM
customer
WHERE debit_id = 0


UNION ALL

SELECT
	`customer`.`customer_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
    `customer`.`customer_id` AS `recepientId`,
	CONCAT('Opening Balance from',' ',`customer`.`start_date`) AS `transactionDescription`,
  	'0' AS `dr_amount`,
	`customer`.`opening_balance` AS `cr_amount`,
	`customer`.`start_date` AS `transactionDate`,
	`customer`.`start_date` AS `createdAt`,
	`customer`.`start_date` AS `referenceDate`,
	`customer`.`customer_status` AS `status`,
	'Opening Balance' AS `transactionCategory`,
	'Customer Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'customer' AS `referenceTable`
FROM
customer
WHERE debit_id = 1


UNION ALL


SELECT
	`pos_order_item`.`pos_order_item_id` AS `transactionId`,
	`order_invoice`.`order_invoice_id` AS `referenceId`,
	'' AS `payingFor`,
	`order_invoice`.`order_invoice_number` AS `referenceCode`,
	`pos_order`.`pos_order_number` AS `transactionCode`,
  	`pos_order`.`customer_id` AS `recepientId`,
	CONCAT('Purchase of',service_charge.service_charge_name,' on ',`pos_order`.`order_date`) AS `transactionDescription`,
	(pos_order_item.pos_order_item_quantity*pos_order_item.pos_order_item_amount) AS `dr_amount`,
	'0' AS `cr_amount`,
	`pos_order`.`order_date` AS `transactionDate`,
	`pos_order`.`created` AS `createdAt`,
	`pos_order`.`order_date` AS `referenceDate`,
	`pos_order`.`pos_order_status` AS `status`,
	'Order' AS `transactionCategory`,
	'Customer Orders' AS `transactionClassification`,
	'pos_order' AS `transactionTable`,
	'pos_order_item' AS `referenceTable`
FROM
pos_order,pos_order_item,order_invoice,customer,service_charge

WHERE 
	pos_order.pos_order_id = pos_order_item.pos_order_id AND 
	pos_order.pos_order_id = order_invoice.pos_order_id AND 
	pos_order_item.service_charge_id = service_charge.service_charge_id AND 
	pos_order.customer_id  = customer.customer_id AND pos_order_item.pos_order_item_quantity > 0



UNION ALL


SELECT
	`customer_payments`.`payment_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	`customer_payments`.`confirm_number` AS `referenceCode`,
	`customer_payments`.`transaction_code` AS `transactionCode`,
  	`customer_payments`.`customer_id` AS `recepientId`,
	CONCAT('Payment to account') AS `transactionDescription`,
	'0' AS `dr_amount`,
	amount_paid AS `cr_amount`,
	`customer_payments`.`payment_date` AS `transactionDate`,
	`customer_payments`.`payment_created` AS `createdAt`,
	`customer_payments`.`payment_date` AS `referenceDate`,
	`customer_payments`.`cancel` AS `status`,
	'Customer Payments' AS `transactionCategory`,
	'Payments' AS `transactionClassification`,
	'pos_order' AS `transactionTable`,
	'pos_order_item' AS `referenceTable`
FROM
customer_payments

WHERE 
customer_payments.cancel = 0;

CREATE OR REPLACE VIEW v_customers_ledger_by_date AS SELECT * FROM v_customers_ledger ORDER BY referenceDate,transactionClassification ASC;

CREATE OR REPLACE VIEW v_customers_grouped_ledger AS
-- Creditr Invoices

SELECT
	`customer`.`customer_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
    `customer`.`customer_id` AS `recepientId`,
	CONCAT('Opening Balance from',' ',`customer`.`start_date`) AS `transactionDescription`,
  	`customer`.`opening_balance` AS `dr_amount`,
	'0' AS `cr_amount`,
	`customer`.`start_date` AS `transactionDate`,
	`customer`.`start_date` AS `createdAt`,
	`customer`.`start_date` AS `referenceDate`,
	`customer`.`customer_status` AS `status`,
	'Opening Balance' AS `transactionCategory`,
	'Customer Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'customer' AS `referenceTable`
FROM
customer
WHERE debit_id = 0


UNION ALL

SELECT
	`customer`.`customer_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	'' AS `referenceCode`,
	'' AS `transactionCode`,
    `customer`.`customer_id` AS `recepientId`,
	CONCAT('Opening Balance from',' ',`customer`.`start_date`) AS `transactionDescription`,
  	'0' AS `dr_amount`,
	`customer`.`opening_balance` AS `cr_amount`,
	`customer`.`start_date` AS `transactionDate`,
	`customer`.`start_date` AS `createdAt`,
	`customer`.`start_date` AS `referenceDate`,
	`customer`.`customer_status` AS `status`,
	'Opening Balance' AS `transactionCategory`,
	'Customer Opening Balance' AS `transactionClassification`,
	'' AS `transactionTable`,
	'customer' AS `referenceTable`
FROM
customer
WHERE debit_id = 1


UNION ALL


SELECT
	`pos_order_item`.`pos_order_item_id` AS `transactionId`,
	`order_invoice`.`order_invoice_id` AS `referenceId`,
	`pos_order`.`pos_order_id` AS `payingFor`,
	`order_invoice`.`order_invoice_number` AS `referenceCode`,
	`pos_order`.`pos_order_number` AS `transactionCode`,
  	`pos_order`.`customer_id` AS `recepientId`,
	CONCAT('Purchase of',service_charge.service_charge_name,' on ',`pos_order`.`order_date`) AS `transactionDescription`,
	SUM(pos_order_item.pos_order_item_quantity*pos_order_item.pos_order_item_amount) AS `dr_amount`,
	'0' AS `cr_amount`,
	`pos_order`.`order_date` AS `transactionDate`,
	`pos_order`.`created` AS `createdAt`,
	`pos_order`.`order_date` AS `referenceDate`,
	`pos_order`.`pos_order_status` AS `status`,
	'Order' AS `transactionCategory`,
	'Customer Orders' AS `transactionClassification`,
	'pos_order' AS `transactionTable`,
	'pos_order_item' AS `referenceTable`
FROM
pos_order,pos_order_item,order_invoice,customer,service_charge

WHERE 
	pos_order.pos_order_id = pos_order_item.pos_order_id AND 
	pos_order.pos_order_id = order_invoice.pos_order_id AND 
	pos_order_item.service_charge_id = service_charge.service_charge_id AND 
	pos_order.customer_id  = customer.customer_id AND pos_order_item.pos_order_item_quantity > 0

GROUP BY order_invoice.order_invoice_id


UNION ALL


SELECT
	`customer_payments`.`payment_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `payingFor`,
	`customer_payments`.`confirm_number` AS `referenceCode`,
	`customer_payments`.`transaction_code` AS `transactionCode`,
  	`customer_payments`.`customer_id` AS `recepientId`,
	CONCAT('Payment to account') AS `transactionDescription`,
	'0' AS `dr_amount`,
	amount_paid AS `cr_amount`,
	`customer_payments`.`payment_date` AS `transactionDate`,
	`customer_payments`.`payment_created` AS `createdAt`,
	`customer_payments`.`payment_date` AS `referenceDate`,
	`customer_payments`.`cancel` AS `status`,
	'Customer Payments' AS `transactionCategory`,
	'Payments' AS `transactionClassification`,
	'pos_order' AS `transactionTable`,
	'pos_order_item' AS `referenceTable`
FROM
customer_payments

WHERE 
customer_payments.cancel = 0;


CREATE OR REPLACE VIEW v_customers_grouped_ledger_by_date AS SELECT * FROM v_customers_grouped_ledger ORDER BY referenceDate,transactionClassification ASC;


CREATE OR REPLACE VIEW v_aged_customer_receivables AS
-- Creditr Invoices
SELECT
	customer.customer_id AS recepientId,
	customer.customer_name as receivables,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  = 0, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  = 0, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  = 0, v_customers_ledger.dr_amount, 0 ))
        - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  = 0, v_customers_ledger.cr_amount, 0 ))
        END
  ) AS `coming_due`,
	(
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 1 AND 30, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 1 AND 30, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 1 AND 30, v_customers_ledger.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  BETWEEN 1 AND 30, v_customers_ledger.cr_amount, 0 ))
			END
  ) AS `thirty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 31 AND 60, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 31 AND 60, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 31 AND 60, v_customers_ledger.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 31 AND 60, v_customers_ledger.cr_amount, 0 ))
			END
  ) AS `sixty_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 61 AND 90, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 61 AND 90, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 61 AND 90, v_customers_ledger.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  BETWEEN 61 AND 90, v_customers_ledger.cr_amount, 0 ))
		END
  ) AS `ninety_days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) >90, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) >90, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) >90, v_customers_ledger.dr_amount, 0 ))
				 - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  >90, v_customers_ledger.cr_amount, 0 ))
			END
  ) AS `over_ninety_days`,

  (

    (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  = 0, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  = 0, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  = 0, v_customers_ledger.dr_amount, 0 ))
					 - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  = 0, v_customers_ledger.cr_amount, 0 ))
  		END
    )-- Getting the Value for 0 Days
    + (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 1 AND 30, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 1 AND 30, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 1 AND 30, v_customers_ledger.dr_amount, 0 ))
 - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  BETWEEN 1 AND 30, v_customers_ledger.cr_amount, 0 ))
  		END
    ) --  AS `1-30 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 31 AND 60, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 31 AND 60, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 31 AND 60, v_customers_ledger.dr_amount, 0 ))
		   - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  BETWEEN 31 AND 60, v_customers_ledger.cr_amount, 0 ))
		END
    ) -- AS `31-60 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 61 AND 90, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 61 AND 90, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 61 AND 90, v_customers_ledger.dr_amount, 0 ))
				   - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) BETWEEN 61 AND 90, v_customers_ledger.cr_amount, 0 ))
  		END
    ) -- AS `61-90 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) >90, v_customers_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) >90, v_customers_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) ) >90, v_customers_ledger.dr_amount, 0 ))
				  - Sum(IF( DATEDIFF( CURDATE( ), date( v_customers_ledger.referenceDate ) )  >90, v_customers_ledger.cr_amount, 0 ))
  		END
    ) -- AS `>90 Days`
  ) AS `Total`

	FROM
		customer
	LEFT JOIN v_customers_ledger ON v_customers_ledger.recepientId = customer.customer_id AND v_customers_ledger.recepientId > 0 AND v_customers_ledger.referenceDate >= customer.start_date

	 GROUP BY customer.customer_id;




CREATE OR REPLACE VIEW v_customers_balances AS

SELECT
	`pos_order_item`.`pos_order_item_id` AS `transactionId`,
	`order_invoice`.`order_invoice_id` AS `referenceId`,
	'' AS `payingFor`,
	`order_invoice`.`order_invoice_number` AS `referenceCode`,
	`pos_order`.`pos_order_number` AS `transactionCode`,
  	`pos_order`.`customer_id` AS `recepientId`,
	CONCAT('Purchase of',service_charge.service_charge_name,' on ',`pos_order`.`order_date`) AS `transactionDescription`,
	SUM(pos_order_item.pos_order_item_quantity*pos_order_item.pos_order_item_amount) AS `dr_amount`,
	(SELECT COALESCE (SUM(customer_payment_item.amount_paid),0) 
		FROM customer_payments,customer_payment_item 
		WHERE customer_payment_item.order_invoice_id = order_invoice.order_invoice_id
	) AS `cr_amount`,
	`pos_order`.`order_date` AS `transactionDate`,
	`pos_order`.`created` AS `createdAt`,
	`pos_order`.`order_date` AS `referenceDate`,
	`pos_order`.`pos_order_status` AS `status`,
	'Order' AS `transactionCategory`,
	'Customer Orders' AS `transactionClassification`,
	'pos_order' AS `transactionTable`,
	'pos_order_item' AS `referenceTable`
FROM
pos_order,pos_order_item,order_invoice,customer,service_charge

WHERE 
	pos_order.pos_order_id = pos_order_item.pos_order_id AND 
	pos_order.pos_order_id = order_invoice.pos_order_id AND 
	pos_order_item.service_charge_id = service_charge.service_charge_id AND 
	pos_order.customer_id = customer.customer_id AND pos_order_item.pos_order_item_quantity > 0

GROUP BY pos_order_item.pos_order_id;