-- product_triggers.sql
--  store product triggers
-- add trigger

DELIMITER $$
DROP TRIGGER IF EXISTS `t_store_product_stock_insert` $$
CREATE TRIGGER t_store_product_stock_insert 
AFTER INSERT ON store_product
FOR EACH ROW
BEGIN
 INSERT IGNORE INTO product_stock_level (transactionId,
 								  product_id,
 								  category_id,
 								  store_id,
 								  receiving_store,
 								  product_name,
 								  store_name,
 								  transactionDescription,
 								  dr_quantity,
 								  cr_quantity,
 								  dr_amount,
 								  cr_amount,
 								  transactionDate,
 								  status,
 								  product_deleted,
 								  transactionCategory,
 								  transactionClassification,
 								  transactionTable,
 								  referenceTable) 

	 SELECT
	    `store_product`.`store_product_id`,
		`product`.`product_id`,
		`product`.`category_id`,
		store_product.owning_store_id,
	    '',
	    product.product_name,
	    store.store_name,
		CONCAT('Opening Balance of',' ',`product`.`product_name`),
	    `store_product`.`store_quantity`,
	    '0',
		(`product`.`product_unitprice` * `store_product`.`store_quantity` ),
		'0',
		`store_product`.`created`,
		`product`.`product_status`,
	    `product`.`product_deleted`,
		'Income',
		'Product Opening Stock',
		'store_product',
		'product'
	FROM
	store_product,product,store
	WHERE  product.product_id = store_product.product_id AND product.product_deleted = 0
	AND store.store_id = store_product.owning_store_id
END $$
DELIMITER ;

-- on update store product

DELIMITER $$
DROP TRIGGER IF EXISTS `t_store_product_stock_update` $$
CREATE TRIGGER t_store_product_stock_update AFTER UPDATE on store_product
FOR EACH ROW
BEGIN

	UPDATE product_stock_level 
		INNER JOIN store_product 
			ON product_stock_level.transactionId = `store_product`.`store_product_id` 
			AND product_stock_level.transactionClassification = 'Product Opening Stock' 
			AND product_stock_level.transactionTable = 'store_product'
		INNER JOIN product ON product.product_id = store_product.product_id AND product.product_deleted = 0
		INNER JOIN store ON store.store_id = store_product.owning_store_id
		SET  product_stock_level.transactionId = `store_product`.`store_product_id`,
			 product_stock_level.product_id = `store_product`.`product_id`,
			 product_stock_level.category_id = `product`.`category_id`,
			 product_stock_level.store_id = store_product.owning_store_id,
			 product_stock_level.receiving_store = '',
			 product_stock_level.product_name = product.product_name,
			 product_stock_level.store_name = store.store_name,
			 product_stock_level.transactionDescription = CONCAT('Opening Balance of',' ',`product`.`product_name`),
			 product_stock_level.dr_quantity = `store_product`.`store_quantity`,
			 product_stock_level.cr_quantity = '0',
			 product_stock_level.dr_amount = (`product`.`product_unitprice` * `store_product`.`store_quantity` ),
			 product_stock_level.cr_amount = '0',
			 product_stock_level.transactionDate = `store_product`.`created`,
			 product_stock_level.status = `product`.`product_status`,
			 product_stock_level.product_deleted = `product`.`product_deleted`,
			 product_stock_level.transactionCategory = 'Income',
			 product_stock_level.transactionClassification = 'Product Opening Stock',
			 product_stock_level.transactionTable = 'store_product',
			 product_stock_level.referenceTable = 'product';
END $$
DELIMITER;

-- DELETE STORE PRODUCT

DELIMITER $$
DROP TRIGGER IF EXISTS `t_store_product_stock_DELETE` $$
CREATE TRIGGER t_store_product_stock_DELETE AFTER DELETE on store_product
FOR EACH ROW
BEGIN
DELETE FROM product_stock_level
    WHERE product_stock_level.transactionId = `store_product`.`store_product_id` 
    AND product_stock_level.transactionClassification = 'Product Opening Stock' 
AND product_stock_level.transactionTable = 'store_product';
END $$
DELIMITER;
