<!-- search -->
<?php echo $this->load->view('test_search', '', TRUE);?>
<!-- end search -->
<div class="row">
	<div class="col-md-12">
		<a href="<?php echo site_url();?>lab_charges/add_lab_test" class="btn btn-info btn-sm pull-right">Add a lab test</a>
		<a href="<?php echo site_url();?>lab_charges/export_results" class="btn btn-success btn-sm pull-right" style="margin-right:10px;">Export</a>
	</div>
</div>
 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>   

           <!-- Widget content -->
        <div class="panel-body">
          <div class="padd">
          
<?php
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}
			
			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}
		$search = $this->session->userdata('lab_tests_search');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'hospital_report/laboratory/close_test_search" class="btn btn-warning">Close Search</a>';
		}
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Test</th>						
						  <th>Cash Price</th>	
						  <th colspan="2">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			foreach ($query->result() as $row)
			{
				
				$lab_test_class_id = $row->lab_test_class_id;
				$lab_test_class = $row->lab_test_class_name;
				$lab_test_name = $row->lab_test_name;
				$lab_test_units = $row->lab_test_units;
				$lab_test_price = $row->lab_test_price;
				$lab_positive_status = $row->lab_positive_status;
				$lab_test_insurance_price = $row->lab_test_insurance_price;
				//$lab_test_malelowerlimit = $row->lab_test_malelowerlimit;
				//$lab_test_malelupperlimit = $row->lab_test_malelupperlimit;
				//$lab_test_femalelowerlimit = $row->lab_test_femalelowerlimit;
				//$lab_test_femaleupperlimit = $row->lab_test_femaleupperlimit;
				$lab_test_delete = $row->lab_test_delete;
				$lab_test_id = $row->lab_test_id;
				$no_done = 0;//$this->laboratory_model->get_tests_done($lab_test_id);
				$revenue = $lab_test_price * $no_done;//$this->laboratory_model->get_tests_revenue($lab_test_id);
				$count++;
				
				if($lab_test_delete == 1)
				{
					$test_button = '<td><a href="'.site_url().'lab_charges/activation/activate/test_lab/'.$lab_test_id.'" class="btn btn-sm btn-default">Activate</a></td>';

				}
				else
				{
					$test_button = '<td><a href="'.site_url().'lab_charges/activation/deactivate/test_lab/'.$lab_test_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to deactivate this test?\');">Deactivate</a></td>';
				}
				if($lab_positive_status == 1)
				{
					$lab_positive_status = 'Yes';
				}
				else
				{
					$lab_positive_status = 'No';
				}
				$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$lab_test_name.'</td>
							<td>'.$lab_test_price.'</td>
							<td><a href="'.site_url().'hospital-reports/test-statement/'.$lab_test_id.'" class="btn btn-sm btn-info">view statement</a></td>
						</tr> 
					';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no tests";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->

      </div>
</section>