<?php
$date_tomorrow = date('Y-m-d');
$visit_date = date('jS M Y',strtotime($date_tomorrow));

$date_tomorrow = date('Y-m-d');

$doctor_results = $this->reports_model->get_all_doctors();

		$counting =0;
		$date_from =$date_tomorrow;
		$date_to =$date_tomorrow;
		$results ='';
		if($doctor_results->num_rows() > 0)
		{
		$count = $full = $percentage = $daily = $hourly = 0;

			$results .=  
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th style="padding:5px;">DOCTOR</th>
						  <th style="padding:5px;">PATIENTS</th>
						  <th style="padding:5px;">INVOICE</th>
						  <th style="padding:5px;">WAIVERS </th>
						  <th style="padding:5px;">REVENUE </th>
						  <th style="padding:5px;">PAYMENTS </th>
						  <th style="padding:5px;">BALANCES </th>
						</tr>
					</thead>
					<tbody>
				';
			$result = $doctor_results->result();
			$grand_total = 0;
			$patients_total = 0;
			$total_charge_waivers = 0;
			$total_revenue = 0;
			$total_payments_made = 0;
			$total_balances = 0;
			foreach($result as $res)
			{
				$personnel_id = $res->personnel_id;
				$personnel_onames = $res->personnel_onames;
				$personnel_fname = $res->personnel_fname;
				$personnel_type_id = $res->personnel_type_id;
				
				//get service total
				$total = $this->reports_model->get_total_collected($personnel_id, $date_from, $date_to);
				$patients = $this->reports_model->get_total_patients($personnel_id, $date_from, $date_to);
				$waivers = $this->reports_model->get_total_waivers($personnel_id, $date_from, $date_to);
				$payments_made = $this->reports_model->get_total_payments_made($personnel_id, $date_from, $date_to);

						// var_dump($doctor_results); die();
				$revenue = $total - $waivers;
				$grand_total += $total;
				$patients_total += $patients;
				$total_revenue += $revenue;
				$total_charge_waivers += $waivers;
				$total_payments_made += $payments_made;

				$balance_charged = $revenue - $payments_made;

				$total_balances += $balance_charged;
				if($patients > 0)
				{
					$count++;
					$results.= '
						<tr>
							<td style="padding:5px;">'.$count.'</td>
							<td >DR. '.strtoupper($personnel_fname).' '.strtoupper($personnel_onames).'</td>
							<td style="text-align:center;padding:5px;">'.$patients.'</td>
							<td style="text-align:center;padding:5px;">'.number_format($total, 2).'</td>
							<td style="text-align:center;padding:5px;">'.number_format($waivers, 2).'</td>
							<td style="text-align:center;padding:5px;">'.number_format($revenue, 2).'</td>
							<td style="text-align:center;padding:5px;">('.number_format($payments_made, 2).')</td>
							<td style="text-align:center;padding:5px;">'.number_format($balance_charged, 2).'</td>
							
						</tr>
					';
				}
			}
			
			$results.= 
			'
				
					<tr>
						<td colspan="2">TOTAL</td>
						<td style="text-align:center;"><span class="bold" >'.$patients_total.'</span></td>
						<td  style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($grand_total, 2).'</span></td>
						<td  style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($total_charge_waivers, 2).'</span></td>
						<td  style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($total_revenue, 2).'</span></td>
						<td  style="text-align:center;border-top:2px solid #000;"><span class="bold">('.number_format($total_payments_made, 2).')</span></td>
						<td  style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($total_balances, 2).'</span></td>
					</tr>
				</tbody>
			</table>
			';
		}


		$pharmacist_results = $this->reports_model->get_all_pharmacists();
		$counting =0;
		$date_from =$date_tomorrow;
		$date_to =$date_tomorrow;
		$pharm_results ='';
		if($pharmacist_results->num_rows() > 0)
		{
		$count = $full = $percentage = $daily = $hourly = 0;

			$pharm_results .=  
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th style="padding:5px;">PERSONNEL</th>
						   <th style="padding:5px;">PATIENTS</th>
						  <th style="padding:5px;">CASH</th>
						  <th style="padding:5px;">INSURANCE</th>
						</tr>
					</thead>
					<tbody>
				';
			$result_pharm = $pharmacist_results->result();
			$grand_total = 0;
			$patients_total = 0;
			$grand_total_insurance =0;
			
			foreach($result_pharm as $res)
			{
				$personnel_id = $res->personnel_id;
				$personnel_onames = $res->personnel_onames;
				$personnel_fname = $res->personnel_fname;
				$personnel_type_id = $res->personnel_type_id;
				$count++;
				
				//get service total
				$total = $this->reports_model->get_total_pharm_collected($personnel_id, $date_from, $date_to);
				$patients = $this->reports_model->get_total_pharm_patients($personnel_id, $date_from, $date_to);
				$insurance = $this->reports_model->get_total_pharm_collected_insurance($personnel_id, $date_from, $date_to);

				
				$grand_total += $total;
				$grand_total_insurance += $insurance;
				$patients_total += $patients;
				

				$pharm_results.= '
					<tr>
						<td style="padding:5px;">'.$count.'</td>
						<td >'.strtoupper($personnel_fname).' '.strtoupper($personnel_onames).'</td>
						<td style="padding:5px;">'.$patients.'</td>
						<td style="text-align:center;padding:5px;">'.number_format($total, 2).'</td>
						<td style="text-align:center;padding:5px;">'.number_format($insurance, 2).'</td>
						
					</tr>
				';
			}
			
			$pharm_results.= 
			'
				
					<tr>
						<td colspan="2">TOTAL</td>
						<td colspan="1">'.$patients_total.'</td>
						<td style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($grand_total, 2).'</span></td>
						<td style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($grand_total_insurance, 2).'</span></td>
					</tr>
				</tbody>
			</table>
			';
		}


		$laboratory_results = $this->reports_model->get_all_lab_techs();
		$counting =0;
		$date_from =$date_tomorrow;
		$date_to =$date_tomorrow;
		$lab_results ='';
		if($laboratory_results->num_rows() > 0)
		{
		$count = $full = $percentage = $daily = $hourly = 0;

			$lab_results .=  
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th style="padding:5px;">PERSONNEL</th>
						   <th style="padding:5px;">PROCEDURES DONE</th>
						  <th style="padding:5px;">CASH</th>
						  <th style="padding:5px;">INSURANCE</th>
						</tr>
					</thead>
					<tbody>
				';
			$result_lab = $laboratory_results->result();
			$grand_total = 0;
			$patients_total = 0;
			$grand_total_insurance =0;
			
			foreach($result_lab as $res_lab)
			{
				$personnel_id = $res_lab->personnel_id;
				$personnel_onames = $res_lab->personnel_onames;
				$personnel_fname = $res_lab->personnel_fname;
				$personnel_type_id = $res_lab->personnel_type_id;
				$count++;
				
				//get service total
				$total = $this->reports_model->get_total_lab_collected($personnel_id, $date_from, $date_to);
				$patients = $this->reports_model->get_total_lab_patients($personnel_id, $date_from, $date_to);
				$insurance = $this->reports_model->get_total_lab_collected_insurance($personnel_id, $date_from, $date_to);

				
				$grand_total += $total;
				$grand_total_insurance += $insurance;
				$patients_total += $patients;
				

				$lab_results.= '
					<tr>
						<td style="padding:5px;">'.$count.'</td>
						<td >'.strtoupper($personnel_fname).' '.strtoupper($personnel_onames).'</td>
						<td style="padding:5px;">'.$patients.'</td>
						<td style="text-align:center;padding:5px;">'.number_format($total, 2).'</td>
						<td style="text-align:center;padding:5px;">'.number_format($insurance, 2).'</td>
						
					</tr>
				';
			}
			
			$lab_results.= 
			'
				
					<tr>
						<td colspan="2">TOTAL</td>
						<td colspan="1">'.$patients_total.'</td>
						<td style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($grand_total, 2).'</span></td>
						<td style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($grand_total_insurance, 2).'</span></td>
					</tr>
				</tbody>
			</table>
			';
		}

echo '<p>Good evening to you,<br>
		Herein is a report on personnel. This is sent at '.date('H:i:s A').'
		</p>
		<h4 style="text-decoration:underline"><strong>DOCTORS SUMMARY</strong></h4>
		'.$results.'

		<h4 style="text-decoration:underline"><strong>PHARMACIST SUMMARY</strong></h4>
		'.$pharm_results.'
		<h4 style="text-decoration:underline"><strong>PHARMACIST SUMMARY</strong></h4>
		'.$lab_results.'


		';
?>