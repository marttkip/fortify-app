        <!-- Basic -->
		<meta charset="UTF-8">

		<title><?php echo $company_name;?> | <?php echo $title;?></title>
		<meta name="keywords" content="ERP" />
		<meta name="description" content="Dobi admin">
		<meta name="author" content="alvaro masitsa">


		<link rel="icon" href="favicon.ico" type="image/x-icon">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/bootstrap/css/bootstrap.min.css">
		<!-- <link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/font-awesome/css/font-awesome.min.css"> -->

		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/chartist/css/chartist.min.css">
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/toastr/toastr.min.css">

		<!-- MAIN CSS -->
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>assets/css/color_skins.css">



		<link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.css">


		<script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script> <!-- jQuery -->


