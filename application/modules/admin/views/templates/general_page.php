<?php 
    
    if(!isset($contacts))
    {
        $contacts = $this->site_model->get_contacts();
    }
    $data['contacts'] = $contacts; 

    if(count($contacts) > 0)
    {
        $email = $contacts['email'];
        $email2 = $contacts['email'];
        $logo = $contacts['logo'];
        $company_name = $contacts['company_name'];
        $phone = $contacts['phone'];
        
        if(!empty($facebook))
        {
            $facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
        }
        
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
    }

?>
<!doctype html>
<html class="fixed sidebar-left-collapsed">
    <head>
        <?php echo $this->load->view('admin/includes/header', $contacts, TRUE); ?>
    </head>

    <body class="theme-orange">

        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img src="<?php echo base_url().'assets/logo/'.$logo;?>" width="90" height="70" alt="<?php echo $company_name;?>"></div>
                <p>Please wait...</p>        
            </div>
        </div>
        <input type="hidden" id="base_url" value="<?php echo site_url();?>">
        <input type="hidden" id="config_url" value="<?php echo site_url();?>">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        <div id="wrapper">
            <!-- Top Navigation -->
            
            
                
                <?php echo $this->load->view('admin/includes/top_level_navigation', $data, TRUE); ?>
                <?php echo $this->load->view('admin/includes/sidebar', $data, TRUE); ?>
                
                
                <div id="main-content">
                    
                    
                    <?php echo $content;?>
                
               </div>
          
            
        </div>


        <script src="<?php echo base_url()."assets/themes/lucid/assets";?>/bundles/libscripts.bundle.js"></script>
        <script src="<?php echo base_url()."assets/themes/lucid/assets";?>/bundles/vendorscripts.bundle.js"></script>

        <script src="<?php echo base_url()."assets/themes/lucid";?>/vendor/toastr/toastr.js"></script>
        <script src="<?php echo base_url()."assets/themes/lucid/assets";?>/bundles/chartist.bundle.js"></script>
        <script src="<?php echo base_url()."assets/themes/lucid/assets";?>/bundles/knob.bundle.js"></script> <!-- Jquery Knob-->

        <script src="<?php echo base_url()."assets/themes/lucid/assets";?>/bundles/mainscripts.bundle.js"></script>
        <script src="<?php echo base_url()."assets/themes/lucid/assets";?>/js/index.js"></script>
        
        

        
    </body>
</html>
