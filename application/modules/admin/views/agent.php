<?php
	$result ='';

if ($query->num_rows() > 0) 
{
    $page = 1;
	foreach ($query->result() as $key => $row)
    {
    	//var_dump($row->user_name);
    	//die();
    	$number = $row->user_number;
    	$name = $row->user_name;
    	$phone = $row->user_phone;
    	$email = $row->user_email;
    	// $company = $row->company_name;

    	$result .= '
    		<tr>
                <td>'.$page.'</td>
                <td><span>'.$number.'</span></td>
                <td><span>'.$name.'</span></td>
                <td><span>'.$phone.'</span></td>
                <td>'.$email.'</td>
            </tr>
    	';
    	$page++;
    }
}
?>


 <div class="row clearfix" style="margin-top: 30px">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Customer Responses</h2>
                    <ul class="header-dropdown">
                        <li><a class="tab_btn" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Weekly">W</a></li>
                        <li><a class="tab_btn" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Monthly">M</a></li>
                        <li><a class="tab_btn active" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Yearly">Y</a></li>
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                            <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another Action</a></li>
                                <li><a href="javascript:void(0);">Something else</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">                            
                    <div>
                        <div class="table-responsive">
                        <table class="table table-hover m-b-0">
                            <thead class="thead-dark">
                                <tr>
                                <th>#</th>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo $result; ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>            
    </div>