<?php

$result ='';

if ($query->num_rows() > 0) 
{
    $page = 1;
    //var_dump($query->result());
   // die();
    foreach ($query->result() as $key => $row)
    {

        $customer_id = $row->customer_id;
        $agent_id = $row->user_name;
        $date = $row->date;
        $remarks = $row->remarks;
        $response = $row->value;
        $job_id = $row->job_id;
        $customer_name = $row->customer_name;

        $result .= '
            <tr>
                <td>'.$page.'</td>
                <td><span>'.$agent_id.'</span></td>
                <td><span><a href="'.site_url().'customers/customer-detail/'.$customer_id.'">'.$customer_name.'</a></span></td>
                <td><span class="badge badge-success">'.$response.'</span></td>
                <td>'.$remarks.'</td>
                <td>'.$job_id.'</td>

            </tr>
        ';
        $page++;

    }
}



?>

    <div class="row clearfix" style="margin-top: 30px">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Customer Responses</h2>
                    <ul class="header-dropdown">
                        <li><a class="tab_btn" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Weekly">W</a></li>
                        <li><a class="tab_btn" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Monthly">M</a></li>
                        <li><a class="tab_btn active" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Yearly">Y</a></li>
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                            <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another Action</a></li>
                                <li><a href="javascript:void(0);">Something else</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">                            
                    <div>
                        <div class="table-responsive">
                        <table class="table table-hover m-b-0">
                            <thead class="thead-dark">
                                <tr>
                                <th>#</th>
                                <th>Agent Name</th>
                                <th>Customer</th>
                                <th>Response</th>
                                <th>Remarks</th>
                                <th>Job</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo $result; ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>            
    </div>