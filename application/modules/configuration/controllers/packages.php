<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class packages extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();

		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('reception/reception_model');
		$this->load->model('packages_model');
		$this->load->model('reception/database');
		$this->load->model('messaging/messaging_model');
		$this->load->model('admin/email_model');

		$this->load->model('auth/auth_model');
		
	}

	public function dashboard()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('dashboard', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function index()
	{
		$where = 'package_id > 0';
		$table = 'package';

		$order = 'package_name';
		$order_method = 'ASC';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'configuration/packages';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->packages_model->get_all_packages($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'All packages';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		// $v_data['all_packages'] = $this->packages_model->all_packages();
		$v_data['page'] = $page;

		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('packages/all_packages', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);

	}

	public function package_detail($package_id)
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$query = $this->packages_model->get_package($package_id);
		
		
		$v_data['package_array'] = $query->row();

		$data['content'] = $this->load->view('package_detail', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}


    
	/*
	*
	*	Add a new package
	*
	*/
	public function add_package() 
	{
		//form validation rules
		$this->form_validation->set_rules('package_name', 'package Name', 'required|xss_clean');
		$this->form_validation->set_rules('package_description', 'Description', 'xss_clean');
		$this->form_validation->set_rules('package_charge', 'Charge', 'xss_clean');
		$this->form_validation->set_rules('package_status', 'Package Status', 'xss_clean');
		$this->form_validation->set_rules('package_users', 'Users', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->packages_model->add_package())
			{
				$this->session->set_userdata('success_message', 'Package added successfully');
				redirect('configuration/packages');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add package. Please try again');
			}
		}
		
		$data['title'] = 'Add  package';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('packages/add_package', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Edit an existing package
	*	@param int $package_id
	*
	*/
	public function edit_package($package_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('package_name', 'package Name', 'required|xss_clean');
		$this->form_validation->set_rules('package_description', 'Description', 'xss_clean');
		$this->form_validation->set_rules('package_charge', 'Charge', 'xss_clean');
		$this->form_validation->set_rules('package_status', 'package Status', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update package
			if($this->packages_model->update_package($package_id))
			{
				$this->session->set_userdata('success_message', 'package updated successfully');
				redirect('configuration/packages');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update package. Please try again');
			}
		}
		
		//open the add new package
		$data['title'] = 'Edit package';
		$v_data['title'] = $data['title'];
		
		//select the package from the database
		$query = $this->packages_model->get_package($package_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['package_array'] = $query->row();
			
			$data['content'] = $this->load->view('packages/edit_package', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'package does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing package
	*	@param int $package_id
	*
	*/
	public function delete_package($package_id)
	{
		$this->packages_model->delete_package($package_id);
		$this->session->set_userdata('success_message', 'package has been deleted');
		redirect('configuration/packages');
	}
    
	/*
	*
	*	Activate an existing package
	*	@param int $package_id
	*
	*/
	public function activate_package($package_id)
	{
		$this->packages_model->activate_package($package_id);
		$this->session->set_userdata('success_message', 'package activated successfully');
		redirect('configuration/packages');
	}
    
	/*
	*
	*	Deactivate an existing package
	*	@param int $package_id
	*
	*/
	public function deactivate_package($package_id)
	{
		$this->packages_model->deactivate_package($package_id);
		$this->session->set_userdata('success_message', 'package disabled successfully');
		redirect('configuration/packages');
	}
}
?>