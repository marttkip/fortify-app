<?php

class packages_model extends CI_Model 
{	
	/*
	*	Retrieve all packages
	*
	*/
	public function all_packages()
	{
		$this->db->where('package_status = 1');
		$this->db->order_by('package_name');
		$query = $this->db->get('package');
		
		return $query;
	}
	
	/*
	*	Retrieve all packages
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_packages($table, $where, $per_page, $page, $order = 'package_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Add a new package
	*	@param string $image_name
	*
	*/
	public function add_package()
	{
		

		// var_dump($_POST);die();
		$data = array(
				'package_name'=>$this->input->post('package_name'),
				'package_description'=>$this->input->post('package_description'),
				'package_status'=>$this->input->post('package_status'),
				'package_charge'=>$this->input->post('package_charge'),
				'package_users'=>$this->input->post('package_users'),
				'created'=>date('Y-m-d'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('package', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing package
	*	@param string $image_name
	*	@param int $package_id
	*
	*/
	public function update_package($package_id)
	{
		
		$data = array(
				'package_name'=>$this->input->post('package_name'),
				'package_description'=>$this->input->post('package_description'),
				'package_status'=>$this->input->post('package_status'),
				'package_charge'=>$this->input->post('package_charge'),
				'package_users'=>$this->input->post('package_users'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		$this->db->where('package_id', $package_id);
		if($this->db->update('package', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single package's details
	*	@param int $package_id
	*
	*/
	public function get_package($package_id)
	{
		//retrieve all users
		$this->db->from('package');
		$this->db->select('*');
		$this->db->where('package_id = '.$package_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing package
	*	@param int $package_id
	*
	*/
	public function delete_package($package_id)
	{
		if($this->db->delete('package', array('package_id' => $package_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated package
	*	@param int $package_id
	*
	*/
	public function activate_package($package_id)
	{
		$data = array(
				'package_status' => 1
			);
		$this->db->where('package_id', $package_id);
		
		if($this->db->update('package', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated package
	*	@param int $package_id
	*
	*/
	public function deactivate_package($package_id)
	{
		$data = array(
				'package_status' => 0
			);
		$this->db->where('package_id', $package_id);
		
		if($this->db->update('package', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
?>