<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <!-- <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul> -->
            </div>            
           
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo $title;?></h2>
                    <ul class="header-dropdown">
                        <li><a href="<?php echo site_url();?>configuration/packages" class="btn btn-success btn-md">Back to packages</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
			$row = $package_array;
			//the package details
			$package_id = $row->package_id;
			$package_name = $row->package_name;
			$package_description = $row->package_description;
			$package_charge = $row->package_charge;
			$package_status = $row->package_status;
            $package_users = $row->package_users;
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
				$package_id = set_value('$row->package_id');
				$package_name = set_value('$row->package_name');
				$package_description = set_value('$row->package_description');
				$package_charge = set_value('$row->package_charge');
				$package_status = set_value('$row->package_status');
                $package_users = set_value('$row->package_users');
				
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
            <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
            <div class="row">
                    	<div class="col-sm-6">
                            <!-- package Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Package Name</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="package_name" placeholder="package Name" value="<?php echo $package_name;?>" required>
                                </div>
                            </div>
                            
                            <!-- package Name -->
                           
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Activate package?</label>
                                <div class="col-lg-8">
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios1" type="radio" checked value="1" name="package_status">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" value="0" name="package_status">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    	<div class="col-sm-6">
                            
                            
                            <!-- package Name -->
                           	 <div class="form-group">
                                <label class="col-lg-4 control-label">Package Charge</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="package_charge" placeholder="Package Charge" value="<?php echo $package_charge;?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Max Users</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="package_users" placeholder="Package users" value="<?php echo $package_users;?>" required>
                                </div>
                            </div>
                            <!-- Activate checkbox -->
                            
                             <div class="form-group">
                                <label class="col-lg-4 control-label">Package Description</label>
                                <div class="col-lg-8">
                                	<textarea name="package_description" class="form-control" rows="5" placeholder="Package Description" required><?php echo $package_description;?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="form-actions center-align">
                        <button class="submit btn btn-primary" type="submit">
                            Edit package
                        </button>
                    </div>
                    <br />
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>