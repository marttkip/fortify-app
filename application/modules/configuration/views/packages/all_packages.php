<?php //echo $this->load->view('search/search_package', '', TRUE);?>

<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Package name</th>
						<th>Package Charge</th>
						<th>Package Description</th>
						<th>Max Users</th>
						<th>Status</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->personnel_model->retrieve_personnel();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$package_id = $row->package_id;
				$package_name = $row->package_name;
				$package_charge = $row->package_charge;
				$package_description = $row->package_description;
				$package_status = $row->package_status;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$package_users = $row->package_users;
				$users = 0;
				
				//create deactivated status display
				if($package_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-sm btn-info" href="'.site_url().'activate-package/'.$package_id.'" onclick="return confirm(\'Do you want to activate '.$package_name.'?\');" title="Activate '.$package_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($package_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-sm btn-default" href="'.site_url().'deactivate-package/'.$package_id.'" onclick="return confirm(\'Do you want to deactivate '.$package_name.'?\');" title="Deactivate '.$package_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
				
				//creators & editors
				if($admins != NULL)
				{
					foreach($admins as $adm)
					{
						$user_id = $adm->personnel_id;
						
						if($user_id == $created_by)
						{
							$created_by = $adm->personnel_fname;
						}
						
						if($user_id == $modified_by)
						{
							$modified_by = $adm->personnel_fname;
						}
					}
				}
				
				else
				{
				}
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$package_name.'</td>
						<td>'.$package_charge.'</td>
						<td>'.$package_description.'</td>
						<td>'.$package_users.'</td>
						<td>'.$status.'</td>
						<td>
							
							<a href="'.site_url().'edit-package/'.$package_id.'" class="btn btn-sm btn-success" title="Edit '.$package_name.'"><i class="fa fa-pencil"></i></a>
							'.$button.'
							<a href="'.site_url().'delete-package/'.$package_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$package_name.'?\');" title="Delete '.$package_name.'"><i class="fa fa-trash"></i></a></td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no insurance companies";
		}
?>
<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <!-- <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul> -->
            </div>            
           
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo $title;?></h2>
                    <ul class="header-dropdown">
                        <li><a href="<?php echo site_url();?>add-package" class="btn btn-success btn-md">Add package</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                	<div class="table-responsive">
						<?php echo $result;?>
					</div>
                </div>
            </div>
       	</div>
    </div>
</div>	