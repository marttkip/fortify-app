<section class="panel">
        <!-- Widget head -->
        <header class="panel-heading">
          <h4 class="pull-left"><i class="icon-reorder"></i><?php echo $title;?></h4>
          <div class="widget-icons pull-right">
          	<a href="<?php echo base_url().'hospital-administration/services';?>" class="btn btn-sm btn-info">Back to services</a>
          </div>
          <div class="clearfix"></div>
        </header>             

        <!-- Widget content -->
         <div class="panel-body">
          <div class="padd">
            
            <div class="row">
                <div class="col-md-12">
		<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
		?>
            <?php
                if(isset($import_response))
                {
                    if(!empty($import_response))
                    {
                        echo $import_response;
                    }
                }
                
                if(isset($import_response_error))
                {
                    if(!empty($import_response_error))
                    {
                        echo '<div class="center-align alert alert-danger">'.$import_response_error.'</div>';
                    }
                }
            ?>
                </div>
            </div>
            <?php echo form_open_multipart('hospital-administration/import-payments-values', array("class" => "form-horizontal", "role" => "form"));?>
           
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li>Download the import template <a href="<?php echo site_url().'hospital-administration/import-payments-template';?>">here.</a></li>
                        
                        <li>Save your file as a <strong>csv</strong> file before importing</li>
                        <li>After adding your patients to the import template please import them using the button below</li>
                    </ul>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                	<div class="col-md-4">
	                	 <div class="form-group">
	                        <label class="col-lg-4 control-label">Bank Account: </label>
	                        
	                        <div class="col-lg-8">
	                            <select class="form-control" name="bank_id">
	                            	<option value="0"> SELECT A BANK</option>
	                               	<option value="77"> Barclays Bank - Madison</option>
	                               	<option value="3"> Cooperative - AAR / AON</option>
	                               	<option value="78"> Housing Finance - NHIF</option>
	                               	<option value="79"> I & M Bank - Maraba</option>
	                               	<option value="4"> Family Bank - Maraba</option>
	                            </select>
	                        </div>
	                    </div>
	                </div>
                    <?php
                    /*$data = array(
                          'class'       => 'custom-file-input btn-red btn-width',
                          'name'        => 'import_csv',
                          'onchange'    => 'this.form.submit();',
                          'type'       	=> 'file'
                        );
                
                    echo form_input($data);*/
                    ?>
                	<div class="col-md-4">
	                    <div class="fileUpload btn btn-info">
	                        <span>Import payments</span>
	                        <input type="file" class="upload"  name="import_csv" />
	                    </div>
	                </div>
	                <div class="col-md-2">
                        <div class="center-align">
                          <button type="submit" class="btn btn-info"> Add remitance</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
		</div>
      </div>
</section>


<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Account Name</th>
						<th>Ref Number</th>
						<th>Date</th>
						<th>Amount Paid</th>
						<th>Amount Reconcilled</th>
						<th colspan="5">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->personnel_model->retrieve_personnel();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$receipt_number = $row->receipt_number;
				$account_name = $row->account_name;
				$payment_date = $row->payment_date;
				$payment_date = date('jS M Y',strtotime($row->payment_date));

				$total_payments = $this->hospital_administration_model->get_receipt_amount($receipt_number);
				$total_reconcilled = $this->hospital_administration_model->get_receipt_amount_paid($receipt_number);

				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$account_name.'</td>
						<td>'.$receipt_number.'</td>
						<td>'.$payment_date.'</td>
						<td>'.number_format($total_payments,2).'</td>
						<td>'.number_format($total_reconcilled,2).'</td>
						<td><a href="'.site_url().'hospital_administration/update_payments" class="btn btn-sm btn-info" title=""><i class="fa fa-recycle"></i> reconcile payment</a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no remitance uploaded";
		}
?>

<section class="panel">
	<header class="panel-heading">
		<div class="panel-actions">
			<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
		</div>

		<h2 class="panel-title"><?php echo $title;?></h2>
	</header>
	<div class="panel-body">
       
		<div class="table-responsive">
        	
			<?php echo $result;?>
	
        </div>
	</div>
    
    <div class="panel-foot">
        
		<?php if(isset($links)){echo $links;}?>
    
        <div class="clearfix"></div> 
    
    </div>
</section>