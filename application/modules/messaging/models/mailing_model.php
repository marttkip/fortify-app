<?php
date_default_timezone_set('Africa/Nairobi');
class Mailing_model extends CI_Model 
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}


	public function get_all_emails($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('emails.*,departments.department_name,personnel.personnel_fname,personnel.personnel_onames');
		$this->db->where($where);
		$this->db->join('departments','departments.department_id = emails.department_id','LEFT');
		$this->db->join('personnel','personnel.personnel_id = emails.created_by','LEFT');
		$this->db->order_by('emails.last_modified','DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_email_detail($email_id)
	{
		$this->db->select('emails.*,departments.department_name,personnel.personnel_fname,personnel.personnel_onames');
		$this->db->where('email_id',$email_id);
		$this->db->join('departments','departments.department_id = emails.department_id','LEFT');
		$this->db->join('personnel','personnel.personnel_id = emails.created_by','LEFT');
		$query = $this->db->get('emails');

		return $query;
	}
}
?>