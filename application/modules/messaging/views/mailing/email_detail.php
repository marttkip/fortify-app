<?php
$query_email = $this->mailing_model->get_email_detail($email_id);

if($query_email->num_rows() > 0)
{
	
	foreach ($query_email->result() as $key => $value) {
		# code...
		$email_id = $value->email_id;
		$email_description = $value->email_description;
		$email_from = $value->email_from;
		$email_cc = $value->email_cc;
		$email_to = $value->email_to;
		$email_to = $value->email_to;
		$email_status = $value->email_status;
		$created = $value->created;
		$last_modified = $value->last_modified;
		$email_deleted = $value->email_deleted;
		$patient_name = $value->patient_name;
		$email_subject = $value->email_subject;
		$last_modified = $value->last_modified;
		$department_name = $value->department_name;
		$department_id = $value->department_id;
		$phone_number = $value->phone_number;
		$personnel_onames = $value->personnel_onames;
		$personnel_fname = $value->personnel_fname;
		$category = $value->category;
		$sms = $value->sms;


		if($department_id == 0)
		{
			$department_name = 'Administration';
		}
		else
		{
			$department_name = $department_name;
		}

		if($category == 2)
		{
			$email_subject = 'Attention : '.$patient_name;
		}
		else
		{
			$email_subject = $email_subject;
		}


		$last_modified = date('jS M Y H:i A',strtotime($last_modified));
	}
}
?>
<div class="inner-body mailbox-email">
	<div class="mailbox-email-header mb-3">
		<h3 class="mailbox-email-subject m-0 font-weight-light">
			<?php echo $email_subject;?>
		</h3>

		<p class="mt-2 mb-0 text-5">From <a ><?php echo $department_name ?> Department </a> to <a ><?php echo $patient_name?></a>, on <?php echo $last_modified?></p>
	</div>
	<div class="mailbox-email-container">
		<div class="mailbox-email-screen pt-4">
			<div class="card mb-3">
				<div class="card-header">
					<div class="card-actions">
						<a class="btn btn-sm btn-warning" onclick="get_emails_view()" style="margin-top: -20px;"><i class="fa fa-reply"></i> Back to messages</a>
					</div>

					<p class="card-title"><?php echo $department_name ?> Department <i class="fa fa-angle-right fa-fw"></i> <?php echo $patient_name?></p>
				</div>
				<div class="card-body">
					<strong><?php echo $email_subject;?></strong> <br>
					<?php echo $email_description;?>
				</div>
				<div class="card-footer">
					<p class="m-0"><small> By : <?php echo $personnel_fname.' '.$personnel_onames;?></small> <small><?php echo $last_modified;?></small></p>
				</div>
			</div>

			<?php 

			if($category == 2 OR $category == 0)
			{
				?>
				<div class="card mb-3">
					<div class="card-header">	
						<p class="card-title">SMS to <?php echo $phone_number;?></p>
					</div>
					<div class="card-body">
						<?php echo $sms;?>
					</div>
					
				</div>
				<?php

			}
			?>

			

		</div>
	</div>
</div>