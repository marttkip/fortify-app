<?php
$department_email = 'appointments@upperhill.co.ke';
?>
<div >
	

<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $patient_name;?>s</h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd" >
        	<form  id="send-patient-message" method="post">
				<div class="form-group">
					<label for="to" class="control-label col-md-2">Type:</label>
					<div class="col-md-10 mailbox-compose-field">
						<div class="radio">
		                    <label>
		                        <input id="optionsRadios1" type="radio" checked value="0" name="category" onclick="get_message_view(0)">
		                        Email and SMS Only
		                    </label>
		                    <label>
		                        <input id="optionsRadios1" type="radio" value="1" name="category" onclick="get_message_view(1)">
		                        Email Only
		                    </label>
		                    <label>
		                        <input id="optionsRadios2" type="radio"  value="2" name="category" onclick="get_message_view(2)">
		                        SMS Only
		                    </label>
		                </div>
		                        
					</div>
				</div>
				<div class="form-group" id="from_div" style="display: block;">
					<label for="to" class="control-label col-md-2">From:</label>
					<div class="col-md-10 mailbox-compose-field">
						<input id="email_from" name="email_from" type="text" class="form-control"  value="<?php echo $department_email?>" required="required" >
					</div>
				</div>
				<div class="form-group" id="to_div">
					<label for="cc" class="control-label col-md-2">To:</label>
					<div class="col-md-10 mailbox-compose-field">
						
						<input id="email" name="email" type="text" class="form-control" style="display: block" required="required" value="<?php echo $patient_email?>">

					</div>
					
				</div>
				<div class="form-group" id="subject_div" style="display: block;">
					<label for="subject" class="control-label col-md-2">Subject:</label>
					<div class="col-md-10 mailbox-compose-field">
						<input id="subject" name="subject" type="text" class="form-control" required="required">
					</div>
				</div>
				<div class="form-group" id="message_div" style="display: block;">
					<label class="col-md-2 control-label">Message : </label>
					<div class="col-md-10">
						<textarea id="rx" name="rx"  class="form-control cleditor" required="required"></textarea>
					</div>
				</div>

				<div class="form-group" id="phone_div" style="display: block;">
					<label for="cc" class="control-label col-md-2">To:</label>
					<div class="col-md-10 mailbox-compose-field">
						<input id="phone_number" name="phone_number" type="text" class="form-control"  required="required" value="<?php echo $patient_phone1?>">
					</div>
					
				</div>
				<div class="form-group" id="sms_div" style="display: block;">
					<label class="col-md-2 control-label">SMS Message : </label>
					<div class="col-md-10">
						<textarea id="sms" name="sms"  class="form-control " required="required"></textarea>
					</div>
				</div>
				<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id?>">
				<br>
				<div class="center-align">
					<button  class="btn btn-sm btn-success" onclick="send_patient_message()" >SEND MESSAGE </button>
				</div> 
			</form>
			
			
        </div>
    </div>
</section>
<div class="col-md-12">					        	
	<div class="center-align">
		<a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
	</div>  
</div>
</div>