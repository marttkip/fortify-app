<?php

$result = '';
if($query != null)
{
if($query->num_rows() > 0)
{
	foreach ($query->result() as $key => $value) {
		# code...
		$patient_id = $value->patient_id;
		$picked_id = $value->picked_id;
		$patient_name = $value->patient_surname.' '.$value->patient_first_name.' '.$value->patient_othernames;
		$patient_number = $value->patient_number;
		$patient_phone1 = $value->patient_phone1;
		$patient_email = $value->patient_email;

		$result .='<li class="status-online">
				    <a onclick="remove_from_list('.$picked_id.')">
				      <input type="checkbox" name="patients[]" class="menu-icon " checked value="'.$patient_id.'#'.$patient_phone1.'#'.$patient_name.'#'.$patient_email.'">
				      <span class="name">'.$patient_name.' - '.$patient_number.' - '.$patient_phone1.'</span>
				    
				    </a>
				  </li>';
	}

	
}
else
{
	$result .='<li class="status-online">
				    <span class="name">No patients with that name</span>
				  </li>';
}

}
else
{
	$result .='<li class="status-online">
				    <span class="name">Kindly search using name , file number or phone number</span>
				  </li>';
}

echo $result;

?>