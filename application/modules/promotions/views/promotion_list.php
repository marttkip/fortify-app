<section class="panel">
	<header class="panel-heading">
		
		<h2 class="panel-title">Promotions</h2>
		<p class="panel-subtitle"></p>
	</header>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-ecommerce-simple table-striped mb-1">
				<thead>
					<tr>
						<th>Date</th>
						<th>Promotion Title</th>
						<th>Products Listed</th>
						<th>Clients Reached</th>
						<th>Clients Responses</th>
						<th>Value</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					
					<tr>
						<td>2020-10-01</td>
						<td>Valentines Promotions</td>
						<td>30</td>
						<td>20</td>
						<td>0</td>
						<td>Kes. 0.00</td>
						<td><a href="<?php echo site_url().'promotions/promotion-detail/1'?>" class="btn btn-sm btn-success"><i class="fa fa-folder"></i> Promotion Detail</a></td>
					</tr>
					
				</tbody>
			</table>
		</div>
	</div>
</section>