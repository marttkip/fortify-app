<!-- start: page -->
<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<a href="<?php echo site_url().'promotions/promotion-list'?>" class="btn btn-xs btn-warning"><i class="fa fa-arrow-left"></i> Back to promotions </a>
		</div>
		
	</div>
</div>
<div class="row">
	
	<div class="col-md-12 col-lg-12 col-xl-12">
		<div class="row">
			<div class="col-md-12 col-lg-3 col-xl-3">
				<section class="panel panel-featured-left panel-featured-primary">
					<div class="panel-body">
						<div class="widget-summary">
							<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon bg-primary">
									<i class="fa fa-life-ring"></i>
								</div>
							</div>
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Clients Reached</h4>
									<div class="info">
										<strong class="amount">0</strong>
										<!-- <span class="text-primary">(14 unread)</span> -->
									</div>
								</div>
								<div class="summary-footer">
									<a class="text-muted text-uppercase">(view all)</a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-12 col-lg-3 col-xl-3">
				<section class="panel panel-featured-left panel-featured-secondary">
					<div class="panel-body">
						<div class="widget-summary">
							<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon bg-secondary">
									<i class="fa fa-usd"></i>
								</div>
							</div>
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Communication Reach</h4>
									<div class="info">
										<strong class="amount"> 0</strong>
									</div>
								</div>
								<div class="summary-footer">
									<a class="text-muted text-uppercase">(list)</a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-12 col-lg-3 col-xl-3">
				<section class="panel panel-featured-left panel-featured-tertiary">
					<div class="panel-body">
						<div class="widget-summary">
							<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon bg-tertiary">
									<i class="fa fa-shopping-cart"></i>
								</div>
							</div>
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Promotions Views</h4>
									<div class="info">
										<strong class="amount">0</strong>
									</div>
								</div>
								<div class="summary-footer">
									<a class="text-muted text-uppercase">(list)</a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-12 col-lg-3 col-xl-3">
				<section class="panel panel-featured-left panel-featured-quartenary">
					<div class="panel-body">
						<div class="widget-summary">
							<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon bg-quartenary">
									<i class="fa fa-user"></i>
								</div>
							</div>
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Amount Earned</h4>
									<div class="info">
										<strong class="amount">3765</strong>
									</div>
								</div>
								<div class="summary-footer">
									<a class="text-muted text-uppercase">(report)</a>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<section class="panel">
		<header class="panel-heading">
			
			<h2 class="panel-title">Products Listed</h2>
			<p class="panel-subtitle"></p>
		</header>
		<div class="panel-body">
			<div class="table-responsive">
						<table class="table table table-condensed table-bordered">
							<thead>
								<tr>
									<th></th>
									<th>Product Name</th>
									<th>Price</th>
									<th>Promotion Price</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td ><a href="#"><img src="img/products/product-1.jpg" class="img-fluid" alt="Porto SmartWatch" width="45"></a></td>
									<td>Product Short Name Example</td>
									<td >Kes. 15.00</td>
									<td >Kes. 14.00</td>
								</tr>
								
							</tbody>
						</table>
					</div>
		</div>
	</section>
</div>


<div class="row">
	<section class="panel">
		<header class="panel-heading">
			
			<h2 class="panel-title">Customer's Purchases</h2>
			<p class="panel-subtitle"></p>
		</header>
		<div class="panel-body">
			<div class="table-responsive">
						<table class="table table table-condensed table-bordered">
							<thead>
								<tr>
									<th></th>
									<th>Date</th>
									<th>Client Name</th>
									<th>Purchased Items</th>
									<th>Amount Spent</th>
								</tr>
							</thead>
							<tbody>
								<tr>

									<td >2020-01-02</td>
									<td>Martin Tarus</td>
									<td >0704808007</td>
									<td >Kes. 0.00</td>
								</tr>
								
							</tbody>
						</table>
					</div>
		</div>
	</section>
</div>



<!-- end: page -->
 <?php //echo $this->load->view('administration/line_graph');?>
 <?php //echo $this->load->view('administration/bar_graph');?>
        

<!--<script type="text/javascript" src="<?php echo base_url().'assets/themes/bluish/js/reports.js';?>"></script>-->