<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Promotions extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();

		$this->load->model('site/site_model');
		$this->load->model('administration/reports_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('payroll/payroll_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('accounting/petty_cash_model');
		$this->load->model('pos/pos_model');
		$this->load->model('nurse/nurse_model');
		$this->load->model('reception/reception_model');
		$this->load->model('dental/dental_model');
		$this->load->model('reception/database');

		$this->load->model('medical_admin/medical_admin_model');
		$this->load->model('pharmacy/pharmacy_model');
		$this->load->model('accounting/hospital_accounts_model');
		$this->load->model('inventory_management/inventory_management_model');
		$this->load->model('administration/sync_model');
		$this->load->model('messaging/messaging_model');
		$this->load->model('admin/email_model');

		$this->load->model('auth/auth_model');
		$this->load->model('accounting/petty_cash_model');
		
	}

	public function dashboard()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('dashboard', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function promotion_list()
	{

		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('promotion_list', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

	public function promotion_detail($promotion_id)
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('promotion_detail', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
}
?>