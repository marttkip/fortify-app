<?php
        $specialist_query = $this->site_model->get_active_items('Our Team');
          $specialist_item = '';
          if($specialist_query->num_rows() > 0)
          {
            $x=0;
            foreach($specialist_query->result() as $row)
            {
              $about_title = $row->post_title;
              $post_id = $row->post_id;
              $blog_category_name = $row->blog_category_name;
              $blog_category_id = $row->blog_category_id;
              $post_title = $row->post_title;
              $web_name = $this->site_model->create_web_name($post_title);
              $post_status = $row->post_status;
              $post_views = $row->post_views;
              $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
              $created_by = $row->created_by;
              $modified_by = $row->modified_by;
              $post_target = $row->post_target;
              $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
              $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
              $description = $row->post_content;
              $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
              $created = $row->created;
              $day = date('j',strtotime($created));
              $month = date('M',strtotime($created));
              $year = date('Y',strtotime($created));
              $created_on = date('jS M Y',strtotime($row->created));
              $x++;
              if($x < 9)
              {
                $x = '0'.$x;
              }
    
              
              ?>
            
               
             
            <?php
        }
       }

        $gallery_rs = $this->site_model->get_branch_gallery_list('Our Team');
         // var_dump($gallery_rs);die();
        $gallery = '';
        if($gallery_rs->num_rows() > 0)
        {
            foreach ($gallery_rs->result() as $key => $value) {
              # code...
              $gallery_image_name = base_url().'assets/gallery/'.$value->gallery_image_name;
              $gallery_image_thumb = $value->gallery_image_thumb;

              $gallery .= ' <div class="col-md-3"><span class="gallery-popover-link" data-full="'.$gallery_image_name.'"><img src="'.$gallery_image_name.'" alt="" class="img-fluid"></span></div>';


            }

             // var_dump($gallery_image_name);die();
        }


        $blog_category_id = $this->site_model->get_category_id('Our Team');

        $category_items = $this->site_model->get_active_post_content_by_category($blog_category_id);
        
        $main_services = '';
        if($category_items->num_rows() > 0)
        {
          
         
          foreach ($category_items->result() as $key => $row) {
            # code...

            $post_title = $row->post_title;

             $post_id = $row->post_id;
            $blog_category_name = $row->blog_category_name;
            $blog_category_id = $row->blog_category_id;
            $post_title = $row->post_title;
            $web_name = $this->site_model->create_web_name($post_title);
            $post_status = $row->post_status;
            $post_views = $row->post_views;
            $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
            $created_by = $row->created_by;
            $modified_by = $row->modified_by;
            $post_target = $row->post_target;
            $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
            $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
            $description = $row->post_content;
            $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
            $created = $row->created;
            $day = date('j',strtotime($created));
            $month = date('M',strtotime($created));
            $year = date('Y',strtotime($created));
            $created_on = date('jS M Y',strtotime($row->created));
              
            $web_name = $this->site_model->create_web_name($post_title);
            if(empty($image_specialist))
            {
              $image_specialist = 'https://via.placeholder.com/500';
            }
            $main_services .= '

                          <div class="container mt-0">
                            <div class="row">
                              <div class="col-md">
                                <div class="doctor-page-photo text-center">
                                 <img src="'.$image_specialist.'" class="img-fluid" alt="">
                                                          
                                </div>
                              <div class="mt-3 mt-md-5"></div>
                              </div>
                              <div class="col-lg-8 mt-0 mt-lg-0">
                                <div class="doctor-info mb-3 mb-lg-4">
                                  <div >
                                    <div class="heading-block border-bottom-0 mb-4">
                                        <h2 class="mb-4 nott">'.$post_title.'</h2>
                                    </div>
                                    <h6>'.$post_target.'</h6>
                                  </div>
                                  
                                </div>

                                '.$description.'
                               </div>
                            </div>
                          </div>
                          ';

          }
         
         
        }
        else
        {
          // put that category here
        }

  



        $directors_gallery_rs = $this->site_model->get_branch_gallery_list('Directors');
         // var_dump($directors_gallery_rs);die();
        $directors_gallery = '';
        if($directors_gallery_rs->num_rows() > 0)
        {
            foreach ($directors_gallery_rs->result() as $key => $value) {
              # code...
              $gallery_image_name = base_url().'assets/gallery/'.$value->gallery_image_name;
              $gallery_image_thumb = $value->gallery_image_thumb;

              $directors_gallery .= '
              ';





            }

             // var_dump($gallery_image_name);die();
        }
        $doctors_gallery_rs = $this->site_model->get_branch_gallery_list('Doctors');
         // var_dump($directors_gallery_rs);die();
        $doctors_gallery = '';
        if($doctors_gallery_rs->num_rows() > 0)
        {
            foreach ($doctors_gallery_rs->result() as $key => $value) {
              # code...
              $gallery_image_name = base_url().'assets/gallery/'.$value->gallery_image_name;
              $gallery_image_thumb = $value->gallery_image_thumb;

              /*$doctors_gallery .= ' <div class="col-sm-6 col-md-4">
            <div class="doctor-box text-center">
              <div class="doctor-box-photo">
                <a href=""><img src="'.$gallery_image_name.'" alt="" class="img-fluid"></a>
              </div>
              <h5 class="doctor-box-name"><a href="doctor-page.html">Dr. Name</a></h5>
              <div class="doctor-box-position">Dental Surgeon.</div>
             
              
            </div>
          </div>';*/

          $doctors_gallery .= '
            <div class="col-sm-6 col-lg-4">
              <div class="team">
                <div class="team-image">
                  <img src="https://via.placeholder.com/500" alt="">
                </div>
                <div class="team-desc">
                  <div class="team-title">
                   <div class="heading-block border-bottom-0 mb-4">
                        <h3 class="mb-4 nott">Dentist </h3>
                    </div>
                  <span>Neurologist</span></div>
                </div>
              </div>
            </div>
          ';
            }
             // var_dump($gallery_image_name);die();
        }
     ?>
<!-- Page Title
    ============================================= -->
   <!-- Page Title
    ============================================= -->
<section id="page-title" class="page-title-dark page-title-parallax page-title-center" style="background: linear-gradient(to bottom, rgba(6, 54, 57,.9), rgba(6, 54, 57,.9)), url('<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/cause/grid/1.jpg') no-repeat center center; background-size: cover; padding: 30px 0;">

  <div class="container clearfix">
    <h1 class="mb-3"><?php echo $title;?></h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo site_url().'home'?>">Home</a></li>
      <li class="breadcrumb-item active"><a href="<?php echo site_url().'services'?>"><?php echo $title;?></a></li>
    </ol>
  </div>

</section><!-- #page-title end -->
<section id="content">

   <div class="content-wrap overflow-visible">

        <svg viewBox="0 0 1382 58" width="100%" height="60" preserveAspectRatio="none" style="position: absolute; top: -58px; left:0; z-index: 1"><path style="fill:#FFF;" d="M1.52.62s802.13,127,1380,0v56H.51Z"/></svg>

        <div class="container">
          <div class="row col-mb-30">

           
            <?php echo $main_services ?>
            
            

          </div>
         
        </div>

      </div>
      <?php echo $this->load->view("site/footer_inline", '');?>  
</section><!-- #content end -->

