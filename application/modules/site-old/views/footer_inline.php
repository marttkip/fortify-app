<?php
    $company_details = $this->site_model->get_contacts();
    
    if(count($company_details) > 0)
    {
        $email = $company_details['email'];
        $email2 = $company_details['email'];
        $facebook = $company_details['facebook'];
        $twitter = $company_details['twitter'];
        $linkedin = '';// $company_details['linkedin'];
        $logo = $company_details['logo'];
        $company_name = $company_details['company_name'];
        $phone = $company_details['phone']; 
        $mission = $company_details['mission']; 
        $about = $company_details['about'];
        $location = $company_details['location'];
        $floor = $company_details['floor'];
        $building = $company_details['building'];  
        $working_weekend = $company_details['working_weekend'];
        $working_weekday = $company_details['working_weekday']; 
        $vision = '';
        $instagram = '';  
    
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $mission = '';
        $vision ='';
        $instagram = '';
    }


?>
 <div class="bg-color subscribe-section position-relative" style="margin-top: 0px;">
    <div class="container" style="z-index: 2;">
        <div class="center collapsed subscribe-section-target" data-toggle="collapse" data-target="#target-1">
            <div class="subscribe-icon"><i class="icon-envelope21"></i></div>
            <h2 class="mb-0 mt-2 position-relative" style="z-index: 1;">Click here to book an appointment with us <i class="icon-arrow-down position-relative" style="top: 5px"></i>
            </h2>
        </div>
        <div class="collapse" id="target-1">
            <div class="form-widget pb-5" data-alert-type="false">

                <div class="form-result"></div>

                <div class="nonprofit-loader css3-spinner" style="position: absolute;">
                    <div class="css3-spinner-bounce1"></div>
                    <div class="css3-spinner-bounce2"></div>
                    <div class="css3-spinner-bounce3"></div>
                </div>
                <div id="nonprofit-submitted" class="center">
                    <h4 class="font-weight-semibold mb-0">Thank You for Contact Us! Our Team will contact you asap on your email Address.</h4>
                </div>

                <form id="nonprofit" class="row mt-2" action="#" method="post" enctype="multipart/form-data">
                    <div class="col-md-4 mb-4 mb-md-1">
                        <label for="nonprofit-name">Name:</label>
                        <input type="text" name="nonprofit-name" id="nonprofit-name" class="form-control border-form-control required" value="" placeholder="Enter your Full Name">
                    </div>
                    <div class="col-md-4 mb-4 mb-md-1">
                        <label for="nonprofit-phone">Contact:</label>
                        <input type="text" name="nonprofit-phone" id="nonprofit-phone" class="form-control border-form-control" value="" placeholder="Enter your Contact Number">
                    </div>
                    <div class="col-md-4 mb-4 mb-md-1">
                        <label for="nonprofit-email">Email:</label>
                        <input type="email" name="nonprofit-email" id="nonprofit-email" class="form-control border-form-control required" value="" placeholder="Enter your Email">
                    </div>
                    <div class="col-12 d-none">
                        <input type="text" id="nonprofit-botcheck" name="nonprofit-botcheck" value="" />
                    </div>
                    <button type="submit" name="nonprofit-submit" class="btn button button-rounded button-xlarge button-dark bg-dark shadow nott ls0 m-0 subscribe-button">Book Appointment Now</button>
                    <input type="hidden" name="prefix" value="nonprofit-">
                </form>
            </div>
        </div>
    </div>
    <div style="background-image: url('<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/divider-4.svg'); position: absolute; bottom: -20px; left: 0; width: 100%; height: 60px; z-index: 1;"></div>
</div>
<div class="line line-sm my-0 clearfix"></div>
    <div class="clear"></div>
    <div class="section section-details mb-0 bg-white" style="padding: 80px 0 160px;">
        <div class="w-100 h-100 d-none d-md-block" style="position: absolute; top: 0; left: 0; background: #FFF url('<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/others/6.jpg') no-repeat bottom right / cover;"></div>
        <div class="container clearfix">
            <div class="row">
                <div class="col-md-4 px-4 mb-5">
                    <h4 class="font-weight-medium mb-4">About <?php echo $company_name;?></h4>
                    <p class="mb-3">Executive summary</p>
                    <abbr title="Location"><strong>Location:</strong></abbr><?php echo $location.' '.$building.' '.$floor;?><br>
                    <abbr title="Phone Number"><strong>Phone:</strong></abbr> <?php echo $phone;?><br>
                    <abbr title="Email Address"><strong>Email:</strong></abbr> <?php echo $email;?>
                    <hr>
                     <a href="<?php echo $facebook?>" class="social-icon si-dark si-small si-facebook" title="Facebook">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>


                    <a href="<?php echo $twitter?>" class="social-icon si-dark si-small si-flattr" title="Twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>

                    <a href="<?php echo $instagram?>" class="social-icon si-dark si-small si-android" title="Android">
                        <i class="icon-instagram"></i>
                        <i class="icon-instagram"></i>
                    </a>

                </div>

                <div class="col-md-4 px-4 mb-5">
                     <h4 class="font-weight-medium mb-4">Our Timings</h4>
                    <p class="mb-3">Compellingly enable premium alignments rather than sustainable content.</p>
                    <a href="#" class="mb-1 d-block"><i class="icon-envelope21 position-relative" style="top: 1px;"></i> <?php echo $email;?></a>
                    <div class="font-weight-medium mb-2 d-block">Monday - Friday <?php echo $working_weekday ?></div>
                    <div class="font-weight-medium mb-2 d-block">Saturday & holidays  <?php echo $working_weekend ?></div>
                   
                   
                </div>

                <div class="col-md-4 px-4 mb-5">
                    <h4 class="font-weight-medium mb-4">Mission</h4>
                    <p><?php echo $mission;?></p>
                     <h4 class="font-weight-medium mb-4">Our Vision</h4>
                    <p><?php echo $vision;?></p>
                </div>

                <div class="col-md-4 px-4 mb-5 mb-md-0">
                   
                </div>

                <div class="col-md-4 px-4 mb-5 mb-md-0">
                 
                </div>

            </div>
        </div>
    </div>