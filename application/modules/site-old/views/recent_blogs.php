<?php
$about_query = $this->site_model->get_active_items('Company Blog',10);
 $blog_list = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 100)));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $x++;
    if($x < 9)
    {
      $x = '0'.$x;
    }
    $blog_list .= '

                    <div class="oc-item text-left">
                        <img src="'.$image_about.'" alt="Image 1" class="rounded">
                        <div class="oc-desc d-flex flex-column justify-content-center shadow-lg">
                            <small class="text-uppercase font-weight-normal ls1 color mb-2 d-block">'.$post_target.'</small>
                            <h3 class="mb-3"><a href="'.site_url().'blog/view-single/'.$web_name.'">'.$post_title.'</a></h3>
                            
                            <p class="mb-4 text-black-50">'.$mini_desc.'</p>
                            <a href="'.site_url().'blog/view-single/'.$web_name.'" class="button button-rounded button-border nott ls0 font-weight-medium m-0 d-flex align-self-start">View Blog</a>
                        </div>
                    </div>
			                ';
  }
}

echo $blog_list;
?>