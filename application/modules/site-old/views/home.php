<?php
    $company_details = $this->site_model->get_contacts();
    
    if(count($company_details) > 0)
    {
        $email = $company_details['email'];
        $email2 = $company_details['email'];
        $facebook = $company_details['facebook'];
        $twitter = $company_details['twitter'];
        $linkedin = '';// $company_details['linkedin'];
        $logo = $company_details['logo'];
        $company_name = $company_details['company_name'];
        $phone = $company_details['phone']; 
        $mission = $company_details['mission']; 
        $about = $company_details['about'];
        $location = $company_details['location'];
        $floor = $company_details['floor'];
        $building = $company_details['building'];  
        $working_weekend = $contacts['working_weekend'];
        $working_weekday = $contacts['working_weekday'];   
    
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $mission = '';
    }


?>


     <?php echo $this->load->view("site/home/slider", '');?>  
      
    <!-- Slider
        ============================================= -->
       

        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap py-0" style="overflow: visible">

                <!-- <svg viewBox="0 0 1382 58" width="100%" height="60" preserveAspectRatio="none" style="position: absolute; top: -58px; left:0; z-index: 1"><path style="fill:#FFF;" d="M1.52.62s802.13,127,1380,0v56H.51Z"/></svg>

                <div class="container">

                    <div class="slider-feature w-100">
                        <div class="row justify-content-center">
                            <div class="col-md-3 px-1">
                                <a href="<?php echo site_url().'about-us'?>" class="card center border-left-0 border-right-0 border-top-0 border-bottom border-bottom shadow py-3 rounded-0 font-weight-semibold text-uppercase ls1">
                                    <div class="card-body">
                                        <i class="icon-line-align-center"></i>View Our Mission
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3 px-1">
                                <a href="<?php echo site_url().'services'?>" class="card center border-left-0 border-right-0 border-top-0 border-bottom border-bottom shadow py-3 rounded-0 font-weight-semibold text-uppercase ls1">
                                    <div class="card-body">
                                        <i class="icon-line-umbrella"></i>Our Services
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3 px-1">
                                <a href="<?php echo site_url().'contact-us'?>" class="card center border-left-0 border-right-0 border-top-0 border-bottom border-bottom shadow py-3 rounded-0 font-weight-semibold text-uppercase ls1">
                                    <div class="card-body">
                                        <i class="icon-line-mail"></i>Contact Us
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                </div> -->
                <!-- <div class="section mt-3" style="background: #FFF url('<?php echo base_url().'assets/backgrounds/'?>why-choose-us.jpg') no-repeat 100% 50% / auto 100%;"> -->
                <div class="section mt-3" style="background: #FFF url('') no-repeat 100% 50% / auto 100%;">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-7 center">
                                <div class="heading-block border-bottom-0 mb-4">
                                    <h2 class="mb-4 nott">WHY CHOOSE US</h2>
                                </div>
                                <div class="svg-line bottommargin-sm">
                                    <img src="<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/divider-1.svg" alt="svg divider" height="20">
                                </div>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, quasi, dolorum blanditiis eius laboriosam, quidem temporibus, dolor quod voluptatum perferendis ullam. Odio sequi at dolore consequatur ad, odit expedita tenetur.</p> -->
                            </div>
                        </div>
                        <div class="row mt-5 col-mb-50 mb-0 col-md-12">
                            <?php echo $this->load->view("site/why_choose_us", '');?>
                           
                        </div>
                    </div>
                </div>

                <!-- <div class="clear"></div> -->

                <div class="section" style="background: url('<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/others/section-bg.jpg') no-repeat center center / cover; padding: 80px 0;">
                    <div class="container clearfix">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="mb-2">Our Services</h3>
                                <div class="svg-line mb-2 clearfix">
                                    <img src="<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/divider-1.svg" alt="svg divider" height="10">
                                </div>
                                <p class="mb-5">Here at VIP Dental we specialist with the following services</p>
                                <div class="row mission-goals gutter-30 mb-0">
                                    <?php echo $this->load->view("main_services", '');?>  
                                   
                                   
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </div>

                <div class="section bg-transparent mt-0 mb-4">
                    <div class="container clearfix">
                        <div class="row justify-content-center" >
                            <div class="col-md-7 center">
                                <div class="heading-block border-bottom-0 mb-4">
                                    <h2 class="mb-4 nott">Whats New</h2>
                                </div>
                                <div class="svg-line bottommargin-sm clearfix">
                                    <img src="<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/divider-1.svg" alt="svg divider" height="20">
                                </div>
                                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, quasi, dolorum blanditiis eius laboriosam, quidem temporibus, dolor quod voluptatum perferendis ullam. Odio sequi at dolore consequatur ad, odit expedita tenetur.</p> -->
                            </div>
                        </div>
                    </div>

                    <div class="owl-carousel owl-carousel-full image-carousel carousel-widget topmargin-sm charity-card" data-stage-padding="20" data-margin="10" data-center="true" data-loop="true" data-nav="true" data-autoplay="500000" data-speed="400" data-pagi="true" data-items-xs="1" data-items-sm="2" data-items-md="2" data-items-lg="3" data-items-xl="4">


                        <?php echo $this->load->view("recent_blogs", '');?>  


                        


                    </div>
                </div>

               

                
               

             

               
                
                
            </div>
        </section><!-- #content end -->

        <?php echo $this->load->view("site/footer_inline", '');?> 


        <?php //echo $this->load->view("site/why_choose_us", '');?>  
        <!--//section why choose us-->
        <!--section testimonials-->
       
        <?php //echo $this->load->view("site/testimonials", '');?>  

        <!--//section testimonials-->
        <!--section achieved-->
       
        <!--//section achieved-->
        <!--section faq-->
       <?php //echo $this->load->view("site/faqs", '');?>  
        <!--//section faq-->

        <?php //echo $this->load->view("site/our_partners", '');?>  
        <!--section special offers-->
        <?php //echo $this->load->view("site/tag_line", '');?>  
        <!--//section special offers-->
        <!--section clients gallery-->
      
        <!--//section clients gallery-->
        <!--section call us-->
       
        <!--section call us-->
</div>