<?php 
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$phone = $contacts['phone'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		$linkedin = $contacts['linkedin'];
        $instagram = $contacts['instagram'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$address = $contacts['address'];
		$city = $contacts['city'];
		$post_code = $contacts['post_code'];
		$building = $contacts['building'];
		$floor = $contacts['floor'];
		$location = $contacts['location'];
		$working_weekend = $contacts['working_weekend'];
		$working_weekday = $contacts['working_weekday'];
		
		// if(!empty($email))
		// {
		// 	$mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$facebook = '<a href="'.$facebook.'" target="_blank">Facebook</a>';
		// }
		
		// if(!empty($twitter))
		// {
		// 	$twitter = '<a href="'.$twitter.'" target="_blank">Twitter</a>';
		// }
		
		// if(!empty($linkedin))
		// {
		// 	$linkedin = '<a href="'.$linkedin.'" target="_blank"></a>';
		// }
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
        $instagram = '';
		$logo = '';
		$company_name = '';
	}

   $branches_rs = $this->site_model->get_all_branches();

    $branches_list = '';
    $branch_dropdownList = '';
    if($branches_rs->num_rows() > 0)
    {
        foreach ($branches_rs->result() as $key => $value) {
            # code...
            $branch_name = $value->branch_name;
            $branch_email = $value->branch_email;
            $branch_phone = $value->branch_phone;
            $branch_address = $value->branch_address;
            // $branch_postal_code = $value->branch_postal_code;
            $branch_location = $value->branch_location;
            $branch_building = $value->branch_building;
            $branch_floor = $value->branch_floor;
            $location_link = $value->location_link;
            $branches_list .= '
                            <li style="margin-bottom:10px !important;">
                                <i class="fa fa-map-marker"></i> <b>'.$branch_name.'</b><br />
                                <p> '.$branch_building.' '.$branch_floor.' '.$branch_location.'</p>
                                <p> '.$branch_phone.'</p>
                            </li>';

            $branch_dropdownList .= '<option value="'.$branch_email.'">'.$branch_name.'</option>';
        }
    }

?>

<section id="page-title" class="page-title-dark page-title-parallax page-title-center" style="background: linear-gradient(to bottom, rgba(6, 54, 57,.9), rgba(6, 54, 57,.9)), url('<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/cause/grid/1.jpg') no-repeat center center; background-size: cover; padding: 30px 0;">

  <div class="container clearfix">
    <h1 class="mb-3"><?php echo 'Contact Us';?></h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo site_url().'home'?>">Home</a></li>
      <li class="breadcrumb-item active"><a href="<?php echo site_url().'blog'?>"><?php echo 'Contact Us';?></a></li>
    </ol>
  </div>

</section><!-- #page-title end -->
<section id="content">
<div class="content-wrap overflow-visible">

    <svg viewBox="0 0 1382 58" width="100%" height="60" preserveAspectRatio="none" style="position: absolute; top: -58px; left:0; z-index: 1"><path style="fill:#FFF;" d="M1.52.62s802.13,127,1380,0v56H.51Z"/></svg>

    <div class="container">
        <div class="row col-mb-30">

            <div class="col-lg-8">
                <h3>Send us an Email</h3>

                <div class="form-widget">

                    <div class="form-result"></div>

                     <?php echo form_open($this->uri->uri_string(), array("class" => "row mb-0", "name"=>"template-contactform"));?>

                    <!-- <form class="row mb-0" id="template-contactform" name="template-contactform" action="include/form.php" method="post"> -->

                        <div class="form-process">
                            <div class="css3-spinner">
                                <div class="css3-spinner-scaler"></div>
                            </div>
                        </div>

                        <div class="col-md-4 form-group">
                            <label for="sender_name">Name <small>*</small></label>
                            <input type="text" id="sender_name" name="sender_name" value="" class="sm-form-control required" />
                        </div>

                        <div class="col-md-4 form-group">
                            <label for="sender_email">Email <small>*</small></label>
                            <input type="email" id="sender_email" name="sender_email" value="" class="required email sm-form-control" />
                        </div>

                        <div class="col-md-4 form-group">
                            <label for="sender_phone">Phone</label>
                            <input type="text" id="sender_phone" name="sender_phone" value="" class="sm-form-control" />
                        </div>

                        <div class="w-100"></div>

                        <div class="col-md-12 form-group">
                            <label for="subject">Subject <small>*</small></label>
                            <input type="text" id="subject" name="subject" value="" class="required sm-form-control" />
                        </div>


                        <div class="w-100"></div>

                        <div class="col-12 form-group">
                            <label for="message">Message <small>*</small></label>
                            <textarea class="required sm-form-control" id="message" name="message" rows="6" cols="30"></textarea>
                        </div>

                        <div class="col-12 form-group d-none">
                            <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                        </div>

                        <div class="col-12 form-group">
                            <button class="button button-3d m-0" type="submit"  name="template-contactform-submit" value="submit">Send Message</button>
                        </div>

                        <!-- <input type="hidden" name="prefix" value="template-contactform-"> -->

                    </form>
                </div>

            </div>

            <div class="col-lg-4">
                <div class="opening-table" style="background-color: #f5f5f5;padding:20px">
                    <div class="heading-block bottommargin-sm border-bottom-0">
                        <h4>Where to find us</h4>
                        <span>Allamano Centre, Consolata Shrine Off Waiyaki Way</span>
                    </div>
                    <div class="time-table-wrap clearfix">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.841961446058!2d36.8002938151688!3d-1.2675779359645956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1740bd6bdfa9%3A0xb59283dc989e4258!2sConsolata%20Shrine!5e0!3m2!1sen!2ske!4v1615438280110!5m2!1sen!2ske" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        <hr>
                        <div class="row time-table">
                            <h5 class="col-lg-5">Weekdays (Monday - Friday)</h5>
                            <span class="col-lg-7">8:00am - 5:00pm</span>
                        </div>
                        <div class="row time-table">
                            <h5 class="col-lg-5">Saturday and Holidays</h5>
                            <span class="col-lg-7">8:00am - 5:00pm</span>
                        </div>
                        <div class="row time-table">
                            <h5 class="col-lg-5">Sunday</h5>
                            <span class="col-lg-7 color font-weight-semibold">Closed</span>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</section>

 <?php echo $this->load->view("site/footer_inline", '');?> 
