<?php
  $about_query = $this->site_model->get_active_content_items($title,1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      // $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= ' <li>
                              <div class="portfolio-thumb">
                                  <div class="gc_filter_cont_overlay_wrapper port_uper_div">
                                      <img src="'.$post_gallery_image_name.'" class="zoom image img-responsive" alt="service_img" />
                                      <div class="gc_filter_cont_overlay zoom_popup">
                                          <div class="gc_filter_text"><a href="'.$post_gallery_image_name.'"><i class="fa fa-plus"></i></a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </li>';
        }
      }

    }
  }
   
?>

<section id="slider" class="slider-element swiper_wrapper min-vh-75 justify-content-start dark" style="background: #063639">

  <div class="container">
    <div class="row h-100 align-items-center justify-content-between">
      <div class="col-lg-6 col-md-6 py-5 py-md-0" style="margin-top: 0px !important;text-align: justify;">
        <div class="heading-block border-bottom-0 mb-4">
          <!-- <h5 class="mb-1 text-uppercase ls2 color op-06">About Us</h5> -->
          <h2 class="mb-4 nott"><?php echo $post_title?></h2>
        </div>
        <div class="svg-line bottommargin-sm">
          <img src="<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/divider-1.svg" alt="svg divider" height="20">
        </div>
        <p class="mb-5" ><?php echo $description;?></p>
        <!-- <a href="demo-nonprofit-causes-single.html" class="button button-rounded button-xlarge bg-color button-light text-dark shadow nott ls0 m-0">Donate Now</a> -->

      </div>
      <div class="col-lg-6 col-md-6">
        <!-- <img src="<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/about/1.png" alt="Image" class="full-width-img"> -->
        <img src="<?php echo $image_about;?>" alt="Image" class="full-width-img">
      </div>
    </div>
  </div>

</section>
 
 <?php echo $this->load->view("site/footer_inline", '');?> 

