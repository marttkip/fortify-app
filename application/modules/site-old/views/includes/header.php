    <?php
    // $mini_desc = '';
    if(!isset($meta_tags) OR empty($meta_tags))
    {
        $contacts = $this->site_model->get_contacts();
        $post_meta = $contacts['post_meta'];
        $mini_desc =$contacts['vision'];
        
    }
    else
    {
        $post_meta = $meta_tags;
        $post_meta = $meta_tags;
    }
    ?>
  	<head>        
        <title><?php echo $title;?></title>
        <!--meta-->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta name="author" content="Admin" />
        <meta name="MobileOptimized" content="320" />



        <meta name="description" content="<?php echo $mini_desc;?>" />
        <meta name="keywords" content="<?php echo $post_meta;?>" />
        <meta property="og:title" content="<?php echo $title;?>" />
        <meta property="og:description" content="<?php echo $mini_desc;?>" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

         <link href="<?php echo base_url().'assets/themes/dentco/'?>vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">

        <link rel="icon" href="<?php echo base_url().'assets/themes/dentco/'?>images/favicon.png" type="image/x-icon">
       

        <link href="https://fonts.googleapis.com/css2?family=Caveat+Brush&family=Poppins:wght@300;400;500;600;700;900&display=swap" rel="stylesheet">
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>css/bootstrap.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>style.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>css/swiper.css" type="text/css" />

            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>css/dark.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>css/font-icons.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>css/animate.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>css/magnific-popup.css" type="text/css" />

            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>css/custom.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>css/calendar.css" type="text/css" />

            <!-- NonProfit Demo Specific Stylesheet -->
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>css/colors.php?color=C6C09C" type="text/css" /> <!-- Theme Color -->
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/css/fonts.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/nonprofit.css" type="text/css" />
            <!-- / -->

            <meta name='viewport' content='initial-scale=1, viewport-fit=cover'>


       
       
  </head>
