<?php

$contacts = $this->site_model->get_contacts();

  if(count($contacts) > 0)
  {
    $email = $contacts['email'];
    $phone = $contacts['phone'];
    $facebook = $contacts['facebook'];
    $twitter = $contacts['twitter'];
    $linkedin = $contacts['linkedin'];
    $instagram = $contacts['instagram'];
    $logo = $contacts['logo'];
    $company_name = $contacts['company_name'];
    $about = $contacts['about'];
    $address = $contacts['address'];
    $city = $contacts['city'];
    $post_code = $contacts['post_code'];
    $building = $contacts['building'];
    $floor = $contacts['floor'];
    $location = $contacts['location'];
    $working_weekend = $contacts['working_weekend'];
    $working_weekday = $contacts['working_weekday'];
    
    if(!empty($email))
    {
      $mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
    }
    
    // if(!empty($facebook))
    // {
    //  $facebook = '<li><a class="social_icon facebook" href="'.$facebook.'" target="_blank"></a></li>';
    // }
    
    // if(!empty($twitter))
    // {
    //  $twitter = '<li><a class="social_icon twitter" href="'.$twitter.'" target="_blank"></li>';
    // }
    
    // if(!empty($linkedin))
    // {
    //  $linkedin = '<li><a class="social_icon googleplus" href="'.$linkedin.'" target="_blank"></li>';
    // }
  }
  else
  {
    $email = '';
    $facebook = '';
    $twitter = '';
    $linkedin = '';
    $logo = '';
    $instagram = '';
    $company_name = '';
  }
$popular_query = $this->blog_model->get_popular_posts();

if($popular_query->num_rows() > 0)
{
    $popular_posts = '';
    $count = 0;
    foreach ($popular_query->result() as $row)
    {
        $count++;
        
        if($count < 3)
        {
            $post_id = $row->post_id;
            $post_title = $row->post_title;
            $image = base_url().'assets/images/posts/thumbnail_'.$row->post_image;
            $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
            $description = $row->post_content;
            $mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 10)));
            $created = date('jS M Y',strtotime($row->created));
            
            $popular_posts .= '

                                <div class="footer-post d-flex">
                                    <div class="footer-post-photo"><img src="'.$image.'" alt="" class="img-fluid"></div>
                                    <div class="footer-post-text">
                                        <div class="footer-post-title"><a href="'.site_url().'blog/view-single/'.$post_id.'">'.$mini_desc.'.</a>
                                        </div>
                                        <p>'.$created.'</p>
                                    </div>
                                </div>
                
            ';
        }
    }
}

else
{
    $popular_posts = 'There are no posts yet';
}


  $branches_rs = $this->site_model->get_all_branches();

    $branches_list = '';
    if($branches_rs->num_rows() > 0)
    {
        foreach ($branches_rs->result() as $key => $value) {
            # code...
            $branch_name = $value->branch_name;
            $branch_email = $value->branch_email;
            $branch_phone = $value->branch_phone;
            $branch_address = $value->branch_address;
            // $branch_postal_code = $value->branch_postal_code;
            $branch_location = $value->branch_location;
            $branch_building = $value->branch_building;
            $branch_floor = $value->branch_floor;
            $location_link = $value->location_link;
            
          $branches_list .= ' <div class="col-md-4">
                                    <h5>'.$branch_name.'</h5>
                                    <p style="line-height: 10px !important;margin-bottom: 24px !important;"><i class="fa fa-map-marker"></i> '.$branch_location.' '.$branch_building.' '.$branch_floor.'
                                    </p>
                                    <p style="line-height: 10px !important;margin-bottom: 24px !important;">
                                        <a href="'.site_url().'our-branches" class="btn btn-xs btn-gradient"><i class="fa fa-map"></i><span>Get directions on the map</span> <i class="fa fa-right-arrow"></i></a>
                                    </p>
                                    <p style="line-height: 10px !important;margin-bottom: 24px !important;"><i class="fa fa-phone"></i><b><span class="phone"><span class="text-nowrap"> '.$branch_phone.'</span></b></p>
                                    <p style="line-height: 10px !important;margin-bottom: 24px !important;"><i class="fa fa-envelope"></i><a href="mailto:<?php echo $email?>"> '.$branch_email.'</a></p>
                                    
                                </div>';
    }
  }



?>

<!-- Footer
        ============================================= -->
        <footer id="footer" style="background-color: #002D40;">

           
            <!-- Copyrights
            ============================================= -->
            <div id="copyrights" class="bg-color">

                <div class="container clearfix">

                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-6">
                            Copyrights &copy; <?php echo date('Y')?> All Rights Reserved by <?php echo $company_name?>.<br>
                            <!-- <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div> -->
                        </div>

                        <div class="col-md-6 d-md-flex flex-md-column align-items-md-end mt-4 mt-md-0">
                            <div class="copyrights-menu copyright-links clearfix">
                                <a href="<?php echo site_url().'home'?>">About</a>/<a href="<?php echo site_url().'our-services'?>">Our Services</a>/<a href="<?php echo site_url().'contact'?>">Contact</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

        <!-- Floating Contact
        ============================================= -->
        <div class="floating-contact-wrap">
            <div class="floating-contact-btn shadow">
                <i class="floating-contact-icon btn-unactive icon-envelope21"></i>
                <i class="floating-contact-icon btn-active icon-line-plus"></i>
            </div>
            <div class="floating-contact-box">
                <div id="q-contact" class="widget quick-contact-widget clearfix">
                    <div class="floating-contact-heading bg-color p-4 rounded-top">
                        <h3 class="mb-0 font-secondary h2 ls0">Quick Contact 👋</h3>
                        <p class="mb-0">Get in Touch with Us</p>
                    </div>
                    <div class="form-widget bg-white" data-alert-type="false">
                        <div class="form-result"></div>
                        <div class="floating-contact-loader css3-spinner" style="position: absolute;">
                            <div class="css3-spinner-bounce1"></div>
                            <div class="css3-spinner-bounce2"></div>
                            <div class="css3-spinner-bounce3"></div>
                        </div>
                        <div id="floating-contact-submitted" class="p-5 center">
                            <i class="icon-line-mail h1 color"></i>
                            <h4 class="font-weight-normal mb-0 font-body">Thank You for Contact Us! Our Team will contact you asap on your email Address.</h4>
                        </div>
                        <form class="mb-0" id="floating-contact" action="include/form.php" method="post" enctype="multipart/form-data">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent"><i class="icon-user-alt"></i></span>
                                </div>
                                <input type="text" name="floating-contact-name" id="floating-contact-name" class="form-control required" value="" placeholder="Enter your Full Name">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent"><i class="icon-at"></i></span>
                                </div>
                                <input type="email" name="floating-contact-email" id="floating-contact-email" class="form-control required" value="" placeholder="Enter your Email Address">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-transparent"><i class="icon-comment21"></i></span>
                                </div>
                                <textarea name="floating-contact-message" id="floating-contact-message" class="form-control required" cols="30" rows="4"></textarea>
                            </div>
                            <input type="hidden" id="floating-contact-botcheck" name="floating-contact-botcheck" value="" />
                            <button type="submit" name="floating-contact-submit" class="btn btn-dark btn-block py-2">Send Message</button>
                            <input type="hidden" name="prefix" value="floating-contact-">
                            <input type="hidden" name="subject" value="Messgae From Floating Contact">
                            <input type="hidden" name="html_title" value="Floating Contact Message">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    