<?php


$contacts = $this->site_model->get_contacts();

  if(count($contacts) > 0)
  {
    $email = $contacts['email'];
    $phone = $contacts['phone'];
    $facebook = $contacts['facebook'];
    $twitter = $contacts['twitter'];
    $linkedin = $contacts['linkedin'];
    $instagram = $contacts['instagram'];
    $logo = $contacts['logo'];
    $company_name = $contacts['company_name'];
    $about = $contacts['about'];
    $address = $contacts['address'];
    $city = $contacts['city'];
    $post_code = $contacts['post_code'];
    $building = $contacts['building'];
    $floor = $contacts['floor'];
    $location = $contacts['location'];
    $working_weekend = $contacts['working_weekend'];
    $working_weekday = $contacts['working_weekday'];
    
    if(!empty($email))
    {
      $mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
    }
    
    // if(!empty($facebook))
    // {
    //  $facebook = '<li><a class="social_icon facebook" href="'.$facebook.'" target="_blank"></a></li>';
    // }
    
    // if(!empty($twitter))
    // {
    //  $twitter = '<li><a class="social_icon twitter" href="'.$twitter.'" target="_blank"></li>';
    // }
    
    // if(!empty($linkedin))
    // {
    //  $linkedin = '<li><a class="social_icon googleplus" href="'.$linkedin.'" target="_blank"></li>';
    // }
  }
  else
  {
    $email = '';
    $facebook = '';
    $twitter = '';
    $linkedin = '';
    $logo = '';
    $instagram = '';
    $company_name = '';
  }

		$result = '';
		
		//if users exist display them
	
		if ($query->num_rows() > 0)
		{	
			//get all administrators
			$administrators = $this->users_model->get_all_administrators();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			$count = 1;
			foreach ($query->result() as $row)
			{
				$post_id = $row->post_id;
				$blog_category_name = $row->blog_category_name;
				$blog_category_id = $row->blog_category_id;
				$post_title = $row->post_title;
				$web_name = $this->site_model->create_web_name($post_title);
				$post_status = $row->post_status;
				$post_target = $row->post_target;
				$post_views = $row->post_views;
				$image = base_url().'assets/images/posts/'.$row->post_image;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
				$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
				$description = $row->post_content;
				$mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 50)));
				$created = $row->created;
				$day = date('j',strtotime($created));
				$month = date('M Y',strtotime($created));
				$created_on = date('jS M Y H:i a',strtotime($row->created));
				$created_on_date = date('jS M Y',strtotime($row->created));

				
				$categories = '';
				
				//get all administrators
					// $administrators = $this->users_model->get_all_administrators();
					// if ($administrators->num_rows() > 0)
					// {
					// 	$admins = $administrators->result();
						
					// 	if($admins != NULL)
					// 	{
					// 		foreach($admins as $adm)
					// 		{
					// 			$user_id = $adm->user_id;
								
					// 			if($user_id == $created_by)
					// 			{
					// 				$created_by = $adm->first_name;
					// 			}
					// 		}
					// 	}
					// }
					
					// else
					// {
					// 	$admins = NULL;
					// }
				
					foreach($categories_query->result() as $res)
					{
						// $count++;
						$category_name = $res->blog_category_name;
						$category_id = $res->blog_category_id;
						
						if($count == $categories_query->num_rows())
						{
							$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>';
						}
						
						else
						{
							$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>, ';
						}
					}
					$comments_query = $this->blog_model->get_post_comments($post_id);
					//comments
					$comments = 'No Comments';
					$total_comments = $comments_query->num_rows();
					if($total_comments == 1)
					{
						$title = 'comment';
					}
					else
					{
						$title = 'comments';
					}
					
					if($comments_query->num_rows() > 0)
					{
						$comments = '';
						foreach ($comments_query->result() as $row)
						{
							$post_comment_user = $row->post_comment_user;
							$post_comment_description = $row->post_comment_description;
							$date = date('jS M Y H:i a',strtotime($row->comment_created));
							
							$comments .= 
							'
								<div class="user_comment">
									<h5>'.$post_comment_user.' - '.$date.'</h5>
									<p>'.$post_comment_description.'</p>
								</div>
							';
						}
					}
				// var_dump($count % 3);die();
				
				$link= "";
				if ($count % 2 == 0) {

						$link = 'bg-grey';
			     }
			/*	$result .= '<div class="blog-post '.$link.'">

								<div class="post-image">
									<a href="'.site_url().'blog/view-single/'.$web_name.'"><img src="'.$image.'" alt="" style="object-fit: cover; !important;max-height:400px !important;"></a>
								</div>
								<div class="blog-post-info">
									<div class="post-date">17<span>JAN</span></div>
									<div>
										<h2 class="post-title"><a href="'.site_url().'blog/view-single/'.$web_name.'">'.$post_title.'</a></h2>
										<div class="post-meta">
											<div class="post-meta-author">by <a href="blog-grid.html#"><i>admin</i></a></div>
											
										</div>
									</div>
								</div>
								<div class="post-teaser" style="min-height:109px !important;">'.$mini_desc.' […]</div>
								<div class="mt-2"><a href="'.site_url().'blog/view-single/'.$web_name.'" class="btn btn-sm btn-hover-fill">Read more</span> <i class="fa fa-arrow-right"></i></a></div>
							</div>

							
		           			 ';
*/

		           	$result .= '
		           				<div class="col-lg-4 col-md-6">
									<div class="charity-card h-shadow shadow-ts card rounded-xxl">
										<a href="'.site_url().'blog/view-single/'.$web_name.'">
											<img src="'.$image.'" alt="Image" class="card-img-top">
										</a>
										<div class="px-4 py-3 charity-card-title">
											<small class="text-uppercase font-weight-normal ls1 mb-2 d-block">'.$post_target.'</small>
											<h3 class="mb-3"><a href="'.site_url().'blog/view-single/'.$web_name.'">'.$post_title.'</a></h3>
											
										</div>
										<div class="p-4">
											<p class="mb-0 text-black-50">'.$mini_desc.'</p>
										</div>
									</div>
								</div>
		           	';
		            $count++;



		        }
			}
			else
			{
				$result = "";
			}
           
          ?>     

<section id="page-title" class="page-title-dark page-title-parallax page-title-center" style="background: linear-gradient(to bottom, rgba(6, 54, 57,.9), rgba(6, 54, 57,.9)), url('<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/cause/grid/1.jpg') no-repeat center center; background-size: cover; padding: 30px 0;">

  <div class="container clearfix">
    <h1 class="mb-3"><?php echo 'Our Blog';?></h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo site_url().'home'?>">Home</a></li>
      <li class="breadcrumb-item active"><a href="<?php echo site_url().'blog'?>"><?php echo 'Our Blog';?></a></li>
    </ol>
  </div>

</section><!-- #page-title end -->
<section id="content">
<div class="content-wrap overflow-visible">

	<svg viewBox="0 0 1382 58" width="100%" height="60" preserveAspectRatio="none" style="position: absolute; top: -58px; left:0; z-index: 1"><path style="fill:#FFF;" d="M1.52.62s802.13,127,1380,0v56H.51Z"/></svg>

	<div class="container">
		<div class="row col-mb-30">

			
			<?php echo $result;?>



		</div>
	</div>

</div>
</section>

 <?php echo $this->load->view("site/footer_inline", '');?> 
		