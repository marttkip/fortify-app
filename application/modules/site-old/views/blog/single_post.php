<?php

$post_id = $row->post_id;
$blog_category_name = $row->blog_category_name;
$blog_category_id = $row->blog_category_id;
$post_title = $row->post_title;
$intro = $row->post_target;
$post_status = $row->post_status;
$post_views = $row->post_views;
$image = base_url().'assets/images/posts/'.$row->post_image;
$created_by = $row->created_by;
$modified_by = $row->modified_by;
$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
$body = $description = $row->post_content;
$mini_desc = implode(' ', array_slice(explode(' ', $body), 0, 50));
$created = $row->created;
$day = date('j',strtotime($created));
$month = date('M Y',strtotime($created));
$transdate = date('jS M Y H:i a',strtotime($row->created));

$web_name = $this->site_model->create_web_name($post_title);

$categories = '';
$count = 0;
//get all administrators
	// $administrators = $this->users_model->get_all_administrators();
	// if ($administrators->num_rows() > 0)
	// {
	// 	$admins = $administrators->result();
		
	// 	if($admins != NULL)
	// 	{
	// 		foreach($admins as $adm)
	// 		{
	// 			$user_id = $adm->user_id;
				
	// 			if($user_id == $created_by)
	// 			{
	// 				$created_by = $adm->first_name;
	// 			}
	// 		}
	// 	}
	// }
	
	// else
	// {
	// 	$admins = NULL;
	// 
$gallery_rs = $this->site_model->get_post_gallery($post_id);

$gallery = '';
if($gallery_rs->num_rows() > 0)
{
	foreach ($gallery_rs->result() as $key => $value) {
	  # code...
	  $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
	  $post_gallery_image_thumb = $value->post_gallery_image_thumb;

	  $gallery .= ' <div class="col-xl4 col-lg-4 col-md-4 col-sm-12 col-12">
	                    <div class="blog_about">
	                        <div class="blog_img blog_post_img">
	                            <figure>
	                                <img src="'.$post_gallery_image_name.'" alt="img" class="img-responsive">
	                            </figure>
	                        </div>
	                    </div>
	                </div>';
	}
}

	foreach($categories_query->result() as $res)
	{
		$count++;
		$category_name = $res->blog_category_name;
		$category_id = $res->blog_category_id;
		
		if($count == $categories_query->num_rows())
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>';
		}
		
		else
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>, ';
		}
	}
	$comments_query = $this->blog_model->get_post_comments($post_id);
	//comments
	$comments = 'No Comments';
	$total_comments = $comments_query->num_rows();
	if($total_comments == 1)
	{
		$title = 'comment';
	}
	else
	{
		$title = 'comments';
	}
	
	if($comments_query->num_rows() > 0)
	{
		$comments = '';
		foreach ($comments_query->result() as $row)
		{
			$post_comment_user = $row->post_comment_user;
			$post_comment_description = $row->post_comment_description;
			$date = date('jS M Y H:i a',strtotime($row->comment_created));
			
			$comments .= 
			'
				<div class="user_comment">
					<h5>'.$post_comment_user.' - '.$date.'</h5>
					<p>'.$post_comment_description.'</p>
				</div>
			';
		}
	}
	



?> 
<section id="page-title" class="page-title-dark page-title-parallax page-title-center" style="background: linear-gradient(to bottom, rgba(6, 54, 57,.9), rgba(6, 54, 57,.9)), url('<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/cause/grid/1.jpg') no-repeat center center; background-size: cover; padding: 30px 0;">

  <div class="container clearfix">
    <h1 class="mb-3"><?php echo $post_title;?></h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo site_url().'home'?>">Home</a></li>
      <li class="breadcrumb-item "><a href="<?php echo site_url().'blog'?>">Our Blog</a></li>
      <li class="breadcrumb-item active"><a href="<?php echo site_url().'blog'.$web_name?>"><?php echo $post_title;?></a></li>
    </ol>
  </div>

</section><!-- #page-title end -->
<!-- Content
		============================================= -->
<section id="content">
	<div class="content-wrap overflow-visible">

        <svg viewBox="0 0 1382 58" width="100%" height="60" preserveAspectRatio="none" style="position: absolute; top: -58px; left:0; z-index: 1"><path style="fill:#FFF;" d="M1.52.62s802.13,127,1380,0v56H.51Z"/></svg>

        <div class="container">
          <div class="row col-mb-30">

          		<div class="container mt-0">
                    <div class="row">
                      <div class="col-md">
                        <div class="doctor-page-photo text-center"><br><br><br>
                         <img src="<?php echo $image?>" class="img-fluid" alt="">
                                                  
                        </div>
                      <div class="mt-3 mt-md-5"></div>
                      </div>
                      <div class="col-lg-8 mt-0 mt-lg-0">
                        <div class="doctor-info mb-3 mb-lg-4">
                          <div >
                            <div class="heading-block border-bottom-0 mb-4">
                                <h2 class="mb-4 nott"><?php echo $post_title?></h2>
                            </div>
                            
                          </div>
                          
                        </div>

                        <?php echo $description?>
                       </div>
                    </div>
                  </div>
          </div>
        </div>
    </div>
	
</section><!-- #content end -->