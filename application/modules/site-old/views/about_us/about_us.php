<?php
$company_details = $company_details;

$mission = $company_details['mission'];
$vision = $company_details['vision'];
$core_values = $company_details['core_values'];



?>

<?php
  $about_query = $this->site_model->get_active_content_items('About Us',1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= '<div class="col-6">
                          <img src="'.$post_gallery_image_name.'" class="w-100" alt="">
                        </div>';
        }
      }

    }
  }
  ?>


 
<!-- Slider
    ============================================= -->
    <section id="slider" class="slider-element swiper_wrapper min-vh-75 justify-content-start dark" style="background: #063639">

      <div class="container">
        <div class="row h-100 align-items-center justify-content-between">
          <div class="col-lg-6 col-md-6 py-5 py-md-0" style="margin-top: 0px !important;font-size: 0.9rem !important;text-align: justify;">
            <div class="heading-block border-bottom-0 mb-4">
              <!-- <h5 class="mb-1 text-uppercase ls2 color op-06">About Us</h5> -->
              <h2 class="mb-4 nott">Our Story</h2>
            </div>
            <div class="svg-line bottommargin-sm">
              <img src="<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/divider-1.svg" alt="svg divider" height="20">
            </div>
            <p class="mb-5" ><?php echo $description;?></p>
            <!-- <a href="demo-nonprofit-causes-single.html" class="button button-rounded button-xlarge bg-color button-light text-dark shadow nott ls0 m-0">Donate Now</a> -->

          </div>
          <div class="col-lg-6 col-md-6">
            <img src="<?php echo $image_about?>" alt="Image" class="full-width-img">
          </div>
        </div>
      </div>

    </section>

    <!-- Content
    ============================================= -->
    <section id="content">
      <div class="content-wrap py-0 overflow-visible">

        <!-- <div class="container">

          <div class="slider-feature">
            <div class="row gutter-10 justify-content-center">
              <div class="col-md-6">
                <div class="card center border-left-0 border-right-0 border-top-0 border-bottom border-bottom shadow py-3 rounded-0 font-weight-semibold text-uppercase ls1 h-translate-y-sm all-ts">
                  <div class="card-body">
                
                     <h3 class="mb-2">Mission</h3>
                    <h5 class="nott ls0 mb-0 mt-2 text-muted"><?php echo $mission?></h5>
                  </div>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="card center border-left-0 border-right-0 border-top-0 border-bottom border-bottom shadow py-3 rounded-0 font-weight-semibold text-uppercase ls1 h-translate-y-sm all-ts">
                  <div class="card-body">
                    
                     <h3 class="mb-2">Vision</h3>
                    <h5 class="nott ls0 mb-0 mt-2 text-muted"><?php echo $vision?></h5>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div> -->

        <div class="clear"></div>
        <!-- <div class="section mt-3" style="background: #FFF url('<?php echo base_url().'assets/backgrounds/'?>why-choose-us.jpg') no-repeat 100% 50% / auto 100%;"> -->
        <div class="section mt-3" style="background: #FFF url('') no-repeat 100% 50% / auto 100%;">
              <div class="container">
                  <div class="row justify-content-center">
                      <div class="col-md-7 center">
                          <div class="heading-block border-bottom-0 mb-4">
                              <h2 class="mb-4 nott">WHY CHOOSE US</h2>
                          </div>
                          <div class="svg-line bottommargin-sm">
                              <img src="<?php echo base_url().'assets/themes/canvas/'?>demos/nonprofit/images/divider-1.svg" alt="svg divider" height="20">
                          </div>
                          <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, quasi, dolorum blanditiis eius laboriosam, quidem temporibus, dolor quod voluptatum perferendis ullam. Odio sequi at dolore consequatur ad, odit expedita tenetur.</p> -->
                      </div>
                  </div>
                  <div class="row mt-5 col-mb-50 mb-0">
                      <?php echo $this->load->view("site/why_choose_us", '');?>
                     
                  </div>
              </div>
          </div>


      </div>
      <?php echo $this->load->view("site/footer_inline", '');?> 
    </section><!-- #content end -->