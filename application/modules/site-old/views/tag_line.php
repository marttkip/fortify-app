<div class="section mt-7">
    <div class="container">
        <div class="banner-call">
            <div class="row no-gutters">
                <div class="col-sm-5 col-lg-4 order-2 order-sm-1 mt-3 mt-md-0 text-center text-md-right">
                    <img src="<?php echo base_url().'assets/images/'?>doctors.jpg" width="156%" alt="" class="shift-left">
                </div>
                <div class="col-sm-7 col-lg-7 d-flex align-items-center order-1 order-sm-2">
                    <div class="text-center">
                        <h2>Looking for <br class="d-lg-none"> a <span class="theme-color">Certified Dentist?</span></h2>
                        <div class="h-decor"></div>
                        <p class="mt-sm-1 mt-lg-4 text-left text-sm-center">We're always accepting new patients! We believe in providing the best possible care to all our existing patients and welcome new patients to sample the service we have to offer. </p>
                        <div class="mt-3 mt-lg-4">
                            <a href="index.html#" class="banner-call-phone"><i class="fa fa-phone"></i> <?php echo $phone;?></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>