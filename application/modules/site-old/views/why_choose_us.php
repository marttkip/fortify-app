<?php
        $specialist_query = $this->site_model->get_active_items('Why Choose Us');
          $main_services = '';
          if($specialist_query->num_rows() > 0)
          {
            $x=0;
            foreach($specialist_query->result() as $row)
            {
              $about_title = $row->post_title;
              $post_id = $row->post_id;
              $blog_category_name = $row->blog_category_name;
              $blog_category_id = $row->blog_category_id;
              $post_title = $row->post_title;
              $web_name = $this->site_model->create_web_name($post_title);
              $post_status = $row->post_status;
              $post_views = $row->post_views;
              $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
              $created_by = $row->created_by;
              $modified_by = $row->modified_by;
              $post_target = $row->post_target;
              $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
              $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
              $description = $row->post_content;
              $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
              $created = $row->created;
              $day = date('j',strtotime($created));
              $month = date('M',strtotime($created));
              $year = date('Y',strtotime($created));
              $created_on = date('jS M Y',strtotime($row->created));
              $x++;
              if($x < 9)
              {
                $x = '0'.$x;
              }

              if($x== 1)
              {
              	$expandable="true";
                $show = 'show';
              }
              else
              {
              	$expandable = 'false';
                 $show = '';
              }
              $main_services .= '<div class="col-md-6">
                                    <div class="feature-box flex-column pl-0">
                                        
                                        <div class="fbox-content px-0">
                                            <h3 class="nott ls0"><a href="#" class="text-dark">'.$post_title.'</a></h3>
                                            <p>'.$description.'</p>
                                        </div>
                                    </div>
                                </div>';
        }
       }
        // $main_services = '</ul>';

      // $gallery_rs =  $this->site_model->get_gallery('Company Gallery');
      // $gallery_result = '';
      // if($gallery_rs->num_rows() > 0)
      // {
      //   foreach ($gallery_rs->result() as $key => $value) {
      //     # code...
      //     $gallery_name = $value->gallery_name;
      //     $gallery_image_name = $value->gallery_image_name;

      //     $image_specialist = base_url().'assets/gallery/'.$gallery_image_name;
      //     $gallery_result .= '<div class="item">
      //                               <img src="'.$image_specialist.'" class="img-responsive" alt="story_img" />
      //                           </div>';
      //   }
      // }
     ?>
<?php echo $main_services?>
            