<?php
$slideshow_result = '';
 if($slides->num_rows() > 0)
{
  $slides_no = $slides->num_rows();
  // var_dump($slides_no);die();
  $x= 0;
  $y= 0;
  $z=100;
  $count=0;
  foreach($slides->result() as $cat => $value)
  {   

    $slideshow_id = $value->slideshow_id;
    $slideshow_status = $value->slideshow_status;
    $slideshow_name = $value->slideshow_name;
    $slideshow_description = $value->slideshow_description;
    $slideshow_image_name = $value->slideshow_image_name;
    $slideshow_link = $value->slideshow_link;
    $slideshow_button_text = $value->slideshow_button_text;
    $subtitle = $value->subtitle;
    $slideshow_thumb_name = 'thumbnail_'.$value->slideshow_image_name;
 

 	$med = explode(' ', $slideshow_name,2);

 	$first_name = $med[0];
 	$second_name = $med[1];

 
    $x+100;


 	if(!empty($slideshow_link))
 	{
 		$buttons = '<a href="'.$slideshow_button_link.'" data-animate="fadeInUp" data-delay="400" class="button button-rounded button-large button-light shadow nott ls0 ml-0 mt-4">'.$slideshow_button_text.'</a>';
	}
	else
	{
		$buttons = '';
	}
    if($y == 0)
    {
        $y = '';
    }
    // if($cou)
    $count++;

    // if ($count % 3 != 0) {
    //      $slide_value = 200;
    //      $item_value = 1;
    // }
    // else
    // {
    //     $slide_value = 100;
    //     $item_value = '';
    // }
    $slide_value = $count.'00';
    $item_value = '';
    // if($count == 1)
    // {
    //     $slide_value = $count.'00';
    //     $item_value = '';
    // }
    // else if($count == 2)
    // {
    //     $slide_value = $count.'00';
    //     $item_value = '';
    // }
    // else if($count == 3)
    // {
    //      $slide_value = $count.'00';
    //      $item_value = '';
    // }
    // else if($count == 4)
    // {
    //      $slide_value = $count.'00';
    //      $item_value = '';
    // }

        $slideshow_result .= '
                                <div class="swiper-slide dark">
                                    <div class="container" >
                                        <div class="slider-caption">
                                           
                                            <div>
                                                <h2 class="nott" data-animate="fadeInUp">'.$slideshow_name.'</h2>
                                                '.$buttons.'
                                            </div>
                                        </div>


                                    </div>
                                    <div class="swiper-slide-bg" style="background:url('.base_url().'assets/slideshow/'.$slideshow_image_name.') no-repeat center center; background-size: cover;"></div>
                                </div>';

	?>
    
     <!-- <div class="swiper-slide-bg" style="background: linear-gradient(rgba(0,0,0,.3), rgba(0,0,0,.5)), url('.base_url().'assets/slideshow/'.$slideshow_image_name.') no-repeat center center; background-size: cover;"></div> -->
        
   
        
<?php

         $z+100;
         $y=1;
	}
}
?>
 <section id="slider" class="slider-element dark swiper_wrapper slider-parallax min-vh-75">
        <div class="slider-inner">

            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper">
                    <?php echo $slideshow_result;?>
                    <!-- <div class="swiper-slide dark">
                        <div class="container" >
                            <div class="slider-caption">
                               
                                <div>
                                    <h2 class="nott" data-animate="fadeInUp">Help the Homeless.</h2>
                                    <a href="#" data-animate="fadeInUp" data-delay="400" class="button button-rounded button-large button-light shadow nott ls0 ml-0 mt-4">Know More</a>
                                </div>
                            </div>


                        </div>
                        <div class="swiper-slide-bg" style="background: linear-gradient(rgba(0,0,0,.3), rgba(0,0,0,.5)), url('demos/nonprofit/images/slider/1.jpg') no-repeat center center; background-size: cover;"></div>
                    </div>
                    <div class="swiper-slide dark">
                        <div class="container">
                            <div class="slider-caption">
                                <div>
                                    <h2 class="nott" data-animate="fadeInUp">Donate with Kindness.</h2>
                                    <a href="#" data-animate="fadeInUp" data-delay="400" class="button button-rounded button-large button-light shadow nott ls0 ml-0 mt-4">Know More</a>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide-bg" style="background: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,.8)), url('demos/nonprofit/images/slider/2.jpg') no-repeat center center; background-size: cover;"></div>
                    </div> -->
                </div>
                <div class="swiper-navs">
                    <div class="slider-arrow-left"><i class="icon-line-arrow-left"></i></div>
                    <div class="slider-arrow-right"><i class="icon-line-arrow-right"></i></div>
                </div>
                <div class="swiper-scrollbar">
                    <div class="swiper-scrollbar-drag">
                    <div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div></div>
                </div>
            </div>

        </div>
    </section>


    <!-- <div class="video-wrap">
        <video poster="<?php echo base_url().'assets/themes/canvas/'?>images/videos/explore-poster.jpg" preload="none" loop autoplay muted>
            <source src='<?php echo base_url().'assets/themes/canvas/'?>images/videos/explore.mp4' type='video/mp4' />
            <source src='<?php echo base_url().'assets/themes/canvas/'?>images/videos/explore.webm' type='video/webm' />
        </video>
        <div class="video-overlay" style="background-color: rgba(0,0,0,0.3);"></div>
    </div> -->