<?php
$about_query = $this->site_model->get_active_items('Special Offers');
 $faqs_list = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $x++;
    // if($x < 9)
    // {
    //   $x = '0'.$x;
    // }
    $faqs_list .= '<div class="col-6">
		                <div class="special-card">
		                    <div class="special-card-photo">
		                        <img src="'.base_url().'assets/themes/dentco/images/content/special-photo-02.jpg" alt="">
		                    </div>
		                    <div class="special-card-caption text-left">
		                        <div class="special-card-txt1">'.$post_title.'</div>
		                        <div class="special-card-txt2">Offer</div>
		                        <div class="special-card-txt3">'.$post_target.'</div>
		                        <div><a href="#" class="btn"><i class="fa fa-angle-right"></i><span>Schedule</span><i class="fa fa-angle-right"></i></a></div>
		                    </div>
		                </div>
		            </div>';
  }
}
?>
<div class="section" id="specialOffer">
    <div class="container">
        <div class="title-wrap text-center">
            <div class="h-sub theme-color">For Our Dear Clients</div>
            <h2 class="h1">Special Offers</h2>
            <div class="h-decor"></div>
        </div>
        <div class="special-carousel js-special-carousel row">
            <?php echo $faqs_list;?>
            
        </div>
    </div>
</div>