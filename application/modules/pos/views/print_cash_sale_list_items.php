
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
        	body
        	{
        		font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
        	}
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			
			.col-md-6 {
			    width: 50%;
			 }
		
			h3
			{
				font-size: 30px;
			}
			p
			{
				margin: 0 0 0px !important;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{ margin:0 auto;}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}

			table.table{
			    border-spacing: 0 !important;
			    font-size:12px;
				margin-top:0px;
				margin-bottom: 10px !important;
				border-collapse: inherit !important;
				font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
			}
			
			.padd
			{
				padding:10px;
			}
			
		</style>
    </head>
    <!-- <body onLoad="window.print();return false;"> -->
    <body >	
 		

    	<div class="padd " >
    		<div class="col-md-12" style="min-height: 100px;">
                <div class="col-print-4 pull-left">
                    <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive"/>
                      &nbsp;
                </div>
                <div class="col-print-8 center-align">
                    <strong>
                   		<?php echo $title;?>
                    </strong>
                </div> 
                 
                
            </div>         
	      	<div class="row">
	      		<div class="col-md-12">
	      			<?php
					$incomplete_result ='<table class="table table-condensed table-bordered table-hover">
											<thead>
												<th>#</th>
												<th>Invoice Number</th>
												<th>Item</th>
												<th>Units</th>
												<th>Unit Charge</th>
												<th>Amount Charged</th>

											</thead>
											<tbody>';

					if($visits_query->num_rows() > 0)
					{
						$x=0;
						foreach ($visits_query->result() as $key => $value) {
							# code...
							$order_invoice_number = $value->order_invoice_number;
							$units = $value->pos_order_item_quantity;
							$procedure_name = $value->service_charge_name;
							$amount = $value->pos_order_item_amount;
							// $visit_invoice_id = $value->visit_invoice_id;
							// $visit_type_id = 1;
							

							$total_amount = $units * $amount;
							// $total= $total +($units * $amount);

							$x++;
							$incomplete_result .='
												<tr >
													<td>'.$x.'</td>
													<td>'.$order_invoice_number.'</td>
													<td>'.$procedure_name.'</td>
													<td>'.$units.'</td>
													<td>'.number_format($amount,2).'</td>
													<td>'.number_format($total_amount,2).'</td>
												</tr>';

						}
					}
					$incomplete_result .='</tbody>
									</table>';

					echo $incomplete_result;
					
				?>
	      		</div>
	      	</div>
	      
	    </div>


    </body>
    
</html>