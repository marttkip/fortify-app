<?php

$refund_note_id ='';
$preauth_date = date('Y-m-d');
$preauth_amount = '';
if(!empty($refund_note_id))
{
	$visit_invoice_detail = $this->pos_model->get_visit_refund_details($refund_note_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$refund_note_id = $value->refund_note_id;

		}
	}
	$visit__rs1 = $this->pos_model->get_visit_refund_charged($pos_refund_id,$refund_note_id);
}
else
{
	$visit__rs1 = $this->pos_model->get_visit_refund_charged($pos_refund_id);	
}
// var_dump($visit__rs1);die()
// $visit_rs = $this->accounts_model->get_visit_details($visit_id);
$visit_type_id = 1;
$close_card = 3;
$visit_rs = $this->pos_model->get_order_details($pos_refund_id);

$sale_type = 0;
if($visit_rs->num_rows() > 0)
{
	foreach ($visit_rs->result() as $key => $value) {
		# code...
		$sale_type = $value->sale_type;
	}
}


// var_dump($visit__rs1->result())	;die();
// echo form_open("finance/creditors/confirm_invoice_note", array("class" => "form-horizontal","id" => "confirm-invoice"));
$result = "

	<table align='center' class='table table-striped table-bordered table-condensed'>
	<tr>
		<th></th>
		<th>SKU</th>
		<th>Invoice Item</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Disc</th>
		<th>VAT</th>
		<th>Total</th>
		<th colspan=2></th>

	</tr>
";
	$total= 0;  
	$total_units= 0;
	$number = 0;
	$vat_charged = 0;
	if($visit__rs1->num_rows() > 0)
	{						
		foreach ($visit__rs1->result() as $key1 => $value) :
			$v_procedure_id = $value->pos_order_item_id;
			$procedure_id = $value->service_charge_id;
			$product_code = $value->product_code;
			$pos_order_item_amount = $value->pos_order_item_amount;
			$units = $value->pos_order_item_quantity;
			$procedure_name = $value->service_charge_name;
			$service_id = $value->service_id;
			$vatable = $value->vatable;
			$product_id = $value->product_id;
			$discount = $value->discount_added;
			$vat_added = $value->vat_added;


			if($vat_added == 1)
			{
				$vat = 1.14;
				$checked_box = 'checked';
				$no_checked_box = '';
			}
			else
			{
				$vat = 1;
				$checked_box = '';
				$no_checked_box = 'checked';
			}

			// var_dump($discount);die();
			// $refund_note_id = $value->refund_note_id;
			// $visit_type_id = 1;
			$total= $total +(($units * $pos_order_item_amount) *$vat) - $discount;

			if($refund_note_id > 0)
			{
				$text_color = "success";
			}
			else
			{
				$text_color = 'default';
			}

			// if($visit_type_id == 1)
			// {
			// 	$visit = 'SELF';
			// }
			// else
			// {
			// 	$visit = 'INSURANCE';
			// }
		
			$checked="";
			$number++;

			// if($vatable)
			// {
			$vat_charged += (($units * $pos_order_item_amount)*$vat) / 1.14;
			// }

			$total_units += $units;
			$personnel_check = TRUE;
			$checked_items ='';
			if($personnel_check AND empty($refund_note_id))
			{
				$checked_items = '
							<td>
								<a class="btn btn-sm btn-success" href="#" onclick="calculatetotal('.$pos_order_item_amount.','.$v_procedure_id.', '.$procedure_id.','.$pos_refund_id.')"><i class="fa fa-pencil"></i></a>
							</td>
							<td>
								<a class="btn btn-sm btn-danger" href="#" onclick="delete_procedure('.$v_procedure_id.', '.$pos_refund_id.')"><i class="fa fa-trash"></i></a>
							</td>';
			}

			if(empty($refund_note_id) )
			{
				$checked_boxes = 'onclick="calculatetotal('.$pos_order_item_amount.','.$v_procedure_id.', '.$procedure_id.','.$pos_refund_id.')"';
			}
			else
			{
				$checked_boxes = '';
			}

			// if($close_card == 3)
			// {

			// }
		 $checkbox_data = array(
			                    'name'        => 'visit_invoice_items[]',
			                    'id'          => 'checkbox'.$v_procedure_id,
			                    'class'          => 'css-checkbox  lrg ',
			                    'checked'=>'checked',
			                    'value'       => $v_procedure_id
			                  );
			// if($refund_note_id > 0)
			// {
			// 	$checked = 'readonly';
			// }
			// else
			// {
			// 	$checked = '';
			// }
		if($pos_order_item_amount == 0)
		{
			$pos_order_item_amount = '';
		}
		$result .='
					<tr> 

						<td class="'.$text_color.'" >'.form_checkbox($checkbox_data).'<label for="checkbox'.$v_procedure_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td class="'.$text_color.'" >'.$product_code.'</td>
						<td  class="'.$text_color.'" align="left">'.strtoupper(strtolower($procedure_name)).'</td>
						<td  class="'.$text_color.'" align="center">
							<input type="number" id="units'.$v_procedure_id.'" class="form-control" value="'.$units.'" size="3"  '.$checked.'/>

							<input type="hidden" id="refund_note_id'.$v_procedure_id.'" class="form-control" value="'.$refund_note_id.'" size="3" />
							<input type="hidden" id="product_id'.$v_procedure_id.'" class="form-control" value="'.$product_id.'" size="3" />
						</td>
						<td  class="'.$text_color.'" align="center">
							<input type="number" class="form-control" size="5" value="'.$pos_order_item_amount.'" id="billed_amount'.$v_procedure_id.'"  '.$checked.'>
						</td>
						<td  class="'.$text_color.'" align="center">
							<input type="number" class="form-control" size="5" value="'.$discount.'" id="discount'.$v_procedure_id.'"  '.$checked.'>
						</td>
						<td>
						 <input id="optionsRadios2" type="radio" name="vatable'.$v_procedure_id.'" id="vatable'.$v_procedure_id.'" value="0" '.$no_checked_box.' '.$checked_boxes.'>
                        No
                        <input id="optionsRadios2" type="radio" name="vatable'.$v_procedure_id.'" id="vatable'.$v_procedure_id.'" value="1" '.$checked_box.' '.$checked_boxes.'  >
                        Yes
						</td>
						<td  class="'.$text_color.'" align="center">'.number_format((($units*$pos_order_item_amount)*$vat)-$discount,2).'</td>
						'.$checked_items.'
					</tr>	
			';
								
			endforeach;

	}
	else
	{
		
	}
	$result .="
		<tr bgcolor='#D9EDF7'>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<th>Grand Total: </th>
		<th colspan='1'><div id='grand_total'>".number_format($total,2)."</div></th>
	
		</tr>
		 </table>
		";
	$result .= "</table>";
	$preauth_amount = $total;
	
?>






<div class="col-print-6" >
	<div class="top-sale-div" >
		
		<div class="panel-body" style="height:55vh">
			<?php echo $result;?>
		</div>
	</div>
</div>
<div class="col-print-6">

	<div class="top-sale-div" style="background-color: white;">
	<div class="panel-body" style="height:55vh">
		<table class="table table-condensed">
			<tr>
				<td><strong>Subtotal</strong></td>
				<td> <strong>Ksh. <?php echo number_format($vat_charged,2)?></strong></td>
			</tr>

			<tr>
				<td><strong>Tax (14%)</strong></td>
				<td><strong>Ksh. <?php echo number_format($total - $vat_charged,2)?></strong></td>
			</tr>

			<tr>
				<td>Items</td>
				<td><?php echo $total_units?></td>
			</tr>

			<tr>
				<td><h4>Grand Total</h4></td>
				<td> <h4>Ksh. <?php echo number_format($total,2)?></h4></td>
			</tr>

		</table>
		<div class="col-md-12">
			<?php

			if($total_units > 0 AND empty($refund_note_id))
			{
				if($sale_type == 1)
				{
					?>
						<a class="btn btn-sm btn-warning col-md-12" id="credit-sale-button" onclick="proceed_to_complete_sale()"> <i class="fa fa-shopping-cart"></i> Complete Credit Sale</a>
					<?php	
				}
				else if($sale_type == 0)
				{
					?>
						<a class="btn btn-sm btn-success col-md-12"  onclick="proceed_to_complete_sale()"> <i class="fa fa-shopping-cart"></i> Complete Cash Sale</a>
					<?php	

				}
				else if($sale_type == 3)
				{
					?>
						<a class="btn btn-sm btn-info col-md-12" onclick="proceed_to_complete_sale()"> <i class="fa fa-shopping-cart"></i> Complete Quote</a>
					<?php	
				}
			}
			?>
			
		</div>
	</div>
	
</div>


<div class="modal fade bs-example-modal-lg" id="add_assessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Receipt Invoice</h4>
            </div>
            <div class="modal-body">
            	<!-- <div class="row">
                	<div class='col-md-12'>
                      	<div class="form-group center-align">
								<h1 class="center-align">Ksh. <?php echo number_format($total,2)?></h1>
						</div>
						 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
                      	<div class="form-group" style="margin-top:10px;margin-left:20px;">
							<label class="col-lg-12 left-align">
							<strong> Tender:</strong> </label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="tendered_amount" id="tendered_amount" onkeyup="get_change(<?php echo $total;?>)" placeholder="" autocomplete="off" >
							</div>
						</div>

						 <input type="hidden" class="form-control" name="change" id="change" utocomplete="off">			
						  <input type="hidden" class="form-control" name="invoice_id" id="invoice_id" placeholder="Invoice id" value="<?php echo $refund_note_id;?>">
                      	<div class="form-group" style="margin-top:10px;margin-left:20px;">
							<label class="col-lg-12 left-align">
							<strong>Change: </strong>
							</label>
						  
							<div class="col-lg-10">
								<div id="balance-div"></div>
							</div>
						</div>
                    </div>
                </div> -->
            </div>
            <div class="modal-footer">
            	<button type="submit" class='btn btn-info btn-sm' type='submit'  onclick="return confirm('Are you sure you want to finalize this sale')">Finalize Sale</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div>
</div>




 <?php //echo form_close();?>