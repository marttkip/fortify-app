<div class="panel-body" style="margin-top:50px;height:80vh;">
<?php

$order_invoice_number ='';
$preauth_date = date('Y-m-d');
$preauth_amount = '';
if(!empty($order_invoice_id))
{
	$visit_invoice_detail = $this->pos_model->get_visit_invoice_details($order_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$order_invoice_number = $value->order_invoice_number;

		}
	}
	$visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,$order_invoice_id);
}
else
{
	$visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id);	
}

$visit_rs = $this->pos_model->get_order_details($pos_order_id);

$sale_type = 0;
if($visit_rs->num_rows() > 0)
{
	foreach ($visit_rs->result() as $key => $value) {
		# code...
		$sale_type = $value->sale_type;
	}
}

if($sale_type == 1)
{
	$sale_div = 'none';
	$credit_div = 'block';
}
else
{
	$sale_div = 'block';
	$credit_div = 'none';
}
// get all the procedures


// var_dump($visit__rs1->result())	;die();
echo form_open("finance/creditors/confirm_invoice_note", array("class" => "form-horizontal","id" => "confirm-invoice"));
$result = "

	<table align='center' class='table table-striped table-bordered table-condensed'>
	<tr>
		<th></th>
		<th>SKU</th>
		<th>Invoice Item</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Disc</th>
		<th>Total</th>
	

	</tr>
";
	$total= 0;  
	$total_units= 0;
	$number = 0;
	if($visit__rs1->num_rows() > 0)
	{						
		foreach ($visit__rs1->result() as $key1 => $value) :
			$v_procedure_id = $value->pos_order_item_id;
			$procedure_id = $value->service_charge_id;
			$product_code = $value->product_code;
			$pos_order_item_amount = $value->pos_order_item_amount;
			$units = $value->pos_order_item_quantity;
			$procedure_name = $value->service_charge_name;
			$service_id = $value->service_id;
			$discount = $value->discount_added;
			$vat_added = $value->vat_added;


			if($vat_added == 1)
			{
				$vat = 1.14;
				$checked_box = 'checked';
				$no_checked_box = '';
			}
			else
			{
				$vat = 1;
				$checked_box = '';
				$no_checked_box = 'checked';
			}

			// var_dump($discount);die();
			// $order_invoice_id = $value->order_invoice_id;
			// $visit_type_id = 1;
			$total= $total +(($units * $pos_order_item_amount) *$vat) - $discount;

			if($visit_invoice_id > 0)
			{
				$text_color = "success";
			}
			else
			{
				$text_color = 'default';
			}

			$vat_charged += (($units * $pos_order_item_amount)*$vat) * 0.14;


		
			$checked="";
			$number++;

			$total_units += $units;
			$personnel_check = TRUE;
			if($personnel_check AND $close_card == 3)
			{
				$checked = "<td>
						
								<a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$pos_order_id.")'><i class='fa fa-trash'></i></a>
							</td>";
			}

			// if($close_card == 3)
			// {

			// }
		 $checkbox_data = array(
			                    'name'        => 'visit_invoice_items[]',
			                    'id'          => 'checkbox'.$v_procedure_id,
			                    'class'          => 'css-checkbox  lrg ',
			                    'checked'=>'checked',
			                    'value'       => $v_procedure_id
			                  );
			if($order_invoice_id > 0)
			{
				$checked = 'readonly';
			}
			else
			{
				$checked = '';
			}
		$result .='
					<tr> 

						<td class="'.$text_color.'" >'.form_checkbox($checkbox_data).'<label for="checkbox'.$v_procedure_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td class="'.$text_color.'" >'.$product_code.'</td>
						<td  class="'.$text_color.'" align="left">'.strtoupper(strtolower($procedure_name)).'</td>
						<td  class="'.$text_color.'" align="center">
							<input type="number" id="units'.$v_procedure_id.'" class="form-control" value="'.$units.'" size="3" onkeyup="calculatetotal('.$pos_order_item_amount.','.$v_procedure_id.', '.$procedure_id.','.$pos_order_id.')" '.$checked.' readonly/>

							<input type="hidden" id="order_invoice_id'.$v_procedure_id.'" class="form-control" value="'.$order_invoice_id.'" size="3" />
						</td>
						<td  class="'.$text_color.'" align="center">
							<input type="number" class="form-control" size="5" value="'.$pos_order_item_amount.'" id="billed_amount'.$v_procedure_id.'" onkeyup="calculatetotal('.$pos_order_item_amount.','.$v_procedure_id.', '.$procedure_id.','.$pos_order_id.')" '.$checked.' readonly>
						</td>
						<td  class="'.$text_color.'" align="center">
							<input type="number" class="form-control" size="5" value="'.$discount.'" id="discount'.$v_procedure_id.'" onkeyup="calculatetotal('.$pos_order_item_amount.','.$v_procedure_id.', '.$procedure_id.','.$pos_order_id.')" '.$checked.' readonly>
						</td>

						<td  class="'.$text_color.'" align="center">'.number_format((($units*$pos_order_item_amount)*$vat)-$discount,2).'</td>
					</tr>	
			';
								
			endforeach;

	}
	else
	{
		
	}
	$result .="
		<tr bgcolor='#D9EDF7'>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<th>Grand Total: </th>
		<th colspan='1'><div id='grand_total'>".number_format($total,2)."</div></th>
	
		</tr>
		 </table>
		";
	$result .= "</table>";
	
	$preauth_amount = $total;
	
?>

	 <input type="hidden" class="form-control" name="charge_visit_id" id="charge_visit_id" placeholder="Middle Name" value="<?php echo $pos_order_id;?>">
 	<input type="hidden" class="form-control" name="amount" id="amount" placeholder="Amount" value="<?php echo $total;?>">
 	<input type="hidden" class="form-control" name="sale_type" id="sale_type" placeholder="Amount" value="<?php echo $sale_type;?>">

 	
	<div class="row">
		<div class='col-md-4'>
			<h2>Invoice Item</h2>
			<?php echo $result;?>
		</div>
    	<div class='col-md-7'>
    		<div id="username-div" style="display: block;">
    			<div class="alert alert-warning">
    				Enter your username and password to login to complete sale 
    			</div>
	          	<div class="form-group" style="margin-top:10px;margin-left:20px;">
					<label class="col-lg-12 left-align">
					<strong><h3> Username: </h3></strong> </label>
					<div class="col-lg-12">
						<input type="text" class="form-control" name="username" id="username"  placeholder="" autocomplete="off"  style="padding: 30px 5px !important;font-size: 36px !important;text-align: center;">
					</div>
				</div>
		        <div class="form-group" style="margin-top:10px;margin-left:20px;">
						<label class="col-lg-12 left-align">
						<strong><h3> Password: </h3></strong> </label>
						<div class="col-lg-12">
							<input type="password" class="form-control" name="password" id="password"  placeholder="" autocomplete="off"  style="padding: 30px 5px !important;font-size: 36px !important;text-align: center;">
						</div>
				</div>

				<div class="center-align">
					<a class='btn btn-info btn-sm'   onclick="login_to_sale(<?php echo $pos_order_id?>,<?php echo $sale_type?>)">Confirm Login</a>
				</div>


    		</div>
    		 <div class="form-group center-align" style="display: none;" id="payment-complete-off">
				<h3>
						<label class="col-lg-2 control-label">Sale Type? </label>
			            <div class="col-lg-5">
			                <div class="radio">
			                    <label>
			                        <input id="optionsRadios2" type="radio" name="sale_type" value="0" onclick="get_invoice_type(<?php echo $pos_order_id;?>,0)">
			                        Cash Sale
			                    </label>
			                </div>
			            </div>
			            
			            <div class="col-lg-5">
			                <div class="radio">
			                    <label>
			                        <input id="optionsRadios2" type="radio" name="sale_type" value="1" onclick="get_invoice_type(<?php echo $pos_order_id;?>,1)">
			                        Credit Sale
			                    </label>
			                </div>
			            </div>
					
				</h3>
				
			</div>
			<hr>

			<div id="quote-div" style="display: none;">
					<div class="form-group center-align">
							<h1 class="center-align">QUOTATION</h1>
							<h1 class="center-align">Quote Amount Ksh. <?php echo number_format($total,2)?></h1>
					</div>
					 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
		          	

					<input type="hidden" class="form-control" name="change" id="change" utocomplete="off">			
					<input type="hidden" class="form-control" name="invoice_id" id="invoice_id" placeholder="Invoice id" value="<?php echo $order_invoice_id;?>">
		          	
					<div class="center-align">
						<button type="submit" class='btn btn-info btn-sm' type='submit'  onclick="return confirm('Are you sure you want to finalize this sale')">Finalize Sale</button>
					</div>

					
			</div>

			<div id="cash-sale-div" style="display: none;">
					<div class="form-group center-align">
							<h1 class="center-align">CASH SALE</h1>
							<h1 class="center-align">Invoice Amount Ksh. <?php echo number_format($total,2)?></h1>
					</div>
					 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
		          	<div class="form-group" style="margin-top:10px;margin-left:20px;">
						<label class="col-lg-12 left-align">
						<strong><h3> Amount Paid: </h3></strong> </label>
						<div class="col-lg-12">
							<input type="number" class="form-control" name="tendered_amount" id="tendered_amount" value="<?php echo $total;?>" onkeyup="get_change(<?php echo $total;?>)" placeholder="" autocomplete="off"  style="padding: 30px 5px !important;font-size: 36px !important;text-align: center;">
						</div>
					</div>

					 <input type="hidden" class="form-control" name="change" id="change" utocomplete="off">			
					  <input type="hidden" class="form-control" name="invoice_id" id="invoice_id" placeholder="Invoice id" value="<?php echo $order_invoice_id;?>">
		          	<div class="form-group" style="margin-top:10px;margin-left:20px;">
						<label class="col-lg-12 left-align">
						<strong><h3>Change:</h3> </strong>
						</label>
					  
						<div class="col-lg-10">
							<h1><div id="balance-div"></div></h1>
						</div>
					</div>
					<div class="center-align">
						<button type="submit" class='btn btn-info btn-sm' type='submit'  onclick="return confirm('Are you sure you want to finalize this sale')">Finalize Sale</button>
					</div>

					
			</div>


			<div id="credit-sale-div" style="display: none;">
				<!-- <div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group center-align">
							<input type="text" class="form-control" name="customer_name" id="q" placeholder="Search Customer by name, customer number, phone or email" autocomplete="off" onkeyup="search_customers()">
						</div>
					</div>
					<div class="col-md-6">
						<a class='btn btn-info btn-sm col-md-12' type='submit'  onclick="add_new_customer()">Add new customer</a>
					</div>
				</div>
				<div class="col-md-12">
					<div id="searched-customers"></div>
				</div> -->
				<div id="searched-div">
					<div class="col-md-12">
						<div class="form-group" style="margin-top:10px;margin-left:20px;">
							
							<h2 class="center-align"><span id="sale-detail"></span> <a class="btn btn-sm btn-danger" onclick="clear_order_invoice(<?php echo $pos_order_id;?>)"><i class="fa fa-trash"></i> </a></h2>
							
						</div>
						<div class="form-group center-align">
							<h2 class="center-align">Debit  Ksh. <?php echo number_format($total,2)?></h2>
						</div>

						<div class="form-group" style="margin-top:10px;margin-left:20px;">
							<label class="col-lg-12 left-align">
							<strong><h3> LPO NUMBER: </h3></strong> </label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="lpo_number" id="lpo_number"  placeholder=" LPO NO." autocomplete="off"  style="padding: 30px 5px !important;font-size: 36px !important; text-align: center;">
							</div>
						</div>
						<div class="center-align">
							<button type="submit" class='btn btn-info btn-sm' type='submit'  onclick="return confirm('Are you sure you want to finalize this sale')">Finalize Credit Sale</button>
						</div>
					</div>
				</div>

			

			</div>
			
          
        </div>
    </div>
<?php echo form_close();?>

<?php echo form_open("finance/creditors/add_client", array("class" => "form-horizontal","id" => "add-customer"));?>

<div id="new-customer-div" style="display: none;">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="form-group">
                    <label class="col-md-4 control-label">Customer Name *: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="customer_name" placeholder="Customer name" value="<?php echo set_value('customer_name');?>" required="required" autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label"> PIN Number *: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="pin_number" placeholder="PIN number" value="<?php echo set_value('pin_number');?>" required="required" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label"> VAT Number *: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="vat_number" placeholder="VAT Number" value="<?php echo set_value('vat_number');?>"  autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label"> Address : </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="address" placeholder="Address" value="<?php echo set_value('address');?>"  autocomplete="off">
                    </div>
                </div>

                


               
                
                
			</div>
			<div class="col-md-6">
				<div class="form-group">
                    <label class="col-md-4 control-label"> P.O Box: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="postal_address" placeholder="Postal Address" value="<?php echo set_value('postal_address');?>"  autocomplete="off">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-md-4 control-label">Customer Email *: </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="customer_email" placeholder="Customer Email" value="<?php echo set_value('customer_email');?>" required="required" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label"> Telephone Number *: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="customer_phone" placeholder="Customer Phone" value="<?php echo set_value('customer_phone');?>" required="required" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label"> Contact Person *: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="contact_person" placeholder="Customer name" value="<?php echo set_value('contact_person');?>"  autocomplete="off">
                    </div>
                </div>

				
			</div>
		</div>
		<div class="col-md-12" style="margin-top:30px;">
			<div class="form-group">
                <div class="col-md-12 center-align">
                	<button class='btn btn-warning btn-sm' onclick="get_invoice_type(1)" > Back to Credit Sale</button>
                    <button class='btn btn-success btn-sm' type='submit' onclick="confirm('Are you sure you want to add the customer ? ')"  > Add Customer</button>
                </div>
            </div>
			
		</div>
	</div>
</div>


<?php echo form_close();?>
</div>
<div class="col-md-12" style="margin-top: 10px;">					        	
	<div class="center-align">
		<a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
	</div>  
</div>


