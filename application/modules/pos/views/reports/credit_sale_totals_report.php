<div class="row">
	<div class="col-md-2">
		<div class="row" style="margin-bottom: 10px;">
			<div class="panel-body" style="height: 68vh;">

				<?php echo $this->load->view("search/credit_sale_summary_search", '', TRUE);?>
			</div>
		</div>

		
		
	</div>
	<div class="col-md-7" >
		 <div class="col-md-12">
			<h4 class="center-align"><?php echo $title?></h4>
		</div>
		<div class="col-md-12">
			<div class="panel-body" style="height: 75vh;">
					<div id="credit-sale-totals"></div>
			</div>
			
		</div>
		
	</div>
	<div class="col-md-5" >
	</div>
	
</div>
<script type="text/javascript">
	$(document).ready(function(){
   		
   		get_sales_totals();

    });
    function get_sales_totals()
    {
    	var config_url = $('#config_url').val();

		var data_url = config_url+"pos/pos_reports/get_credit_sales_totals";
		
		
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{date_from : null,date_to: null},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);
			$("#credit-sale-totals").html(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
    }
	function get_order_detail(pos_order_id,order_invoice_id)
	{

		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/pos_reports/view_sold_items/"+pos_order_id+"/"+order_invoice_id;
		
		
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
			$("#sale-detail").html(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});


	}

</script>