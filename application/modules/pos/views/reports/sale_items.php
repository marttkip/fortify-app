<?php

$order_invoice_number ='';
$preauth_date = date('Y-m-d');
$preauth_amount = '';
if(!empty($order_invoice_id))
{
	$visit_invoice_detail = $this->pos_model->get_visit_invoice_details($order_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$order_invoice_number = $value->order_invoice_number;

		}
	}
	$visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,$order_invoice_id);
}
else
{
	$visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id);	
}

$visit_type_id = 1;
$close_card = 3;
$result = "

	<table align='center' class='table table-striped table-bordered table-condensed'>
	<tr>
		<th>SKU</th>
		<th>Invoice Item</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>
	

	</tr>
";
	$total= 0;  
	$total_units= 0;
	$number = 0;
	$vat_charged = 0;
	if($visit__rs1->num_rows() > 0)
	{						
		foreach ($visit__rs1->result() as $key1 => $value) :
			$v_procedure_id = $value->pos_order_item_id;
			$procedure_id = $value->service_charge_id;
			$product_code = $value->product_code;
			$pos_order_item_amount = $value->pos_order_item_amount;
			$units = $value->pos_order_item_quantity;
			$procedure_name = $value->service_charge_name;
			$service_id = $value->service_id;
			$vatable = $value->vatable;
			$product_id = $value->product_id;
			// $order_invoice_id = $value->order_invoice_id;
			// $visit_type_id = 1;
			$total= $total +($units * $pos_order_item_amount);

			if($order_invoice_id > 0)
			{
				$text_color = "success";
			}
			else
			{
				$text_color = 'default';
			}

			// if($visit_type_id == 1)
			// {
			// 	$visit = 'SELF';
			// }
			// else
			// {
			// 	$visit = 'INSURANCE';
			// }
		
			$checked="";
			$number++;

			if($vatable)
			{
				$vat_charged += ($units * $pos_order_item_amount) * 0.16;
			}

			$total_units += $units;
			$personnel_check = TRUE;
			
		$result .='
					<tr> 

						
						<td>'.$product_code.'</td>
						<td>'.$procedure_name.'</td>
						<td>'.$units.'</td>
						<td>'.$pos_order_item_amount.'</td>
						<td>'.number_format($units*$pos_order_item_amount,2).'</td>
					</tr>	
			';
								
			endforeach;

	}
	else
	{
		
	}
	$result .="
		<tr bgcolor='#D9EDF7'>
		<td></td>
		<td></td>
		<td></td>
		<th>Grand Total: </th>
		<th colspan='1'><div id='grand_total'>".number_format($total,2)."</div></th>
	
		</tr>
		 </table>
		";
	$result .= "</table>";
	$preauth_amount = $total;
	
?>








<div class="top-sale-div" style="height:60vh; overflow-y: scroll;">

	<?php echo $result;?>
	
	<table class="table table-striped table-bordered table-condensed">
			<tr>
				<td><h4>Subtotal</h4></td>
				<td> <h4>Ksh. <?php echo number_format($total,2)?></h4></td>
			</tr>

			<tr>
				<td>Tax (16%)</td>
				<td>Ksh. <?php echo number_format($vat_charged,2)?></td>
			</tr>

			<tr>
				<td>Items</td>
				<td><?php echo $total_units?></td>
			</tr>

		</table>
</div>


 <?php //echo form_close();?>