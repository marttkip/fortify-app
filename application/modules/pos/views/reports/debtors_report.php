<!-- search -->
<?php echo $this->load->view('search/debtors', '', TRUE);?>
<!-- end search -->
<?php //echo $this->load->view('payments_statistics', '', TRUE);?>
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
          <div class="panel-body">
<?php
		$result = '';
		$search = $this->session->userdata('debtor_report_date');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'pos/pos_reports/close_debtors_search" class="btn btn-sm btn-warning">Close Search</a>';
		}
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Order Date</th>
						  <th>Order Number</th>
						  <th>Invoiced Amount</th>
						  <th>Paid Amount</th>
						  <th>Balance</th>
						  <th>Sales Rep</th>
						</tr>
					  </thead>
					  <tbody>
			';
			$total_invoiced = 0;
			$total_paid = 0;
			$total_balance =0;
			foreach ($query->result() as $value)
			{
				$count++;
				
				$order_date = date('jS M Y',strtotime($value->order_date));
				
				$pos_order_id = $value->pos_order_id;
				$customer_id = $value->customer_id;
				$order_date = $value->order_date;
				$pos_order_number = $value->pos_order_number;
				$personnel_fname = $value->personnel_fname;
				$order_invoice_id = $value->order_invoice_id;
				$total_paid_amount = $value->total_paid_amount;
				$total_amount = $value->total_amount;
				$balance = $total_amount - $total_paid_amount;
				$total_invoiced += $total_amount;
				$total_paid += $total_paid_amount;
				$total_balance += $balance;
				
				$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$order_date.'</td>
								<td>'.$pos_order_number.'</td>
								<td>'.number_format($total_amount, 2).'</td>
								<td>'.number_format($total_paid_amount, 2).'</td>
								<td>'.number_format($balance, 2).'</td>
								<td>'.$personnel_fname.'</td>
							</tr> 
					';
			}
			$result .= 
						'
							<tr>
								<th></th>
								<th></th>
								<th>Total Amount</th>
								<th>'.number_format($total_invoiced, 2).'</th>
								<th>'.number_format($total_paid, 2).'</th>
								<th>'.number_format($total_balance, 2).'</th>
								<th></th>
							</tr> 
					';
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no payments";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>