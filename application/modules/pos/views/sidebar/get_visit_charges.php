<?php

$visit_invoice_number ='';
$preauth_date = date('Y-m-d');
$preauth_amount = '';
if(!empty($visit_invoice_id))
{
	$visit_invoice_detail = $this->accounts_model->get_visit_invoice_details($visit_invoice_id);

	if($visit_invoice_detail->num_rows() > 0)
	{
		foreach ($visit_invoice_detail->result() as $key => $value) {
			# code...
			$visit_invoice_number = $value->visit_invoice_number;
			$preauth_date = $value->preauth_date;
			$preauth_amount = $value->preauth_amount;

		}
	}
}


// get all the procedures

$visit__rs1 = $this->pos_model->get_visit_charges_charged($visit_id,$visit_invoice_id);
// var_dump($visit__rs1->result())	;die();
echo form_open("finance/creditors/confirm_invoice_note/".$visit_id."/".$patient_id, array("class" => "form-horizontal","id" => "confirm-invoice"));
$result = "

	<table align='center' class='table table-striped table-bordered table-condensed'>
	<tr>
		<th></th>
		<th>SKU</th>
		<th>Invoice Item</th>
		<th>Units</th>
		<th>Unit Cost</th>
		<th>Total</th>
		<th></th>
		<th></th>

	</tr>
";
	$total= 0;  
	$number = 0;
	$vat_charged =0;
	if($visit__rs1->num_rows() > 0)
	{						
		foreach ($visit__rs1->result() as $key1 => $value) :
			$v_procedure_id = $value->visit_charge_id;
			$procedure_id = $value->service_charge_id;
			$product_code = $value->product_code;
			$visit_charge_amount = $value->visit_charge_amount;
			$units = $value->visit_charge_units;
			$procedure_name = $value->service_charge_name;
			$service_id = $value->service_id;
			$visit_invoice_id = $value->visit_invoice_id;
			$discount = $value->discount_added;
			$vat_added = $value->vat_added;


			if($vat_added == 1)
			{
				$vat = 1.14;
				$checked_box = 'checked';
				$no_checked_box = '';
			}
			else
			{
				$vat = 1;
				$checked_box = '';
				$no_checked_box = 'checked';
			}

			// var_dump($discount);die();
			// $order_invoice_id = $value->order_invoice_id;
			// $visit_type_id = 1;
			$total= $total +(($units * $pos_order_item_amount) *$vat) - $discount;

			if($visit_invoice_id > 0)
			{
				$text_color = "success";
			}
			else
			{
				$text_color = 'default';
			}

			$vat_charged += (($units * $pos_order_item_amount)*$vat) * 0.14;

			// if($visit_type_id == 1)
			// {
			// 	$visit = 'SELF';
			// }
			// else
			// {
			// 	$visit = 'INSURANCE';
			// }
		
			$checked="";
			$number++;
			$personnel_check = TRUE;
			if($personnel_check )
			{
				$checked = '
							<td>
								<a class="btn btn-sm btn-danger" href="#" onclick="calculatetotal('.$visit_charge_amount.','.$v_procedure_id.', '.$procedure_id.','.$visit_id.')"><i class="fa fa-trash"></i></a>
							</td>
							<td>
								<a class="btn btn-sm btn-danger" href="#" onclick="delete_procedure('.$v_procedure_id.', '.$visit_id.')"><i class="fa fa-trash"></i></a>
							</td>';
			}

			// if($close_card == 3)
			// {

			// }
		 $checkbox_data = array(
			                    'name'        => 'visit_invoice_items[]',
			                    'id'          => 'checkbox'.$v_procedure_id,
			                    'class'          => 'css-checkbox  lrg ',
			                    'checked'=>'checked',
			                    'value'       => $v_procedure_id
			                  );
			
		$result .='
					<tr> 

						<td class="'.$text_color.'">'.form_checkbox($checkbox_data).'<label for="checkbox'.$v_procedure_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
						<td class="'.$text_color.'" >'.$product_code.'</td>
						<td  class="'.$text_color.'" align="left">'.strtoupper(strtolower($procedure_name)).'</td>
						<td  class="'.$text_color.'" align="center">
							<input type="text" id="units'.$v_procedure_id.'" class="form-control" value="'.$units.'" size="3" />

							<input type="hidden" id="visit_invoice_id'.$v_procedure_id.'" class="form-control" value="'.$visit_invoice_id.'" size="3" />
							<input type="text" id="patient_id'.$v_procedure_id.'" class="form-control" value="'.$patient_id.'" size="3" />
						</td>
						<td  class="'.$text_color.'" align="center"><input type="text" class="form-control" size="5" value="'.$visit_charge_amount.'" id="billed_amount'.$v_procedure_id.'"></div></td>

						<td  class="'.$text_color.'" align="center">'.number_format((($units*$pos_order_item_amount)*$vat)-$discount,2).'</td>
						'.$checked.'
					</tr>	
			';
								
			endforeach;

	}
	else
	{
		
	}
	$result .="
		<tr bgcolor='#D9EDF7'>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<th>Grand Total: </th>
		<th colspan='3'><div id='grand_total'>".number_format($total,2)."</div></th>
		<td></td>
		<td></td>
		</tr>
		 </table>
		";
	$result .= "</table>";
	$preauth_amount = $total;
	echo $result;
?>

<div class="row" style="margin-top: 10px;">
	<div class="col-md-12">
		 <input type="hidden" class="form-control" name="charge_visit_id" id="charge_visit_id" placeholder="Middle Name" value="<?php echo $visit_id;?>">
		 <input type="hidden" class="form-control" name="charge_patient_id" id="charge_patient_id" placeholder="Patient Id" value="<?php echo $patient_id;?>">
		 <input type="hidden" class="form-control" name="amount" id="amount" placeholder="Amount" value="<?php echo $total;?>">

		<div class="col-md-4">
		
		</div>
		<div class="col-md-8">
			<?php
			if($visit_type_id != 1)
			{


			?>
				<div class="form-group">
	                <label class="col-md-4 control-label">Preauth Date: </label>
	                
	                <div class="col-md-8">
	                    <div class="input-group">
	                        <span class="input-group-addon">
	                            <i class="fa fa-calendar"></i>
	                        </span>
	                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control datepicker" name="preauth_date" placeholder="Preauth Date" value="<?php echo $preauth_date;?>">
	                    </div>
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-md-4 control-label">Preauth Amount: </label>
	                
	                <div class="col-md-8">
	                    <input type="text" class="form-control" name="preauth_amount" placeholder="Preauth Amount" value="<?php echo number_format($preauth_amount,2)?>">
	                </div>

	            </div>
	        <?php
	    	}
	        ?>

            <div class="form-group">
                <label class="col-md-4 control-label">Invoice number: </label>
                
                <div class="col-md-8">
                    <input type="text" class="form-control" name="visit_invoice_number" placeholder="Invoice number" value="<?php echo $visit_invoice_number?>" readonly="readonly">
                </div>
                
            </div>

            <div class="form-group">
                <div class="center-align" style="margin-top:10px;">
					<button type="submit" class="btn btn-sm btn-success" onclick="return confirm('Are you sure you want to confirm this invoice ? ')"> COMPLETE INVOICE</button>
				</div>
	                
            </div>
			
		</div>
		
	</div>

</div>
 <?php echo form_close();?>