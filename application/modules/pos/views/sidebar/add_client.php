<div>
<section class="panel">
    <header class="panel-heading">
            <h5 class="pull-left"><i class="icon-reorder"></i><?php echo $title?></h5>
          <div class="widget-icons pull-right">
              
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <div class="padd">
		<?php echo form_open("finance/creditors/add_client", array("class" => "form-horizontal","id" => "add-customer"));?>

		<div id="new-customer-div" >
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
		                    <label class="col-md-4 control-label">Customer Name *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="customer_name" placeholder="Customer name" value="<?php echo set_value('customer_name');?>" required="required" autocomplete="off">
		                    </div>
		                </div>

		                <div class="form-group">
		                    <label class="col-md-4 control-label"> PIN Number *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="pin_number" placeholder="PIN number" value="<?php echo set_value('pin_number');?>" required="required" autocomplete="off">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label"> VAT Number *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="vat_number" placeholder="VAT Number" value="<?php echo set_value('vat_number');?>"  autocomplete="off">
		                    </div>
		                </div>

		                <div class="form-group">
		                    <label class="col-md-4 control-label"> Address : </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="address" placeholder="Address" value="<?php echo set_value('address');?>"  autocomplete="off">
		                    </div>
		                </div> 
		                
					</div>
					<div class="col-md-6">
						<div class="form-group">
		                    <label class="col-md-4 control-label"> P.O Box: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="postal_address" placeholder="Postal Address" value="<?php echo set_value('postal_address');?>"  autocomplete="off">
		                    </div>
		                </div>
						<div class="form-group">
		                    <label class="col-md-4 control-label">Customer Email *: </label>
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="customer_email" placeholder="Customer Email" value="<?php echo set_value('customer_email');?>" required="required" autocomplete="off">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label"> Telephone Number *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="customer_phone" placeholder="Customer Phone" value="<?php echo set_value('customer_phone');?>" required="required" autocomplete="off">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-md-4 control-label"> Contact Person *: </label>
		                    
		                    <div class="col-md-8">
		                        <input type="text" class="form-control" name="contact_person" placeholder="Customer name" value="<?php echo set_value('contact_person');?>"  autocomplete="off">
		                    </div>
		                </div>

						
					</div>
				</div>
				<div class="col-md-12" style="margin-top:30px;">
					<div class="form-group">
		                <div class="col-md-12 center-align">
		                
		                    <button class='btn btn-success btn-sm' type='submit' onclick="confirm('Are you sure you want to add the customer ? ')"  > Add Client</button>
		                </div>
		            </div>
					
				</div>
			</div>
		</div>


		<?php echo form_close();?>
        </div>
    </div>
</section>
</div>

<div class="col-md-12" style="margin-top: 10px;">					        	
	<div class="center-align">
		<a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
	</div>  
</div>