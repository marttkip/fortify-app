<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<a  class="btn btn-sm btn-success" onclick="add_payment()"><i class="fa fa-plus"></i> Add Payment </a>
			 <a class='btn btn-sm btn-success'    onclick="add_invoice()"> <i class="fa fa-plus"></i> New Sale </a>  

		</div>
	</div>
</div>
<div class="row">
	<div class="panel-body">
		<div id="patient-statement"></div>
	</div>
</div>

  <!-- END OF ROW -->
<script type="text/javascript">


   $(function() {
       $("#service_id_item").customselect();
       $("#provider_id_item").customselect();
       $("#parent_service_id").customselect();

   });
   $(document).ready(function(){
   		// display_patient_bill(<?php echo $visit_id;?>);
   		// display_procedure(<?php echo $visit_id;?>);
   		get_patient_statement();
   		// get_patient_incomplete_invoices(<?php echo $patient_id;?>);
   });



  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");

        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }

  }
  function check_payment_type(payment_type_id){


    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check

      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }

  }

   function display_patient_bill(visit_id){

      var XMLHttpRequestObject = false;

      if (window.XMLHttpRequest) {

          XMLHttpRequestObject = new XMLHttpRequest();
      }

      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }

      var config_url = document.getElementById("config_url").value;
      var close_page = document.getElementById("close_page").value;
      var url = config_url+"pos/view_patient_bill/"+visit_id+"/1";
      // alert(url);
      if(XMLHttpRequestObject) {

          XMLHttpRequestObject.open("GET", url);

          XMLHttpRequestObject.onreadystatechange = function(){

              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }

          XMLHttpRequestObject.send(null);
      }


  }

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){

	    var units = document.getElementById('units'+id).value;
	    var billed_amount = document.getElementById('billed_amount'+id).value;

	    grand_total(id, units, billed_amount, v_id);

	}

  function grand_total(procedure_id, units, amount, v_id)
  {

		 var config_url = document.getElementById("config_url").value;
		 var notes = document.getElementById('notes'+procedure_id).value;
	     var data_url = config_url+"pos/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;

	      // var tooth = document.getElementById('tooth'+procedure_id).value;
	     // alert(data_url);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{procedure_id: procedure_id,notes: notes},
	    dataType: 'text',
	    success:function(data){
	     // get_medication(visit_id);
	         display_patient_bill(v_id);
	         alert('You have successfully updated your billing');
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        display_billing(v_id);
	    alert(error);
	    }

	    });




	   //  var XMLHttpRequestObject = false;

	   //  if (window.XMLHttpRequest) {

	   //      XMLHttpRequestObject = new XMLHttpRequest();
	   //  }

	   //  else if (window.ActiveXObject) {
	   //      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	   //  }
	   //  var config_url = document.getElementById("config_url").value;

	   //  var url = config_url+"pos/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	   //  // alert(url);
	   //  if(XMLHttpRequestObject) {

	   //      XMLHttpRequestObject.open("GET", url);

	   //      XMLHttpRequestObject.onreadystatechange = function(){

	   //          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
				// {
	   //  			// display_patient_bill(v_id);
	   //  			display_billing(v_id);
	   //          }
	   //      }

	   //      XMLHttpRequestObject.send(null);
	   //  }
	}


	function delete_service(id, visit_id){

		var res = confirm('Do you want to remove this charge ? ');

		if(res)
		{
			var XMLHttpRequestObject = false;

		    if (window.XMLHttpRequest) {

		        XMLHttpRequestObject = new XMLHttpRequest();
		    }

		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"pos/delete_service_billed/"+id;

		    if(XMLHttpRequestObject) {

		        XMLHttpRequestObject.open("GET", url);

		        XMLHttpRequestObject.onreadystatechange = function(){

		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		                // display_patient_bill(visit_id);
		                display_procedure(visit_id);
		            }
		        }

		        XMLHttpRequestObject.send(null);
		    }
		}

	}
	function save_service_items(visit_id)
	{
		var provider_id = $('#provider_id'+visit_id).val();
		var service_id = $('#service_id'+visit_id).val();
		var visit_date = $('#visit_date_date'+visit_id).val();
		var url = "<?php echo base_url();?>pos/add_patient_bill/"+visit_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'text',
		success:function(data){
			alert("You have successfully billed");
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}



	function parse_procedures(visit_id,suck)
    {
      var procedure_id = document.getElementById("service_id_item").value;
       procedures(procedure_id, visit_id, suck);

    }

	function procedures(id, v_id, suck){

        var XMLHttpRequestObject = false;

        if (window.XMLHttpRequest) {

            XMLHttpRequestObject = new XMLHttpRequest();
        }

        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var url = "<?php echo site_url();?>pos/accounts_update_bill/"+id+"/"+v_id+"/"+suck;

         if(XMLHttpRequestObject) {

            XMLHttpRequestObject.open("GET", url);

            XMLHttpRequestObject.onreadystatechange = function(){

                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    // document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
                    display_patient_bill(v_id);
                }
            }

            XMLHttpRequestObject.send(null);
        }

    }
    function display_procedure(visit_id){

	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var config_url = document.getElementById("config_url").value;
	    // var url = config_url+"nurse/view_procedure/"+visit_id;
	    var url = config_url+"pos/view_procedure/"+visit_id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
	}
	function delete_procedure(id, visit_id){
	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/delete_procedure/"+id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {


	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
      display_patient_bill(visit_id);
	}

	$(document).on("change","select#visit_type_id",function(e)
	{
		var visit_type_id = $(this).val();

		if(visit_type_id != '1')
		{
			$('#insured_company').css('display', 'block');
			// $('#consultation').css('display', 'block');
		}
		else
		{
			$('#insured_company').css('display', 'none');
			// $('#consultation').css('display', 'block');
		}




	});

	function change_payer(visit_charge_id, service_charge_id, v_id)
	{

		var res = confirm('Do you want to change who is being billed ? ');

		if(res)
		{

			var config_url = document.getElementById("config_url").value;
		    var data_url = config_url+"pos/change_payer/"+visit_charge_id+"/"+service_charge_id+"/"+v_id;
		   
		      // var tooth = document.getElementById('tooth'+procedure_id).value;
		     // alert(data_url);
		    $.ajax({
		    type:'POST',
		    url: data_url,
		    data:{visit_charge_id: visit_charge_id},
		    dataType: 'text',
		    success:function(data){
		     // get_medication(visit_id);
		         display_patient_bill(v_id);
		     alert('You have successfully updated your billing');
		    //obj.innerHTML = XMLHttpRequestObject.responseText;
		    },
		    error: function(xhr, status, error) {
		    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        display_patient_bill(v_id);
		    	alert(error);
		    }

		    });

		}

	}


	function get_patient_statement()
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"pos/get_patient_statement";
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#patient-statement").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}

	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

	function add_invoice()
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_invoices";
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);
			$("#visit-invoice-div").html(data);
			get_visit_procedures_done();
			// get_visit_charges(visit_id,patient_id);
			// tinymce.init({
			//                 selector: ".cleditor",
			//                	height: "200"
			// 	            });
			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}


	function get_visit_invoices(patient_id)
	{
		var myTarget = document.getElementById("visit_id").value;
		var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_invoices/"+visit_id+"/"+patient_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			document.getElementById("visit-invoice-div").style.display = "block"; 
			$("#visit-invoice-div").html(data);
			get_visit_charges(visit_id,patient_id);
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function get_procedures_done()
	{
		var config_url = $('#config_url').val();
		// var visit_type_id = document.getElementById("visit_type_id").value;
		var data_url = config_url+"pos/search_procedures";
		// window.alert(data_url);
		$('#charges-div').css('display', 'block');
		var lab_test = $('#search_procedures').val();
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : lab_test},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#searched-procedures").html(data);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function get_visit_charges(visit_id,patient_id,visit_invoice_id = null)
	{
		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_charges/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#visit-charges").html(data);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	function add_payment(patient_id)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/add_payment/"+patient_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			get_visit_payments(patient_id,null);
			// alert(data);

			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
		
	}


	function get_visit_payments(patient_id,payment_id=null)
	{
		// var myTarget = document.getElementById("visit_id").value;
		// var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_payments/"+patient_id+"/"+payment_id;
		window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			// document.getElementById("visit-payments-div").style.display = "block"; 
			$("#visit-payments-div").html(data);
			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
		
			
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function add_payment_item(patient_id,payment_id= null)
	{
		var config_url = $('#config_url').val();

		 var amount_paid = document.getElementById("amount_paid").value;
		 var invoice_id = document.getElementById("invoice_id").value;

		 // window.alert(amount_paid);
		var data_url = config_url+"pos/add_payment_item/"+patient_id+"/"+payment_id;
		
		// window.alert(invoice_id);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{invoice_detail: invoice_id,amount: amount_paid},
		dataType: 'text',
		success:function(data){
		
			get_visit_payments(patient_id,payment_id);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	$(document).on("submit","form#confirm-payment",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		// var visit_id = $('#charge_visit_id').val();
		var patient_id = $('#payment_patient_id').val();
		var payment_id = $('#payment_payment_id').val();
		var config_url = $('#config_url').val();	

		var url = config_url+"pos/confirm_payment/"+patient_id+"/"+payment_id;
		// alert(url);
		
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function get_patient_incomplete_invoices(patient_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"pos/get_incomplete_invoices/"+patient_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#incomplete-invoices").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}



$(document).on("submit","form#confirm-invoice",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	alert(form_data);
	// var visit_id = $('#charge_visit_id').val();
	// var patient_id = $('#charge_patient_id').val();
	var config_url = $('#config_url').val();	

	var url = config_url+"pos/confirm_visit_charge";
	 
	 
   $.ajax({
   type:'POST',
   url: url,
   data:form_data,
   dataType: 'text',
   processData: false,
   contentType: false,
   success:function(data){
      var data = jQuery.parseJSON(data);
    
      	if(data.message == "success")
		{
			
			
			close_side_bar();
			// get_patient_incomplete_invoices(patient_id);
			get_patient_statement();
			
		}
		else
		{
			alert(data.result);
		}
   
   },
   error: function(xhr, status, error) {
   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
   
   }
   });
	 
	
   
	
});

function invoice_details_view(visit_invoice_id,visit_id,patient_id)
{

	document.getElementById("sidebar-right").style.display = "block"; 
	document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_visit_invoices/"+visit_id+"/"+patient_id+"/"+visit_invoice_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);
		get_visit_charges(visit_id,patient_id,visit_invoice_id);
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}

function add_service_charge_test(service_charge_id,visit_invoice_id =null)
{
	var res = confirm('Are you sure you want to charge ?');

	if(res)
	{	
		var visit_id=null;
		var patient_id= null;
		var visit_type_id=1;

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/add_visit_charge/"+service_charge_id+"/"+visit_id+"/"+patient_id+"/"+visit_type_id+"/"+visit_invoice_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{service_charge_id: service_charge_id,visit_type_id: visit_type_id,visit_invoice: visit_invoice_id},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById('search_procedures').value = '';
			$('#charges-div').css('display', 'none');

			
				get_visit_procedures_done();
			
			
			
			// $('#bottom-div').css('display', 'block');
			// tinymce.init({
   //              selector: ".cleditor",
   //             	height: "150"
	  //           });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	


}
	
function get_visit_procedures_done()
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/visit_procedures_view";
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
		var data = jQuery.parseJSON(data);
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#visit-charges").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}



</script>