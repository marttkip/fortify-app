<div class="row">
	<div class="col-md-2">
		<div class="row" style="margin-bottom: 10px;">
			<div class="panel-body" style="height: 78vh;">

				<?php echo $this->load->view("search/customer_search", '', TRUE);?>
			</div>
		</div>
		
	</div>
	<div class="col-md-7" >
		<div class="col-md-12">
			<h4 class="center-align"><?php echo $title?></h4>
		</div>
		<div class="col-md-12">
			<div class="panel-body" style="height: 75vh;">
				<div id="pending-orders"></div>
			</div>
			
		</div>
		
	</div>
	<div class="col-md-3">
		<div class="row">
			<div class="panel-body" style="height: 80vh;margin:0 auto;">
				<div id="sale-detail"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
	function get_order_detail(pos_order_id,order_invoice_id)
	{

		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/pos_reports/view_sold_items/"+pos_order_id+"/"+order_invoice_id;
		
		
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
			$("#sale-detail").html(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});


	}
	function get_customer_details(customer_id)
	{
		// get all the orders by this client


		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/get_customer_orders/"+customer_id;
		
		
		// window.alert(data_url);
		$.ajax({
				type:'POST',
				url: data_url,
				data:{query : null},
				dataType: 'text',
				success:function(data){
					$("#pending-orders").html(data);
				},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});


	}
	function pass_topay(order_invoice_id,customer_id,amount_paid)
	{
		var config_url = $('#config_url').val();

		var data_url = config_url+"pos/add_order_payment_item/"+order_invoice_id+"/"+customer_id+"/"+amount_paid;
		
		
		// window.alert(data_url);
		$.ajax({
				type:'POST',
				url: data_url,
				data:{query : null},
				dataType: 'text',
				success:function(data){
					// $("#pending-orders").html(data);

					// document.getElementById("amount_paid"+order_invoice_id).value = amount_paid; 

					get_customer_details(customer_id);
				},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function check_payment_type(payment_type_id){
   
	    var myTarget1 = document.getElementById("cheque_div");

	    var myTarget2 = document.getElementById("mpesa_div");

	    var myTarget4 = document.getElementById("debit_card_div");

	    var myTarget5 = document.getElementById("bank_deposit_div");

	    var myTarget3 = document.getElementById("insuarance_div");

	    if(payment_type_id == 1)
	    {
	      // this is a check     
	      myTarget1.style.display = 'block';
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'none';
	      myTarget4.style.display = 'none';
	      myTarget5.style.display = 'none';
	    }
	    else if(payment_type_id == 2)
	    {
	      myTarget1.style.display = 'none';
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'none';
	      myTarget4.style.display = 'none';
	      myTarget5.style.display = 'none';
	    }
	    else if(payment_type_id == 7)
	    {
	      myTarget1.style.display = 'none';
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'none';
	      myTarget4.style.display = 'none';
	      myTarget5.style.display = 'block';
	    }
	    else if(payment_type_id == 8)
	    {
	      myTarget1.style.display = 'none';
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'none';
	      myTarget4.style.display = 'block';
	      myTarget5.style.display = 'none';
	    }
	    else if(payment_type_id == 5)
	    {
	      myTarget1.style.display = 'none';
	      myTarget2.style.display = 'block';
	      myTarget3.style.display = 'none';
	      myTarget4.style.display = 'none';
	      myTarget5.style.display = 'none';
	    }
	    else if(payment_type_id == 6)
	    {
	       myTarget1.style.display = 'none';
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'block';
	      myTarget4.style.display = 'none';
	      myTarget5.style.display = 'none';  
	    }

	}

	$(document).on("submit","form#confirm-creditor-payment",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		var customer_id = $('#customer_id').val();
		var config_url = $('#config_url').val();	

		var url = config_url+"pos/confirm_credit_sale_payment/"+customer_id;
		 
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				get_customer_details(customer_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});


</script>