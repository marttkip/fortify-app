<!-- search -->
<?php echo $this->load->view('search/customers', '', TRUE);?>

<!-- end search -->
<div class="row">
	<section class="panel">
	    <header class="panel-heading">
	        <h2 class="panel-title"><?php echo $title;?> </h2>
	        <a href="<?php echo site_url();?>add-customer" class="btn btn-sm btn-primary pull-right" style="margin-top: -25px;"><i class="fa fa-plus"></i> Add customer</a>
	                	
	    </header>

	    <!-- Widget content -->
	    <div class="panel-body">
	    	<div class="padd">
	          <?php
	            	$error = $this->session->userdata('error_message');
					$success = $this->session->userdata('success_message');
					
					if(!empty($error))
					{
						echo '<div class="alert alert-danger">'.$error.'</div>';
						$this->session->unset_userdata('error_message');
					}
					
					if(!empty($success))
					{
						echo '<div class="alert alert-success">'.$success.'</div>';
						$this->session->unset_userdata('success_message');
					}
				?>
	           
	          <div style="min-height:30px;">
	            	<div class="pull-right">
	                	<?php
						$search = $this->session->userdata('search_customers');
			
						if(!empty($search))
						{
							echo '<a href="'.site_url().'close-customer-search" class="btn btn-warning btn-sm">Close Search</a>';
						}
						?>
	                </div>
	            </div>
	            <table class="table table-hover table-bordered ">
					<thead>
						<tr>
						  <th>#</th>
						  <th>Customer</th>
						  <th>Coming Due</th>
						  <th>30 Days</th>
						  <th>60 Days</th>
						  <th> 90 Days</th>
						  <th>Over 90 Days</th>
						  <th>Balance</th>
						  <th colspan="1">Actions</th>
						</tr>
					  </thead>
					  <tbody>
					  	<?php
					  	$income_rs = $this->pos_model->get_customer_ledger();

						$income_result = '';
						$total_income = 0;
						if($income_rs->num_rows() > 0)
						{
							$x=0;
							foreach ($income_rs->result() as $key => $value) {
								# code...
								// $total_amount = $value->total_amount;
								$payables = $value->receivables;
								$thirty_days = $value->thirty_days;
								$sixty_days = $value->sixty_days;
								$ninety_days = $value->ninety_days;
								$over_ninety_days = $value->over_ninety_days;
								$coming_due = $value->coming_due;
								$creditor_id = $value->recepientId;
								$Total = $value->Total;
								$x++;
								$income_result .='<tr>
													<td>'.$x.'</td>
													<td >'.strtoupper($payables).'</td>
													<td >'.number_format($coming_due,2).'</td>
													<td >'.number_format($thirty_days,2).'</td>
													<td >'.number_format($sixty_days,2).'</td>
													<td >'.number_format($ninety_days,2).'</td>
													<td >'.number_format($over_ninety_days,2).'</td>
													<td >'.number_format($Total,2).'</td>
													<td ><a href="'.site_url().'customer-statement/'.$creditor_id.'" class="btn btn-warning btn-xs"> Open Account</a>
														<a href="'.site_url().'edit-customer/'.$creditor_id.'" class="btn btn-success btn-xs"> Edit customer</a>
														<a href="'.site_url().'delete-customer/'.$creditor_id.'" onclick="return confirm(\'Are you sure you want to delete the customer ? \')" class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i></a>

													</td>
													</tr>';
							}

						}
						echo $income_result;
					  	?>
					  </tbody>
				</table>
			
	          </div>
	          
	          <div class="widget-foot">
	                                
				
	            
	                <div class="clearfix"></div> 
	            
	            </div>
	        </div>
	        <!-- Widget ends -->

	      </div>
	</section>
</div>