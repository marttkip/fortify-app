<?php

		

	$query = $this->pos_model->customer_pending_bills($customer_id);

	$incomplete_result ='<table class="table table-condensed table-bordered table-hover">
							<thead>
								<th></th>
								<th>#</th>
								<th>Date</th>
								<th>Order No.</th>
								<th>Total Items</th>
								<th>Total Amount</th>
								<th>Amount Paid</th>
								<th></th>

							</thead>
							<tbody>';
	$payable_amount = 0;
	 echo form_open("finance/creditors/confirm_invoice_note", array("class" => "form-horizontal","id" => "confirm-creditor-payment"));
	if($query->num_rows() > 0)
	{
		$x=0;
		foreach ($query->result() as $key => $value) {
			# code...
			$pos_order_id = $value->pos_order_id;
			$customer_id = $value->customer_id;
			$order_date = $value->order_date;
			$pos_order_number = $value->pos_order_number;
			$personnel_fname = $value->personnel_fname;
			$total_amount = $value->total_amount;
			$order_invoice_id = $value->order_invoice_id;
			$count = $value->total_items;
			$payment_item_amount = $value->payment_item_amount;
			if(empty($payment_item_amount))
			{
				$payment_item_amount = 0;
			}

			$amount_paid = $this->pos_model->get_bill_balance($order_invoice_id);

			$balance = $total_amount - $amount_paid;

			if(!empty($payment_item_amount) AND $balance > 0)
			{
				$checkbox_data = array(
				                    'name'        => 'customer_payments_items[]',
				                    'id'          => 'checkbox'.$order_invoice_id,
				                    'class'          => 'css-checkbox  lrg ',
				                    'checked'=>'checked',
				                    'onclick'=>'pass_topay('.$order_invoice_id.','.$customer_id.','.$balance.')',
				                    'value'       => $order_invoice_id
				                  );
				$checked_status = TRUE;
				$payable_amount += $payment_item_amount;
				$link = '<td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$order_invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus" ></label>'.'</td>';
			}
			else if(empty($payment_item_amount) AND $balance > 0)
			{
				$checkbox_data = array(
				                    'name'        => 'customer_payments_items[]',
				                    'id'          => 'checkbox'.$order_invoice_id,
				                    'class'          => 'css-checkbox  lrg ',
				                    'checked'=>'',
				                    'onclick'=>'pass_topay('.$order_invoice_id.','.$customer_id.','.$balance.')',
				                    'value'       => $order_invoice_id
				                  );
				$checked_status = FALSE;
				$payable_amount += 0;
				$link = '<td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$order_invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus" ></label>'.'</td>';
			}
			else
			{
				$checkbox_data = '';
				$checked_status = FALSE;
				$payable_amount += 0;
				$link = '<td>-</td>';
			}

			if($balance == 0)
			{
				$closed = 'readonly';
			}
			else
			{
				$closed = '';
			}
			
			
			$x++;
			$incomplete_result .='
								<tr >
									'.$link.'
									<td>'.$x.'</td>
									<td>'.date('jS M Y',strtotime($order_date)).'</td>
									<td><a onclick="get_order_detail('.$pos_order_id.','.$order_invoice_id.')">'.$pos_order_number.'</a></td>
									<td>'.$count.'</td>
									<td>'.number_format($total_amount,2).'</td>
									<td>'.number_format($amount_paid,2).'</td>
									<td><input name="amount_paid'.$order_invoice_id.'" id="amount_paid'.$order_invoice_id.'" class="form-control" size="4" value="'.$payment_item_amount.'" '.$closed.'/></td>
									
								</tr>';

		}
	}
	$incomplete_result .='</tbody>
					</table>';

	echo $incomplete_result;
	
?>
<?php
if($checked_status)
{

?>
	<div class="row">
    	<div class='col-md-6 pull-right'>
          	<div class="form-group center-align">
					<h4 class="center-align">Ksh. <?php echo number_format($payable_amount,2)?></h4>
			</div>
			 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
          	<div class="form-group" style="margin-top:10px;margin-left:20px;">
				<label class="col-lg-12 left-align">
				<strong> Tendered Amount:</strong> </label>
				<div class="col-lg-10">
					<input type="text" class="form-control" name="tendered_amount" id="tendered_amount" value="<?php echo $payable_amount;?>" autocomplete="off" readonly >
				</div>
			</div>
			<input type="hidden" class="form-control" name="customer_id" id="customer_id" value="<?php echo $customer_id?>">	
			<input type="hidden" class="form-control" name="change" id="change" autocomplete="off">		
			<div class="form-group" style="margin-top:10px;margin-left:20px;">
				<label class="col-lg-12 left-align">
				<strong> Payment Method:</strong> </label>
				<div class="col-lg-10">
					<select class="form-control" name="payment_method" id="payment_method" onchange="check_payment_type(this.value)">
						<option value="0">Select a group</option>
                    	<?php
						  $method_rs = $this->pos_model->get_payment_methods();
						  $num_rows = count($method_rs);
						 if($num_rows > 0)
						  {
							
							foreach($method_rs as $res)
							{
							  $payment_method_id = $res->payment_method_id;
							  $payment_method = $res->payment_method;
							  
								echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
							  
							}
						  }
					  ?>
					</select>
				</div>
			</div>

			<div id="mpesa_div" class="form-group" style="display:none;margin-top:10px;margin-left:20px;" >
				<label class="col-lg-12 left-align"><strong> Mpesa TX Code: </strong> </label>

				<div class="col-lg-10">
					<input type="text" class="form-control" name="mpesa_code" id="mpesa_code" placeholder="">
				</div>
			</div>
		  
			<div id="insuarance_div" class="form-group" style="display:none;margin-top:10px;margin-left:20px;" >
				<label class="col-lg-12 left-align"><strong> Credit Card Detail: </strong>  </label>
				<div class="col-lg-10">
					<input type="text" class="form-control" name="insuarance_number" id="insuarance_number" placeholder="">
				</div>
			</div>
		  
			<div id="cheque_div" class="form-group" style="display:none;margin-top:10px;margin-left:20px;" >
				<label class="col-lg-12 left-align"><strong> Cheque Number:  </strong> </label>
			  
				<div class="col-lg-10">
					<input type="text" class="form-control" name="cheque_number" id="cheque_number" placeholder="">
				</div>
			</div>
			<div id="bank_deposit_div" class="form-group" style="display:none;margin-top:10px;margin-left:20px;" >
				<label class="col-lg-12 left-align"> <strong> Deposit Detail: </strong>  </label>
			  
				<div class="col-lg-10">
					<input type="text" class="form-control" name="deposit_detail" id="deposit_detail" placeholder="">
				</div>
			</div>
			<div class="form-group center-align">
				<button type="submit" class='btn btn-info btn-sm' type='submit' onclick="return confirm('Are you sure you want to submit the payment ?')" >Submit Payment</button>
			</div>
          	
        </div>
    </div>
<?php
}
?>
 <?php echo form_close();?>