<?php
$creditor_result = $this->pos_model->get_customer_invoice_balances($customer_id);

// var_dump($creditor_result);die();
?>

<div class="row">
	<div class="col-md-12">
		

				<input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer_id?>">
				<input type="hidden" name="receiving_account" id="receiving_account" value="<?php echo $receiving_account?>">
				<input type="hidden" name="paid_amount" id="paid_amount" value="<?php echo $paid_amount?>">
			

			<table class="table table-hover table-bordered ">
			 	<thead>
					<tr>
					  <th>#</th>
					  <th></th>	
					  <th>Transaction Date</th>						  
					  <th>Document number</th>
					  <th>Invoice Amount</th>
					  <th>Amount Payable</th>	
			          <th>Balance</th>   					
					</tr>
				 </thead>
			  	<tbody>
			  		<?php 
			  		$result ='';
			  		$total_arrears = 0;
			  		$total_received_balance = $paid_amount;

			  		$total_invoice = 0;
					$total_payment = 0;
					$total_balance = 0;
					// if($invoice_balance <)
			  		if($creditor_result->num_rows() > 0)
			  		{
			  			$x=0;

			  			foreach ($creditor_result->result() as $key => $value) {
			  				# code...
			  				$transactionDate = $value->transactionDate;
			  				$referenceCode = $value->referenceCode;
			  				$transactionCategory = $value->transactionCategory;
			  				$dr_amount = $value->dr_amount;
			  				$cr_amount = $value->cr_amount;
			  				$referenceId = $value->referenceId;

			  				

			  				$balance = $dr_amount - $cr_amount;

			  				$total_arrears += $balance; 

			  				$total_debits += $dr_amount;
			  				$total_credits += $cr_amount;

			  			

			  				if($total_received_balance >= $dr_amount)
			  				{
			  					$invoice_balance = 0;
			  					$amount_payable = $dr_amount;
			  					$total_received_balance = $total_received_balance-$dr_amount;
			  				}
			  				else if($total_received_balance < $dr_amount)
			  				{
			  					$invoice_balance = $dr_amount - $total_received_balance;
			  					$amount_payable = $total_received_balance;
			  					$total_received_balance -=$amount_payable;
			  				}
			  			
			  				$x++;
			  				$total_invoice += $dr_amount;
			  				$total_payment += $amount_payable;
			  				$total_balance += $invoice_balance;
			  				// if($invoice_balance <)
			  				// if($total_received_balance > 0)
			  				// {
			  				$checkbox_data = array(
								                    'name'        => 'customer_payment_items[]',
								                    'id'          => 'checkbox'.$referenceId,
								                    'class'          => 'css-checkbox  lrg ',
								                    'checked'=>'checked',
								                    'value'       => $referenceId
								                  );

			  				$result .= '<tr>
			  								<td>'.$x.'</td>
			  								<td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$referenceId.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
			  								<td>'.$transactionDate.'</td>
			  								<td>'.strtoupper(strtolower($referenceCode)).'</td>
			  								<td>'.number_format($dr_amount,2).'</td>
			  								<td><input type="text" class="form-control" name="amount_payable'.$referenceId.'" id="amount_payable'.$referenceId.'" value="'.$amount_payable.'" readonly/></td>
			  								<td>'.number_format($invoice_balance,2).'</td>
			  						   </tr>';
			  				// }
			  				// $total_received_balance -= $dr_amount;
			  			}
			  			$result .= '<tr>
			  								<th></th>
			  								<th></th>
			  								
			  								<th>Totals</th>
			  								<th>'.number_format($total_invoice,2).'</th>
			  								<th>'.number_format($total_payment,2).'</th>
			  								<th>'.number_format($total_balance,2).'</th>
			  						   </tr>';
			  		}
			  		echo $result;
			  		?>
				</tbody>
			</table>
	
	</div>
	<div class="col-md-12">
		<div class="col-md-6 pull-right">
			<div class="row" style="margin-bottom: 20px;">
				<div class="col-md-12">
    				
    				<div class="form-group">
			            <label class="col-md-4 control-label">Receiving Account: </label>
			            
			            <div class="col-md-8">
			               <select id="account_receiving" name="receiving_account" class="form-control" required="required">	
				                <?php
				                $accounts = $this->petty_cash_model->get_child_accounts("Bank");
				                if($accounts->num_rows() > 0)
				                {
				                	foreach ($accounts->result() as $key => $value) {
				                		# code...
				                		$account_id = $value->account_id;
				                		$account_name = $value->account_name;

				                		echo '<option value="'.$account_id.'">'.$account_name.'</option>';
				                	}
				                }
				                ?>
			                </select> 
			            </div>
			        </div>
    			</div>
			</div>
			<div class="row" style="margin-bottom: 20px;">
				<div class="col-md-12" >
					<div class="form-group">
						<label class="col-md-4 control-label">Cheque No: </label>
						<div class="col-md-8">
	                        <input  type="text"  class="form-control" name="transaction_number" id="transaction_number" placeholder="Cheque No" autocomplete="off" required="required">
	                    </div>
					</div>	
				</div>
			</div>
			<div class="row" style="margin-bottom: 20px;">
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-4 control-label">Date: </label>
						<div class="col-md-8">
	                        <div class="input-group">
	                            <span class="input-group-addon">
	                                <i class="fa fa-calendar"></i>
	                            </span>
	                            <input data-format="yyyy-mm-dd" type="date" data-plugin-datepicker class="form-control" name="transaction_date" id="transaction_date" placeholder="Transaction date" value="<?php echo date('Y-m-d')?>" required>
	                        </div>
	                    </div>
					</div>	 
				</div>
			</div>
			<div class="center-align" style="margin-bottom: 20px;">
                <button type="submit" class="btn btn-info btn-sm" onclick="return confirm('Are you sure you want to complate payment')">Complete Payment</button>
            </div>
        </div>
	</div>
</div>