<div>
	<section class="panel">
	   <header class="panel-heading">
	            <h5 class="pull-left"><i class="icon-reorder"></i>Receipt Customer Payment</h5>
	          <div class="widget-icons pull-right">
	              
	          </div>
	          <div class="clearfix"></div>
	    </header> 
	    <div class="panel-body">
	    	<?php echo form_open("pos/record_customer_account/".$customer_id, array("class" => "form-horizontal","id" => "receipt-customer-payment"));?>
	        <div class="padd" style="height: 10vh !important;">
	        	<input  type="hidden"  class="form-control" name="customer_id" placeholder="customer_id" autocomplete="off" value="1">
		        	<div class="row">
		        		<div class="col-md-12">
		        			
		        			<div class="col-md-10">
			                    <div class="form-group">
			                        <label class="col-md-2 control-label">Amount: </label>
			                        
			                        <div class="col-md-10">
			                            <input  type="number"  class="form-control" name="amount_billed" id="billed_amount" placeholder="Amount of money to reconcile" autocomplete="off">
			                        </div>
			                    </div>
		        			</div>
		        			

		        			<div class="col-md-2">
			        			<div class="center-align">
					                <a  class="btn btn-info btn-sm" onclick="reconcille_payment(<?php echo $customer_id;?>)">Reconcille</a>
					            </div>
					        </div>
		        		</div>
		        	</div>
		         
	       	</div>
	       	<div class="padd" style="height: 50vh !important;">
	       		<div class="row">
	        		<div class="col-md-12">
	        			<div id="invoice-payment-items"></div>
	        		</div>
	        	</div>
	       	</div>
	       	<?php echo form_close();?>
	       	<div class="padd" style="height: 10vh !important;">
	       		<div class="row">
	        		<div class="col-md-12">
	        		</div>
	        	</div>
	       	</div>
	    </div>
	</section>
	<div class="col-md-12">					        	
		<div class="center-align">
			<a  class="btn btn-sm btn-warning" onclick="close_side_bar()"><i class="fa fa-folder-closed"></i> CLOSE SIDEBAR </a>
		</div>  
	</div>
</div>

<script type="text/javascript">
	

    $(document).on("submit","form#update-customer-payments",function(e)
    {
        e.preventDefault();
        
        var form_data = new FormData(this);
        window.alert(customer_id);

        // var config_url = $('#config_url').val();
        // var customer_id = $('#customer_id').val();


        // var data_url = config_url+"pos/update_client_payments/"+customer_id;
        
        //   $.ajax({
        //    type:'POST',
        //    url: data_url,
        //    data:form_data,
        //    dataType: 'text',
        //    processData: false,
        //    contentType: false,
        //     success:function(data){
        //         //window.alert("You have successfully updated the symptoms");
        //         //obj.innerHTML = XMLHttpRequestObject.responseText;
        //         var data = jQuery.parseJSON(data);
        //         if(data.message == "success")
        //         {
        //             // alert(data.result);
        //             $("#invoice-payment-items").html(data.result);
        //         }
        //         else
        //         {
        //             alert('Please ensure you have added included all the items');
        //         }
                
        //     },
        //     error: function(xhr, status, error) {
        //         //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        //         alert(error);
        //     }

        // });

    });
</script>