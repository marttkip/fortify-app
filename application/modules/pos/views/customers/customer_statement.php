<!-- search -->
<?php echo $this->load->view('search/customer_statement_search', '', TRUE);?>
<!-- end search -->

<div class="row">
    <div class="col-md-12">

        <section class="panel">
            <header class="panel-heading">
                
                <h2 class="panel-title"><?php echo $title;?></h2>
                <a href="<?php echo site_url();?>accounts-receivables/customers" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px; "><i class="fa fa-arrow-left"></i> Back to customers</a>
                <a href="<?php echo base_url().'print-customer-statement/'.$customer_id?>" class="btn btn-sm btn-success pull-right"  style="margin-top: -25px;margin-right: 5px;" target="_blank"><i class="fa fa-print"></i> Print</a>
               <!--  <button type="button" class="btn btn-sm btn-primary pull-right"  data-toggle="modal" data-target="#record_creditor_account" style="margin-top: -25px; margin-right: 5px;"><i class="fa fa-plus"></i> Record</button> -->
                <button type="button" class="btn btn-sm btn-success pull-right" onclick="receipt_customer_payment()" style="margin-top: -25px; margin-right: 5px;"><i class="fa fa-plus"></i> Receipt Payment</button>
              

            </header>
            
            <div class="panel-body">
              <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer_id?>">
                <div class="modal fade" id="record_creditor_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Record Transaction</h4>
                            </div>
                            <div class="modal-body">
                                <?php echo form_open("pos/record_customer_account/".$customer_id, array("class" => "form-horizontal"));?>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Transaction date: </label>
                                    
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input data-format="yyyy-MM-dd" type="date" data-plugin-datepicker class="form-control" name="transaction_date" placeholder="Transaction date" value="<?php echo date('Y-m-d')?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Account*</label>
                                    
                                    <div class="col-md-8">
                                         <select  class="form-control custom-select" name="account_from_id" id='account_from_id' required>
                                            <option value="">-- Select account --</option>
                                            <?php
                                            if($accounts->num_rows() > 0)
											{
												foreach($accounts->result() as $res)
												{
													$account_id = $res->account_id;
													$account_name = $res->account_name;
													?>
                                                    <option value="<?php echo $account_id;?>"><?php echo $account_name;?></option>
                                                    <?php
												}
											}
											?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Transaction Code *</label>
                                    
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="transaction_code" placeholder="Code e.g cheque number/MPESA code" autocomplete="off" required="required" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Description *</label>
                                    
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="transaction_description"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Amount *</label>
                                    
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="amount_paid" placeholder="Amount" autocomplete="off" required="required" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-4">
                                        <div class="center-align">
                                            <button type="submit" class="btn btn-primary">Save record</button>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close();?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="modal fade" id="import_creditor_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Import Invoices</h4>
                            </div>
                            <div class="modal-body">
                                <?php echo form_open_multipart('creditors/validate-import/'.$customer_id, array("class" => "form-horizontal", "role" => "form"));?>            
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul>
                                                <li>Download the import template <a href="<?php echo site_url().'creditors/import-template';?>">here.</a></li>
                                                
                                                <li>Save your file as a <strong>csv</strong> file before importing</li>
                                                <li>After adding your patients to the import template please import them using the button below</li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Account*</label>
                                                
                                                <div class="col-md-8">
                                                     <select  class="form-control custom-select" name="billed_account_id" id='billed_supplier_id'>
                                                        <option value="">-- Select account --</option>
                                                        <?php
                                                        if($accounts->num_rows() > 0)
                                                        {
                                                            foreach($accounts->result() as $res)
                                                            {
                                                                $account_id = $res->account_id;
                                                                $account_name = $res->account_name;
                                                                ?>
                                                                <option value="<?php echo $account_id;?>"><?php echo $account_name;?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="fileUpload btn btn-primary">
                                                <span>Import patients</span>
                                                <input type="file" class="upload" onChange="this.form.submit();" name="import_csv" />
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


                
                
			<?php
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}
			
			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}
					
			$search = $this->session->userdata('customer_statement_search');
			$search_title = $this->session->userdata('customer_statement_search_title');
		
			if(!empty($search))
			{
				echo '
				<a href="'.site_url().'pos/close_customer_statement_search/'.$customer_id.'" class="btn btn-warning btn-sm ">Close Search</a>
				';
				echo $search_title;
			}	

				$creditor_result = $this->pos_model->get_customer_statement($customer_id,$start_date);
			?>

				<table class="table table-hover table-bordered ">
				 	<thead>
						<tr>
						  <th>#</th>	
						  <th>Transaction Date</th>	
                          <th>Transaction Type</th>   
						  <th>Transaction Number</th> 
						  <th>Amount</th>  
                          <th></th>  					
						</tr>
					 </thead>
				  	<tbody>
				  		<?php 
				  		$result ='';
				  		$total_arrears = 0;
				  		if($creditor_result->num_rows() > 0)
				  		{
				  			$x=0;

				  			foreach ($creditor_result->result() as $key => $value) {
				  				# code...
				  				$transactionDate = $value->transactionDate;
				  				$referenceCode = $value->referenceCode;
				  				$transactionCategory = $value->transactionCategory;
				  				$dr_amount  = $value->dr_amount;
				  				$cr_amount = $value->cr_amount;
                                $referenceId = $value->referenceId;
                                $payingFor = $value->payingFor;

				  				if($cr_amount > 0)
                                {

                                    $amount_viewed = '('.number_format($cr_amount,2).')';
                                }
                                else
                                {
                                    $amount_viewed = number_format($dr_amount,2);
                                }
                                $button ='';
                                if($transactionCategory == "Order")
                                {
                                    $transactionCategory = 'Customer Invoice';
                                    $button ='<a href="'.base_url().'print-invoice/'.$referenceId.'/'.$payingFor.'" target="_blank" class="btn btn-xs btn-warning">Reprint Invoice</a>';
                                }

				  				$balance = $dr_amount - $cr_amount;

				  				$total_arrears += $balance; 

				  				$total_debits += $dr_amount;
				  				$total_credits += $cr_amount;

				  				$x++;

				  				$result .= '<tr>
				  								<td>'.$x.'</td>
				  								<td>'.$transactionDate.'</td>
                                                <td>'.strtoupper(strtolower($transactionCategory)).'</td>
				  								<td>'.strtoupper(strtolower($referenceCode)).'</td>
				  								<td style="text-align:center">'.$amount_viewed.'</td>
                                                <td style="text-align:center">'.$button.'</td>
				  						   </tr>';
				  			}
				  			$result .= '<tr>
				  								<th></th>
				  								<th></th>
				  								<th></th>
				  								<th>Totals</th>
				  								<th  style="text-align:center">'.number_format($total_debits- $total_credits,2).'</th>
                                                <th></th>
				  						   </tr>';
				  		}
				  		echo $result;
				  		?>
					</tbody>
				</table>
          	</div>
		</section>
    </div>
</div>
<script type="text/javascript">
    $(function() {
       $("#billed_account_id").customselect();
       $("#billed_supplier_id").customselect();
    });

    function receipt_customer_payment()
    {
        document.getElementById("sidebar-right").style.display = "block"; 
        document.getElementById("existing-sidebar-div").style.display = "none"; 

        var config_url = $('#config_url').val();
        var customer_id = $('#customer_id').val();
        var data_url = config_url+"pos/receipt_payment_view/"+customer_id;
        //window.alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{appointment_id: 1},
        dataType: 'text',
        success:function(data){
            //window.alert("You have successfully updated the symptoms");
            //obj.innerHTML = XMLHttpRequestObject.responseText;
            document.getElementById("current-sidebar-div").style.display = "block"; 
            $("#current-sidebar-div").html(data);

            $('.datepicker').datepicker({
                        format: 'yyyy-mm-dd'
                    });



            $('.timepicker').timepicker({
                timeFormat: 'h:mm p',
                interval: 60,
                minTime: '10',
                maxTime: '6:00pm',
                defaultTime: '11',
                startTime: '10:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
            // alert(data);
            },
            error: function(xhr, status, error) {
            //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
            alert(error);
        }

        });
    }

    function close_side_bar()
    {
        // $('html').removeClass('sidebar-right-opened');
        document.getElementById("sidebar-right").style.display = "none"; 
        document.getElementById("current-sidebar-div").style.display = "none"; 
        document.getElementById("existing-sidebar-div").style.display = "none"; 
        tinymce.remove();
    }

    $(document).on("submit","form#receipt-customer-payment",function(e)
    {
        e.preventDefault();
        
        var form_data = new FormData(this);


        var config_url = $('#config_url').val();
        var customer_id = $('#customer_id').val();

        // alert(customer_id);


        var data_url = config_url+"pos/update_client_payments/"+customer_id;
        
          $.ajax({
           type:'POST',
           url: data_url,
           data:form_data,
           dataType: 'text',
           processData: false,
           contentType: false,
            success:function(data){
                //window.alert("You have successfully updated the symptoms");
                //obj.innerHTML = XMLHttpRequestObject.responseText;
                var data = jQuery.parseJSON(data);
                if(data.message == "success")
                {
                    // alert(data.result);
                    // $("#invoice-payment-items").html(data.result);
                    close_side_bar();
                    window.location.href = config_url+"customer-statement/"+customer_id;
                }
                else
                {
                    alert('Please ensure you have added included all the items');
                }
                
            },
            error: function(xhr, status, error) {
                //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                alert(error);
            }

        });

    });

    function reconcille_payment($customer_id)
    {
         var config_url = $('#config_url').val();
        var customer_id = $('#customer_id').val();
        var amount_billed = $('#billed_amount').val();
        var receiving_account = $('#account_receiving').val();
        var transaction_number = $('#transaction_number').val();
        var transaction_date = $('#transaction_date').val();

        var data_url = config_url+"pos/get_client_payment/"+customer_id+"/"+amount_billed+"/"+receiving_account+"/"+transaction_number+"/"+transaction_date;
        // window.alert(amount_billed);
          $.ajax({
           type:'POST',
           url: data_url,
           // data:{amount_billed: amount_billed, receiving_account: receiving_account,customer_id: customer_id},
           data:{query: null},
           dataType: 'text',
            processData: false,
            contentType: false,
            success:function(data){
                //window.alert("You have successfully updated the symptoms");
                //obj.innerHTML = XMLHttpRequestObject.responseText;
                var data = jQuery.parseJSON(data);
                if(data.message == "success")
                {
                    // alert(data.result);
                    $("#invoice-payment-items").html(data.result);
                }
                else
                {
                    alert('Please ensure you have added included all the items');
                }
                
            },
            error: function(xhr, status, error) {
                //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                alert(error);
            }

        });
    }



</script>