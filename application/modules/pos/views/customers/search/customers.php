 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title pull-right"></h2>

        <h2 class="panel-title">Search Client</h2>

    </header>
    
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
            <?php

            echo form_open("search-customers", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group" style="width:800px; margin:0 auto;">
                        <label class="col-lg-4 control-label">Client name: </label>
                        
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="customer_name" placeholder="Client Name">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="center-align">
                        <button type="submit" class="btn btn-info btn-sm">Search</button>
                    </div>
                </div>
                    
            </div>
            
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>