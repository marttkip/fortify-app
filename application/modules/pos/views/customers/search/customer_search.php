    
<?php
echo form_open("search-cash-sale-report", array("class" => "form-horizontal"));
?>
<div class="row">
	<div class="col-md-12" style="margin-bottom: 20px;">
        
         <div class="form-group">
            <label class="col-md-12 left-align">Customer: </label>
            
            <div class="col-md-12">
               <select class="form-control" name="customer_id" id="customer_id" onchange="get_customer_details(this.value)" required="required">
               		<option value="">--- Select Customer --- </option>
               		<?php
               		$customers = $this->pos_model->get_all_customers();

               		if($customers->num_rows() > 0)
               		{
               			foreach ($customers->result() as $key => $value) {
               				# code...
               				$customer_name = $value->customer_name;
               				$customer_id = $value->customer_id;

               				echo '<option value="'.$customer_id.'"> '.$customer_name.' </option>';
               			}
               		}
               		?>
               </select>
            </div>
        </div>
    </div>
    
</div>


<?php
echo form_close();
?>
       