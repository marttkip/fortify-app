 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title pull-right"></h2>

        <h2 class="panel-title">Search Statement</h2>

    </header>
    
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
			<?php
			echo form_open("search-customer-statement-report/".$customer_id, array("class" => "form-horizontal"));
			?>
			<div class="row">
			    <div class="col-md-5" >
			        
			         <div class="form-group">
			            <label class="col-md-4 left-align">Date From: </label>
			            
			            <div class="col-md-8">
			                <div class="input-group">
			                    <span class="input-group-addon">
			                        <i class="fa fa-calendar"></i>
			                    </span>
			                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date From" autocomplete="off">
			                </div>
			            </div>
			        </div>
			    </div>
			    
			    <div class="col-md-5" >
			        <div class="form-group">
			            <label class="col-md-4 left-align">Date To: </label>
			            
			            <div class="col-md-8">
			            	<div class="input-group">
			                    <span class="input-group-addon">
			                        <i class="fa fa-calendar"></i>
			                    </span>
			                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date To" autocomplete="off">
			                </div>
			            </div>
			        </div>
			        
			    </div>
			    
			    <div class="col-md-2">
			        <div class="form-group">
			        	<div class="center-align">
			           		<button type="submit" class="btn btn-info">Search</button>
						</div>
			        </div>
			    </div>
			</div>


			<?php
			echo form_close();
			?>
		</div>
	</div>
</section>
       