<?php
$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
//served by
$served_by = $this->pos_model->get_personnel($this->session->userdata('personnel_id'));

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Creditors</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        
      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<h3><strong><?php echo $title;?></strong></h3>
            </div>
        </div>
        
    	<div class="row">
        	<div class="col-md-12">
        		<?php
	            	$creditor_result = $this->pos_model->get_customer_invoice_balances_statement($customer_id,$start_date);
				?>

				<table class="table table-hover table-bordered ">
				 	<thead>
						 <th>#</th>	
						  <th>Transaction Date</th>	
                          <th>Transaction Type</th>   
						  <th>Transaction Number</th> 
						  <th>Amount</th> 
					 </thead>
				  	<tbody>
				  		<?php 
				  		$result ='';
				  		$total_arrears = 0;
				  		if($creditor_result->num_rows() > 0)
				  		{
				  			$x=0;

				  			foreach ($creditor_result->result() as $key => $value) {
				  				# code...
				  				$transactionDate = $value->transactionDate;
				  				$referenceCode = $value->referenceCode;
				  				$transactionCategory = $value->transactionCategory;
				  				$dr_amount  = $value->dr_amount;
				  				$cr_amount = $value->cr_amount;

				  				if($cr_amount > 0)
                                {

                                    $amount_viewed = '('.number_format($cr_amount,2).')';
                                }
                                else
                                {
                                    $amount_viewed = number_format($dr_amount,2);
                                }

                                if($transactionCategory == "Order")
                                {
                                    $transactionCategory = 'Customer Invoice';
                                }

				  				$balance = $dr_amount - $cr_amount;

				  				$total_arrears += $balance; 

				  				$total_debits += $dr_amount;
				  				$total_credits += $cr_amount;

				  				$x++;

				  				$result .= '<tr>
				  								<td>'.$x.'</td>
				  								<td>'.date('Y.m.d',strtotime($transactionDate)).'</td>
                                                <td>'.strtoupper(strtolower($transactionCategory)).'</td>
				  								<td>'.strtoupper(strtolower($referenceCode)).'</td>
				  								<td style="text-align:center">'.$amount_viewed.'</td>
				  						   </tr>';
				  			}
				  			$result .= '<tr>
				  								<th></th>
				  								<th></th>
				  								<th></th>
				  								<th>Totals</th>
				  								<th  style="text-align:center">'.number_format($total_debits- $total_credits,2).'</th>
				  						   </tr>';
				  		}
				  		echo $result;
				  		?>
					</tbody>
				</table>
            </div>

            <?php
        
			  	$income_rs = $this->pos_model->get_customer_ledger($customer_id);

				$income_result = '';
				$total_income = 0;
				if($income_rs->num_rows() > 0)
				{
					$x=0;
					foreach ($income_rs->result() as $key => $value) {
						# code...
						// $total_amount = $value->total_amount;
						$payables = $value->receivables;
						$thirty_days = $value->thirty_days;
						$sixty_days = $value->sixty_days;
						$ninety_days = $value->ninety_days;
						$over_ninety_days = $value->over_ninety_days;
						$coming_due = $value->coming_due;
						$creditor_id = $value->recepientId;
						$Total = $value->Total;
						$x++;
						$income_result .='<tr>
											
											<td class="center-align">'.number_format($coming_due,2).'</td>
											<td class="center-align">'.number_format($thirty_days,2).'</td>
											<td class="center-align">'.number_format($sixty_days,2).'</td>
											<td class="center-align">'.number_format($ninety_days,2).'</td>
											<td class="center-align">'.number_format($over_ninety_days,2).'</td>
											<td class="center-align">'.number_format($Total,2).'</td>
											</tr>';
					}

				}
				
					  	

            ?>
            <!-- <div class="col-md-12">
            	<h4>AGING REPORT</h4>
            	<table class="table table-hover table-bordered ">
					<thead>
						<tr>

						  <th class="center-align">Current</th>
						  <th class="center-align">30 Days</th>
						  <th class="center-align">60 Days</th>
						  <th class="center-align">90 Days</th>
						  <th class="center-align">Over 90 days</th>
						  <th class="center-align">Total</th>
						</tr>					
					</thead>
					<tbody>
						
					</tbody>
				</table>
            </div> -->

        </div>
        
    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-md-10 pull-left">
            	Prepared by: <?php echo $served_by;?> 
          	</div>
        	<div class="col-md-2 pull-right">
            	<?php echo date('jS M Y H:i a'); ?>
            </div>
        </div>
    </body>
    
</html>