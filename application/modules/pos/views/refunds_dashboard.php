
<div class="row" id="dashboard-disengaged" style="display: block;">
	<div class="col-md-12">
		<div class="pull-right">
			 <a class='btn btn-sm btn-info' onclick="create_new_visit(3)"> <i class="fa fa-plus"></i> Add Refunds </a> 
		</div>
	</div>

	<div class="col-md-12">
		<div id="refunds-div"></div>
	</div>
	
	
</div>
<div class="row" id="dashboard-engaged" style="display: none;">
	
	
	
	<div class="row" style=" ">
		<div class="col-md-10">
			<div class="panel-body">
				
					
				
					<!-- <div id="patient-statement"></div> -->
					<?php echo  form_open("finance/creditors/confirm_invoice_note", array("class" => "form-horizontal","id" => "confirm-search"));?>
					<div class="row" id="invoice-div" style="padding:10px;">
						<div class="col-md-2">
							<input type="text" name="description" id="description" class="form-control"  placeholder="Description">
						</div>
						<div class="col-md-2">
							<input type="text" name="vehicle_name" id="vehicle_name" class="form-control"  placeholder="Vehicle Name">
						</div>
						<div class="col-md-3">
							<input type="text" name="vehicle_model" id="vehicle_model" class="form-control"  placeholder="Model">
						</div>
						<div class="col-md-2">
							<input type="text" name="part_no" id="part_no" class="form-control"  placeholder="Part No">
						</div>
						<div class="col-md-2">
							<input type="text" name="valley_no" id="valley_no" class="form-control"  placeholder="Valley No">
						</div>
						<div class="col-md-1">
							<button class="btn btn-sm btn-success" type="submit">search</button>
						</div>
			           
			                
			        </div>
			        <?php echo form_close();?>
			   
		    </div>
		</div>
	    <div class="col-md-2">
	    	 <a class='btn btn-xs btn-warning' onclick="back_to_dashboard()"> <i class="fa fa-arrow-left"></i> Back to Dashboard </a>  
	    </div>
	</div>

	<div class="row" style="height:30vh; overflow-y: scroll;padding:10px;">
		<div class="panel-body">
			
			<div id="orders-list"></div>
    		
    	</div>
    		
    </div>

	<div class="row" style="height:60vh; overflow-y: scroll;padding:10px;">
		<div id="visit-charges"></div>
		
	</div>
</div>
<div class="row" id="dashboard-payment" style="display: none;">
	<div style="margin-bottom: 20px;">
		<div class="col-md-12">
			<div class="pull-right">
				 <a class='btn btn-sm btn-warning' onclick="back_to_dashboard()"> <i class="fa fa-arrow-left"></i> Back to Dashboard </a>  

			</div>
		</div>
	</div>
	<div class="col-print-8">
		<div id="visit-charges-closed"></div>
	</div>
	<div class="col-print-4" >
		<div class="panel-body" >
			<div id="payment-div"></div>
		</div>
	</div>
</div>


  <!-- END OF ROW -->
<script type="text/javascript">


   $(function() {
       $("#service_id_item").customselect();
       $("#provider_id_item").customselect();
       $("#parent_service_id").customselect();

   });
   $(document).ready(function(){
   		// display_patient_bill(<?php echo $visit_id;?>);
   		// display_procedure(<?php echo $visit_id;?>);
   		// get_patient_statement();

   		// get_visit_procedures_done();
   		// get_parent_categories();
   		// get_product_list();
   		// get_patient_incomplete_invoices(<?php echo $patient_id;?>);

   		window.localStorage.setItem('visit_id',null);
		window.localStorage.setItem('order_invoice_id',null);
   		get_days_refunds();

   });

   function get_order_items_list()
   {
   		var myTarget2 = document.getElementById("dashboard-disengaged");
        myTarget2.style.display = 'none';


        var order_invoice_id = window.localStorage.getItem('order_invoice_id');
        var pos_order_id = window.localStorage.getItem('pos_order_id');

        // alert(order_invoice_id);
        if(order_invoice_id > 0)
        {
        	var myTarget5 = document.getElementById("dashboard-engaged");
	        myTarget5.style.display = 'none';

	        var myTarget6 = document.getElementById("dashboard-payment");
	    	myTarget6.style.display = 'block';
	    	get_payment_div(pos_order_id,order_invoice_id);
        }
        else
        {
        	var myTarget5 = document.getElementById("dashboard-engaged");
	        myTarget5.style.display = 'block';

	        var myTarget6 = document.getElementById("dashboard-payment");
	    	myTarget6.style.display = 'none';

	    	get_parent_categories();
   			// get_product_list();
        }

   		get_visit_procedures_done(pos_order_id);
   		
   }

  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");

        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }

  }
  function check_payment_type(payment_type_id){


    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check

      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }

  }

   function display_patient_bill(visit_id){

      var XMLHttpRequestObject = false;

      if (window.XMLHttpRequest) {

          XMLHttpRequestObject = new XMLHttpRequest();
      }

      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }

      var config_url = document.getElementById("config_url").value;
      var close_page = document.getElementById("close_page").value;
      var url = config_url+"pos/view_patient_bill/"+visit_id+"/1";
      // alert(url);
      if(XMLHttpRequestObject) {

          XMLHttpRequestObject.open("GET", url);

          XMLHttpRequestObject.onreadystatechange = function(){

              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }

          XMLHttpRequestObject.send(null);
      }


  }

	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, pos_order_id){

	    var units = document.getElementById('units'+id).value;
	    var billed_amount = document.getElementById('billed_amount'+id).value;

	    grand_total(id, units, billed_amount, pos_order_id);

	}

  function grand_total(procedure_id, units, amount, pos_order_id)
  {

		 var config_url = document.getElementById("config_url").value;
		 var product_id = document.getElementById('product_id'+procedure_id).value;
		  var discount = document.getElementById('discount'+procedure_id).value;
		  // var vatable = document.getElementById('vatable'+procedure_id).value;

		 var radios = document.getElementsByName('vatable'+procedure_id);
		 var vatable = 0;
		for (var i = 0, length = radios.length; i < length; i++) {
		  if (radios[i].checked) {
		    // do whatever you want with the checked radio
		    var vatable = radios[i].value;

		    // only one radio can be logically checked, don't check the rest
		    break;
		  }
		}
	     var data_url = config_url+"pos/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+pos_order_id;

	      // var tooth = document.getElementById('tooth'+procedure_id).value;
	     // alert(vatable);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{procedure_id: procedure_id,product_id: product_id,discount: discount,vatable: vatable},
	    dataType: 'text',
	    success:function(data){
	     // get_medication(visit_id);
	         // display_patient_bill(v_id);
	        get_visit_procedures_done(pos_order_id);
	         // alert('You have successfully updated your billing');
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        get_visit_procedures_done(pos_order_id);
	    	alert(error);
	    }

	    });


	    // get_totals_div(pos_order_id);

	   //  var XMLHttpRequestObject = false;

	   //  if (window.XMLHttpRequest) {

	   //      XMLHttpRequestObject = new XMLHttpRequest();
	   //  }

	   //  else if (window.ActiveXObject) {
	   //      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	   //  }
	   //  var config_url = document.getElementById("config_url").value;

	   //  var url = config_url+"pos/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	   //  // alert(url);
	   //  if(XMLHttpRequestObject) {

	   //      XMLHttpRequestObject.open("GET", url);

	   //      XMLHttpRequestObject.onreadystatechange = function(){

	   //          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
				// {
	   //  			// display_patient_bill(v_id);
	   //  			display_billing(v_id);
	   //          }
	   //      }

	   //      XMLHttpRequestObject.send(null);
	   //  }
	}


	function delete_service(id, visit_id){

		var res = confirm('Do you want to remove this charge ? ');

		if(res)
		{
			var XMLHttpRequestObject = false;

		    if (window.XMLHttpRequest) {

		        XMLHttpRequestObject = new XMLHttpRequest();
		    }

		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"pos/delete_service_billed/"+id;

		    if(XMLHttpRequestObject) {

		        XMLHttpRequestObject.open("GET", url);

		        XMLHttpRequestObject.onreadystatechange = function(){

		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

		                // display_patient_bill(visit_id);
		                display_procedure(visit_id);
		            }
		        }

		        XMLHttpRequestObject.send(null);
		    }
		}

	}
	function save_service_items(visit_id)
	{
		var provider_id = $('#provider_id'+visit_id).val();
		var service_id = $('#service_id'+visit_id).val();
		var visit_date = $('#visit_date_date'+visit_id).val();
		var url = "<?php echo base_url();?>pos/add_patient_bill/"+visit_id;

		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'text',
		success:function(data){
			alert("You have successfully billed");
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}



	function parse_procedures(visit_id,suck)
    {
      var procedure_id = document.getElementById("service_id_item").value;
       procedures(procedure_id, visit_id, suck);

    }

	function procedures(id, v_id, suck){

        var XMLHttpRequestObject = false;

        if (window.XMLHttpRequest) {

            XMLHttpRequestObject = new XMLHttpRequest();
        }

        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }

        var url = "<?php echo site_url();?>pos/accounts_update_bill/"+id+"/"+v_id+"/"+suck;

         if(XMLHttpRequestObject) {

            XMLHttpRequestObject.open("GET", url);

            XMLHttpRequestObject.onreadystatechange = function(){

                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    // document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
                    display_patient_bill(v_id);
                }
            }

            XMLHttpRequestObject.send(null);
        }

    }
    function display_procedure(visit_id){

	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var config_url = document.getElementById("config_url").value;
	    // var url = config_url+"nurse/view_procedure/"+visit_id;
	    var url = config_url+"pos/view_procedure/"+visit_id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
	}
	function delete_procedure(id, pos_order_id){
	    var XMLHttpRequestObject = false;

	    if (window.XMLHttpRequest) {

	        XMLHttpRequestObject = new XMLHttpRequest();
	    }

	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"pos/delete_charged_item/"+id;

	    if(XMLHttpRequestObject) {

	        XMLHttpRequestObject.open("GET", url);

	        XMLHttpRequestObject.onreadystatechange = function(){

	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {


	            }
	        }

	        XMLHttpRequestObject.send(null);
	    }
      // display_patient_bill(visit_id);
       get_visit_procedures_done(pos_order_id);
       
	}

	$(document).on("change","select#visit_type_id",function(e)
	{
		var visit_type_id = $(this).val();

		if(visit_type_id != '1')
		{
			$('#insured_company').css('display', 'block');
			// $('#consultation').css('display', 'block');
		}
		else
		{
			$('#insured_company').css('display', 'none');
			// $('#consultation').css('display', 'block');
		}




	});

	function change_payer(visit_charge_id, service_charge_id, v_id)
	{

		var res = confirm('Do you want to change who is being billed ? ');

		if(res)
		{

			var config_url = document.getElementById("config_url").value;
		    var data_url = config_url+"pos/change_payer/"+visit_charge_id+"/"+service_charge_id+"/"+v_id;
		   
		      // var tooth = document.getElementById('tooth'+procedure_id).value;
		     // alert(data_url);
		    $.ajax({
		    type:'POST',
		    url: data_url,
		    data:{visit_charge_id: visit_charge_id},
		    dataType: 'text',
		    success:function(data){
		     // get_medication(visit_id);
		         display_patient_bill(v_id);
		     alert('You have successfully updated your billing');
		    //obj.innerHTML = XMLHttpRequestObject.responseText;
		    },
		    error: function(xhr, status, error) {
		    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        display_patient_bill(v_id);
		    	alert(error);
		    }

		    });

		}

	}


	function get_patient_statement()
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"pos/get_patient_statement";
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#patient-statement").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}

	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}

	function add_invoice()
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_invoices";
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);
			$("#visit-invoice-div").html(data);
			get_visit_procedures_done();
			// get_visit_charges(visit_id,patient_id);
			// tinymce.init({
			//                 selector: ".cleditor",
			//                	height: "200"
			// 	            });
			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}


	function get_visit_invoices(patient_id)
	{
		var myTarget = document.getElementById("visit_id").value;
		var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_invoices/"+visit_id+"/"+patient_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			document.getElementById("visit-invoice-div").style.display = "block"; 
			$("#visit-invoice-div").html(data);
			get_visit_charges(visit_id,patient_id);
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function get_procedures_done(category_id = null)
	{
		var config_url = $('#config_url').val();
		// var visit_type_id = document.getElementById("visit_type_id").value;

		var visit_id = window.localStorage.getItem('visit_id');
		var patient_id = window.localStorage.getItem('patient_id');
		var data_url = config_url+"pos/get_product_list";
		// window.alert(data_url);
		$('#charges-div').css('display', 'block');
		var lab_test = $('#search_procedures').val();

		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : lab_test,category_id : category_id},
		dataType: 'text',
		success:function(data)
		{
			// alert(data);
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#product-list").html(data);
			// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function get_visit_charges(visit_id,patient_id,order_invoice_id = null)
	{
		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_charges/"+visit_id+"/"+patient_id+"/"+order_invoice_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#visit-charges").html(data);
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	function add_payment(patient_id,order_invoice_id)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/add_payment/"+patient_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			get_visit_payments(patient_id,null,order_invoice_id);
			// alert(data);

			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
		
	}


	function get_visit_payments(patient_id,payment_id=null,order_invoice_id=null)
	{
		// var myTarget = document.getElementById("visit_id").value;
		// var visit_id = myTarget;

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/get_visit_payments/"+patient_id+"/"+payment_id+"/"+order_invoice_id;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			// document.getElementById("visit-payments-div").style.display = "block"; 
			$("#visit-payments-div").html(data);
			
			$('.datepicker').datepicker({
					    format: 'yyyy-mm-dd'
					});



			$('.timepicker').timepicker({
			    timeFormat: 'h:mm p',
			    interval: 60,
			    minTime: '10',
			    maxTime: '6:00pm',
			    defaultTime: '11',
			    startTime: '10:00',
			    dynamic: false,
			    dropdown: true,
			    scrollbar: true
			});
		
			
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function add_payment_item(patient_id,payment_id= null)
	{
		var config_url = $('#config_url').val();

		 var amount_paid = document.getElementById("amount_paid").value;
		 var invoice_id = document.getElementById("invoice_id").value;

		 // window.alert(amount_paid);
		var data_url = config_url+"pos/add_payment_item/"+patient_id+"/"+payment_id;
		
		// window.alert(invoice_id);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{invoice_detail: invoice_id,amount: amount_paid},
		dataType: 'text',
		success:function(data){
		
			get_visit_payments(patient_id,payment_id);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	$(document).on("submit","form#confirm-payment",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		// var visit_id = $('#charge_visit_id').val();
		var patient_id = $('#payment_patient_id').val();
		var payment_id = $('#payment_payment_id').val();
		var config_url = $('#config_url').val();	

		var url = config_url+"pos/confirm_payment/"+patient_id+"/"+payment_id;
		// alert(url);
		
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				get_patient_statement(patient_id);
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function get_patient_incomplete_invoices(patient_id)
	{

		var config_url = $('#config_url').val();
	 	var url = config_url+"pos/get_incomplete_invoices/"+patient_id;
	 	// alert(url);
		$.ajax({
			type:'POST',
			url: url,
			data:{query: null},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			var data = jQuery.parseJSON(data);
			  // alert(data.content);
			if(data.message == "success")
			{
				$("#incomplete-invoices").html(data.result);
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});

	}



$(document).on("submit","form#confirm-invoice",function(e)
{
	e.preventDefault();
	
	var form_data = new FormData(this);

	// alert(form_data);
	var amount = $('#amount').val();
	var tendered_amount = $('#tendered_amount').val();
	var visit_id = $('#charge_visit_id').val();
	// var patient_id = $('#charge_patient_id').val();
	// var order_invoice_id = $('#order_invoice_id').val();
	var pos_order_id = window.localStorage.getItem('pos_order_id');
	// var patient_id = window.localStorage.getItem('patient_id');
	// var order_invoice_id = window.localStorage.getItem('order_invoice_id');
	// alert(pos_order_id);


	var sale_type = $("input[name='sale_type']:checked").val();

	var proceed = false;
	if(sale_type == 0)
	{
		if(tendered_amount >= amount)
		{
			proceed = true;
		}
		else
		{
			alert('Sorry please check on the amount entered ');
			proceed = false;
		}

	}
	else
	{
		proceed = true;

	}
         
	if(proceed == true)
	{


		var config_url = $('#config_url').val();	

		var url = config_url+"pos/confirm_visit_charge";
		 
		 
	   $.ajax({
		   type:'POST',
		   url: url,
		   data:form_data,
		   dataType: 'text',
		   processData: false,
		   contentType: false,
		   success:function(data){
		      var data = jQuery.parseJSON(data);
		    
		      	if(data.message == "success")
				{
					var order_invoice_id = data.order_invoice_id;
					
					window.localStorage.setItem('order_invoice_id',order_invoice_id);

					var myTarget2 = document.getElementById("dashboard-disengaged");
				    myTarget2.style.display = 'none';


					var myTarget5 = document.getElementById("dashboard-engaged");
				    myTarget5.style.display = 'none';

				    var myTarget6 = document.getElementById("dashboard-payment");
				    myTarget6.style.display = 'block';
					
					get_visit_procedures_done(pos_order_id,order_invoice_id);

					get_payment_div(pos_order_id,order_invoice_id);
					// window.localStorage.setItem('customer_id','');
					// document.getElementById("customer_name").value = "";
					// document.getElementById("customer-balance").value = "";
					close_side_bar();
					// get_patient_incomplete_invoices(patient_id);
					// get_patient_statement();
					
				}
				else
				{
					alert(data.result);
				}
		   
		   },
		   error: function(xhr, status, error) {
		   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		   
		   }
		   });
	
	
	 }
	
   
	
});

function invoice_details_view(order_invoice_id,visit_id,patient_id)
{

	document.getElementById("sidebar-right").style.display = "block"; 
	document.getElementById("existing-sidebar-div").style.display = "none"; 

	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_visit_invoices/"+visit_id+"/"+patient_id+"/"+order_invoice_id;
	//window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{appointment_id: 1},
	dataType: 'text',
	success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
		document.getElementById("current-sidebar-div").style.display = "block"; 
		$("#current-sidebar-div").html(data);
		$("#visit-invoice-div").html(data);
		get_visit_charges(visit_id,patient_id,order_invoice_id);
		tinymce.init({
		                selector: ".cleditor",
		               	height: "200"
			            });
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
	}

	});

}

function add_service_charge_test(service_charge_id,product_id,service_charge_amount,order_invoice_id =null)
{
	// var res = confirm('Are you sure you want to charge ?');

	// if(res)
	// {	
		
		var pos_order_id = window.localStorage.getItem('pos_order_id');
		// var patient_id = window.localStorage.getItem('patient_id');
		var visit_type_id=1;

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/add_visit_charge/"+service_charge_id+"/"+pos_order_id+"/"+visit_type_id+"/"+order_invoice_id;
		// window.alert(product_id);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{service_charge_id: service_charge_id,visit_type_id: visit_type_id,visit_invoice: order_invoice_id,product_id: product_id, service_charge_amount: service_charge_amount},
		dataType: 'text',
		success:function(data){

			var data = jQuery.parseJSON(data);

			if(data.message == "success")
			{
				// document.getElementById('search_procedures').value = '';
				$('#charges-div').css('display', 'none');


				get_visit_procedures_done(pos_order_id);

			}
			else
			{
				alert(data.result);
			}
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	// }

	


}
	
function get_visit_procedures_done(pos_order_id,order_invoice_id = null)
{
	var config_url = $('#config_url').val();

	var order_invoice_id = window.localStorage.getItem('order_invoice_id');


	if(order_invoice_id > 0)
	{
		var data_url = config_url+"pos/visit_procedures_view/"+pos_order_id+"/"+order_invoice_id;
	}
	else
	{
		var data_url = config_url+"pos/visit_procedures_view/"+pos_order_id;
	}
	
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		var data = jQuery.parseJSON(data);
		if(order_invoice_id > 0)
		{
			$("#visit-charges-closed").html(data.page);
		}else
		{
			$("#visit-charges").html(data.page);
		}
		// alert(data.customer_balance);
		window.localStorage.setItem('customer_id',data.customer_id);
		document.getElementById("customer_name").value = data.customer_name;
		$('#customer-balance').html(data.customer_balance);
		// document.getElementById("customer-balance").value = data.customer_balance;
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});

	// get_totals_div();
}





// pos functions 

function get_parent_categories()
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_parent_categories";
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#categories-list").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}

function get_product_list()
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_product_list";
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#product-list").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}

function get_totals_div(visit_id,patient_id)
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_totals_div/"+visit_id+"/"+patient_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#totals-div").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}


function create_new_visit(sale_type)
{

		
		var sale = 'Refund ';
		
		var res = confirm('Are you sure you want to start a new '+sale+' sale ? ');

		if(res)
		{


			var config_url = $('#config_url').val();
			var data_url = config_url+"pos/create_new_refund/"+sale_type;
			// window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{query : null},
			dataType: 'text',
			success:function(data){
				var data = jQuery.parseJSON(data);

				if(data.status == "success")
				{
					// var patient_id = data.patient_id;
					var pos_refund_id = data.pos_refund_id;
					window.localStorage.setItem('pos_refund_id',pos_refund_id);
					// window.localStorage.setItem('patient_id',patient_id);
					get_refund_details(pos_refund_id);

					
				}
			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});
		}
}

function back_to_dashboard()
{
	var myTarget2 = document.getElementById("dashboard-disengaged");
    myTarget2.style.display = 'block';


	var myTarget5 = document.getElementById("dashboard-engaged");
    myTarget5.style.display = 'none';

    var myTarget6 = document.getElementById("dashboard-payment");
    myTarget6.style.display = 'none';

    window.localStorage.setItem('visit_id',null);
	window.localStorage.setItem('patient_id',null);
	window.localStorage.setItem('order_invoice_id',null);
    get_days_refunds();	
}

function get_days_refunds()
{

	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_todays_refunds";
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
	//window.alert("You have successfully updated the symptoms");
	//obj.innerHTML = XMLHttpRequestObject.responseText;
		$("#refunds-div").html(data);
	// alert(data);
	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
}



function get_change(total_amount)
{ 

	var tendered_amount = document.getElementById('tendered_amount').value;

	var balance = 0;
	if(tendered_amount < total_amount)
	{
		balance = 0;
	}
	else if(tendered_amount == total_amount)
	{
		balance = 0;
	}
	else if(tendered_amount > total_amount)
	{
		balance = tendered_amount - total_amount;
	}
	// alert(tendered_amount);
	document.getElementById("change").value = balance;
	var data = '<h2 class="center-align">Ksh.'+balance+' </h2>';
	$("#balance-div").html(data);
}

function get_payment_div(pos_order_id,order_invoice_id)
{
	var config_url = $('#config_url').val();
	var data_url = config_url+"pos/get_payment_div/"+pos_order_id+"/"+order_invoice_id;
	// window.alert(data_url);
	$.ajax({
	type:'POST',
	url: data_url,
	data:{query : null},
	dataType: 'text',
	success:function(data){
		var data = jQuery.parseJSON(data);

		if(data.status == "success")
		{
			
		}
		$("#payment-div").html(data.result);
	// alert(data);

	},
	error: function(xhr, status, error) {
	//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	alert(error);
	}

	});
	// get_visit_payments(patient_id,null,order_invoice_id);

}

function proceed_to_complete_sale()
{

	document.getElementById("sidebar-right").style.display = "block"; 
	document.getElementById("existing-sidebar-div").style.display = "none"; 

	var pos_order_id = window.localStorage.getItem('pos_order_id');
	var sale_type = window.localStorage.getItem('sale_type');


	

	if(sale_type == 1)
	{
		var customer_id = window.localStorage.getItem('customer_id');
		// alert(customer_id);

		if(customer_id > 0)
		{
			var config_url = $('#config_url').val();

			var data_url = config_url+"pos/get_complete_view/"+pos_order_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: data_url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){
				// var data = jQuery.parseJSON(data);
				//window.alert("You have successfully updated the symptoms");
				//obj.innerHTML = XMLHttpRequestObject.responseText;
				document.getElementById("current-sidebar-div").style.display = "block"; 
				$("#current-sidebar-div").html(data);
				// $("#payment-div").html(data.result);
					document.getElementById("username-div").style.display = "block"; 
					document.getElementById("payment-complete-off").style.display = "none"; 
				},
				error: function(xhr, status, error) {
				//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				alert(error);
			}

			});

		}
		else
		{
			alert('Sorry please select a customer ');
		}
	}
	else
	{

		 var config_url = $('#config_url').val();

		var data_url = config_url+"pos/get_complete_view/"+pos_order_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			// var data = jQuery.parseJSON(data);
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);
			// $("#payment-div").html(data.result);
				document.getElementById("username-div").style.display = "block"; 
				document.getElementById("payment-complete-off").style.display = "none"; 
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});

	}

	

}

function get_invoice_type(pos_order_id,id)
{

        var myTarget1 = document.getElementById("cash-sale-div");
        var myTarget2 = document.getElementById("credit-sale-div");
        var myTarget3 = document.getElementById("new-customer-div");
        var myTarget4 = document.getElementById("searched-div");
        var myTarget5 = document.getElementById("quote-div");
        if(id == 0)
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        else if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        else if(id == 3)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'block';
        }

        if(id <= 1)
        {
        	 var config_url = $('#config_url').val();
	  	 
			  var data_url = config_url+"pos/update_sale_type/"+pos_order_id+"/"+id;
			  
			  $.ajax({
			  type:'POST',
			  url: data_url,
			  data:{pos_order_id: pos_order_id},
			  dataType: 'text',
			  success:function(data){
			  	var data = jQuery.parseJSON(data);

			  	if(data.message == "success")
			  	{
			  		
			  	}
			  	else
			  	{

			  	}
			 	
			   		
			    // alert(data);
			  },
			  error: function(xhr, status, error) {
			  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			  alert(error);
			  }

			  });
        }

  }

  function search_customers()
  {
  	 $('#searched-customers-list').attr("style", "display:block");
  	  var config_url = $('#config_url').val();
  	  var pos_order_id = window.localStorage.getItem('pos_order_id');
	  var data_url = config_url+"pos/search_customer/"+pos_order_id;
	  //window.alert(data_url);
	  var lab_test = $('#q').val();
	  $.ajax({
	  type:'POST',
	  url: data_url,
	  data:{pos_order_id: pos_order_id, query : lab_test},
	  dataType: 'text',
	  success:function(data){
	  //window.alert("You have successfully updated the symptoms");
	  //obj.innerHTML = XMLHttpRequestObject.responseText;
	   $("#searched-customers-list").html(data);
	   
	    // alert(data);
	  },
	  error: function(xhr, status, error) {
	  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	  alert(error);
	  }

	  });
  }


  function search_customers_list()
  {
  	$('#searched-customers-list').attr("style", "display:block");
  	  var config_url = $('#config_url').val();
  	  var pos_order_id = window.localStorage.getItem('pos_order_id');
	  var data_url = config_url+"pos/search_customer/"+pos_order_id;
	  //window.alert(data_url);
	  var lab_test = $('#customer_name').val();
	  $.ajax({
	  type:'POST',
	  url: data_url,
	  data:{pos_order_id: pos_order_id, query : lab_test},
	  dataType: 'text',
	  success:function(data){
	  //window.alert("You have successfully updated the symptoms");
	  //obj.innerHTML = XMLHttpRequestObject.responseText;
	   $("#searched-customers-list").html(data);
	   
	    // alert(data);
	  },
	  error: function(xhr, status, error) {
	  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	  alert(error);
	  }

	  });
  }

  	function add_new_customer()
	{

	        var myTarget1 = document.getElementById("cash-sale-div");
	        var myTarget2 = document.getElementById("credit-sale-div");
	        var myTarget3 = document.getElementById("new-customer-div");
	        var myTarget4 = document.getElementById("searched-div");
	        var myTarget5 = document.getElementById("quote-div");
	       
			myTarget1.style.display = 'none';
			myTarget2.style.display = 'none';
			myTarget3.style.display = 'block';
			myTarget4.style.display = 'none';
			myTarget5.style.display = 'none';
	        

	 }

	$(document).on("submit","form#add-customer",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		
		var config_url = $('#config_url').val();	

		var url = config_url+"pos/add_customer";
		 // var pos_order_id = window.localStorage.getItem('pos_order_id');
		 
	   $.ajax({
		   type:'POST',
		   url: url,
		   data:form_data,
		   dataType: 'text',
		   processData: false,
		   contentType: false,
		   success:function(data){
		      var data = jQuery.parseJSON(data);
		    
		      	if(data.message == "success")
				{
					// get_invoice_type(pos_order_id,1);
					document.getElementById("add-customer").reset(); 
					close_side_bar();
				}
				else
				{
					alert(data.result);
				}
		   
		   },
		   error: function(xhr, status, error) {
		   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		   
		   }
		   });
		
		
	});
	function add_customer_to_order(customer_id,pos_order_id)
	{

		  var config_url = $('#config_url').val();
	  	 
		  var data_url = config_url+"pos/add_customer_to_order/"+pos_order_id+"/"+customer_id;
		  
		  $.ajax({
		  type:'POST',
		  url: data_url,
		  data:{pos_order_id: pos_order_id, customer_id : customer_id},
		  dataType: 'text',
		  success:function(data){
		  	var data = jQuery.parseJSON(data);

		  	if(data.message == "success")
		  	{
		  		// var myTarget4 = document.getElementById("searched-div");
		  		// myTarget4.style.display = 'block';
		  		// $('#q').val('');
		  		// window.localStorage.setItem('customer_id',customer_id);
		  		// $('#customer_name').val('');
		  		// document.getElementById("customer_name").value = data.customer_name; 
		  		// $("#sale-detail").html(data.customer_name);
		  		
		  		window.localStorage.setItem('customer_id',customer_id);
		  		$('#customer-balance').html(data.customer_balance);
		  		$('#customer_name').val(data.customer_name);
		  		$('#credit-sale-button').attr("style", "display:block");
		  		$('#searched-customers-list').attr("style", "display:none");
		  		//alert('ok');
		  		//var cutomerName = document.getElementById('customer-name')

		  	}
		  	else
		  	{

		  	}
		 	
		   		
		    // alert(data);
		  },
		  error: function(xhr, status, error) {
		  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		  alert(error);
		  }

		  });

	}
	function clear_order_invoice(pos_order_id)
	{
		var res = confirm('Are you sure you want to remove the customer from the order ? ');

		if(res)
		{

			 var config_url = $('#config_url').val();
	  	 
			  var data_url = config_url+"pos/remove_customer_from_order/"+pos_order_id;
			  
			  $.ajax({
			  type:'POST',
			  url: data_url,
			  data:{pos_order_id: pos_order_id},
			  dataType: 'text',
			  success:function(data){
			  	var data = jQuery.parseJSON(data);

			  	if(data.message == "success")
			  	{
			  		var myTarget4 = document.getElementById("searched-div");
			  		myTarget4.style.display = 'none';
			  	
			  		// $("#sale-detail").html(data.customer_name);
			  	}
			  	else
			  	{

			  	}
			 	
			   		
			    // alert(data);
			  },
			  error: function(xhr, status, error) {
			  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			  alert(error);
			  }

			  });
		}
	}
	function login_to_sale(pos_order_id,sale_type)
	{

		var username = $('#username').val();
		var password = $('#password').val();
		var config_url = $('#config_url').val();	

		var url = config_url+"pos/confirm_user/"+pos_order_id;
		 
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:{username: username, password: password},
	   dataType: 'text',
	   success:function(data){
	      var data = jQuery.parseJSON(data);

	      // alert(data.result_items);
	    
	      	if(data.message == "success")
			{
				
				window.localStorage.setItem('user_id',data.user_id);
				document.getElementById("username-div").style.display = "none"; 
				document.getElementById("payment-complete-off").style.display = "none"; 
				alert(sale_type);
				if(sale_type == 1)
				{
					var myTarget1 = document.getElementById("cash-sale-div");
			        var myTarget2 = document.getElementById("credit-sale-div");
			        var myTarget3 = document.getElementById("quote-div");
			       
					myTarget1.style.display = 'none';
					myTarget2.style.display = 'block';
					myTarget3.style.display = 'none';

				}
				else if(sale_type == 0)
				{
					var myTarget1 = document.getElementById("cash-sale-div");
			        var myTarget2 = document.getElementById("credit-sale-div");
			        var myTarget3 = document.getElementById("quote-div");
			       
					myTarget1.style.display = 'block';
					myTarget2.style.display = 'none';
					myTarget3.style.display = 'none';
				}
				else if(sale_type == 3)
				{
					var myTarget1 = document.getElementById("cash-sale-div");
			        var myTarget2 = document.getElementById("credit-sale-div");
			        var myTarget3 = document.getElementById("quote-div");
			       
					myTarget1.style.display = 'none';
					myTarget2.style.display = 'none';
					myTarget3.style.display = 'block';
				}
				
			}
			else
			{
				document.getElementById("username-div").style.display = "block"; 
				document.getElementById("payment-complete-off").style.display = "none"; 
				alert(data.result_items);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });

	}

	$(document).on("submit","form#cancel-order",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		
		var config_url = $('#config_url').val();	

		var url = config_url+"pos/cancel_order";
		// alert(url);
		
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
	   success:function(data){
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				// close_side_bar();
				back_to_dashboard();
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});

	function add_client()
	{
		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = $('#config_url').val();
		var data_url = config_url+"pos/add_client";
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);
			$("#visit-invoice-div").html(data);
			// get_visit_charges(visit_id,patient_id,order_invoice_id);
			tinymce.init({
			                selector: ".cleditor",
			               	height: "200"
				            });
			// alert(data);
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
		}

		});
	}

	$(document).on("submit","form#confirm-search",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		var config_url = $('#config_url').val();
		// var visit_type_id = document.getElementById("visit_type_id").value;

		var visit_id = window.localStorage.getItem('visit_id');
		var patient_id = window.localStorage.getItem('patient_id');
		var data_url = config_url+"pos/get_product_list";
		// window.alert(data_url);
		$('#charges-div').css('display', 'block');
		// var description = $('#description').val();
		// var vehicle = $('#vehicle').val();
		// var part_no = $('#part_no').val();
		// alert(form_data);
		$.ajax({
		type:'POST',
		url: data_url,
		data:form_data,
		dataType: 'text',
		processData: false,
		contentType: false,
	   success:function(data){
			// alert(data);
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#product-list").html(data);
			// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

	});

	$(document).on("submit","form#search-items",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		var config_url = $('#config_url').val();
		// var visit_type_id = document.getElementById("visit_type_id").value;

		var visit_id = window.localStorage.getItem('visit_id');
		var patient_id = window.localStorage.getItem('patient_id');
		var data_url = config_url+"pos/get_product_list_items_inquiry";
		// window.alert(data_url);
		$('#charges-div').css('display', 'block');
		// var description = $('#description').val();
		// var vehicle = $('#vehicle').val();
		// var part_no = $('#part_no').val();
		// alert(form_data);
		$.ajax({
		type:'POST',
		url: data_url,
		data:form_data,
		dataType: 'text',
		processData: false,
		contentType: false,
	   success:function(data){
			// alert(data);
			//window.alert("You have successfully updated the symptoms");
			//obj.innerHTML = XMLHttpRequestObject.responseText;
			$("#product-list-item").html(data);
			// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

	});


	

	function update_order_details()
	{

		var res = confirm('Are you sure you want to update this order ?');

		if(res)
		{
			var config_url = $('#config_url').val();	

			var pos_order_id = window.localStorage.getItem('pos_order_id');

			var data_url = config_url+"pos/update_order_details/"+pos_order_id;

			var name = $('#name').val();
			var phone = $('#phone').val();
			 // alert(name);
		   $.ajax({
				  type:'POST',
				  url: data_url,
				  data:{name: name,phone: phone},
				  dataType: 'text',
				  success:function(data){
				  	var data = jQuery.parseJSON(data);

				  	if(data.message == "success")
				  	{
				  		var myTarget4 = document.getElementById("searched-div");
				  		myTarget4.style.display = 'none';
				  	
				  		// $("#sale-detail").html(data.customer_name);
				  	}
				  	else
				  	{

				  	}
				 	
				   		
				    // alert(data);
				  },
				  error: function(xhr, status, error) {
				  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				  alert(error);
				  }

			});

		}
		



	}

	function get_refund_details(pos_refund_id,pos_order_id=0,refund_note_id=null)
	{

		// alert(visit_id);
		// alert(patient_id);

		var myTarget2 = document.getElementById("dashboard-disengaged");
	    myTarget2.style.display = 'none';


	    var refund_note_id = window.localStorage.getItem('refund_note_id');
	    var pos_refund_id = window.localStorage.getItem('pos_refund_id');

	    // alert(refund_note_id);
	    if(refund_note_id > 0)
	    {
	    	var myTarget5 = document.getElementById("dashboard-engaged");
	        myTarget5.style.display = 'none';

	        var myTarget6 = document.getElementById("dashboard-payment");
	    	myTarget6.style.display = 'block';
	    	get_payment_div(pos_order_id,refund_note_id);
	    }
	    else
	    {
	    	var myTarget5 = document.getElementById("dashboard-engaged");
	        myTarget5.style.display = 'block';

	        var myTarget6 = document.getElementById("dashboard-payment");
	    	myTarget6.style.display = 'none';

	    	
	    }


		

		window.localStorage.setItem('pos_refund_id',pos_refund_id);
		// window.localStorage.setItem('patient_id',patient_id);
		// alert(order_invoice_id);
		if(refund_note_id > 0)
		{
			window.localStorage.setItem('refund_note_id',refund_note_id);
		}
		else
		{
			window.localStorage.setItem('refund_note_id','');
		}

	
		get_refund_procedures_done(pos_refund_id,pos_order_id,refund_note_id);
	}


	function get_refund_procedures_done(pos_refund_id,pos_order_id,refund_note_id = null)
	{
		var config_url = $('#config_url').val();

		var refund_note_id = window.localStorage.getItem('refund_note_id');


		
		var data_url = config_url+"pos/pos_refund_view/"+pos_refund_id;
		
		
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: data_url,
		data:{query : null},
		dataType: 'text',
		success:function(data){
		//window.alert("You have successfully updated the symptoms");
		//obj.innerHTML = XMLHttpRequestObject.responseText;
			var data = jQuery.parseJSON(data);
			if(refund_note_id > 0)
			{
				$("#visit-charges-closed").html(data.page);
			}else
			{
				$("#visit-charges").html(data.page);
			}
			// alert(data.customer_balance);
			window.localStorage.setItem('customer_id',data.customer_id);
			document.getElementById("customer_name").value = data.customer_name;
			$('#customer-balance').html(data.customer_balance);
			// document.getElementById("customer-balance").value = data.customer_balance;
		// alert(data);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

		// get_totals_div();
	}

</script>