<div class="row" style="margin-top: 10px">
	
	<div class="col-md-6">
		<h4> Counter Orders</h4>
		<div class="panel-body">
			

			<?php


			$incomplete_orders_rs = $this->pos_model->get_order_lists(0);

			$incomplete_result ='<table class="table table-condensed table-bordered">
									<thead>
										<th>#</th>
										<th>Date</th>
										<th>Order Number</th>
										<th>Items</th>
										<th>Customer</th>
										<th>Phone</th>
										<th>Created By</th>
										<th>Sale Type</th>
										<th>Sale Status</th>

									</thead>
									<tbody>';

			if($incomplete_orders_rs->num_rows() > 0)
			{
				$x=0;
				foreach ($incomplete_orders_rs->result() as $key => $value) {
					# code...
					$pos_order_id = $value->pos_order_id;
					$customer_id = $value->customer_id;
					$order_date = $value->order_date;
					$sale_type = $value->sale_type;
					$pos_order_number = $value->pos_order_number;
					$personnel_fname = $value->personnel_fname;
					$order_invoice_id = $value->order_invoice_id;
					$count = $value->total_items;
					if($sale_type == 0)
					{
						$sale = 'Cash Sale';
					}
					else if($sale_type == 1)
					{
						$sale = 'Credit Sale';
					}
					else if($sale_type == 3)
					{
						$sale = 'Quotation';
					}

					if($order_invoice_id > 0)
					{
						$color = 'success';
						$status = 'Completed';
					}
					else
					{
						$color = 'warning';
						$status = 'Incomplete';
					}
					$x++;
					$incomplete_result .='
										<tr onclick="get_order_detail('.$pos_order_id.','.$sale_type.','.$order_invoice_id.')">
											<td class="'.$color.'">'.$x.'</td>
											<td class="'.$color.'">'.date('jS M Y',strtotime($order_date)).'</td>
											<td>'.$pos_order_number.'</td>
											<td>'.$count.'</td>
											<td>-</td>
											<td>-</td>
											<td>'.$personnel_fname.'</td>
											<td>'.$sale.'</td>
											<td>'.$status.'</td>
										</tr>';

				}
			}
			$incomplete_result .='</tbody>
							</table>';

			echo $incomplete_result;
			?>

		</div>
		
		
		
	</div>
	<div class="col-md-6">
		<h4>Online Orders</h4>
		<div class="panel-body">
			


			<?php


			$complete_orders_rs = $this->pos_model->get_order_lists(1);

			// var_dump($complete_orders_rs);die();

			$complete_result ='<table class="table table-condensed table-bordered">
									<thead>
										<th>#</th>
										<th>Date</th>
										<th>Order Number</th>
										<th>Items</th>
										<th>Created By</th>
										<th>Sale Type</th>

									</thead>
									<tbody>';

			if($complete_orders_rs->num_rows() > 0)
			{
				$x=0;
				foreach ($complete_orders_rs->result() as $key => $value) {
					# code...
					# code...
					$pos_order_id = $value->pos_order_id;
					$order_invoice_id = $value->order_invoice_id;
					// $customer_id = $value->customer_id;
					$order_date = $value->order_date;
					$pos_order_number = $value->pos_order_number;
					$personnel_fname = $value->personnel_fname;
					$count = $value->total_items;
					$sale_type = $value->sale_type;

					if($sale_type == 0)
					{
						$sale = 'Cash Sale';
					}
					else if($sale_type == 1)
					{
						$sale = 'Credit Sale';
					}
					else if($sale_type == 3)
					{
						$sale = 'Quotation';
					}

					if($order_invoice_id > 0)
					{
						$color = 'success';
					}
					else
					{
						$color = 'warning';
					}
					$x++;
					$complete_result .='
										<tr onclick="get_order_detail('.$pos_order_id.','.$sale_type.','.$order_invoice_id.')">
											<td class="'.$color.'">'.$x.'</td>
											<td class="'.$color.'">'.date('jS M Y',strtotime($order_date)).'</td>
											<td>'.$pos_order_number.'</td>
											<td>'.$count.'</td>
											<td>'.$personnel_fname.'</td>
											<td>'.$sale.'</td>
										</tr>';

				}
			}
			$complete_result .='</tbody>
							</table>';

			echo $complete_result;
			?>
		</div>
		
	</div>
	
	
</div>
