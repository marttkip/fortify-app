<?php



$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y');

//doctor
// $doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->pos_model->get_personnel($this->session->userdata('personnel_id'));
// var_dump($served_by);die();
if(empty($served_by))
{
    $served_by = 'Admin';
}
// $credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
// $debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);

$payments_rs = $this->pos_model->get_payment_details($payment_id);

// var_dump($payments_rs);die();
$total_payments = 0;
$invoices = '';
$confirm_number = '';
if($payments_rs->num_rows() > 0){
    $x = $s;
    foreach ($payments_rs->result() as $key_items):
        
        $payment_method = $key_items->payment_method;
        $amount_paid = $key_items->amount_paid;
        $payment_item_id = $key_items->payment_item_id;
        $time = $key_items->time;
        $order_invoice_id = $key_items->order_invoice_id;
        $pos_order_id = $key_items->pos_order_id;
        $payment_date = $key_items->payment_date;
        $payment_method = $key_items->payment_method;
        $transaction_code = $key_items->transaction_code;
        $payment_item_amount = $key_items->payment_item_amount;
        $order_invoice_number = $key_items->order_invoice_number;
        $amount_paidd = number_format($amount_paid,2);
        $payment_service_id = $key_items->payment_service_id;
        $confirm_number = $key_items->confirm_number;
        $change = $key_items->change;
        $name = $key_items->name;
        $phone = $key_items->phone;

        $total_payments += $payment_item_amount;

        if(!empty($order_invoice_id))
        {
            $invoices .= $order_invoice_number.' ';
        }
        if(empty($phone))
        {
            $phone = '&nbsp;';
        }
        // var_dump($payment_item_amount);die();
      
    endforeach;
}
$today = date('d/m/Y',strtotime($payment_date));
?>

<!DOCTYPE html>
<html lang="en">

    <head>
          <title><?php echo $contacts['company_name'];?> | Invoice</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <style type="text/css">
            body
            {
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            
            .col-md-6 {
                width: 50%;
             }
        
            h3
            {
                font-size: 30px;
            }
            p
            {
                margin: 0 0 0px !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{ margin:0 auto;}
            .col-print-1 {width:8%;  float:left;}
            .col-print-2 {width:16%; float:left;}
            .col-print-3 {width:25%; float:left;}
            .col-print-4 {width:33%; float:left;}
            .col-print-5 {width:42%; float:left;}
            .col-print-6 {width:50%; float:left;}
            .col-print-7 {width:58%; float:left;}
            .col-print-8 {width:66%; float:left;}
            .col-print-9 {width:75%; float:left;}
            .col-print-10{width:83%; float:left;}
            .col-print-11{width:92%; float:left;}
            .col-print-12{width:100%; float:left;}

            table.table{
                border-spacing: 0 !important;
                font-size:12px;
                margin-top:0px;
                margin-bottom: 10px !important;
                border-collapse: inherit !important;
                font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            }

            table tr td
            {
                padding: 5px !important;
            }
            
            .padd
            {
                padding:20px;
            }

            @page { size: A5;font-size: 15px !important;}
            @media print
            {
                @page { size: A5;font-size: 15px !important;}
            }

            @media print
            {
                td.col1
                {
                    height: 200px !important;width:10% !important;
                }
                td.col2
                {
                    height: 200px !important;width:70% !important;
                }
                td.col3
                {
                    height: 200px !important;width:10% !important;
                }
                td.col4
                {
                    height: 200px !important;width:10% !important;
                }
            }

            .pull-left
            {
                text-align: left !important;
            }
            
            
        </style>
    </head>
    <body class="receipt_spacing">
        <div class="col-md-12">
            <div class="padd sheet padding-120mm" >
            <div class="col-md-12" style="min-height: 60px;">
                <div class="col-print-4 pull-left">
                    
                      &nbsp;
                </div>
                <div class="col-print-4 center-align">
                    <strong>
                        &nbsp;
                    </strong>
                </div>
                 <div class="col-print-2 pull-right">
                     &nbsp;
                </div>
                
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="col-print-4 left-align" style="min-height: 100px;  ">
                       <!-- <div class="center-align">&nbsp;</div> -->
                       <strong>To:</strong>

                       <?php echo $name; ?> 
                       
                    </div>
                    <div class="col-print-4">
                       <h3 class="center-align" style="padding-top:0px !important;margin-left: -20px !important;">CASH SALE</h3>
                    </div>
                    <div class="col-print-4">
                        <table class="table">
                            <tr>
                                <td >
                                    <span class="pull-left"> </span> 
                                </td>
                                <td >
                                    <span class="pull-right" ><?php echo $order_invoice_number; ?>  </span>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <span class="pull-left"> </span> 
                                </td>
                                <td >
                                    <span class="pull-right" ><?php echo $visit_date; ?>  </span>
                                </td>
                            </tr>
                                <tr>
                                <td >
                                    <span class="pull-left"></span> 
                                </td>
                                <td >
                                    <span class="pull-right" ><?php echo $phone; ?>  </span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td >
                                    <span class="pull-left"> </span> 
                                </td>
                                <td style="border-radius: 0px 0px 10px 0 !important;">
                                    <span class="pull-right" ><?php echo $served_by; ?>  </span>
                                </td>
                            </tr>
                            
                        </table>
                        
                    </div>
                </div>
            </div>
                    <div class="row">
                    <div class="col-md-12">
                        <table class="table" style="margin-top: 3px!important;">
                               <!--  <tr>
                                    <td  style="width: 20%;">
                                        <span class="pull-left">Item </span> 
                                    
                                    </td>
                                    <td  style="width: 50%;">
                                        <span class="pull-left">Description </span> 
                                    
                                    </td>
                                    <td  style="width: 10%;">
                                        <span class="pull-left">Qty </span> 
                                    
                                    </td>
                                    <td  style="width: 10%;">
                                        <span class="pull-left">Rate </span> 
                                    
                                    </td>
                                    <td  style="width: 10%;">
                                        <span class="pull-left">Amount </span> 
                                    
                                    </td>
                                </tr> -->

                                <?php


                            $order_invoice_number ='';
                            $preauth_date = date('Y-m-d');
                            $preauth_amount = '';
                            
                            $visit__rs1 = $this->pos_model->get_visit_charges_charged($pos_order_id,$order_invoice_id); 
                            
                            // var_dump($pos_order_id);die();
                            // $visit_rs = $this->accounts_model->get_visit_details($visit_id);
                            $visit_type_id = 1;
                            $close_card = 3;
                            $total_amount= 0; 
                            $days = 0;
                            $count = 0;
                            $item_list = '';
                            $description_list = "";
                            $quantity_list = "";
                            $rate_list = "";
                            $amount_list = "";
                            $total= 0;  
                            $total_units= 0;
                            $number = 0;
                            $vat_charged = 0;
                            $vat_charged =0;
                            if($visit__rs1->num_rows() > 0)
                            {                       
                                foreach ($visit__rs1->result() as $key1 => $value) :
                                    $v_procedure_id = $value->pos_order_item_id;
                                    $procedure_id = $value->service_charge_id;
                                    $product_code = $value->product_code;
                                    $pos_order_item_amount = $value->pos_order_item_amount;
                                    $units = $value->pos_order_item_quantity;
                                    $procedure_name = $value->service_charge_name;
                                    $service_id = $value->service_id;
                                    $vatable = $value->vatable;
                                    $product_id = $value->product_id;
                                    $discount = $value->discount;
                                    $vat_added = $value->vat_added;


                                    if($vat_added == 1)
                                    {
                                        $vat = 1.14;
                                        $checked_box = 'checked';
                                        $no_checked_box = '';
                                    }
                                    else
                                    {
                                        $vat = 1;
                                        $checked_box = '';
                                        $no_checked_box = 'checked';
                                    }

                                    // var_dump($discount);die();
                                    // $order_invoice_id = $value->order_invoice_id;
                                    // $visit_type_id = 1;
                                    $total= $total +(($units * $pos_order_item_amount) *$vat) - $discount;



                                    if($order_invoice_id > 0)
                                    {
                                        $text_color = "success";
                                    }
                                    else
                                    {
                                        $text_color = 'default';
                                    }

                                
                                    $checked="";
                                    $number++;

                                    $vat_charged += (($units * $pos_order_item_amount)*$vat) / 1.14;


                                    $total_units += $units;
                                    $personnel_check = TRUE;
                                    if($personnel_check AND $close_card == 3)
                                    {
                                        $checked = "<td>
                                                
                                                        <a class='btn btn-xs btn-danger' href='#' onclick='delete_procedure(".$v_procedure_id.", ".$pos_order_id.")'><i class='fa fa-trash'></i></a>
                                                    </td>";
                                    }

                                    $item_list .= strtoupper($product_code).'<br> ';
                                    $description_list .= strtoupper($procedure_name)."<br>";
                                    $quantity_list .= $units."<br>";
                                    $rate_list .= number_format($pos_order_item_amount,2)."<br>";
                                    $amount_list .= number_format(((($units * $pos_order_item_amount))-$discount),2)."<br>";
        
                                    endforeach;

                            }
                            else
                            {
                                
                            }
                            
                            $preauth_amount = $total;


                            $payments_value = $total_payments;
                            $balance = $preauth_amount - $total_payments;

                            ?>
                                <tr style="margin-top: 0px !important">
                                    <td  style="height: 190px !important;width:10% !important;" >
                                        <span class="pull-left" style="margin-left: -15px!important;"> <?php echo $item_list;?> </span> 
                                    
                                    </td>
                                    <td  style="height: 190px !important;width:60% !important;">
                                        <span class="pull-left" style="margin-left: 2px!important;">  <?php echo $description_list;?>  </span> 
                                    
                                    </td>
                                    <td  style="height: 190px !important;width:10% !important;">
                                        <span class="pull-left"> <?php echo $quantity_list;?>  </span> 
                                    
                                    </td>
                                    <td  style="height: 190px !important;width:10% !important;">
                                        <span class="pull-left" style="margin-left: -15px!important;"> <?php echo $rate_list;?> </span> 
                                    
                                    </td>
                                    <td  style="height: 190px !important;width:10% !important;">
                                        <span class="pull-left"> <?php echo $amount_list;?> </span> 
                                    
                                    </td>
                                </tr>

                                  

                                <tr>
                                    <td style="border:none !important;"></td>
                                    <td  style="border:none !important;"></td>
                                    <td colspan="3"  style="border:none !important; padding:0px !important;">
                                        <table class="table" style="margin:none !important; padding: none !important;margin-top:0px !important;">
                                        <tr>
                                            <td >
                                                <span class="pull-left"><strong></strong> </span> 
                                            </td>
                                            <td >
                                                <span class="pull-right" > <?php echo number_format($vat_charged,2); ?> </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <span class="pull-left"><strong></strong> </span> 
                                            </td>
                                            <td >
                                                <span class="pull-right" > <?php echo number_format($preauth_amount - $vat_charged ,2); ?>  </span>
                                            </td>
                                        </tr>
                                        
                                        
                               
                                        <tr>
                                            <td >
                                                <span class="pull-left"><strong></strong> </span> 
                                            </td>
                                            <td >
                                                <span class="pull-right" > <?php echo number_format($preauth_amount,2); ?> </span>
                                            </td>
                                        </tr>
                                        
                                    </table>

                                    </td>
                                </tr>
                                

                                
                            </table>
                    </div>
                </div>
                  
            
            </div>
        </div>
       
    </body>
    
</html>