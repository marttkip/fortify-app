<?php

$order_amount = $this->pos_model->get_order_invoice_amount($order_invoice_id);

$visit_rs = $this->pos_model->get_order_details($pos_order_id);

$sale_type = 0;
if($visit_rs->num_rows() > 0)
{
    foreach ($visit_rs->result() as $key => $value) {
        # code...
        $sale_type = $value->sale_type;
    }
}


if($sale_type == 0)
{

    $payment_rs = $this->pos_model->get_payment_visit($pos_order_id,$order_invoice_id);

    if($payment_rs->num_rows() > 0)
    {
        foreach ($payment_rs->result() as $key => $value) {
            # code...
            $payment_id = $value->payment_id;
            // $order_invoice_id = $value->order_invoice_id;
            $amount_paid = $value->amount_paid;
            $change = $value->change;


        }
    }

    ?>
    <div class="col-md-12">
        <div style="margin:0 auto;">
            <div class="center-align">
                <h2> Amount Paid Ksh. <?php echo number_format($amount_paid,2)?></h2>
            </div>

            
             <div class="center-align">
                <a href="<?php echo base_url().'print-receipt/'.$payment_id.'/'.$order_invoice_id;?>" target="_blank">
                    <i class="fa fa-print" style="font-size: 220px !important;"></i>
                    <br>
                    Print Receipt
                </a>
            </div>

            <div class="center-align">
                <h3> Balance Ksh. <?php echo number_format(0,2)?></h3>
            </div>
             <div class="center-align">
                <h4> Change Ksh. <?php echo number_format($change,2)?></h4>
            </div>

            

        </div>
    </div>

    <?php
    $authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
    if($authorize_invoice_changes == TRUE)
    {
        ?>
       
        <?php
    }
}
else if($sale_type == 1)
{



    ?>
    <div class="col-md-12">
        <div style="margin:0 auto;">
            <div class="center-align">
                <h2> CREDIT SALE Ksh. <?php echo number_format($order_amount,2)?></h2>
            </div>

            
             <div class="center-align">
                <a href="<?php echo base_url().'print-invoice/'.$order_invoice_id.'/'.$pos_order_id?>" target="_blank">
                    <i class="fa fa-print" style="font-size: 220px !important;"></i>
                    <br>
                    Print Invoice
                </a>
            </div>

            

        </div>
    </div>
    <?php

}
else if($sale_type == 3)
{



    ?>
    <div class="col-md-12">
        <div style="margin:0 auto;">
            <div class="center-align">
                <h2> QUOTATION SALE Ksh. <?php echo number_format($order_amount,2)?></h2>
            </div>

            
             <div class="center-align">
                <a href="<?php echo base_url().'print-quotation/'.$order_invoice_id.'/'.$pos_order_id?>" target="_blank">
                    <i class="fa fa-print" style="font-size: 220px !important;"></i>
                    <br>
                    Print Quote
                </a>
            </div>

            

        </div>
    </div>
    <?php

}


