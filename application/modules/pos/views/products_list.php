<?php

$categories_result ='
						<table class="table table-bordered ">
							<thead>
								<th>#</th>
								<th></th>
								<th>Name</th>
								<th>Code</th>
								<th>Category</th>
								<th>Brand</th>
								<th>Price</th>
								<th>Shop</th>
								<th>Store</th>
							</thead>
							<tbody>';
// var_dump($query->num_rows());die();

if(!empty($query))
{


	if($query->num_rows() > 0)
	{
		$x=0;
		foreach ($query->result() as $key => $value) {
			# code...

			$service_charge_name = $value->service_charge_name;
			$service_charge_amount = $value->service_charge_amount;
			$service_charge_id = $value->service_charge_id;
			$product_code = $value->product_code;
			$category_name = $value->category_name;
			$product_id = $value->product_id;
			$brand_name = $value->brand_name;
			$product_image = $value->product_image;


			if($product_id > 0)
			{
				// $stock_quantity = $this->inventory_management_model->get_total_stock($product_id);
				$stock_level = $this->inventory_management_model->get_stock_purchases_new($product_id,5);

                $purchases = $stock_level['dr_quantity'];
                $deductions = $stock_level['cr_quantity'];
               
                $store_stock_quantity = $purchases - $deductions;

                $stock_level = $this->inventory_management_model->get_stock_purchases_new($product_id,6);

                $shop_purchases = $stock_level['dr_quantity'];
                $shop_deductions = $stock_level['cr_quantity'];
               
                $shop_stock_quantity = $shop_purchases - $shop_deductions;
			}
			else
			{
				$store_stock_quantity = 0;
				$shop_stock_quantity = 0;
			}
			$x++;
			$categories_result .= '<tr onclick="add_service_charge_test('.$service_charge_id.','.$product_id.','.$service_charge_amount.');">
										<td>'.$x.'</td>
										<td><img src="'.base_url()."assets/products/".$product_image.'" style=" width: 50px !important;"></td>
					        			<td>'.strtoupper(strtolower($service_charge_name)).'</td>
					        			
					        			<td>'.$product_code.' </td>
					        			<td>'.strtoupper(strtolower($category_name)).'</td>
					        			<td>'.$brand_name.' </td>
					        			<td>'.number_format($service_charge_amount,2).'</td>
					        			<td>'.$shop_stock_quantity.'</td>
					        			<td>'.$store_stock_quantity.'</td>
					        		</tr>';

			// $categories_result .= '<div class="col-print-2" >
			// 		        			<a  class="btn btn-lg btn-primary" onclick="add_service_charge_test('.$service_charge_id.','.$product_id.','.$service_charge_amount.');" style="height: 125px !important;"> 
			// 		        				'.$service_charge_name.' <br>
			// 		        				COD: '.$product_code.' <br>
			// 		        				VCH: '.ucfirst($vehicle_name).'<br>
			// 		        				PRT: '.$part_no.' <br>
			// 		        				Kes. '.number_format($service_charge_amount,2).'</a>
			// 		        		</div>';
		}
	}
	else
	{
		$categories_result .= '<tr >
									<td colspan="7">Could not be able to find that product</td>
					        	</tr>';
	}
}
else
{
	$categories_result .= '<tr >
									<td colspan="7">Search a product to sale</td>
					        	</tr>';
}
$categories_result .='</tbody>
				</table>';
echo $categories_result;
?>