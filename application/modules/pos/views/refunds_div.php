<div class="row" style="margin-top: 10px">
	<div class="col-md-8">
		<div class="panel-body">
			<h4>Todays Refunds</h4>

			<?php

			$incomplete_orders_rs = $this->pos_model->get_refund_lists(3);

			$incomplete_result ='<table class="table table-condensed table-bordered">
									<thead>
										<th>#</th>
										<th>Date</th>
										<th>Order Number</th>
										<th>Items</th>
										<th>Created By</th>
										<th>Sale Type</th>

									</thead>
									<tbody>';

			if($incomplete_orders_rs->num_rows() > 0)
			{
				$x=0;
				foreach ($incomplete_orders_rs->result() as $key => $value) {
					# code...
					$pos_refund_id = $value->pos_refund_id;
					$customer_id = $value->customer_id;
					$refund_date = $value->refund_date;
					$pos_order_id = $value->pos_order_id;
					$pos_refund_number = $value->pos_refund_number;
					$personnel_fname = $value->personnel_fname;
					$count = $value->total_items;
					$refund_note_id = $value->refund_note_id;
					if($pos_order_id == 0)
					{
						$sale = 'Cash Sale';
					}
					else if($pos_order_id == 1)
					{
						$sale = 'Credit Sale';
					}
					else if($pos_order_id == 3)
					{
						$sale = 'Quotation';
					}

					if($refund_note_id > 0)
					{
						$color = 'success';
					}
					else
					{
						$color = 'warning';
					}
					$x++;
					$incomplete_result .='
										<tr onclick="get_refund_details('.$pos_refund_id.','.$pos_order_id.','.$refund_note_id.')">
											<td class="'.$color.'">'.$x.'</td>
											<td class="'.$color.'">'.date('jS M Y',strtotime($refund_date)).'</td>
											<td>'.$pos_refund_number.'</td>
											<td>'.$count.'</td>
											<td>'.$personnel_fname.'</td>
											<td>'.$sale.'</td>
										</tr>';

				}
			}
			$incomplete_result .='</tbody>
							</table>';

			echo $incomplete_result;
			?>

		</div>
		
		
		
	</div>
	
</div>
