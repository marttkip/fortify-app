<?php
$categories_rs = $this->pos_model->get_product_categories();

$categories_result ='';
if($categories_rs->num_rows() > 0)
{
	foreach ($categories_rs->result() as $key => $value) {
		# code...

		$category_name = $value->category_name;
		$category_id = $value->category_id;

		$categories_result .= '<div class="col-print-2" onclick="get_procedures_done('.$category_id.')">
				        			<a  class="btn btn-lg btn-success"> '.$category_name.'</a>
				        		</div>';
	}
}

echo $categories_result;
?>