<?php
class Pos_Reports_model extends CI_Model
{
    
	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_order_sales($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
                $this->db->select('count(pos_order_item.pos_order_item_quantity) AS total_items,SUM(pos_order_item.pos_order_item_quantity* pos_order_item.pos_order_item_amount) AS total_amount,pos_order.pos_order_id,pos_order.customer_id,pos_order.order_date,customer.customer_number,personnel.personnel_fname,order_invoice.order_invoice_id,pos_order.pos_order_number,pos_order.sale_type');
                $this->db->where($where);
                $this->db->join('order_invoice', 'order_invoice.order_invoice_id = pos_order_item.order_invoice_id', 'left');
                $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
                $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');

                $this->db->group_by('pos_order_item.pos_order_id');
                $query = $this->db->get('',$per_page,$page);

		return $query;
	}

	public function get_total_sale($table,$where)
	{

		$this->db->from($table);
                $this->db->select('SUM(pos_order_item.pos_order_item_quantity* pos_order_item.pos_order_item_amount) AS total_amount');
                $this->db->where($where);
                $query = $this->db->get('');

                $total_amount = 0;

                if($query->num_rows() > 0)
                {
                	foreach ($query->result() as $key => $value) {
                		# code...
                		$total_amount = $value->total_amount;
                	}
                }

                if(empty($total_amount))
                {
                	$total_amount = 0;
                }
		return $total_amount;

	}


        /*
        *       Retrieve visits
        *       @param string $table
        *       @param string $where
        *       @param int $per_page
        *       @param int $page
        *
        */
        public function get_all_payments_report($table, $where, $per_page, $page, $order = NULL)
        {
                //retrieve all users
                $this->db->from($table);
                $this->db->select('pos_order.pos_order_id,pos_order.customer_id,pos_order.order_date,customer.customer_number,personnel.personnel_fname,order_invoice.order_invoice_id,pos_order.pos_order_number,pos_order.sale_type,SUM(pos_payment_item.payment_item_amount) AS total_paid_amount, pos_payments.*,payment_method.payment_method');
                $this->db->where($where);
                $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
                $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');
                $this->db->group_by('pos_payments.payment_id');
                $query = $this->db->get('',$per_page,$page);

                return $query;
        }

         /*
        *       Retrieve visits
        *       @param string $table
        *       @param string $where
        *       @param int $per_page
        *       @param int $page
        *
        */
        public function get_all_debtors_report($table, $where, $per_page, $page, $order = NULL)
        {
               //retrieve all users
                $this->db->from($table);
                $this->db->select("count(pos_order_item.pos_order_item_quantity) AS total_items,SUM(pos_order_item.pos_order_item_quantity* pos_order_item.pos_order_item_amount) AS total_amount,pos_order.pos_order_id,pos_order.customer_id,pos_order.order_date,customer.customer_number,personnel.personnel_fname,order_invoice.order_invoice_id,pos_order.pos_order_number,pos_order.sale_type,(SELECT SUM(pos_payment_item.payment_item_amount) FROM pos_payments,pos_payment_item WHERE pos_payments.pos_order_id = pos_order.pos_order_id AND pos_payments.payment_id = pos_payment_item.payment_id AND pos_payments.cancel = 0)  AS total_paid_amount");
                $this->db->where($where);
                $this->db->join('order_invoice', 'order_invoice.pos_order_id = pos_order.pos_order_id', 'left');
                $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
                $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');
                // $this->db->join('pos_payments', 'pos_payments.pos_order_id = pos_order.pos_order_id', 'left');
                // $this->db->join('pos_payment_item', 'pos_payments.payment_id = pos_payment_item.payment_id', 'left');

                $this->db->group_by('pos_order_item.pos_order_id');
                $query = $this->db->get('',$per_page,$page);

                return $query;
        }

         /*
        *       Retrieve visits
        *       @param string $table
        *       @param string $where
        *       @param int $per_page
        *       @param int $page
        *
        */
        public function get_all_sales_report($table, $where, $per_page, $page, $order = NULL)
        {
               //retrieve all users
                $this->db->from($table);
                $this->db->select("pos_order_item.*,pos_order.pos_order_id,pos_order.customer_id,pos_order.order_date,customer.customer_number,personnel.personnel_fname,order_invoice.order_invoice_id,pos_order.pos_order_number,pos_order.sale_type,product.discount,service_charge.service_charge_amount,service_charge.service_charge_name,");
                $this->db->where($where);
                $this->db->join('order_invoice', 'order_invoice.pos_order_id = pos_order.pos_order_id', 'left');
                $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
                $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');

                // $this->db->group_by('pos_order_item.pos_order_id');
                $query = $this->db->get('',$per_page,$page);

                return $query;
        }

        public function get_cash_sales_summary($table,$where)
        {

            $this->db->from($table);
            $this->db->select("SUM(pos_payment_item.payment_item_amount) AS total_amount, pos_order.order_date");
            $this->db->where($where);
            // $this->db->join('order_invoice', 'order_invoice.pos_order_id = pos_order.pos_order_id', 'left');
            // $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
            // $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');

            $this->db->group_by('pos_order.order_date');
            $query = $this->db->get('');

            return $query;

        }



        public function get_credit_sales_summary($table,$where)
        {

            $this->db->from($table);
            $this->db->select("SUM(pos_order_item.pos_order_item_quantity * pos_order_item.pos_order_item_amount) AS total_amount, pos_order.order_date");
            $this->db->where($where);
            // $this->db->join('order_invoice', 'order_invoice.pos_order_id = pos_order.pos_order_id', 'left');
            // $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
            // $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');

            $this->db->group_by('pos_order.order_date');
            $query = $this->db->get('');

            return $query;

        }

        public function get_all_cash_sale_item_list($table, $where, $per_page, $page, $order = NULL)
        {
             
            $this->db->from($table);
            $this->db->select('pos_order.*,pos_order_item.*,service_charge.service_charge_name,order_invoice.order_invoice_number');
            $this->db->where($where);
            $this->db->join('service_charge', 'pos_order_item.service_charge_id = service_charge.service_charge_id', 'left');
            // $this->db->join('order_invoice', '', 'left');
            $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
            $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');
            $this->db->group_by('pos_order.pos_order_id');
           
            $query = $this->db->get('',$per_page,$page);

                return $query;
        }
        public function export_cash_sale_list_items()
        {
            $this->load->library('excel');

            $where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 0 AND order_invoice.pos_order_id = pos_order.pos_order_id';
            $table = 'pos_order_item,pos_order,order_invoice';



            $visit_search = $this->session->userdata('cash_sale_item_report_date');

            if(!empty($visit_search))
            {
                $where .= $visit_search;
            }
            else
            {
                $where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
            }
        

            $this->db->select('pos_order.*,pos_order_item.*,service_charge.service_charge_name,order_invoice.order_invoice_number');
            $this->db->where($where);
            $this->db->join('service_charge', 'pos_order_item.service_charge_id = service_charge.service_charge_id', 'left');
          
            $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
            $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');
            $this->db->group_by('pos_order.pos_order_id');
            $visits_query = $this->db->get($table);

            $title = $this->session->userdata('cash_sale_item_report_title');
            if(empty($title))
            {
                $title = 'Cash Sale item list report for '.date('Y-m-d');
            }

            if($visits_query->num_rows() > 0)
            {
                $count = 0;
                /*
                    -----------------------------------------------------------------------------------------
                    Document Header
                    -----------------------------------------------------------------------------------------
                */

                $row_count = 0;
                $report[$row_count][0] = '#';
                $report[$row_count][1] = 'Invoice Number';
                $report[$row_count][2] = 'Item';
                $report[$row_count][3] = 'Units';
                $report[$row_count][4] = 'Unit Charge';
                $report[$row_count][5] = 'Amount Charged';

                //get & display all services

                //display all patient data in the leftmost columns
                foreach($visits_query->result() as $row)
                {
                    $row_count++;
                    $total_invoiced = 0;
                    # code...
                    $order_invoice_number = $row->order_invoice_number;
                    $units = $row->pos_order_item_quantity;
                    $procedure_name = $row->service_charge_name;
                    $amount = $row->pos_order_item_amount;
                    // $visit_invoice_id = $value->visit_invoice_id;
                    // $visit_type_id = 1;
                    

                    $total_amount = $units * $amount;
                    // $total= $total +($units * $amount);
                    $count++;


                    //display the patient data
                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $order_invoice_number;
                    $report[$row_count][2] = $procedure_name;
                    $report[$row_count][3] = $units;
                    $report[$row_count][4] = number_format($amount,2);
                    $report[$row_count][5] = number_format($total_amount,2);
                  



                }
            }

            //create the excel document
            $this->excel->addArray ( $report );
            $this->excel->generateXML ($title);
        }

        public function export_cash_sale_summary(){
              $this->load->library('excel');

            $data['contacts'] = $this->site_model->get_contacts();
            $data['page_item'] = 0;
            $where = 'pos_payments.pos_order_id = pos_order.pos_order_id AND pos_payments.payment_id = pos_payment_item.payment_id  AND pos_payments.cancel = 0 AND payment_method.payment_method_id = pos_payments.payment_method_id';
            $table = 'pos_order,pos_payments,pos_payment_item,payment_method';



            $visit_search = $this->session->userdata('cash_sale_summary_report_date');

            if(!empty($visit_search))
            {
                $where .= $visit_search;
            }
            else
            {
                $where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
            }


            $query = $this->pos_reports_model->get_cash_sales_summary($table,$where);
            $title = $this->session->userdata('cash_sale_summary_report_title');
            if(empty($title))
            {
                $title = 'Cash Sale Summary report for '.date('Y-m-d');
            }

            if($query->num_rows() > 0)
            {
                $count = 0;
                /*
                    -----------------------------------------------------------------------------------------
                    Document Header
                    -----------------------------------------------------------------------------------------
                */

                $row_count = 0;
                $report[$row_count][0] = '#';
                $report[$row_count][1] = 'Date';
                $report[$row_count][2] = 'Amount';
                // $report[$row_count][1] = 'Invoice Number';
                // $report[$row_count][2] = 'Item';
                // $report[$row_count][3] = 'Units';
                // $report[$row_count][4] = 'Unit Charge';
                // $report[$row_count][5] = 'Amount Charged';

                //get & display all services

                //display all patient data in the leftmost columns
                foreach($query->result() as $row)
                {
                    $row_count++;
                    $total_invoiced = 0;
                    # code...
                    $order_date = $row->order_date;
                    $total_amount = $row->total_amount;

                    // $order_invoice_number = $row->order_invoice_number;
                    // $units = $row->pos_order_item_quantity;
                    // $procedure_name = $row->service_charge_name;
                    // $amount = $row->pos_order_item_amount;
                    // $visit_invoice_id = $value->visit_invoice_id;
                    // $visit_type_id = 1;
                    

                    // $total_amount = $units * $amount;
                    // $total= $total +($units * $amount);
                    $count++;


                    //display the patient data
                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $order_date;
                    $report[$row_count][2] = $total_amount;
                    
                    // $report[$row_count][1] = $order_invoice_number;
                    // $report[$row_count][2] = $procedure_name;
                    // $report[$row_count][3] = $units;
                    // $report[$row_count][4] = number_format($amount,2);
                    // $report[$row_count][5] = number_format($total_amount,2);
                  



                }
            }

            //create the excel document
            $this->excel->addArray ( $report );
            $this->excel->generateXML ($title);

        }


        public function export_credit_sale_summary(){
               $this->load->library('excel');

            $data['contacts'] = $this->site_model->get_contacts();
            $data['page_item'] = 0;
            $where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 1 AND order_invoice.pos_order_id = pos_order.pos_order_id';
            $table = 'pos_order_item,pos_order,order_invoice';



            $visit_search = $this->session->userdata('credit_sale_summary_report_date');

            if(!empty($visit_search))
            {
                $where .= $visit_search;
            }
            else
            {
                $where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
            }


            $query = $this->pos_reports_model->get_credit_sales_summary($table,$where);

            $title = $this->session->userdata('credit_sale_summary_report_title');
            if(empty($title))
            {
                $title = 'Credit Sale Summary report for '.date('Y-m-d');
            }

            if($query->num_rows() > 0)
            {
                $count = 0;
                /*
                    -----------------------------------------------------------------------------------------
                    Document Header
                    -----------------------------------------------------------------------------------------
                */

                $row_count = 0;
                $report[$row_count][0] = '#';
                $report[$row_count][1] = 'Date';
                $report[$row_count][2] = 'Amount';
                // $report[$row_count][1] = 'Invoice Number';
                // $report[$row_count][2] = 'Item';
                // $report[$row_count][3] = 'Units';
                // $report[$row_count][4] = 'Unit Charge';
                // $report[$row_count][5] = 'Amount Charged';

                //get & display all services

                //display all patient data in the leftmost columns
                foreach($query->result() as $row)
                {
                    $row_count++;
                    $total_invoiced = 0;
                    # code...
                    $order_date = $row->order_date;
                    $total_amount = $row->total_amount;

                    // $order_invoice_number = $row->order_invoice_number;
                    // $units = $row->pos_order_item_quantity;
                    // $procedure_name = $row->service_charge_name;
                    // $amount = $row->pos_order_item_amount;
                    // $visit_invoice_id = $value->visit_invoice_id;
                    // $visit_type_id = 1;
                    

                    // $total_amount = $units * $amount;
                    // $total= $total +($units * $amount);
                    $count++;


                    //display the patient data
                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $order_date;
                    $report[$row_count][2] = $total_amount;
                    
                    // $report[$row_count][1] = $order_invoice_number;
                    // $report[$row_count][2] = $procedure_name;
                    // $report[$row_count][3] = $units;
                    // $report[$row_count][4] = number_format($amount,2);
                    // $report[$row_count][5] = number_format($total_amount,2);
                  



                }
            }

            //create the excel document
            $this->excel->addArray ( $report );
            $this->excel->generateXML ($title);
        }



        public function export_credit_sale_list_items(){
                $this->load->library('excel');

            $where = 'pos_order_item.pos_order_id = pos_order.pos_order_id AND pos_order.sale_type = 1 AND order_invoice.pos_order_id = pos_order.pos_order_id';
            $table = 'pos_order_item,pos_order,order_invoice';


            $visit_search = $this->session->userdata('credit_sale_item_report_date');

            if(!empty($visit_search))
            {
                $where .= $visit_search;
            }
            else
            {
                $where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
            }
        

            $this->db->select('pos_order.*,pos_order_item.*,service_charge.service_charge_name,order_invoice.order_invoice_number');
            $this->db->where($where);
            $this->db->join('service_charge', 'pos_order_item.service_charge_id = service_charge.service_charge_id', 'left');
          
            $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
            $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');
            $this->db->group_by('pos_order.pos_order_id');
            $visits_query = $this->db->get($table);

            $title = $this->session->userdata('credit_sale_item_report_title');
            if(empty($title))
            {
                $title = 'Credit Sale item list report for '.date('Y-m-d');
            }

            if($visits_query->num_rows() > 0)
            {
                $count = 0;
                /*
                    -----------------------------------------------------------------------------------------
                    Document Header
                    -----------------------------------------------------------------------------------------
                */

                $row_count = 0;
                $report[$row_count][0] = '#';
                $report[$row_count][1] = 'Invoice Number';
                $report[$row_count][2] = 'Item';
                $report[$row_count][3] = 'Units';
                $report[$row_count][4] = 'Unit Charge';
                $report[$row_count][5] = 'Amount Charged';

                //get & display all services

                //display all patient data in the leftmost columns
                foreach($visits_query->result() as $row)
                {
                    $row_count++;
                    $total_invoiced = 0;
                    # code...
                    $order_invoice_number = $row->order_invoice_number;
                    $units = $row->pos_order_item_quantity;
                    $procedure_name = $row->service_charge_name;
                    $amount = $row->pos_order_item_amount;
                    // $visit_invoice_id = $value->visit_invoice_id;
                    // $visit_type_id = 1;
                    

                    $total_amount = $units * $amount;
                    // $total= $total +($units * $amount);
                    $count++;


                    //display the patient data
                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $order_invoice_number;
                    $report[$row_count][2] = $procedure_name;
                    $report[$row_count][3] = $units;
                    $report[$row_count][4] = number_format($amount,2);
                    $report[$row_count][5] = number_format($total_amount,2);
                  



                }
            }

            //create the excel document
            $this->excel->addArray ($report);
            $this->excel->generateXML ($title);
        }

        public function export_cash_sale()
        {

            $this->load->library('excel');

            $where = 'pos_order_item.pos_order_id = pos_order.pos_order_id  AND sale_type = 0 AND pos_order_item.order_invoice_id > 0';
            $table = 'pos_order_item,pos_order';

            $visit_search = $this->session->userdata('cash_sale_search');

            if(!empty($visit_search))
            {
                $where .= $visit_search;
            }
            else
            {
                $where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
            }
            
        

            $this->db->select('count(pos_order_item.pos_order_item_quantity) AS total_items,SUM(pos_order_item.pos_order_item_quantity* pos_order_item.pos_order_item_amount) AS total_amount,pos_order.pos_order_id,pos_order.customer_id,pos_order.order_date,customer.customer_number,personnel.personnel_fname,order_invoice.order_invoice_id,pos_order.pos_order_number,pos_order.sale_type');
            $this->db->where($where);
            $this->db->join('order_invoice', 'order_invoice.order_invoice_id = pos_order_item.order_invoice_id', 'left');
            $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
            $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');
            $this->db->group_by('pos_order_item.pos_order_id');
            $visits_query = $this->db->get($table);

            $title = $this->session->userdata('cash_sale_title');
            if(empty($title))
            {
                $title = 'Credit Sale report for '.date('Y-m-d');
            }

            if($visits_query->num_rows() > 0)
            {
                $count = 0;
                /*
                    -----------------------------------------------------------------------------------------
                    Document Header
                    -----------------------------------------------------------------------------------------
                */

                $row_count = 0;
                $report[$row_count][0] = '#';
                $report[$row_count][1] = 'TranNo';
                $report[$row_count][2] = 'TranDate';
                $report[$row_count][3] = 'BPName';
                $report[$row_count][4] = 'TranType';
                $report[$row_count][5] = 'TotalExclDesc';
                $report[$row_count][6] = 'Tax';
                $report[$row_count][7] = 'TotalAmount';

                //get & display all services

                //display all patient data in the leftmost columns
                foreach($visits_query->result() as $row)
                {
                    $row_count++;
                    $total_invoiced = 0;
                    # code...
                    $pos_order_number = $row->pos_order_number;
                    $units = $row->total_items;
                    // $procedure_name = $row->service_charge_name;
                    $amount = $row->total_amount;
                    $order_date = $row->order_date;
                    // $visit_invoice_id = $value->visit_invoice_id;
                    // $visit_type_id = 1;
                    

                    $total_amount = $amount;

                    $untaxed_amount = $total_amount * 0.86;
                    $tax = $total_amount * 0.14;
                    // $total= $total +($units * $amount);
                    $count++;


                    //display the patient data
                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $pos_order_number;
                    $report[$row_count][2] = date('d-M-Y',strtotime($order_date));
                    $report[$row_count][3] = 'Cash';
                    $report[$row_count][4] = 'Invoice';
                    $report[$row_count][5] = number_format($untaxed_amount,2);
                    $report[$row_count][6] = number_format($tax,2);
                    $report[$row_count][7] = number_format($total_amount,2);
                  



                }
            }

            //create the excel document
            $this->excel->addArray ( $report );
            $this->excel->generateXML ($title);

        }

        public function export_credit_sale()
        {

            $this->load->library('excel');

            $where = 'pos_order_item.pos_order_id = pos_order.pos_order_id  AND sale_type = 1 AND pos_order_item.order_invoice_id > 0';
            $table = 'pos_order_item,pos_order';

            $visit_search = $this->session->userdata('cash_sale_search');

            if(!empty($visit_search))
            {
                $where .= $visit_search;
            }
            else
            {
                $where .= ' AND pos_order.order_date = "' .date('Y-m-d') .'"';
            }
            
        

            $this->db->select('count(pos_order_item.pos_order_item_quantity) AS total_items,SUM(pos_order_item.pos_order_item_quantity* pos_order_item.pos_order_item_amount) AS total_amount,pos_order.pos_order_id,pos_order.customer_id,pos_order.order_date,customer.customer_number,personnel.personnel_fname,order_invoice.order_invoice_id,pos_order.pos_order_number,pos_order.sale_type,customer.customer_name');
            $this->db->where($where);
            $this->db->join('order_invoice', 'order_invoice.order_invoice_id = pos_order_item.order_invoice_id', 'left');
            $this->db->join('personnel', 'personnel.personnel_id = pos_order.created_by', 'left');
            $this->db->join('customer', 'customer.customer_id = pos_order.customer_id', 'left');
            $this->db->group_by('pos_order_item.pos_order_id');
            $visits_query = $this->db->get($table);

            $title = $this->session->userdata('cash_sale_title');
            if(empty($title))
            {
                $title = 'Credit Sale report for '.date('Y-m-d');
            }

            if($visits_query->num_rows() > 0)
            {
                $count = 0;
                /*
                    -----------------------------------------------------------------------------------------
                    Document Header
                    -----------------------------------------------------------------------------------------
                */

                $row_count = 0;
                $report[$row_count][0] = '#';
                $report[$row_count][1] = 'TranNo';
                $report[$row_count][2] = 'TranDate';
                $report[$row_count][3] = 'BPName';
                $report[$row_count][4] = 'TranType';
                $report[$row_count][5] = 'TotalExclDesc';
                $report[$row_count][6] = 'Tax';
                $report[$row_count][7] = 'TotalAmount';

                //get & display all services

                //display all patient data in the leftmost columns
                foreach($visits_query->result() as $row)
                {
                    $row_count++;
                    $total_invoiced = 0;
                    # code...
                    $pos_order_number = $row->pos_order_number;
                    $units = $row->total_items;
                    $customer_name = $row->customer_name;
                    $amount = $row->total_amount;
                    $order_date = $row->order_date;
                    // $visit_invoice_id = $value->visit_invoice_id;
                    // $visit_type_id = 1;
                    

                    $total_amount = $amount;

                    $untaxed_amount = $total_amount * 0.86;
                    $tax = $total_amount * 0.14;
                    // $total= $total +($units * $amount);
                    $count++;


                    //display the patient data
                    $report[$row_count][0] = $count;
                    $report[$row_count][1] = $pos_order_number;
                    $report[$row_count][2] = date('d-M-Y',strtotime($order_date));
                    $report[$row_count][3] = strtoupper($customer_name);
                    $report[$row_count][4] = 'Invoice';
                    $report[$row_count][5] = number_format($untaxed_amount,2);
                    $report[$row_count][6] = number_format($tax,2);
                    $report[$row_count][7] = number_format($total_amount,2);
                  



                }
            }

            //create the excel document
            $this->excel->addArray ( $report );
            $this->excel->generateXML ($title);

        }



}
?>
