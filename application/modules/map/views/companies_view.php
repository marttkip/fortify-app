<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4aMmzYaIn4DiPAA4dWbueAtu4wMaVwJ8&callback=initMap">
    </script>

<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul>
            </div>            
            <div class="col-lg-6 col-md-4 col-sm-12 text-right">
                
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <div id="map" style="height: 80vh; "></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$where = 'company_status = 1';
$table = 'company';
$select = '*';
$order_by = 'company_name';

$client_query = $this->map_model->get_clients_list($table,$select,$where,$order_by);
$locations='';
if($client_query->num_rows() > 0)
{
  // $locations = array();
  foreach ($client_query->result() as $key) {
    # code...
      $company_name = $key->company_name;
      $company_id = $key->company_id;      
      $company_latitude  = $key->company_latitude;
      $company_longitude  = $key->company_longitude;
      $company_description  = $key->company_description;

      $locations .= '["'.$company_name.'", '.$company_latitude.','.$company_longitude.', '.$company_id.',""],';

  }
}


?>


<script type="text/javascript">
    function initMap() {

            var map = new google.maps.Map(document.getElementById("map"), {
                              center: new google.maps.LatLng(-1.265385,36.816444),
                              zoom: 10,
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                            });


              var locations = [
                                <?php echo $locations?>
                              ];

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            for (i = 0; i < locations.length; i++) {  
              marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
              });

              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                  infowindow.setContent(locations[i][0]+''+locations[i][4]);
                  infowindow.open(map, marker);
                }
              })(marker, i));

            }
        }
    
  </script>