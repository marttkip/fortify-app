<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Map extends MX_Controller 
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('map_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}


	public function companies_map()
	{
		# code...

		$data['title'] = 'Customers';
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('companies_view', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);



	}


	public function agent_map()
	{
		# code...

		$data['title'] = 'Agents Locations';
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('map_view', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);



	}

	public function our_location()
	{
		# code...

		$data['title'] = 'Agents Coverage';
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('our_location_view', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}



}
?>
