<?php
$data['visit_id'] = $visit_id;
$mobile_personnel_id = NULL;
$v_data['mobile_personnel_id'] = $mobile_personnel_id;

//symptoms
$rs2 = $this->nurse_model->get_visit_symptoms($visit_id);
$num_rows2 = count($rs2);
$v_data['query'] = $this->nurse_model->get_notes(3, $visit_id);
$symptoms = $this->load->view('nurse/patients/notes', $v_data, TRUE);

//objective findings
$rs3 = $this->nurse_model->get_visit_objective_findings($visit_id);
$num_rows3 = count($rs2);
$v_data['query'] = $this->nurse_model->get_notes(4, $visit_id);
$objective_findings = $this->load->view('nurse/patients/notes', $v_data, TRUE);

//Assessment
$v_data['query'] = $this->nurse_model->get_notes(5, $visit_id);
$assessment = $this->load->view('nurse/patients/notes', $v_data, TRUE);

//plan
$v_data['query'] = $this->nurse_model->get_notes(6, $visit_id);
$plan = $this->load->view('nurse/patients/notes', $v_data, TRUE);
?>
<div class="row">
	<div class="col-md-12">
		<section class="panel panel-featured panel-featured-info">
			<header class="panel-heading">
				<h2 class="panel-title">Symptoms</h2>
			</header>

			<div class="panel-body">
                <div class="row">
                	<div class="col-md-6">
                    	<?php
                        if($num_rows2 > 0)
						{
							echo"<table class='table table-striped table-condensed table-bordered'>"; 
								echo"<tr>"; 
									echo"<th>";
										echo"#"; 
									echo"</th>"; 
									echo"<th>";
										echo"Symptom"; 
									echo"</th>"; 
									echo"<th>";
										echo"Yes/ No"; 
									echo"</th>"; 
									echo"<th>";
										echo"Description"; 
									echo"</th>"; 
								echo"</tr>"; 
								$count=0;
								foreach ($rs2 as $key):	
									$count++;
									$symptoms_name = $key->symptoms_name;
									$status_name = $key->status_name;
									$visit_symptoms_id = $key->visit_symptoms_id;
									$description= $key->description;
									
									echo"<tr>"; 
										echo"<td>";
											echo $count; 
										echo"</td>"; 
										echo"<td>";
											echo $symptoms_name; 
										echo"</td>"; 
										echo"<td>";
											echo $status_name; 
										echo"</td>"; 
										echo"<td>";
											echo $description; 
										echo"</td>"; 
									echo"<tr>"; 
								endforeach;
								
							echo "</table>";
						}
						?>
                    </div>
                    
                    <div class="col-md-6">
                    	<?php echo $symptoms;?>
                    </div>
                </div>
            </div>
		</section>
    </div>
</div>

<div class="row">
 	<div class="col-md-12">
		<section class="panel panel-featured panel-featured-info">
			<header class="panel-heading">
				<h2 class="panel-title">Objective findings</h2>
			</header>

			<div class="panel-body">
                <div class="row">
                	<div class="col-md-6">
                    	<?php
						if($num_rows3 > 0){
							
							echo
							"
								<div class='col-md-4' id='visit_objective_findings1'>"; 
									
							echo"<table class='table table-condensed table-striped table-bordered'>"; 
								echo"<tr>"; 
									echo"<th>";
										echo"#"; 
									echo"</th>"; 
									echo"<th>";
										echo"Group"; 
									echo"</th>"; 
									echo"<th>";
										echo"Name"; 
									echo"</th>"; 
									echo"<th>";
										echo"Description"; 
									echo"</th>"; 
								echo"</tr>"; 
								$count=0;
								
									foreach ($rs3 as $key):
										$count++;
										$objective_findings_name = $key->objective_findings_name;
										$visit_objective_findings_id = $key->visit_objective_findings_id;
										$objective_findings_class_name = $key->objective_findings_class_name;
										$description= $key->description;
										
										echo"<tr>"; 
											echo"<td>";
												echo $count; 
											echo"</td>"; 
											echo"<td>";
												echo $objective_findings_class_name; 
											echo"</td>"; 
											echo"<td>";
												echo $objective_findings_name; 
											echo"</td>"; 
											echo"<td>";
												echo $description; 
											echo"</td>"; 
										echo"<tr>"; 
									endforeach; 
									echo "
									</table>
								</div>
							</div>
							";
						}
						?>
                    </div>
                    
                    <div class="col-md-6">
                    	<?php echo $objective_findings;?>
                    </div>
                </div>
            </div>
		</section>
    </div>
</div>

<div class="row">
 	<div class="col-md-12">
		<section class="panel panel-featured panel-featured-info">
			<header class="panel-heading">
				<h2 class="panel-title">Assesment</h2>
			</header>

			<div class="panel-body">
                <div class="row">
                	<div class="col-md-12">
                		<?php echo $assessment;?>
                    </div>
                </div>
            </div>
		</section>
    </div>
</div>

<div class="row">
 	<div class="col-md-12">
		<section class="panel panel-featured panel-featured-info">
			<header class="panel-heading">
				<h2 class="panel-title">Plan</h2>
			</header>

			<div class="panel-body">
                <div class="row">
                	<div class="col-md-12">
                		<?php echo $assessment;?>
                    </div>
                </div>
                <div id="plan"></div>
                <!-- end of visit procedures -->
                <div id='visit_diagnosis_original'></div>
            </div>
		</section>
    </div>
</div>

<?php echo $this->load->view("laboratory/tests/test2", $data, TRUE); ?>

<?php echo $this->load->view("radiology/tests/test2", $data, TRUE); ?>

<?php echo $this->load->view("radiology/tests_ultrasound/test2", $data, TRUE); ?>

<div class="row">
 	<div class="col-md-12">
		<section class="panel panel-featured panel-featured-info">
			<header class="panel-heading">
				<h2 class="panel-title">Prescription</h2>
			</header>
			
			<div class="panel-body">
                <!-- vitals from java script -->
                <?php echo $this->load->view("pharmacy/display_prescription", $data, TRUE); ?>
                <!-- end of vitals data -->
            </div>
		</section>
    </div>
</div>
