<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
<!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/font-awesome/css/font-awesome.min.css"> -->

    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/chartist/css/chartist.min.css">
    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>vendor/toastr/toastr.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/lucid/";?>assets/css/color_skins.css">



    <link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.css">


    <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script> <!-- jQuery -->

  
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

</head>

<body class="theme-orange">

  <!-- WRAPPER -->
  <div id="wrapper">
    <div class="vertical-align-wrap">
      <div class="vertical-align-middle auth-main">
        <div class="auth-box">
                    <div class="top">
                        <img src="<?php echo base_url()."assets/themes/lucid";?>/images/logo-white.svg" alt="Lucid">
                    </div>
                    <div class="card">
                        <div class="header">
                            <p class="lead">Login to your account</p>
                        </div>
                        <div class="body">
                           <?php
                          if(isset($error)){
                            echo '<div class="alert alert-danger">'.$error.'</div>';
                          }
                          ?>
                          <!-- Login form -->
                          <?php 
                            echo form_open($this->uri->uri_string(),"class='form-auth-small'"); 
                          ?>
                                <div class="form-group">
                                    <label for="signin-email" class="control-label sr-only">Email</label>
                                    <input type="email" class="form-control" id="username" name="username" value="" placeholder="Username" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="signin-password" class="control-label sr-only">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" value="" placeholder="Password" autocomplete="off">
                                </div>
                                <div class="form-group clearfix">
                                    <label class="fancy-checkbox element-left">
                                        <input type="checkbox">
                                        <span>Remember me</span>
                                    </label>                
                                </div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                                <div class="bottom">
                                    <!-- <span class="helper-text m-b-10"><i class="fa fa-lock"></i> <a href="page-forgot-password.html">Forgot password?</a></span> -->
                                    <!-- <span>Don't have an account? <a href="page-register.html">Register</a></span> -->
                                </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END WRAPPER -->


		

</body>
</html>