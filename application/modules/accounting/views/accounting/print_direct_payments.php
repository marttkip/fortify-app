

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Petty cash</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
        	<div class="col-xs-12">
            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
    	<div class="row">
        	<div class="col-md-12 center-align receipt_bottom_border">
            	<strong>
                	<?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
                </strong>
            </div>
        </div>
        <?php
        ?>
      <div class="row receipt_bottom_border" >
        	<div class="col-md-12 center-align">
            	<strong><?php echo $title?></strong>
            </div>
        </div>
        
    	<div class="row">
        	<div class="col-md-12">
        	<?php
        		// var_dump($account); die();
            	$query = $this->petty_cash_model->get_direct_purchases();
			
			?>			
				<table class="table table-hover table-bordered ">
                        <thead>
                            <tr>
                              <th>#</th>   
                              <th>Date</th>      
                              <th>Document No.</th>              
                              <th>Account From</th>
                              <th>Payment To</th>
                              <th>Description</th>
                              <th >Amount</th>                    
                            </tr>
                         </thead>
                        <tbody>
                            <?php
                                $result = '';
                                // var_dump($query); die();
                                $total_amount_paid = 0;
                               if($query->num_rows() > 0)
                               {
                                 $x=0;
                                    foreach ($query->result() as $key => $value) {
                                        # code...
                                        $account_from_id = $value->account_from_id;
                                        $account_to_type = $value->account_to_type;
                                        $account_to_id = $value->account_to_id;
                                        $receipt_number = $value->receipt_number;
                                        $account_payment_id = $value->account_payment_id;
                                         $payment_date = $value->payment_date;
                                         $created = $value->created;
                                        $amount_paid = $value->amount_paid;
                                        $account_payment_description = $value->account_payment_description;

                                        $account_from_name = $this->petty_cash_model->get_account_name($account_from_id);
                                        if($account_to_type == 1)
                                        {
                                            $payment_type = 'Transfer';
                                            $account_to_name = $this->petty_cash_model->get_account_name($account_to_id);
                                        }
                                        else if($account_to_type == 3)
                                        {
                                            // doctor payments
                                            $payment_type = "Doctor Payment";
                                            $account_to_name = $this->petty_cash_model->get_doctor_name($account_to_id);
                                        }
                                        else if($account_to_type == 2)
                                        {
                                            // creditor
                                            $payment_type = "Creditor Payment";
                                            $account_to_name = $this->petty_cash_model->get_creditor_name($account_to_id);
                                        }
                                        else if($account_to_type == 4)
                                        {
                                            // expense account
                                            $payment_type = "Direct Expense Payment";
                                            $account_to_name = $this->petty_cash_model->get_account_name($account_to_id);
                                        }


                                        // if($created == date('Y-m-d'))
                                        // {
                                            $add_invoice = '<td><a onclick="edit_direct_payment('.$account_payment_id.')"   class="btn btn-sm btn-success fa fa-pencil"></a></td>
                                                            <td><a href="'.site_url().'delete-payment-ledger-entry/'.$account_payment_id.'"  onclick="return confirm(\'Are you sure you want to delete this payment ? \')" class="btn btn-sm btn-danger fa fa-trash" ></a></td>
                                                            ';
                                        // }
                                        // else
                                        // {
                                        //     $add_invoice = '';
                                        // }
                                        $total_amount_paid += $amount_paid;
                                        $x++;

                                        $result .= '<tr>
                                                        <td>'.$x.'</td>
                                                        <td>'.$payment_date.'</td>
                                                        <td>'.strtoupper($receipt_number).'</td>
                                                        <td>'.$account_from_name.'</td>
                                                        <td>'.$account_to_name.'</td>
                                                        <td>'.$account_payment_description.'</td>
                                                        <td>'.number_format($amount_paid,2).'</td>
                                                    </tr>';

                                    }

                                    $result .= '<tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Total</th>
                                                        <td>'.number_format($total_amount_paid,2).'</td>
                                                        
                                                    </tr>';
                               }
                               echo $result;
                            ?>
                        </tbody>
                    </table>
            </div>
        </div>
        
    	<div class="row" style="font-style:italic; font-size:11px;">
        	<div class="col-sm-12">
                <div class="col-sm-10 pull-left">
                    <strong>Prepared by: </strong><?php echo $served_by;?> 
                </div>
                <div class="col-sm-2 pull-right">
                    <?php echo date('jS M Y H:i a'); ?>
                </div>
            </div>
        	<div class="col-sm-12" style="margin-top:60px;">
                <div class="col-sm-2">
                	<strong>Checked by: </strong>
                </div>
                <div class="col-sm-4">
                	
                </div>
            </div>
        	<div class="col-sm-12" style="margin-top:60px;">
                <div class="col-sm-2">
                	<strong>Approved by: </strong>
                </div>
                <div class="col-sm-4">
                	
                </div>
            </div>
        </div>
    </body>
    
</html>