<?php

class Customers_model extends CI_Model 
{
	


/*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	/*
	*	Retrieve all patients
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_customers($table, $where, $per_page, $page, $items = '*')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('customer.*, package.package_name');
		$this->db->where($where);
		$this->db->join('customer_packages','customer_packages.customer_id = customer.customer_id AND customer_packages.customer_package_status = 1','LEFT');
		$this->db->join('package','package.package_id = customer_packages.package_id', 'LEFT');
		// $this->db->order_by('created','ASC');
		// $this->db->limit(1);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	public function get_customer_name($id)
	{
		//retrieve all users
		$this->db->from('customer');
		// $this->db->select($items);
		$this->db->where('customer_id = '.$id);
		$query = $this->db->get();

		$row = $query->row();

		$customer_name = $row->customer_name;

		return $customer_name;
	}

	public function get_customer_details($id)
	{
		//retrieve all users
		$this->db->from('customer');
		// $this->db->select($items);
		$this->db->where('customer_id = '.$id);
		$query = $this->db->get();

		return $query;
	}

	public function get_child_accounts($id)
	{
		//retrieve all users
		$this->db->from('customer');
		// $this->db->select($items);
		$this->db->where('parent_customer_id = '.$id);
		$query = $this->db->get();

		return $query;
	}

	public function get_customer_invoice($id)
	{
		//retrieve all users
		$this->db->from('customer_invoice');
		// $this->db->select($items);
		$this->db->where('customer_id = '.$id.' AND invoice_status = 1');
		$query = $this->db->get();

		return $query;
	}

	public function get_invoice($id)
	{
		//retrieve all users
		$this->db->from('customer_invoice');
		// $this->db->select($items);
		$this->db->where('invoice_id = '.$id);
		$query = $this->db->get();

		return $query;
	}

	public function get_customer_reviews($id)
	{
		$this->db->from('customer_responses');
		$this->db->where('customer_id = '.$id);
		$this->db->join('responses','responses.response_id = customer_responses.response_id');
		$this->db->join('user','user.user_id = customer_responses.agent_id');
		// $this->db->join('customer','customer.customer_id = customer_responses.customer_id');
		$query = $this->db->get();
		return $query;
	}
}


?>