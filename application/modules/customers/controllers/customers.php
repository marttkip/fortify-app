<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL);
class Customers extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();

		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('reception/reception_model');
		$this->load->model('dental/dental_model');
		$this->load->model('reception/database');
		$this->load->model('messaging/messaging_model');
		$this->load->model('admin/email_model');
		$this->load->model('customers/customers_model');

		$this->load->model('auth/auth_model');
		
	}

	public function dashboard()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('dashboard', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function customer_list()
	{

		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$where = 'customer.customer_id > 0';
		
		$table = 'customer';
		$customers_search = $this->session->userdata('customers_search');
		
		if(!empty($customers_search))
		{
			$where .= $customers_search;
		}
		
		
		$segment = 3;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'customers/customer-accounts';
		$config['total_rows'] = $this->customers_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->customers_model->get_all_customers($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		
		$data['title'] = 'Clients';
		$v_data['title'] = 'Clients';
		$v_data['module'] = 1;
		
		
		$data['content'] = $this->load->view('customers_list', $v_data, true);
		
		
		
		$this->load->view('admin/templates/general_page', $data);


	}

	public function customer_detail($customer_id)
	{
		$query = $this->customers_model->get_customer_details($customer_id);
		$child_accounts = $this->customers_model->get_child_accounts($customer_id);
		$customer_invoices = $this->customers_model->get_customer_invoice($customer_id);
		$customer_reviews = $this->customers_model->get_customer_reviews($customer_id);
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['children']= $child_accounts;
		$v_data['invoices'] = $customer_invoices;
		$v_data['reviews'] = $customer_reviews;

		$data['content'] = $this->load->view('customer_detail', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}


	public function invoice_details($id)
	{
		
		
		$query = $this->customers_model->get_invoice($id);
		
		$data['title'] = $this->site_model->display_page_title();
		$v_data['query'] = $query;
		$data['content'] = $this->load->view('invoice_details', $v_data, true);		
		
		$this->load->view('admin/templates/general_page', $data);


	}
}
?>