<?php

if ($query->num_rows() > 0) 
{
	foreach ($query->result() as $row)
	{
		$invoice_number = $row->invoice_number;
		$amount = $row->invoice_amount;
		$date = $row->created;
		$customer_id = $row->customer_id;
		$status = $row->invoice_status;

		$customer_name = $this->customers_model->get_customer_name($customer_id);
	}
}

?>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Invoice</div>

  <!-- Table -->
  <table class="table">
  	<th>Invoice Number</th>
  	<th>Customer Name</th>
  	<th>Amount</th>
  	<th>Date</th>
  	<th>Status</th>
  	<tr>
  		<td><?php echo $invoice_number ?></td>
  		<td><?php echo $customer_name ?></td>
  		<td><?php echo $amount ?></td>
  		<td><?php echo date('jS M Y',strtotime($date)) ?></td>
  		<td><?php echo $status = 1? 'Active' : 'Inactive';  ?></td>
  	</tr>

  </table>

  <div class="pull-right">
					<a href="<?php echo site_url().'clients/client-detail/'.$customer_id.''?>" class="btn btn-xs btn-warning"><i class="fa fa-arrow-left"></i> Back</a>
				</div>
</div>