<?php
	$children_accounts = '';
	$invoice_list = '';
	$review_result = '';



if ($query->num_rows() > 0) 
{
	foreach ($query->result() as $row)
	{
		$customer_id = $row->customer_id;
		$customer_name = $row->customer_name;
		$customer_username = $row->username;
		$customer_phone = $row->customer_phone;
		$customer_email = $row->customer_email;
		$customer_postcode = $row->customer_post_code;
		$customer_address = $row->customer_address;
		$customer_city = $row->customer_city;
		$customer_account_number = $row->account_number;
		$parent_id = $row->parent_customer_id;
		$registration_date = $row->created;
		$parent_name;

		if($parent_id > 0){
			$parent_name = $this->customers_model->get_customer_name($parent_id);
				
		} else {
			$parent_name = '';
		}

	}

}

if($children->num_rows() > 0){
	foreach ($children->result() as $row)
	{

		$name = $row->customer_name;
		$id = $row->customer_id;
		$children_accounts .='
			<a href="'.site_url().'clients/client-detail/'.$id.'" class="list-group-item"><i class="fa fa-user"></i> '.$name.'</a>
		
		';
	}

} 

if($invoices->num_rows() > 0){
	$a = 1;
	foreach ($invoices->result() as $row)
	{

		$amount = $row->invoice_amount;
		$id = $row->invoice_id;
		$date = $row->created;

		$invoice_list .='
		<tr>
			<td>'.$a.'</td>
			<td>'.$id.'</td>
			<td>'.date('jS M Y',strtotime($date)).'</td>
			<td>'.$amount.'</td>
			<td><a href="'.site_url().'invoice-details/'.$id.'" class="btn btn-xs btn-warning">View</a></td>
		</tr>
		';
		$a++;
	}

} 

if($reviews->num_rows()>0){
	$page =1;
	foreach ($reviews->result() as $row) {


        $customer_id = $row->customer_id;
        $agent_id = $row->user_name;
        $date = $row->date;
        $remarks = $row->remarks;
        $response = $row->value;
        $job_id = $row->job_id;
        // $customer_name = $row->customer_name;

		$review_result = '
			<tr>
                <td>'.$page.'</td>
                <td><span>'.$agent_id.'</span></td>
                <td><span><a href="'.site_url().'customers/customer-detail/'.$customer_id.'">'.$customer_name.'</a></span></td>
                <td><span class="badge badge-success">'.$response.'</span></td>
                <td>'.$remarks.'</td>
                <td>'.$job_id.'</td>

            </tr>
		';
	}
}


?>




<div class="row">
	<div class="card col-md-2">
		<section class="">
			<div class="card-body">
				<div class="thumb-info mb-3">
					<img src="https://via.placeholder.com/200" class="rounded img-fluid" alt="John Doe">
					<div class="thumb-info-title">
						<span class="thumb-info-inner"><?php echo $customer_name ;?></span>
						<span class="thumb-info-type"><?php echo $customer_account_number;?></span>
					</div>
				</div>

				<div class="widget-toggle-expand mb-3">
					
					<div class="widget-content-expanded">
						<ul class="mt-3" style="list-style: none;padding: 0px !important;">
							<li><strong>Phone: </strong><br> <?php echo $customer_phone;?></li>
							<li><strong>Email: </strong><br><?php echo $customer_email;?></li>
							<li><strong>Reg Date: </strong><br> <?php echo date('jS M Y',strtotime($registration_date));?></li>

						</ul>
					</div>
				</div>


			</div>
		</section>
	</div>
	<div class="col-md-10">
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a href="<?php echo site_url().'clients'?>" class="btn btn-xs btn-warning"><i class="fa fa-arrow-left"></i> Back to customer </a>
				</div>
				
			</div>
		</div>
		<div class="card tabs">
			<ul class="nav nav-tabs tabs-primary">
				<li class="nav-item active">
					<a class="nav-link" href="#visit_info" data-toggle="tab" >General Information</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="#overview" data-toggle="tab">Orders</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="#reviews" data-toggle="tab">Reviews</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="#invoice" data-toggle="tab">Invoices</a>
				</li>
			</ul>
			<div class="tab-content">

				
				<div id="visit_info" class="tab-pane active">
				
						
						<div class="row">
							<div class="col-md-6">
								<div class="panel panel-default">
  									<div class="panel-heading">
    									<h3 class="panel-title">Personal Details</h3>
 									</div>
 									<div class="panel-body">
   									<div class="list-group">
										<ul class="list-group">
											<li class="list-group-item">Name: <?php echo $customer_name;?></li>
											<li class="list-group-item">Phone: <?php echo $customer_phone;?></li>
											<li class="list-group-item">Email: <?php echo $customer_email;?></li>
											<li class="list-group-item">Post Code: <?php echo $customer_postcode;?></li>
											<li class="list-group-item">Address: <?php echo $customer_address;?></li>
											<li class="list-group-item">City: <?php echo $customer_city;?></li>
											<li class="list-group-item">Account Number: <?php echo $customer_account_number;?></li>
											<li class="list-group-item">Parent: <?php echo $parent_name;?></li>
											<li class="list-group-item">Registration Date: <?php echo date('jS M Y',strtotime($registration_date));?></li>									
										</ul>									
									</div>
  									</div>
								</div>	
								
							</div>
							<div class="col-md-6">

								<div class="panel panel-default">
  									<div class="panel-heading">
    									<h3 class="panel-title">Children Accounts</h3>
 									</div>
 									<div class="panel-body">
   									<div class="list-group">
										<?php echo $children_accounts; ?>										
									</div>
  									</div>
								</div>	
							</div>							
						</div>

				</div>
				<div id="overview" class="tab-pane >">
					<div class="padd">
						
						<div id="general-detail"></div>
						
					</div>
					

				</div>
				<div id="reviews" class="tab-pane ">
					<div class="padd">
						<!-- <div class="table-responsive"> -->
                        <table class="table table-hover m-b-0">
                            <thead class="thead-dark">
                                <tr>
                                <th>#</th>
                                <th>Agent Name</th>
                                <th>Customer</th>
                                <th>Response</th>
                                <th>Remarks</th>
                                <th>Job</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo $review_result; ?>
                            </tbody>
                        </table>
						
						<!-- <div id="general-detail"></div> -->
						
					</div>
				</div>

				<div id="invoice" class="tab-pane ">
					<div class="padd">
						
						<div id="general-detail">
							<div class="panel panel-default">
  								<!-- Default panel contents -->
  								<div class="panel-heading">Invoices</div>
  								<table class="table table-hover">
  									<th>#</th>
  									<th>Invoice ID</th>
  									<th>Date</th>
  									<th>Amount</th>
  									<?php echo $invoice_list;  ?>
  								</table>
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
	</div>

</div>