<?php //echo $this->load->view('search/search_clients', '', TRUE);?>

<?php
$result ='';
//if users exist display them
if ($query->num_rows() > 0) 
{
	$count = $page;
	

	
	// $personnel_query = $this->personnel_model->get_all_personnel();
	
	foreach ($query->result() as $row)
	{

		$customer_id = $row->customer_id;
		$customer_phone = $row->customer_phone;
		$customer_email = $row->customer_email;
		$customer_name = $row->customer_name;
		$account_number = $row->account_number;
		$created = $row->created;
		$package_name = $row->package_name;
		$parent_id = $row->parent_customer_id;
		// $patient_id = $row->patient_id;
	
		$count++;
		$parent;



		if($parent_id == 0){
			$parent = '<td></td>';
			
		} else {
			$parent_name = $this->customers_model->get_customer_name($parent_id);
			$parent = '<td><a href="'.site_url().'clients/client-detail/'.$parent_id.'" class="btn btn-xs btn-info"><i class="fa fa-user"></i> '.$parent_name.'</a></td>';

		}
		
	

	
			$result .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$account_number.'</td>

					<td>'.$customer_name.'</td>
					<td>'.$customer_phone.'</td>
					<td>'.$customer_email.'</td>
					<td>'.date('jS M Y',strtotime($created)).'</td>
					<td>Active</td>
					<td>'.$package_name.'</td>
					'.$parent.'
					<td><a href="'.site_url().'customers/customer-detail/'.$customer_id.'" class="btn btn-outline-success">View Profile</a></td>
				</tr>
	
					  
			';
		
	}
	
	$result .= 
	'
				 
	';
}

else
{
	$result .= "There are no clients registered";
}

?>


    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-12">
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                        <li class="breadcrumb-item active"><?php echo $title;?></li>
                    </ul>
                </div>            
                <div class="col-lg-6 col-md-4 col-sm-12 text-right">
                    <div class="bh_chart hidden-xs">
                        <div class="float-left m-r-15">
                            <small>Visitors</small>
                            <h6 class="mb-0 mt-1"><i class="icon-user"></i> 1,784</h6>
                        </div>
                        <span class="bh_visitors float-right">2,5,1,8,3,6,7,5</span>
                    </div>
                    <div class="bh_chart hidden-sm">
                        <div class="float-left m-r-15">
                            <small>Visits</small>
                            <h6 class="mb-0 mt-1"><i class="icon-globe"></i> 325</h6>
                        </div>
                        <span class="bh_visits float-right">10,8,9,3,5,8,5</span>
                    </div>
                    <div class="bh_chart hidden-sm">
                        <div class="float-left m-r-15">
                            <small>Chats</small>
                            <h6 class="mb-0 mt-1"><i class="icon-bubbles"></i> 13</h6>
                        </div>
                        <span class="bh_chats float-right">1,8,5,6,2,4,3,2</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><?php echo $title;?></h2>
                        <ul class="header-dropdown">
                            <li><a href="javascript:void(0);" class="btn btn-info" data-toggle="modal" data-target="#addcontact">Add New</a></li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table  table-striped table-bordered">
								<thead>
									<tr>
										<th>#</th>
										<th>Account Number</th>
										<th>Name</th>
										<th>Phone</th>
										<th>Email</th>
										<th>Registration Date</th>
										<th>Status</th>
										<th>Package</th>
										<th>Parent</th>
										<th>Profile</th>
									</tr>
								</thead>
								<tbody>
									<?php echo $result;?>
									
								</tbody>
							</table>
                        </div>
                        <div class="widget-foot">
                            
							<?php if(isset($links)){echo $links;}?>
				        
				            <div class="clearfix"></div> 
				        
				        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


