<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Agents extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			// header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header("Access-Control-Allow-Origin: *");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		
		$this->load->model('agents_model');
		$this->load->model('email_model');
		$this->load->model('customers_model');
		$this->load->model('messaging/messaging_model');
		
		// $this->load->library('Mandrill', $this->config->item('mandrill_key'));
	}

	public function login_agent() 
	{
		$agent_number = $this->input->post('agent_number');
		$password = $this->input->post('password');
		
		$result = $this->agents_model->validate_agent($agent_number, $password);
	
		if($result != FALSE)
		{
			//create user's login session
			$newdata = array(
				   'agent_login_status' => TRUE,
				   'agent_name'     	=> $result[0]->user_name,
				   'agent_id'  			=> $result[0]->user_id,
				   'day_login'  		=> TRUE
			   );
			$this->session->set_userdata($newdata);
			
			$response['message'] = 'success';
			$response['result'] = $newdata;
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'You have entered incorrect details. Please try again';
		}
		
		echo json_encode($response);
	}

	public function set_patrol_status($status, $user_id)
	{
		$table = "user_status";
		$this->db->where('user_id', $user_id);
		if($this->db->update($table, array('user_status_status' => 0)))
		{
			$data = array(
				'user_status_date' => date('Y-m-d H:i:s'), 
				'user_id' => $user_id, 
				'user_status_availability' => $status, 
				'user_status_status' => 1
			);
			if($this->db->insert($table, $data))
			{
				$result['message'] = 'success';
			}
			
			else
			{
				$result['message'] = 'error';
			}
		}
			
		else
		{
			$result['message'] = 'error';
		}
		
		echo json_encode($result);
	}

	public function reverse_geocode($latitude, $longitude)
	{
		$response = $this->agents_model->reverse_geocode($latitude, $longitude);
		//var_dump($response);// json_decode($response);
		
		$decoded = json_decode($response, true);
		$result['message'] = 'success';
		$result['result'] = $decoded['results'][0]["formatted_address"];
		echo json_encode($result);
	}

	public function send_agent_location($user_id, $latitude, $longitude)
	{
		$data = array(
			'latitude' => $latitude, 
			'longitude' => $longitude
		);
		
		$this->db->where('user_id', $user_id);
		if($this->db->update('user', $data))
		{
			$result['message'] = 'success';
		}
		
		else
		{
			$result['message'] = 'error';
		}
		
		echo json_encode($result);
	}
	public function add_registration_id()
	{
		$registration_id = $this->input->post('registration_id');
		$personnel_id = $this->input->post('personnel_id');
		
		if(($personnel_id != NULL) && ($personnel_id != 'null') && ($registration_id != NULL) && ($registration_id != 'null'))
		{
			$where = array('user_id' => $personnel_id);
			$data2 = array('registration_id' => $registration_id);
			
			$this->db->where($where);
			if($this->db->update('user', $data2))
			{
				$response['message'] = 'success';
			}
			
			else
			{
				$response['message'] = 'fail 2';
			}
		}
			
		else
		{
			$response['message'] = $personnel_id;
		}
		
		echo json_encode($response);
	}
	public function get_all_requests()
	{
		$requests = $this->agents_model->get_all_requests();
		$rides = array();
		if($requests->num_rows() > 0)
		{
			foreach($requests->result() as $res)
			{
				$ride_data = array(
					'from_route' => $res->from_route,
					'to_route' => $res->to_route,
					'distance' => $res->distance,
					'duration' => $res->duration,
					'request_lat' => $res->request_lat,
					'request_lng' => $res->request_lng,
					'destination_lat' => $res->destination_lat,
					'destination_lng' => $res->destination_lng,
					'customer_first_name' => $res->customer_first_name,
					'customer_phone' => $res->customer_phone,
					'ride_type_id' => 1,
					'ride_id' => $res->call_id
				);
				
				array_push($rides, $ride_data);
			}
			$response['message'] = 'success';
			$response['result'] = $rides;
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'There are no calls';
		}
		
		echo json_encode($response);
	}



}
?>