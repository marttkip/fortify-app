<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL);
class Login extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			// header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header("Access-Control-Allow-Origin: *");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTPS_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTPS_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		
		$this->load->model('login_model');
		$this->load->model('email_model');
		$this->load->model('customers_model');
		$this->load->model('messaging/messaging_model');
		
		// $this->load->library('Mandrill', $this->config->item('mandrill_key'));
	}
	
	public function get_logged_in_member()
	{
		
		$newdata = array(
                   'personnel_email'     		=> $this->session->userdata('personnel_email'),
                   'personnel_first_name'     	=> $this->session->userdata('personnel_first_name'),
                   'personnel_id'  			=> $this->session->userdata('personnel_id'),
                   'personnel_username'  			=> $this->session->userdata('personnel_username')
               );
		
		$response['result'] = $newdata;
		
		echo json_encode($newdata);
	}
	
	public function login_member() 
	{

		// var_dump('sdada');die();
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		// if(($username == 'amasitsa') && ($password == 'r6r5bb!!'))
		// {
		// 	$newdata = array(
		// 	   'login_status' => TRUE,
		// 	   'first_name'   => 'Alvaro',
		// 	   'username'     => 'amasitsa',
		// 	   'personnel_type_id'     => '2',
		// 	   'personnel_id' => 0,
		// 	   'branch_code'   => 'OSH',
		// 	   'branch_name'     => 'KISII',
		// 	   'branch_id' => 2
		//    );

		// 	$this->session->set_userdata($newdata);
			
		// 	$response['message'] = 'success';
		// 	$response['result'] = $newdata;
			
		// }
		
		// else
		// {
			$result = $this->login_model->validate_member($username, $password);
		
			if($result != FALSE)
			{
				//create user's login session
				$newdata = array(
					   'customer_login_status'    => TRUE,
					   'customer_email'     		=> $result[0]->customer_email,
					   'customer_name'     	=> $result[0]->customer_name,
					   'customer_id'  			=> $result[0]->customer_id,
					   'username'  			=> $result[0]->username,
					   'first_time_login' => $result[0]->first_time_login
				   );
				$this->session->set_userdata($newdata);
				
				$response['message'] = 'success';
				$response['result'] = $newdata;
			}
			
			else
			{
				$response['message'] = 'fail';
				$response['result'] = 'You have entered incorrect details. Please try again';
			}
		// }
		echo json_encode($response);
	}
	public function login_home_owner() 
	{
		$home_owner_email = $this->input->post('owner_email');
		$home_owner_phone = $this->input->post('owner_phone');
		$result = $this->login_model->validate_owner($home_owner_email,$home_owner_phone);
	
		if($result != FALSE)
		{
			//create user's login session
			$newdata = array(
                   'home_owner_login_status'    => TRUE,
                   'home_owner_email'     		=> $result[0]->home_owner_email,
                   'home_owner_phone'     		=> $result[0]->home_owner_phone_number,
                   'home_owner_name'     	=> $result[0]->home_owner_name,
                   'home_owner_id'  			=> $result[0]->home_owner_id,
                   'email_login'  			=> $result[0]->email_login,
                   'phone_login'  			=> $result[0]->phone_login
               );
			$this->session->set_userdata($newdata);
			
			$response['message'] = 'success';
			$response['result'] = $newdata;
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'You have entered incorrect details. Please try again';
		}
		
		echo json_encode($response);die();
	}



	public function signup_member() 
	{
		

		$this->form_validation->set_rules('name', 'Name','required|xss_clean');
		$this->form_validation->set_rules('id_number', 'Id number','required|xss_clean');
    	$this->form_validation->set_rules('phone_number', 'Phone Number','required|xss_clean');
		$this->form_validation->set_rules('email_address', 'Email Address','required|xss_clean');
		// $this->form_validation->set_rules('username', 'username','required|xss_clean');
		$this->form_validation->set_rules('password', 'Password','required|xss_clean');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password','required|xss_clean');
		
		if ($this->form_validation->run())
		{
			//update order
			$password = $this->input->post('password');
			$confirm_password = $this->input->post('confirm_password');
			if($password == $confirm_password)
			{

				$referenece_code = $this->input->post('referenece_code');
				$checked = FALSE;
				$customer_id = NULL;
				if(!empty($referenece_code))
				{

					// check if the account is activated 

					$query = $this->login_model->check_parent_account($referenece_code);
					
					if($query->num_rows() > 0)
					{
						foreach ($query->result() as $key => $value) {
							# code...
							$customer_id = $value->customer_id;
							$package_users = $value->package_users;
							$customer_package_charge = $value->customer_package_charge;
							// $customer_ = $value->customer_;

							

							if($package_users > 1)
							{
								// check count of the number of users
								$package_users_rs = $this->login_model->get_child_accounts($customer_id);

								$count = $package_users_rs->num_rows();


								if($package_users > $count)
								{
									$checked = TRUE;
								}
								else
								{
									$return_check = 'Sorry the user seem not to be accepting any more users to their account';
								}
							}
							else
							{
								$return_check = 'Sorry the user seem not to be accepting any more users to their account';
							}
						}
					}
					else
					{
						$return_check = 'Sorry could not find a record with the reference code provided. Please try again';
					}

				}
				else
				{
					$checked = TRUE;
				}

				if($checked == TRUE)
				{
					if($this->login_model->register_account($customer_id))
					{
						// $this->session->set_userdata('success_message', 'Cheque successfully writted to account');


						// redirect('accounting/purchases');
						$response['message'] = 'success';
						$response['result'] = 'You have successfully registered';
					}

					else
					{
						// $this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
						$response['message'] = 'fail';
						$response['result'] = 'You have entered incorrect details. Please try again';
					}
				}
				else
				{
					$response['message'] = 'fail';
					$response['result'] = $return_check;
				}
				
			}
			else
			{
				$response['message'] = 'fail';
				$response['result'] = 'Sorry password entered do not match';
			}
		}
		else
		{
			// $this->session->set_userdata('error_message', validation_errors());
			$response['message'] = 'fail';
			$response['result'] = strip_tags(validation_errors());
		}



		echo json_encode($response);
	}

	public function profile_details($customer_id) 
	{
		$v_data['customer_id'] = $customer_id;

		$array['first_time_login'] = 1;

		$this->db->where('customer_id',$customer_id);
		$this->db->update('customer',$array);

		$customer_package_rs = $this->login_model->customer_package_detail($customer_id);

		$v_data['customer_package_rs'] = $customer_package_rs;

		$profile_rs = $this->login_model->get_customer_details($customer_id);


		$customer_invoice_rs = $this->customers_model->get_customer_invoices($customer_id);

		$v_data['customer_invoice_rs'] = $customer_invoice_rs;

		
		$data['message'] = 'success'; 
		

		$v_data['profile_rs'] = $profile_rs;
		$data['message'] = 'success'; 
		$data['profile_details'] = $this->load->view('profile_details', $v_data,true);
		$data['account_details'] = $this->load->view('account_details', $v_data,true);
		$data['customer_invoices'] = $this->load->view('customers/customer_invoices', $v_data,true);
		echo json_encode($data);
	}

	public function update_customer_package($customer_id,$package_id,$package_charge=0)
	{
		$update_array['customer_package_status'] = 0;

		$this->db->where('customer_id',$customer_id);
		$this->db->update('customer_packages',$update_array);

		if($package_charge == 0)
		{
			$package_rs = $this->login_model->get_package_detail($package_id);
		

			if($package_rs->num_rows() > 0)
			{
				foreach ($package_rs->result() as $key => $value) {
					# code...
					$array['customer_package_charge'] = $value->package_charge;
				}
			}
			
		}
		else
		{
			$array['customer_package_charge'] = $package_charge;
		}

		$array['customer_id'] = $customer_id;
		$array['package_id'] = $package_id;

		$array['customer_package_status'] = 1;
		$array['created'] = date('Y-m-d H:i:s');
		

		if($this->db->insert('customer_packages',$array))
		{
			$data['message'] = 'success';


			$this->db->where('customer_id',$customer_id);
			$query = $this->db->get('customer');
			if($query->num_rows() == 1)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$customer_phone = $value->customer_phone;
				}
				// create invoice for that period
				$invoice_array['invoice_amount'] = 10;
				$invoice_array['invoice_year'] = date('Y');
				$invoice_array['invoice_month'] = date('m');
				$invoice_array['created'] = date('Y-m-d');
				$invoice_array['invoice_status'] = 1;
				$invoice_array['invoice_number'] = date('Ymdhis');
				$invoice_array['customer_id'] = $customer_id;

				$this->db->insert('customer_invoice',$invoice_array);




				$this->messaging_model->sms($customer_phone,"Thank you for subscribing to the the basic package.\nYou can now enjoy the our service to you");
			}
			
		}
		else
		{
			$data['message'] = 'fail';
			$data['result'] = 'Could not update the package. Please try again';
		}

		echo json_encode($data);

	}

	public function get_package_detail($package_id)
	{
		$package_rs = $this->login_model->get_package_detail($package_id);
		

		if($package_rs->num_rows() > 0)
		{
			foreach ($package_rs->result() as $key => $value) {
				# code...
				$data['package_description'] = $value->package_description;
				$data['package_charge'] = $value->package_charge;
				$data['package_name'] = $value->package_name;
				$data['message'] = 'success';
			}
		}
		else
		{
			$data['message'] = 'fail';
		}

		echo json_encode($data);



	}

	public function add_registration_id($customer_id = NULL, $registration_id = NULL)
	{
		if(($customer_id != NULL) && ($customer_id != 'null') && ($registration_id != NULL) && ($registration_id != 'null'))
		{
			//check if exists
			$data = array('customer_id' => $customer_id, 'app_registration_id' => $registration_id);
			$this->db->where($data);
			$query = $this->db->get('customer');
			
			//save if not exists
			if($query->num_rows() == 0)
			{
				$where = array('customer_id' => $customer_id);
				$data2 = array('app_registration_id' => $registration_id);
				
				$this->db->where($where);
				$this->db->update('customer', $data);
				$response['message'] = 'success';
			}
			
			else
			{
				$response['message'] = 'success';
			}
		}
			
		else
		{
			$response['message'] = 'success';
		}
		
		echo json_encode($response);
	}

}