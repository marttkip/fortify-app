<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rides extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		
		$this->load->model('rides_model');
		// $this->load->model('riders_model');
		// $this->load->model('accounts/ride_model');
		
	}
	
	public function request_ride()
	{
		$this->form_validation->set_rules('from_route', 'Pick up location', 'required|xss_clean');
		$this->form_validation->set_rules('to_route', 'Destination', 'required|xss_clean');
		$this->form_validation->set_rules('distance', 'Distance', 'xss_clean');
		$this->form_validation->set_rules('duration', 'Duration', 'xss_clean');
		$this->form_validation->set_rules('request_lat', 'Request Latitude', 'xss_clean');
		$this->form_validation->set_rules('request_lng', 'Request Longitude', 'xss_clean');
		$this->form_validation->set_rules('destination_lat', 'Destination Latitude', 'xss_clean');
		$this->form_validation->set_rules('destination_lng', 'Destination Longitude', 'xss_clean');
		$this->form_validation->set_rules('customer_id', 'Customer ID', 'required|xss_clean');
		$this->form_validation->set_rules('ride_type_id', 'Request type', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$ride_id = $this->rides_model->save_ride();
			if($ride_id > 0)
			{
				$ride_type_id = $this->input->post('ride_type_id');
				$from_route = $this->input->post('from_route');
				$to_route = $this->input->post('to_route');
				
				if($ride_type_id == 1)
				{
					$title = 'New ride requested';
				}
				
				else
				{
					$title = 'New delivery requested';
				}
				$result['message'] = 'success';
				$result['result']['message'] = 'Your request was placed successfully. We are searching for a rider.';
				$result['result']['ride_id'] = $ride_id;
				
				$message = 'A new request has come from '.$from_route.' to '.$to_route;
				// $this->riders_model->notify_riders($title, $message);
			}
			
			else
			{
				$result['message'] = 'error';
				$result['result'] = 'Unable to request ride. Please try again';
			}
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = validation_errors();
		}
		
		echo json_encode($result);
	}
	public function schedule_ride()
	{
		$this->form_validation->set_rules('from_route', 'Pick up location', 'required|xss_clean');
		$this->form_validation->set_rules('to_route', 'Destination', 'required|xss_clean');
		$this->form_validation->set_rules('distance', 'Distance', 'xss_clean');
		$this->form_validation->set_rules('duration', 'Duration', 'xss_clean');
		$this->form_validation->set_rules('request_lat', 'Request Latitude', 'xss_clean');
		$this->form_validation->set_rules('request_lng', 'Request Longitude', 'xss_clean');
		$this->form_validation->set_rules('destination_lat', 'Destination Latitude', 'xss_clean');
		$this->form_validation->set_rules('destination_lng', 'Destination Longitude', 'xss_clean');
		$this->form_validation->set_rules('customer_id', 'Customer ID', 'required|xss_clean');
		$this->form_validation->set_rules('ride_type_id', 'Request type', 'required|xss_clean');
		$this->form_validation->set_rules('ride_time', 'Ride Time', 'required|xss_clean');
		$this->form_validation->set_rules('ride_date', 'Ride Date', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$ride_id = $this->rides_model->schedule_ride();
			if($ride_id > 0)
			{
				$ride_type_id = $this->input->post('ride_type_id');
				$from_route = $this->input->post('from_route');
				$to_route = $this->input->post('to_route');
				
				if($ride_type_id == 1)
				{
					$title = 'New ride requested';
				}
				
				else
				{
					$title = 'New delivery requested';
				}
				$result['message'] = 'success';
				$result['result']['message'] = 'Your request was placed successfully. We are searching for a rider.';
				$result['result']['ride_id'] = $ride_id;
				
				$message = 'A new request has come from '.$from_route.' to '.$to_route;
				// $this->riders_model->notify_riders($title, $message);
			}
			
			else
			{
				$result['message'] = 'error';
				$result['result'] = 'Unable to request ride. Please try again';
			}
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = validation_errors();
		}
		
		echo json_encode($result);
	}
	
	public function get_all_requests()
	{
		$requests = $this->rides_model->get_all_requests();
		$rides = array();
		if($requests->num_rows() > 0)
		{
			foreach($requests->result() as $res)
			{
				$ride_data = array(
					'from_route' => $res->from_route,
					'to_route' => $res->to_route,
					'distance' => $res->distance,
					'duration' => $res->duration,
					'request_lat' => $res->request_lat,
					'request_lng' => $res->request_lng,
					'destination_lat' => $res->destination_lat,
					'destination_lng' => $res->destination_lng,
					'customer_first_name' => $res->customer_first_name,
					'customer_phone' => $res->customer_phone,
					'ride_type_id' => $res->ride_type_id,
					'ride_id' => $res->ride_id
				);
				
				array_push($rides, $ride_data);
			}
			$response['message'] = 'success';
			$response['result'] = $rides;
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'There are no available rides';
		}
		
		echo json_encode($response);
	}
	
	public function get_customer($ride_id)
	{
		$requests = $this->rides_model->get_customer($ride_id);
		$rides = array();
		if($requests->num_rows() > 0)
		{
			foreach($requests->result() as $res)
			{
				$ride_data = array(
					'from_route' => $res->from_route,
					'to_route' => $res->to_route,
					'distance' => $res->distance,
					'duration' => $res->duration,
					'request_lat' => $res->request_lat,
					'request_lng' => $res->request_lng,
					'destination_lat' => $res->destination_lat,
					'destination_lng' => $res->destination_lng,
					'customer_first_name' => $res->customer_first_name,
					'customer_phone' => $res->customer_phone,
					'ride_type_id' => $res->ride_type_id,
					'ride_id' => $res->ride_id
				);
				
				array_push($rides, $ride_data);
			}
			$response['message'] = 'success';
			$response['result'] = $rides;
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'There are no available rides';
		}
		
		echo json_encode($response);
	}
	
	public function accpet_request($personnel_id = NULL, $ride_id = NULL)
	{
		//if form has been submitted
		if ($ride_id != NULL)
		{
			if($this->rides_model->accpet_ride($ride_id, $personnel_id))
			{
				//get personnel data
				$this->db->where('personnel_id', $personnel_id);
				$query = $this->db->get('personnel');
				
				$title = 'You have been allocated an agent';
				$message = 'An agent is en route';
				$source = json_encode($query->row());
				
				$this->rides_model->notify_customer($ride_id, $title, $message, $source);
				
				$result['message'] = 'success';
				$result['result'] = 'Call accepted successfully. You can respond to the client\'s call';
			}
			
			else
			{
				$result['message'] = 'error';
				$result['result'] = 'Unable to request ride. Please try again';
			}
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = 'Ride not selected';
		}
		
		echo json_encode($result);
	}
	
	public function reverse_geocode($latitude, $longitude)
	{
		$response = $this->rides_model->reverse_geocode($latitude, $longitude);
		//var_dump($response);// json_decode($response);
		
		$decoded = json_decode($response, true);
		$result['message'] = 'success';
		$result['result'] = $decoded['results'][0]["formatted_address"];
		echo json_encode($result);
	}
	
	public function set_rider_location($latitude, $longitude, $ride_id)
	{
		if($this->rides_model->set_rider_location($latitude, $longitude, $ride_id))
		{
			$result['message'] = 'success';
			$result['result'] = 'Agent location set successfully';
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = 'Agent location not set';
		}
		
		echo json_encode($result);
	}
	
	public function begin_ride($ride_id, $latitude, $longitude)
	{
		if($this->rides_model->begin_ride($ride_id))
		{
			$title = 'You are in transit';
			$message = 'Your ride has begun';
			$source = 'begin_ride';
			
			$this->rides_model->notify_customer($ride_id, $title, $message, $source);
			
			$result['message'] = 'success';
			$result['result'] = 'Ride begun successfully';
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = 'Ride not begun';
		}
		
		echo json_encode($result);
	}
	
	public function end_ride($ride_id, $start_latitude, $start_longitude, $end_latitude, $end_longitude, $start_kms, $pickup_kms, $destination_kms, $base_kms, $ride_distance, $base_distance)
	{
		if($this->rides_model->end_ride($ride_id, $start_latitude, $start_longitude, $end_latitude, $end_longitude, $start_kms, $pickup_kms, $destination_kms))
		{
			// $response = $this->rides_model->reverse_geocode($start_latitude, $start_longitude);
			// $decoded = json_decode($response, true);
			// //var_dump($decoded);
			// $location['start_location'] = $decoded['results'][0]["formatted_address"];
			
			// $response = $this->rides_model->reverse_geocode($end_latitude, $end_longitude);
			// $decoded = json_decode($response, true);
			// $location['end_location'] = $decoded['results'][0]["formatted_address"];
			
			$title = 'We hope that you have been assisted';
			$message = 'Thank you for sorting out the call';
			$source = 'end_ride';
			// $customer_id = $this->ride_model->get_ride_customer_id($ride_id);
			// $vehicle_id =  $this->ride_model->get_ride_vehicle_id($ride_id);
			// $total_distance = ($pickup_kms-$start_kms)+($destination_kms-$pickup_kms)+($base_kms-$destination_kms);

			// $total_waiting_time = $this->ride_model->get_total_waiting_time($ride_id);
			
			// $cost = $this->ride_model->calcluate_ride_cost($vehicle_id, $customer_id, $total_distance, $total_waiting_time);
			
			$this->rides_model->notify_customer($ride_id, $title, $message, $source);
			
			$result['message'] = 'success';
			$result['result'] = 0;
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = 'Ride not ended successfully';
		}
		
		echo json_encode($result);
	}
	
	public function get_rider_location($ride_id)
	{
		$requests = $this->rides_model->get_ride($ride_id);
		$rides = array();
		if($requests->num_rows() > 0)
		{
			$res = $requests->row();
			$ride_data = array(
				'rider_latitude' => $res->rider_latitude,
				'rider_longitude' => $res->rider_longitude
			);
			
			array_push($rides, $ride_data);
			
			$response['message'] = 'success';
			$response['result'] = $rides;
		}
		
		else
		{
			$response['message'] = 'fail';
			$response['result'] = 'There are no available rides';
		}
		
		echo json_encode($response);
	}

	public function calcluate_passenger_cost()
	{
		$vehicle_type = $this->input->post('vehicle_type');
		$distance = $this->input->post('distance');
		$utu_distance = $this->input->post('utu_distance');
		$utu_destination_distance = $this->input->post('utu_destination_distance');
		$utu_customer_id = $this->input->post('utu_customer_id');
		
		$this->db->where('customer_id', $utu_customer_id);
		$query = $this->db->get('customer');
		$total_cost = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$customer_type_id = $row->customer_type_id;
			$customer_rate = $row->customer_rate;
			$customer_van_rate = $row->customer_van_rate;
			
			if($customer_type_id == 3)
			{
				$total_distance = $utu_distance + $utu_destination_distance + $distance;
			}
			
			else
			{
				$total_distance = $distance;
			}
			
			if($vehicle_type == 1)
			{
				if(empty($customer_rate))
				{
					//get ride cost
					$this->db->where('service_charge.service_id = 35 AND service_charge.customer_type_id = '.$customer_type_id.' AND service_charge.vehicle_capacity_id = '.$vehicle_type);
					$query = $this->db->get('service_charge');
					$total_cost = 0;
					
					if($query->num_rows() > 0)
					{
						$row = $query->row();
						$total_cost = $row->service_charge_amount * $total_distance;
					}
				}
				
				else
				{
					$total_cost = $customer_rate * $total_distance;
				}
			}
			
			if($vehicle_type == 2)
			{
				if(empty($customer_van_rate))
				{
					//get ride cost
					$this->db->where('service_charge.service_id = 35 AND service_charge.customer_type_id = '.$customer_type_id.' AND service_charge.vehicle_capacity_id = '.$vehicle_type);
					$query = $this->db->get('service_charge');
					$total_cost = 0;
					
					if($query->num_rows() > 0)
					{
						$row = $query->row();
						$total_cost = $row->service_charge_amount * $total_distance;
					}
				}
				
				else
				{
					$total_cost = $customer_van_rate * $total_distance;
				}
			}
		}

		if($total_cost<300){
			$total_cost = 300;
		}
		$response['message'] ="success";
		$response['result'] = $total_cost;
		echo json_encode($response);
	}
	public function submit_waiting_time()
	{
		$this->form_validation->set_rules('ride_id', 'ride_id', 'required|xss_clean');
		$this->form_validation->set_rules('waiting_lat', 'Waiting Latitude', 'xss_clean');
		$this->form_validation->set_rules('waiting_lng', 'Waiting Longitude', 'xss_clean');
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$ride_waiting_id = $this->rides_model->submit_waiting_time();
			
			if($ride_waiting_id>0)
			{
				$result['message'] ="success";
		        $result['result'] = "1";
		        $result['ride_waiting_id'] = $ride_waiting_id;

			}
			else
			{
				$result['message'] = 'error';
				$result['result'] = '0';
			}
		}
    	else
		{
			$result['message'] = 'error';
			$result['result'] = validation_errors();
		}
		
		echo json_encode($result);
	}
	public function update_waiting_time()
	{
		
		$this->form_validation->set_rules('ride_waiting_id', 'ride_waiting_id', 'required|xss_clean');
		//$this->form_validation->set_rules('waiting_end_time', 'Waiting End Time', 'required|xss_clean');
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->rides_model->update_waiting_ride())
			{
				$result['message'] ="success";
		        $result['result'] = "1";

			}
			else
			{
				$result['message'] = 'error';
				$result['result'] = '0';
			}
		}
    	else
		{
			$result['message'] = 'error';
			$result['result'] = validation_errors();
		}
		
		echo json_encode($result);
	}
}