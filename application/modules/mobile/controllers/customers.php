<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Customers extends MX_Controller {
	
	function __construct()
	{
		parent:: __construct();
		
		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			// header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header("Access-Control-Allow-Origin: *");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		
		$this->load->model('mobile/login_model');
		$this->load->model('mobile/email_model');
		$this->load->model('mobile/customers_model');
		$this->load->model('mobile/agents_model');
		$this->load->model('messaging/messaging_model');
		
		// $this->load->library('Mandrill', $this->config->item('mandrill_key'));
	}

	public function customer_invoices($customer_id)
	{
		$v_data['customer_id'] = $customer_id;


		$customer_invoice_rs = $this->customers_model->get_customer_invoices($customer_id);

		$v_data['customer_invoice_rs'] = $customer_invoice_rs;
		$data['message'] = 'success'; 
		$data['customer_invoices'] = $this->load->view('customer_invoices', $v_data,true);
		echo json_encode($data);
	}

	public function submit_payment($customer_id)
	{

		$phone = $this->input->post('phone_number');
		$invoice_amount = $this->input->post('invoice_amount');
		$invoice_id = $this->input->post('invoice_id');

		if (substr($phone, 0, 1) === '0') 
		{
			$phone = ltrim($phone, '0');
		}
		else if(substr($phone, 0, 4) === '+254') 
		{
			$phone = ltrim($phone, '+254');
		}
		else if(substr($phone, 0, 3) === '254') 
		{
			$phone = ltrim($phone, '254');
		}
		else if(substr($phone, 0, 1) === '7') 
		{
			$phone = $phone;
		}
		$phone = '254'.$phone;

		date_default_timezone_set('Africa/Nairobi');

	  	# access token
	  	$consumerKey = 'T6YKClGZiiRa2ASFumpc63VW0YitEKfF'; //Fill with your app Consumer Key
	  	$consumerSecret = 'vjmOBba3W2zaDA09'; // Fill with your app Secret

		$consumerKey = 'rgfADfcCfrHuW4LwhC2t89DoRvhUyh0j';
		$consumerSecret = 'QrvpfeTSzgOa4Fwq';
		// $headers = ['Content-Type:application/json; charset=utf8'];
		  # define the variales
		  # provide the following details, this part is found on your test credentials on the developer account
		  $BusinessShortCode = '4022029';
		  $Passkey = 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919';  
		  
		  /*
		    This are your info, for
		    $PartyA should be the ACTUAL clients phone number or your phone number, format 2547********
		    $AccountRefference, it maybe invoice number, account number etc on production systems, but for test just put anything
		    TransactionDesc can be anything, probably a better description of or the transaction
		    $Amount this is the total invoiced amount, Any amount here will be 
		    actually deducted from a clients side/your test phone number once the PIN has been entered to authorize the transaction. 
		    for developer/test accounts, this money will be reversed automatically by midnight.
		  */
		  
		  $PartyA = $phone; // This is your phone number, 
		  $AccountReference = $invoice_id;
		  $TransactionDesc = 'Subscription Fee';
		  $Amount = $invoice_amount;
		 
		  # Get the timestamp, format YYYYmmddhms -> 20181004151020
		  $Timestamp = date('YmdHis');    
		  
		  # Get the base64 encoded string -> $password. The passkey is the M-PESA Public Key
		  $Password = base64_encode($BusinessShortCode.$Passkey.$Timestamp);

		  # header for access token
		  $headers = ['Content-Type:application/json; charset=utf8'];

		    # M-PESA endpoint urls
		  // $access_token_url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
		  // $initiate_url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
		  $access_token_url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
		  $initiate_url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

		  # callback url
		  $CallBackURL = 'http://simplex-financials.com/transaction/callback_url.php';  

		  $curl = curl_init($access_token_url);
		  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		  curl_setopt($curl, CURLOPT_HEADER, FALSE);
		  curl_setopt($curl, CURLOPT_USERPWD, $consumerKey.':'.$consumerSecret);
		  $result = curl_exec($curl);
		  $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		  $result = json_decode($result);
		  $access_token = $result->access_token;
		  
		  curl_close($curl);

		  # header for stk push
		  $stkheader = ['Content-Type:application/json','Authorization:Bearer '.$access_token];

		  # initiating the transaction
		  $curl = curl_init();
		  curl_setopt($curl, CURLOPT_URL, $initiate_url);
		  curl_setopt($curl, CURLOPT_HTTPHEADER, $stkheader); //setting custom header

		  $curl_post_data = array(
		    //Fill in the request parameters with valid values
		    'BusinessShortCode' => $BusinessShortCode,
		    'Password' => $Password,
		    'Timestamp' => $Timestamp,
		    'TransactionType' => 'CustomerPayBillOnline',
		    'Amount' => $Amount,
		    'PartyA' => $PartyA,
		    'PartyB' => $BusinessShortCode,
		    'PhoneNumber' => $PartyA,
		    'CallBackURL' => $CallBackURL,
		    'AccountReference' => $AccountReference,
		    'TransactionDesc' => $TransactionDesc
		  );

		  $data_string = json_encode($curl_post_data);
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		  curl_setopt($curl, CURLOPT_POST, true);
		  curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
		  $curl_response = curl_exec($curl);
		  
		  // echo $curl_response;
		  $checked_response['status'] ='success';
		  echo json_encode($checked_response);
	}

	public function get_all_agents()
	{
		$today = date('Y-m-d');
		$table = "user";
		$where = "user_status = 1 AND user_id NOT IN (SELECT user_id FROM `call` WHERE call_status_id >= 1 OR call_status_id <= 3) AND user_id IN (SELECT user_id FROM user_status WHERE user_status_date LIKE '".$today."%' AND user_status_availability = 1 AND user_status_status = 1)";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		if($query->num_rows() > 0)
		{
			$result['message'] = 'success';
			$result['result'] = $query->result();
		}
		
		else
		{
			$result['message'] = 'fail';
		}
		
		echo json_encode($result);
	}
	public function set_my_location($latitude, $longitude, $ride_id)
	{

		// var_dump('dasda');
		if($this->customers_model->set_customer_location($latitude, $longitude, $ride_id))
		{
			$result['message'] = 'success';
			$result['result'] = 'Agent location set successfully';
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = 'Agent location not set';
		}
		
		echo json_encode($result);
	}

	public function get_current_ride_status($call_id)
	{
		$this->db->where('call_id',$call_id);
		$query = $this->db->get('call');
		$call_status_id = 1;

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$call_id = $value->call_id;
				$call_status_id = $value->call_status_id;
			}
		}

		$response['call_status_id'] = $call_status_id;
		$response['message'] = 'success';
		echo json_encode($response);


	}

	public function request_ride()
	{
		// $this->form_validation->set_rules('from_route', 'Pick up location', 'required|xss_clean');
		// $this->form_validation->set_rules('to_route', 'Destination', 'required|xss_clean');
		$this->form_validation->set_rules('distance', 'Distance', 'xss_clean');
		$this->form_validation->set_rules('duration', 'Duration', 'xss_clean');
		$this->form_validation->set_rules('request_lat', 'Request Latitude', 'xss_clean');
		$this->form_validation->set_rules('request_lng', 'Request Longitude', 'xss_clean');
		$this->form_validation->set_rules('destination_lat', 'Destination Latitude', 'xss_clean');
		$this->form_validation->set_rules('destination_lng', 'Destination Longitude', 'xss_clean');
		$this->form_validation->set_rules('customer_id', 'Customer ID', 'required|xss_clean');
		$this->form_validation->set_rules('ride_type_id', 'Request type', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$ride_id = $this->customers_model->save_call();
			if($ride_id > 0)
			{
				$ride_type_id = $this->input->post('ride_type_id');
				$from_route = $this->input->post('from_route');
				$to_route = $this->input->post('to_route');
				
				if($ride_type_id == 1)
				{
					$title = 'New call requested';
				}
				
				else
				{
					$title = 'New call requested';
				}
				$result['message'] = 'success';
				$result['result']['message'] = 'Your request was placed successfully. We are searching for a close by agent.';
				$result['result']['ride_id'] = $ride_id;
				
				$message = 'A new call has come from '.$from_route;
				$this->agents_model->notify_agents($title, $message);
			}
			
			else
			{
				$result['message'] = 'error';
				$result['result'] = 'Unable to request ride. Please try again';
			}
		}
		
		else
		{
			$result['message'] = 'error';
			$result['result'] = validation_errors();
		}
		
		echo json_encode($result);
	}

	

}
?>