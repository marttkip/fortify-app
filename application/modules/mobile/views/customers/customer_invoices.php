<?php

if($profile_rs->num_rows() > 0)
{
	foreach ($profile_rs->result() as $key => $value) {
		# code...
		$customer_id = $value->customer_id;
		$customer_name = $value->customer_name;
		$customer_email = $value->customer_email;
		$customer_phone = $value->customer_phone;
		$account_number = $value->account_number;
		$password = $value->password;
		$parent_customer_id = $value->parent_customer_id;
	}
}


$invoice_result = '<div class="list-block">
						 <ul id="employer-list">';
if($customer_invoice_rs->num_rows() > 0)
{
	foreach ($customer_invoice_rs->result() as $key => $value) {
		# code...

		$created  = $value->created;
		$invoice_amount  = $value->invoice_amount;
		$invoice_id  = $value->invoice_id;
		$invoice_number  = $value->invoice_number;
		$invoice_year  = $value->invoice_year;
		$invoice_month  = $value->invoice_month;

		$invoice_result .= '
						       
							        <li class="list-benefit">
							        	<a onclick="get_invoice_detail('.$invoice_id.','.$invoice_amount.',\''.$customer_phone.'\')">
							        		<div class="item-title"><i class="fa fa-calendar"></i> '.date('jS M Y',strtotime($created)).' </div>
							        		<div class="item-subtitle"><strong> <i class="fa fa-credit-card"></i> '.$invoice_number.' </strong> </div>
								            <div class="item-text">
								            	<strong><i class="fa fa-credit-card"></i> Due  </strong> Kes. '.number_format($invoice_amount,2).' 
								            	<strong><i class="fa fa-credit-card"></i> Paid </strong> Kes. 0.00 
								           	</div>
								            
							        	</a>
					                   
							        </li>
					      
						      	';

	}
}
else
{
	$invoice_result .= '<li class="list-benefit">
				        	 No invoices created yet
				        </li>';
}

$invoice_result .= '</ul>
				</div>';
echo $invoice_result;
?>