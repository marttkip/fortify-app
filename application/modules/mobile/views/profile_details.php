<?php


if($profile_rs->num_rows() > 0)
{
	foreach ($profile_rs->result() as $key => $value) {
		# code...
		$customer_id = $value->customer_id;
		$customer_name = $value->customer_name;
		$customer_email = $value->customer_email;
		$customer_phone = $value->customer_phone;
		$account_number = $value->account_number;
		$password = $value->password;
		$parent_customer_id = $value->parent_customer_id;
	}
}



?>
<div class="row">

	<div class="col-100" style="text-align: left;">
		<p><strong>Name : </strong><br> <?php echo ucfirst($customer_name)?></p>
	   <p><strong>Email : </strong><br> <?php echo $customer_email;?></p>
	   <p><strong>Phone : </strong><br> <?php echo $customer_phone?></p>
	</div>
	
</div>
<div class="content-block-title">PERSONNAL INFORMATION <i class="fa fa-pencil"></i></div>
<form id="update_contacts" method="post">
	<div class="list-block">
		<ul>
			<li>
			  <div class="item-content">
			    <div class="item-inner">
			      <div class="item-title label">E-MAIL :</div>
			      <div class="item-input">
			        <input type="email" name="email" placeholder="E-mail" value="<?php echo $customer_email;?>">
			      </div>
			    </div>
			  </div>
			</li>
			<li>
			  <div class="item-content">
			    <div class="item-inner">
			      <div class="item-title label">PHONE :</div>
			      <div class="item-input">
			        <input type="text" name="phone_number" placeholder="Your name" value="<?php echo $customer_phone;?>">
			      </div>
			    </div>
			  </div>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="col-100">
			<a href="#" class="button button-small button-round active">UPDATE DETAILS</a>  
		</div>
	</div>
	<!-- <div class="content-block-title">PACKAGE INFORMATION <i class="fa fa-pencil"></i></div>
	<div class="list-block">
			<ul>
			<li>
			  <div class="item-content">
			    <div class="item-inner">
			      <div class="item-title label">POSTAL CODE :</div>
			      <div class="item-input">
			        <input type="text" placeholder="Code">
			      </div>
			    </div>
			  </div>
			</li>

			<li>
			  <div class="item-content">
			    <div class="item-inner">
			      <div class="item-title label">ADDRESS :</div>
			      <div class="item-input">
			        <input type="text" placeholder="address">
			      </div>
			    </div>
			  </div>
			</li>
			<li>
			  <div class="item-content">
			    <div class="item-inner">
			      <div class="item-title label">CITY :</div>
			      <div class="item-input">
			        <input type="text" placeholder="City">
			      </div>
			    </div>
			  </div>
			</li>
			<li>
			  <div class="item-content">
			    <div class="item-inner">
			      <div class="item-title label">RESIDENCE :</div>
			      <div class="item-input">
			        <input type="text" placeholder="Residence">
			      </div>
			    </div>
			  </div>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="col-100">
			<a href="#" class="button button-small button-round active">UPDATE DETAILS</a>  
		</div>
	</div>	 -->
</form>		

<div class="row">
	<div class="col-100">
		<h2>A/C No: <?php echo $account_number;?></h2>

		<!-- <a  class="button button-small button-round active">UPDATE DETAILS</a>   -->

		<?php
		if($parent_customer_id == 0)
		{
			?>
			<div class="card">
			  <div class="card-content">
			    <div class="card-content-inner">Use your account number to invite other people to create accounts with us.</div>
			  </div>
			</div>
			<?php
		}
		?>
		
	</div>
</div>
<hr>
<?php

$current_package_id =0;
if($parent_customer_id == 0)
{
	if($customer_package_rs->num_rows() > 0)
	{
		foreach ($customer_package_rs->result() as $key => $value) {
			# code...

			$customer_package_id = $value->customer_package_id;
			$current_package_id = $value->package_id;

			$package_name = $value->package_name;
		}
		?>
		<div class="col-100">
			<div class="card">
			  <div class="card-content">
			    <div class="card-content-inner"><?php echo $package_name?></div>
			  </div>
			</div>
		</div>
		<?php
	}
	else
	{
		?>
		<div class="col-100">
			<a class="button col-100" onclick="activate_basic_package(1)"> Activate to Basic Package</a>
		</div>
		<?php
	}
}
else
{
	?>
	<div class="col-100">
			<div class="card">
			  <div class="card-content">
			    <div class="card-content-inner">You are covered by another user</div>
			  </div>
			</div>
		</div>
	<?php
}

?>

<!-- <div class="col-100"><a href="#" class="button button-fill pb-popup">Popup</a></div> -->