<?php

$profile_rs = $this->login_model->get_customer_details($customer_id);
$result = '<ul >';
if($profile_rs->num_rows() > 0)
{
	foreach ($profile_rs->result() as $key => $value) {
		# code...
		$customer_id = $value->customer_id;
		$customer_name = $value->customer_name;
		$customer_email = $value->customer_email;
		$customer_phone = $value->customer_phone;
		$account_number = $value->account_number;
		$username = $value->username;
		$created = $value->created;
		$password = $value->password;

		$result .= '<li class="item-content">
			        	<a href="#">
			        		<div class="item-title"><i class="fa fa-user"></i> '.ucfirst($customer_name).' </div>
			        		<div class="item-subtitle"><strong> <i class="fa fa-calendar"></i> '.date('jS M Y',strtotime($created)).'</strong> </div>
				            <div class="item-text">
				            	<strong><i class="fa fa-credit-card"></i>   </strong> '.$account_number.'
				            	<strong><i class="fa fa-phone"></i>  </strong> '.$customer_phone.'
				           	</div>
				            
			        	</a>
	                   
			        </li>';
	}
}
$result .= '</ul>';

echo $result;

?>