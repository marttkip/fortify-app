<?php

class Agents_model extends CI_Model 
{

	public function send_push_notification($to, $title, $message, $source = NULL)
	{
		// API access key from Google API's Console
		// replace API
		
		$registrationIds = array($to);
		
		$msg = array
		(
			'message' => $message,
			'title' => $title,
			'vibrate' => 1,
			'sound' => 1,
			'additionalData' => $source
			// you can also add images, additionalData
		);
		
		$fields = array
		(
			'registration_ids' => $registrationIds,
			'data' => $msg
		);
		
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		
		$result = curl_exec($ch );
		
		curl_close( $ch );
		
		return $result;
	}
	/*
	*	Check if member has logged in
	*
	*/
	public function check_agent_login()
	{
		if($this->session->userdata('agent_login_status'))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	


	

	/*
	*	Validate a member's login request
	*
	*/
	public function validate_agent($user_number, $password)
	{
		//select the user by email from the database
		$this->db->select('*');
		$this->db->where(array('user_number' =>strtolower($user_number),'user_password' => md5($password)));
		$query = $this->db->get('user');
		
		//if users exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			
			return $result;
		}
		
		else
		{
			return FALSE;
		}
	}

	public function reverse_geocode($latitude, $longitude)
	{
		$maps_api_key = 'AIzaSyCRL4A7M9ZGM7GIPaZqbfv67xtcPFLc2xc';
		$request = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&key='.$maps_api_key;
		
		$json = file_get_contents($request);
		
		return $json;
	}
	
	public function get_all_requests()
	{
		$this->db->where('call_status_id = 1 AND call.customer_id = customer.customer_id AND call.call_date = \''.date('Y-m-d').'
		\'');
		$rides = $this->db->get('call, customer');
		
		return $rides;
	}

	public function notify_agents($message_title = 'Fortify', $message = NULL)
	{
		if($message != NULL)
		{
			//get riders
			$where = 'user_status.user_id = user.user_id AND user_status.user_status_status = 1 AND user_status.user_status_availability = 1 AND user.registration_id IS NOT NULL';
			$this->db->where($where);
			$query = $this->db->get('user, user_status');
			
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $res)
				{
					$to = $res->registration_id;
					$title = $message_title;
					$result = $this->send_push_notification($to, $title, $message);
				}
			}
		}
	}
}
?>