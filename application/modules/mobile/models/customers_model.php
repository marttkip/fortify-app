<?php

class Customers_model extends CI_Model 
{


	public function get_customer_invoices($customer_id)
	{
		$this->db->where('customer_id',$customer_id);
		$query = $this->db->get('customer_invoice');

		return $query;
	}

	public function save_call()
	{
		$data = array
		(
			'from_route' => $this->input->post('from_route'),
			'to_route' => $this->input->post('to_route'),
			'distance' => $this->input->post('distance'),
			'duration' => $this->input->post('duration'),
			'request_lat' => $this->input->post('request_lat'),
			'request_lng' => $this->input->post('request_lng'),
			'destination_lat' => $this->input->post('destination_lat'),
			'destination_lng' => $this->input->post('destination_lng'),
			'customer_id' => $this->input->post('customer_id'),
			'call_type_id' => $this->input->post('ride_type_id'),
			
			'call_date' => date('Y-m-d'),
			'call_status_id' => 1
		);
		if($this->db->insert('call', $data))
		{
			return $this->db->insert_id();
		}
		
		else
		{
			return FALSE;
		}
	}

	public function set_customer_location($latitude, $longitude, $call_id)
	{
		$data = array
		(
			'request_lat' => $latitude,
			'request_lng' => $longitude
		);
		$this->db->where('call_id', $call_id);
		if($this->db->update('call', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	

}
?>