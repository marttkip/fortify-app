<?php

class Rides_model extends CI_Model 
{
	public function save_ride()
	{
		$data = array
		(
			'from_route' => $this->input->post('from_route'),
			'to_route' => $this->input->post('to_route'),
			'distance' => $this->input->post('distance'),
			'duration' => $this->input->post('duration'),
			'request_lat' => $this->input->post('request_lat'),
			'request_lng' => $this->input->post('request_lng'),
			'destination_lat' => $this->input->post('destination_lat'),
			'destination_lng' => $this->input->post('destination_lng'),
			'customer_id' => $this->input->post('customer_id'),
			'ride_type_id' => $this->input->post('ride_type_id'),
			
			'ride_date' => date('Y-m-d'),
			'ride_status_id' => 1
		);
		if($this->db->insert('ride', $data))
		{
			return $this->db->insert_id();
		}
		
		else
		{
			return FALSE;
		}
	}
	public function schedule_ride()
	{
		$data = array
		(
			'from_route' => $this->input->post('from_route'),
			'to_route' => $this->input->post('to_route'),
			'distance' => $this->input->post('distance'),
			'duration' => $this->input->post('duration'),
			'request_lat' => $this->input->post('request_lat'),
			'request_lng' => $this->input->post('request_lng'),
			'destination_lat' => $this->input->post('destination_lat'),
			'destination_lng' => $this->input->post('destination_lng'),
			'customer_id' => $this->input->post('customer_id'),
			'ride_type_id' => $this->input->post('ride_type_id'),
			'ride_time_scheduled' => $this->input->post('ride_time'),
			'ride_date_scheduled' => $this->input->post('ride_date'),
			'ride_date' => date('Y-m-d'),
			'ride_status_id' => 1,
			'ride_book' => 1
		);
		if($this->db->insert('ride', $data))
		{
			return $this->db->insert_id();
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function get_all_requests()
	{
		$this->db->where('ride_status_id = 1 AND ride.customer_id = customer.customer_id AND ride.ride_date = \''.date('Y-m-d').'
		\'');
		$rides = $this->db->get('ride, customer');
		
		return $rides;
	}
	
	public function get_customer($ride_id)
	{
		$this->db->where('call.customer_id = customer.customer_id AND call.call_id = '.$ride_id);
		$rides = $this->db->get('call, customer');
		
		return $rides;
	}
	
	public function accpet_ride($call_id, $personnel_id)
	{
		// $vehicle_id = $this->get_active_vehicle_id($personnel_id);

		$data = array
		(
			'user_id' => $personnel_id,
			// 'vehicle_id' => $vehicle_id,
			'time_accepted' => date('Y-m-d H:i:s'),
			'call_status_id' => 3,
			'call_start_time' => date('Y-m-d H:i:s'),
			'start_time' => date('H:i:s')
		);
		$this->db->where('call_id', $call_id);
		if($this->db->update('call', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function notify_customer($ride_id, $message_title = 'Ubiker', $message = NULL, $source = NULL)
	{
		if($message != NULL)
		{
			//get riders
			$where = 'ride.customer_id = customer.customer_id AND ride.ride_id = '.$ride_id;
			$this->db->where($where);
			$query = $this->db->get('customer, ride');
			
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $res)
				{
					$to = $res->app_registration_id;
					$title = $message_title;
					$result = $this->riders_model->send_push_notification($to, $title, $message, $source);
				}
			}
		}
	}
	
	public function reverse_geocode($latitude, $longitude)
	{
		$maps_api_key = 'AIzaSyCRL4A7M9ZGM7GIPaZqbfv67xtcPFLc2xc';
		$request = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&key='.$maps_api_key;
		
		$json = file_get_contents($request);
		
		return $json;
	}
	
	public function set_rider_location($latitude, $longitude, $call_id)
	{
		$data = array
		(
			'user_latitude' => $latitude,
			'user_longitude' => $longitude,
			'time_changed' => date('Y-m-d H:i:s')
		);
		$this->db->where('call_id', $call_id);
		if($this->db->update('call', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function begin_ride($call_id)
	{
		$data = array
		(
			'call_status_id' => 3,
			'user_start_time' => date('Y-m-d H:i:s'),
			'start_time' => date('H:i:s')
		);
		$this->db->where('call_id', $call_id);
		if($this->db->update('call', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function end_ride($ride_id, $start_latitude, $start_longitude, $end_latitude, $end_longitude, $start_kms, $pickup_kms, $destination_kms)
	{
		$data = array
		(
			// 'begin_latitude' => $start_latitude,
			// 'begin_longitude' => $start_longitude,
			// 'end_latitude' => $end_latitude,
			// 'end_longitude' => $end_longitude,
			// 'start_kms' => $start_kms,
			// 'picking_kms' => $pickup_kms,
			// 'destination_kms' => $destination_kms,
			'call_status_id' => 4,
			// 'call_end_time' => date('Y-m-d H:i:s'),
			// 'end_time' => date('H:i:s')
		);
		$this->db->where('call_id', $ride_id);
		if($this->db->update('call', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function calculate_ride_cost($ride_id)
	{
		$this->db->where('ride_id', $ride_id);
		$query = $this->db->get('ride');
		
		$cost = 0;
		
		$this->db->where('customer_id', $utu_customer_id);
		$query = $this->db->get('customer');
		$total_cost = 0;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$customer_type_id = $row->customer_type_id;
			$customer_rate = $row->customer_rate;
			$customer_van_rate = $row->customer_van_rate;
			
			if($customer_type_id == 3)
			{
				$total_distance = $utu_distance + $utu_destination_distance + $distance;
			}
			
			else
			{
				$total_distance = $distance;
			}
			
			if($vehicle_type == 1)
			{
				if(empty($customer_rate))
				{
					//get ride cost
					$this->db->where('service_charge.service_id = 35 AND service_charge.customer_type_id = '.$customer_type_id.' AND service_charge.vehicle_capacity_id = '.$vehicle_type);
					$query = $this->db->get('service_charge');
					$total_cost = 0;
					
					if($query->num_rows() > 0)
					{
						$row = $query->row();
						$total_cost = $row->service_charge_amount * $total_distance;
					}
				}
				
				else
				{
					$total_cost = $customer_rate * $total_distance;
				}
			}
			
			if($vehicle_type == 2)
			{
				if(empty($customer_van_rate))
				{
					//get ride cost
					$this->db->where('service_charge.service_id = 35 AND service_charge.customer_type_id = '.$customer_type_id.' AND service_charge.vehicle_capacity_id = '.$vehicle_type);
					$query = $this->db->get('service_charge');
					$total_cost = 0;
					
					if($query->num_rows() > 0)
					{
						$row = $query->row();
						$total_cost = $row->service_charge_amount * $total_distance;
					}
				}
				
				else
				{
					$total_cost = $customer_van_rate * $total_distance;
				}
			}
		}

		if($total_cost<300){
			$total_cost = 300;
		}
		
		return $total_cost;
	}
	
	public function get_ride($ride_id)
	{
		$this->db->where('ride.ride_id = '.$ride_id);
		$rides = $this->db->get('ride');
		
		return $rides;
	}
	public function submit_waiting_time()
	{
		 
		$data = array
		(
			'ride_id' => $this->input->post('ride_id'),
			'waiting_latitude' => $this->input->post('waiting_lat'),
			'waiting_longitude' => $this->input->post('waiting_lng'),
			'start_waiting_time' => date("Y-m-d H:i:s"),
			'ride_waiting_status' => 0
		);
		if($this->db->insert('ride_waiting', $data))
		{
			return $this->db->insert_id();
		}
		
		else
		{
			return FALSE;
		}
	}
	public function update_waiting_ride()
	{
		
		$data = array
		(
			
			'stop_waiting_time' => date("Y-m-d H:i:s"),
			'ride_waiting_status' => 1
		);
		$ride_waiting_id = $this->input->post('ride_waiting_id');
		$this->db->where('ride_waiting_id', $ride_waiting_id);
		if($this->db->update('ride_waiting', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	public function get_active_vehicle_id($personnel_id)
	{
		$this->db->where('personnel_vehicle_status = 1 AND personnel_id = '.$personnel_id);
		$query = $this->db->get('personnel_vehicle');
		$customer_id = NULL;
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$customer_id = $row->vehicle_id;
		}
		
		return $customer_id;
	}
	
}
?>