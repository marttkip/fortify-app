<?php

class Login_model extends CI_Model 
{
	/*
	*	Check if member has logged in
	*
	*/
	public function check_member_login()
	{
		if($this->session->userdata('member_login_status'))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	


	/*
	*	Reset a user's password
	*
	*/
	public function get_profile_details()
	{
		// 9530
		$this->db->where('personnel_id = '.$this->session->userdata('personnel_id'));
		$query = $this->db->get('personnel');
		
		return $query;
	}
	

	/*
	*	Validate a member's login request
	*
	*/
	public function validate_member($username, $password)
	{
		//select the user by email from the database
		$this->db->select('*');
		$this->db->where(array('account_number' =>strtolower($username),'password' => md5($password)));
		$query = $this->db->get('customer');
		
		//if users exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			
			return $result;
		}
		
		else
		{
			return FALSE;
		}
	}

	public function validate_owner($home_owner_email,$home_owner_phone)
	{
		//select the user by email from the database
		
		$this->db->select('*');
		// $this->db->where(array('email_login' =>strtolower($home_owner_email),'phone_login' => $home_owner_phone));
		$this->db->where('home_owner_id',207);
		$query = $this->db->get('home_owners');
		
		//if users exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			
			//update user's last login date time
			// $this->update_member_login($result[0]->member_id);

			$home_owner_id = $result[0]->home_owner_id;
			$home_owner_name = $result[0]->home_owner_name;

			// $one_time_password = '1234';
			// $array['one_time_password'] = $one_time_password;

			// $this->db->where('home_owner_id',$home_owner_id);
			// $this->db->update('home_owners',$array);

			// $delivery_message = "Hello ".$home_owner_name.",  your one time password is ".$one_time_password.".";
			// $this->messaging_model->sms($home_owner_phone,$delivery_message);


			// update one time password.


			return $result;
		}
		
		//if user doesn't exist
		else
		{
			return FALSE;
		}
	}
	

	public function register_account($customer_id = NULL)
	{
		$name = $this->input->post('name');
		$id_number = $this->input->post('id_number');
		$phone_number = $this->input->post('phone_number');
		$email_address = $this->input->post('email_address');
		// $username = $this->input->post('username');
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');



		$account_number = $this->create_customer_number();
		$array['customer_name'] = $name;
		$array['customer_phone'] = $phone_number;
		$array['customer_email'] = $email_address;
		$array['created'] = date('Y-m-d H:i:s');
		$array['account_number'] = $account_number;
		$array['password'] = md5($password);

		if(!empty($customer_id))
		{
			$array['parent_customer_id'] = $customer_id;
		}


		if($this->db->insert('customer',$array))
		{

			

			if(empty($customer_id))
			{
				$this->messaging_model->sms($phone_number,"Thank you for registering with us. Your A/C No. is ".$account_number.". Please login to complete your profile ");
			}
			else
			{
				$this->messaging_model->sms($phone_number,"Thank you for registering with us. The referred user will accept your request and an account number will be sent to you.");
			}
			return TRUE;



		}
		else
		{

			return FALSE;
		}

	}

	public function create_customer_number()
	{
		//select product code
		$this->db->where('customer_id > 0');
		$this->db->from('customer');
		$this->db->select('MAX(account_number) AS number');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number++;//go to the next number
			if($number == 1){
				$number = "1000001";
			}

			if($number == 1)
			{
				$number = "1000001";
			}

		}
		else{//start generating receipt numbers
			$number = "1000001";
		}
		return $number;
	}

	

	public function get_customer_details($customer_id)
	{
		// 9530
		$this->db->where('customer_id = '.$customer_id);
		$query = $this->db->get('customer');
		
		return $query;
	}

	public function all_packages()
	{
		// 9530
		$this->db->where('package_status = 1');
		$query = $this->db->get('package');
		
		return $query;
	}
	

	public function customer_package_detail($customer_id)
	{
		$this->db->where('package.package_status = 1 AND customer_packages.package_id = package.package_id AND customer_packages.customer_package_status = 1 AND customer_packages.customer_id ='.$customer_id);
		$query = $this->db->get('package,customer_packages');
		
		return $query;
	}
	public function get_package_detail($package_id)
	{
		$this->db->where('package_id',$package_id);
		$query = $this->db->get('package');
		return $query;
	}
	public function check_parent_account($reference_code)
	{

		$this->db->where('customer.customer_id = customer_packages.customer_id AND package.package_id = customer_packages.package_id AND customer_packages.customer_package_status = 1 AND customer.account_number = "'.$reference_code.'"');
		$query = $this->db->get('customer,customer_packages,package');

		return $query;
	}
	public function get_child_accounts($customer_id)
	{
		$this->db->where('customer.customer_status = 1 AND customer.parent_customer_id ='.$customer_id);
		$query = $this->db->get('customer');
		
		return $query;
	}
	
}
?>