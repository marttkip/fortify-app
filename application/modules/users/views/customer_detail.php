<div class="row">
	<div class="col-md-2">
		<section class="card">
			<div class="card-body">
				<div class="thumb-info mb-3">
					<img src="img/!logged-user.jpg" class="rounded img-fluid" alt="John Doe">
					<div class="thumb-info-title">
						<span class="thumb-info-inner">Martin Tarus</span>
						<span class="thumb-info-type"><?php echo 'CUST0001';?></span>
					</div>
				</div>

				<div class="widget-toggle-expand mb-3">
					
					<div class="widget-content-expanded">
						<ul class="mt-3" style="list-style: none;padding: 0px !important;">
							<li><strong>Phone: </strong><br> <?php echo '0704808007';?></li>
							<li><strong>Email: </strong><br><?php echo 'marttkip@gmail.com';?></li>
							<li><strong>Reg Date: </strong><br> <?php echo date('jS M Y',strtotime(date('Y-m-d')));?></li>

						</ul>
					</div>
				</div>


			</div>
		</section>

		<hr class="dotted short">
		<h4 class="mb-3 mt-0">Personal Stats</h4>
		<ul class="simple-card-list mb-3">
			<li class="primary">
				<h4><?php echo '0'?></h4>
				<p class="text-light">Purcahses.</p>
			</li>
			<li class="warning">
				<h4>Kes. <?php echo number_format(0,2);?></h4>
				<p class="text-light">Invoice</p>
			</li>
			<li class="info">
				<h4>Kes. <?php echo number_format(0,2);?></h4>
				<p class="text-light">Payments</p>
			</li>
			<li class="danger">
				<h3>Kes. <?php echo number_format(0,2);?></h3>
				<p class="text-light">Total Revenue</p>
			</li>
		</ul>
	</div>
	<div class="col-md-10">
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a href="<?php echo site_url().'clients'?>" class="btn btn-xs btn-warning"><i class="fa fa-arrow-left"></i> Back to customer </a>
				</div>
				
			</div>
		</div>
		<div class="tabs">
			<ul class="nav nav-tabs tabs-primary">
				<li class="nav-item active">
					<a class="nav-link" href="#visit_info" data-toggle="tab" >General Information</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="#overview" data-toggle="tab">Orders</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="#reviews" data-toggle="tab">Reviews</a>
				</li>
			</ul>
			<div class="tab-content">

				
				<div id="visit_info" class="tab-pane active">
					<div class="padd">
						
						<div id="visit-information"></div>
						
					</div>
					

				</div>
				<div id="overview" class="tab-pane >">
					<div class="padd">
						
						<div id="general-detail"></div>
						
					</div>
					

				</div>
				<div id="reviews" class="tab-pane ">
					<div class="padd">
						
						<div id="general-detail"></div>
						
					</div>
					

				</div>
				
			</div>
		</div>
	</div>

</div>