
          <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    </div>
            
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>users/teams" class="btn btn-info pull-right">Back to teams</a>
                        </div>
                    </div>
                <!-- Adding Errors -->
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
			$row = $team_array;

			// var_dump($row);die();
			//the company details
			$team_id = $row->team_id;
			$team_name = $row->team_name;
			$team_status = $row->team_status;
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
				$team_id = set_value('$row->team_id');
				$team_name = set_value('$row->team_name');
				$team_status = set_value('$row->team_status');
				
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
            <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
            <div class="row">
                    	<div class="col-sm-6">
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Team Name</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="team_name" placeholder="Team Name" value="<?php echo $team_name;?>">
                                </div>
                            </div>
                           
                        </div>
                        
                    	<div class="col-sm-6">
                            
                            
                            <!-- Activate checkbox -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Activate team?</label>
                                <div class="col-lg-8">
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios1" type="radio" checked value="1" name="team_status">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" value="0" name="team_status">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions center-align">
                        <button class="submit btn btn-primary" type="submit">
                            Edit Team
                        </button>
                    </div>
                    <br />
                    <?php echo form_close();?>
                </div>
            </section>