<?php //echo $this->load->view('search/search_teams', '', TRUE);?>

<?php
		
		$result = '';
		
		//if teams exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Created By</th>
						<th>Active Users</th>
						<th>Co-odinates</th>
						<th>Status</th>
						<th colspan="5">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->personnel_model->retrieve_personnel();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			// var_dump($administrators);die();
			
			foreach ($query->result() as $row)
			{
				$team_id = $row->team_id;
				$team_name = $row->team_name;
				$team_status = $row->team_status;
				$created_by = $row->created_by;

				$modified_by = $row->modified_by;
				$teams = 0;
				
				//create deactivated status display
				if($team_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-sm btn-info" href="'.site_url().'activate-team/'.$team_id.'" onclick="return confirm(\'Do you want to activate '.$team_name.'?\');" title="Activate '.$team_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($team_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-sm btn-default" href="'.site_url().'deactivate-team/'.$team_id.'" onclick="return confirm(\'Do you want to deactivate '.$team_name.'?\');" title="Deactivate '.$team_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
				
				//creators & editors
				if($admins != NULL)
				{
					foreach($admins as $adm)
					{
						$team_id = $adm->personnel_id;
						
						if($team_id == $created_by)
						{
							$created_by = $adm->personnel_fname;
						}
						
						if($team_id == $modified_by)
						{
							$modified_by = $adm->personnel_fname;
						}
					}
				}
				
				else
				{
				}

				$team_location = $this->teams_model->get_active_team_location($team_id);;
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$team_name.'</td>
						<td>'.$created_by.'</td>
						<td>0</td>
						<td>'.$team_location.'</td>
						<td>'.$status.'</td>
						<td>
							
							<a href="'.site_url().'allocate-team/'.$team_id.'" class="btn btn-sm btn-info" title="Edit '.$team_name.'"><i class="fa fa-plus"></i> Allocation</a></td>
						<td>
							
							<a href="'.site_url().'edit-team/'.$team_id.'" class="btn btn-sm btn-success" title="Edit '.$team_name.'"><i class="fa fa-pencil"></i></a></td>

							<td>'.$button.'</td>
							<td><a href="'.site_url().'delete-team/'.$team_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$team_name.'?\');" title="Delete '.$team_name.'"><i class="fa fa-trash"></i></a></td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no teams created";
		}
?>
<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <!-- <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul> -->
            </div>            
           
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo $title;?></h2>
                    <ul class="header-dropdown">
                        <li><a href="<?php echo site_url();?>add-team" class="btn btn-success btn-md">Add Team</a></li>
                    </ul>
                </div>
             	<div class="col-md-12">  
				<?php
					$search = $this->session->userdata('personnel_search_title2');
					
					if(!empty($search))
					{
						echo '<h6>Filtered by: '.$search.'</h6>';
						echo '<a href="'.site_url().'hr/personnel/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
					}
	                $success = $this->session->userdata('success_message');

					if(!empty($success))
					{
						echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
						$this->session->unset_userdata('success_message');
					}
					
					$error = $this->session->userdata('error_message');
					
					if(!empty($error))
					{
						echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
						$this->session->unset_userdata('error_message');
					}
					?>
					<div class="table-responsive">
						<?php echo $result;?>
						
					</div>
				</div>
		</div>

	</div>
</div>
</div>
