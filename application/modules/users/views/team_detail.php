<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyBZeFelAM-fM04FBNSbn0DynIuP6klITBw" type="text/javascript"></script> 
<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <!-- <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul> -->
            </div>            
           
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo $title;?></h2>
                    <ul class="header-dropdown">
                        <li><a href="<?php echo site_url();?>security-agents/teams" class="btn btn-success btn-md">Back to Teams</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
	                <div class="row"> 
	                	<div class="col-md-6"> 
								<h3>Team Members</h3>
								<?php
								$users = $this->teams_model->get_unallocated_users($team_id);
								
								echo form_open("add-team-member/".$team_id, array("class" => "form-horizontal"));
								
					            
					            ?>
					            <div class="row">
					                <div class="col-md-8">                   
					                    
					                    <div class="form-group">
					                        <label class="col-md-4 control-label">User: </label>
					                        
					                        <div class="col-md-8">
					                            <!-- <input type="text" class="form-control" name="user_name" placeholder="User Name"> -->
					                            <select class="form-control" name="user_id" required="required">
					                            	<option>---- SELECT A USER -----</option>
					                            	<?php
					                            		if($users->num_rows() > 0)
					                            		{
					                            			foreach ($users->result() as $key => $value) {
					                            				# code...
					                            				$user_id = $value->user_id;
					                            				$user_name = $value->user_name;
					                            				?>
					                            				<option value="<?php echo $user_id;?>"><?php echo $user_name;?></option>
					                            				<?php
					                            			}
					                            		}
					                            	?>
					                            </select>
					                        </div>
					                    </div>
					                </div>
					                
					                
					                <div class="col-md-4">
					                	<div class="form-group">
					                        <div class="col-md-8 col-md-offset-4">
					                            <div class="center-align">
					                                <button type="submit" class="btn btn-info" onclick="return confirm('Are you sure you want to add to team ? ')">Add to team</button>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					            </div>
					            <?php
					            echo form_close();
					            ?>
					            <hr>
					            <?php
					            $query = $this->teams_model->get_active_team_members($team_id);
					            $result = '';
					            if($query->num_rows() > 0)
					            {
					            	$count=0;
					            	$result .= 
										'
										<table class="table table-bordered table-striped table-condensed">
											<thead>
												<tr>
													<th>#</th>
													<th>Name</th>
													<th>Number</th>
													<th colspan="1">Actions</th>
												</tr>
											</thead>
											  <tbody>
											  
										';
					            	foreach ($query->result() as $key => $value) {
					            		# code...
					            		$user_name = $value->user_name;
					            		$team_user_id = $value->team_user_id;
					            		$user_number = $value->user_number;
					            		$count++;
					            		$result .= 
													'
														<tr>
															<td>'.$count.'</td>
															<td>'.$user_name.'</td>
															<td>'.$user_number.'</td>
															<td>
																<a href="'.site_url().'delete-team-user/'.$team_user_id.'/'.$team_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$user_name.'?\');" title="Delete '.$user_name.'"><i class="fa fa-trash"></i></a>
															</td>
															
														</tr> 
													';
					            	}

					            	$result .= 
									'
												  </tbody>
												</table>
									';
								}
								
								else
								{
									// $result .= "There are no users allocated";
								}
					            echo $result;

					            $team_location = $this->teams_model->get_active_team_location($team_id);

					            // var_dump($team_location);die();
					            ?>
					          <hr>
					          <h2 class="center-align">Current Allocation</h2>
					          <br>

					          <h3 class="center-align"><?php echo $team_location;?></h3>

						</div>
						<div class="col-md-6">
							<?php
							
							echo form_open("update-team-allocation/".$team_id, array("class" => "form-horizontal"));
							
				            
				            ?>
							 <div id="map_1" style=" height:60vh;"></div>
				              <input type="hidden" id="location" name="location"  >
				               <div class="col-md-12" style="margin-top: 20px;">
				                	<div class="form-group">
				                        <div class="center-align">
				                            <div class="center-align">
				                                <button type="submit" class="btn btn-info" onclick="return confirm('Are you sure you want to update the location ? ')">Update Location</button>
				                            </div>
				                        </div>
				                    </div>
				                </div>
					            
				            <?php
				            echo form_close();
				            ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


	

<script type="text/javascript">
    var map = new GMap2(document.getElementById("map_1"));
    //var start = new GLatLng(65,25);
    map.setCenter(new GLatLng(-1.265385,36.816444), 12);
    map.addControl(new GMapTypeControl(1));
    map.addControl(new GLargeMapControl());

    map.enableContinuousZoom();
    map.enableDoubleClickZoom();



    // "tiny" marker icon
    var icon = new GIcon();
    icon.image = "http://labs.google.com/ridefinder/images/mm_20_red.png";
    icon.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
    icon.iconSize = new GSize(12, 20);
    icon.shadowSize = new GSize(22, 20);
    icon.iconAnchor = new GPoint(6, 20);
    icon.infoWindowAnchor = new GPoint(5, 1);



    /////Draggable markers




    var point = new GLatLng(-1.265385,36.816444);
    var markerD2 = new GMarker(point, {icon:G_DEFAULT_ICON, draggable: true}); 
    map.addOverlay(markerD2);

    markerD2.enableDragging();

    GEvent.addListener(markerD2, "drag", function(){
    document.getElementById("location").value=markerD2.getPoint().toUrlValue();
    });





    ////Mouse pointer

    GEvent.addListener(map, "mousemove", function(point){
    document.getElementById("mouse").value=point.toUrlValue();
    });

    // second map items

</script>