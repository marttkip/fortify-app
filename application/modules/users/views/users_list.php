<?php //echo $this->load->view('search/search_users', '', TRUE);?>

<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Number</th>
						<th>Created By</th>
						<th>Status</th>
						<th colspan="5">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->personnel_model->retrieve_personnel();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$user_id = $row->user_id;
				$user_name = $row->user_name;
				$user_phone = $row->user_phone;
				$user_email = $row->user_email;
				$user_number = $row->user_number;
				$user_status = $row->user_status;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$users = 0;
				
				//create deactivated status display
				if($user_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-sm btn-info" href="'.site_url().'activate-user/'.$user_id.'" onclick="return confirm(\'Do you want to activate '.$user_name.'?\');" title="Activate '.$user_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($user_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-sm btn-default" href="'.site_url().'deactivate-user/'.$user_id.'" onclick="return confirm(\'Do you want to deactivate '.$user_name.'?\');" title="Deactivate '.$user_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
				
				//creators & editors
				if($admins != NULL)
				{
					foreach($admins as $adm)
					{
						$user_id = $adm->personnel_id;
						
						if($user_id == $created_by)
						{
							$created_by = $adm->personnel_fname;
						}
						
						if($user_id == $modified_by)
						{
							$modified_by = $adm->personnel_fname;
						}
					}
				}
				
				else
				{
				}
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$user_name.'</td>
						<td>'.$user_phone.'</td>
						<td>'.$user_email.'</td>
						<td>'.$user_number.'</td>
						<td>'.$created_by.'</td>
						<td>'.$status.'</td>
						<td>
							<a href="'.site_url().'reset-password/'.$user_id.'" onclick="return confirm(\'Do you really want to rest password '.$user_name.'?\');" class="btn btn-sm btn-warning"><i class="fa fa-recycle"></i> Reset Password</a>
							<a href="'.site_url().'edit-user/'.$user_id.'" class="btn btn-sm btn-success" title="Edit '.$user_name.'"><i class="fa fa-pencil"></i></a>
							'.$button.'
							<a href="'.site_url().'delete-user/'.$user_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$user_name.'?\');" title="Delete '.$user_name.'"><i class="fa fa-trash"></i></a></td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no users created";
		}
?>

 <div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <!-- <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul> -->
            </div>            
           
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo $title;?></h2>
                    <ul class="header-dropdown">
                        <li><a href="<?php echo site_url();?>add-user" class="btn btn-success btn-md">Add User</a></li>
                    </ul>
                </div>
				<?php
					$search = $this->session->userdata('personnel_search_title2');
					
					if(!empty($search))
					{
						echo '<h6>Filtered by: '.$search.'</h6>';
						echo '<a href="'.site_url().'hr/personnel/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
					}
	                $success = $this->session->userdata('success_message');

					if(!empty($success))
					{
						echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
						$this->session->unset_userdata('success_message');
					}
					
					$error = $this->session->userdata('error_message');
					
					if(!empty($error))
					{
						echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
						$this->session->unset_userdata('error_message');
					}
					?>
					<div class="col-md-12">
						<div class="table-responsive">
							<?php echo $result;?>
							<!-- <table class="table  table-striped table-bordered">
								<thead>
									<tr>
										<th>Name</th>
										<th>Phone</th>
										<th>Email</th>
										<th>Registration Date</th>
										<th>Address</th>
										<th>Items Ordered</th>
										<th>Value</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									
									<tr>
										
										<td>Martin Tarus</td>
										<td>0704808007</td>
										<td>marttkip@gmail.com</td>
										
										<td>2020-10-01</td>
										<td>Kilimani</td>
										<td>0</td>
										<td>Kes. 0.00</td>
										<td><a href="<?php echo site_url().'clients/client-detail/1'?>" class="btn btn-xs btn-success"><i class="fa fa-folder"></i> Profile</a></td>
									</tr>
									
								</tbody>
							</table> -->
						</div>
					</div>
			</div>
		</div>
	</div>
</div>