          
<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <!-- <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul> -->
            </div>            
           
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo $title;?></h2>
                    <ul class="header-dropdown">
                        <li><a href="<?php echo site_url();?>security-agents/agents-list" class="btn btn-success btn-md">Back to all agents</a></li>
                    </ul>
                </div>
                <?php
                if(isset($error)){
                    echo '<div class="alert alert-danger"> Oh snap! '.$error.'. </div>';
                }
                
                $validation_errors = validation_errors();
                
                if(!empty($validation_errors))
                {
                    echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                }
                ?>
                
                <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                <div class="row">
                	<div class="col-sm-6">
                        <!-- Company Name -->
                        <div class="form-group">
                            <label class="col-lg-4 control-label">User Name</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="user_name" placeholder="User Name" value="<?php echo set_value('user_name');?>">
                            </div>
                        </div>
                        
                        <!-- Company Name -->
                        <div class="form-group">
                            <label class="col-lg-4 control-label">User Number</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="user_number" placeholder="User Number" value="<?php echo set_value('user_number');?>">
                            </div>
                        </div>
                        
                        <!-- Company Name -->
                        <div class="form-group">
                            <label class="col-lg-4 control-label">User Phone</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="user_phone" placeholder="Phone" value="<?php echo set_value('user_phone');?>">
                            </div>
                        </div>
                       
                    </div>
                    
                	<div class="col-sm-6">
                        
                        <!-- Company Name -->
                        <div class="form-group">
                            <label class="col-lg-4 control-label">User Email</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="user_email" placeholder="Email" value="<?php echo set_value('user_email');?>">
                            </div>
                        </div>
                        
                       
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Activate User?</label>
                            <div class="col-lg-8">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios1" type="radio" checked value="1" name="user_status">
                                        Yes
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" value="0" name="user_status">
                                        No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-actions center-align">
                    <button class="submit btn btn-primary" type="submit">
                        Add User
                    </button>
                </div>
                <br />
                <?php echo form_close();?>
             </div>
        </div>
    </div>
</div>
