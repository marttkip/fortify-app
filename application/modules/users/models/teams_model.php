<?php

class teams_model extends CI_Model 
{	
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	/*
	*	Retrieve all companies
	*
	*/
	public function all_teams()
	{
		$this->db->where('team_status = 1');
		$this->db->order_by('team_name');
		$query = $this->db->get('team');
		
		return $query;
	}
	
	/*
	*	Retrieve all companies
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_teams($table, $where, $per_page, $page, $order = 'team_name', $order_method = 'ASC')
	{
		//retrieve all teams
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Add a new user
	*	@param string $image_name
	*
	*/
	public function add_team()
	{
		$company_id = $this->session->userdata('company_id');
		$data = array(
				'team_name'=>$this->input->post('team_name'),
				'team_status'=>$this->input->post('team_status'),
				'created'=>date('Y-m-d H:i:s'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'modified_by'=>$this->session->userdata('personnel_id'),
				'company_id'=>$company_id
			);
			
		if($this->db->insert('team', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing user
	*	@param string $image_name
	*	@param int $team_id
	*
	*/
	public function update_team($team_id)
	{
		$data = array(
				'team_name'=>$this->input->post('team_name'),
				'team_status'=>$this->input->post('team_status'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		$this->db->where('team_id', $team_id);
		if($this->db->update('team', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single user's details
	*	@param int $team_id
	*
	*/
	public function get_user($team_id)
	{
		$company_id = $this->session->userdata('company_id');
		//retrieve all teams
		$this->db->from('team');
		$this->db->select('*');
		$this->db->where('team_id = '.$team_id.' AND company_id ='.$company_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing user
	*	@param int $team_id
	*
	*/
	public function delete_user($team_id)
	{
		if($this->db->delete('team', array('team_id' => $team_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated user
	*	@param int $team_id
	*
	*/
	public function activate_user($team_id)
	{
		$data = array(
				'team_status' => 1
			);
		$this->db->where('team_id', $team_id);
		
		if($this->db->update('team', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated user
	*	@param int $team_id
	*
	*/
	public function deactivate_user($team_id)
	{
		$data = array(
				'team_status' => 0
			);
		$this->db->where('team_id', $team_id);
		
		if($this->db->update('team', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_unallocated_users($team_id)
	{

		$company_id = $this->session->userdata('company_id');
		//retrieve all teams
		$this->db->from('user');
		$this->db->select('*');
		$this->db->where('user.company_id = '.$company_id.' AND user.user_id NOT IN (SELECT team_user.user_id FROM team_user WHERE team_user.company_id = '.$company_id.' AND team_user.team_id = '.$team_id.' AND team_user.team_user_status = 1)');
		$query = $this->db->get();
		
		return $query;

	}

	public function get_active_team_members($team_id)
	{
		$company_id = $this->session->userdata('company_id');
		//retrieve all teams
		$this->db->from('user,team_user');
		$this->db->select('*');
		$this->db->where('team_user.company_id = '.$company_id.' AND team_user.team_id = '.$team_id.' AND team_user.user_id = user.user_id AND team_user.team_user_status = 1');
		$query = $this->db->get();
		
		return $query;
	}

	public function get_active_team_location($team_id)
	{
		$company_id = $this->session->userdata('company_id');
		//retrieve all teams
		$this->db->from('team_location');
		$this->db->select('*');
		$this->db->where('team_location.company_id = '.$company_id.' AND team_location.team_id = '.$team_id.' AND team_location.team_location_deleted = 0');
		$query = $this->db->get();

		$team_location ='';

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$team_location_latitude = $value->team_location_latitude;
				$team_location_longitude = $value->team_location_longitude;


			}

			$team_location = $team_location_latitude.','.$team_location_longitude;

		}

		// var_dump($team_location);die();
		
		return $team_location;
	}
}
?>