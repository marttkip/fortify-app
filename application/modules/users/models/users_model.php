<?php

class Users_model extends CI_Model 
{	
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	/*
	*	Retrieve all companies
	*
	*/
	public function all_users()
	{
		$this->db->where('user_status = 1');
		$this->db->order_by('user_name');
		$query = $this->db->get('user');
		
		return $query;
	}
	
	/*
	*	Retrieve all companies
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_users($table, $where, $per_page, $page, $order = 'user_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Add a new user
	*	@param string $image_name
	*
	*/
	public function add_user()
	{
		$company_id = $this->session->userdata('company_id');
		$data = array(
				'user_name'=>$this->input->post('user_name'),
				'user_phone'=>$this->input->post('user_phone'),
				'user_email'=>$this->input->post('user_email'),
				'user_number'=>$this->input->post('user_number'),
				'user_status'=>$this->input->post('user_status'),
				'user_password'=>md5(123456),
				'created'=>date('Y-m-d H:i:s'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'modified_by'=>$this->session->userdata('personnel_id'),
				'company_id'=>$company_id
			);
			
		if($this->db->insert('user', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing user
	*	@param string $image_name
	*	@param int $user_id
	*
	*/
	public function update_user($user_id)
	{
		$data = array(
				'user_name'=>$this->input->post('user_name'),
				'user_phone'=>$this->input->post('user_phone'),
				'user_email'=>$this->input->post('user_email'),
				'user_number'=>$this->input->post('user_number'),
				'user_status'=>$this->input->post('user_status'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		$this->db->where('user_id', $user_id);
		if($this->db->update('user', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single user's details
	*	@param int $user_id
	*
	*/
	public function get_user($user_id)
	{
		$company_id = $this->session->userdata('company_id');
		//retrieve all users
		$this->db->from('user');
		$this->db->select('*');
		$this->db->where('user_id = '.$user_id.' AND company_id ='.$company_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing user
	*	@param int $user_id
	*
	*/
	public function delete_user($user_id)
	{
		if($this->db->delete('user', array('user_id' => $user_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated user
	*	@param int $user_id
	*
	*/
	public function activate_user($user_id)
	{
		$data = array(
				'user_status' => 1
			);
		$this->db->where('user_id', $user_id);
		
		if($this->db->update('user', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated user
	*	@param int $user_id
	*
	*/
	public function deactivate_user($user_id)
	{
		$data = array(
				'user_status' => 0
			);
		$this->db->where('user_id', $user_id);
		
		if($this->db->update('user', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
?>