<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Users extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();

		$this->load->model('site/site_model');
		$this->load->model('users/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('reception/reception_model');
		$this->load->model('dental/dental_model');
		$this->load->model('reception/database');
		$this->load->model('messaging/messaging_model');
		$this->load->model('admin/email_model');

		$this->load->model('auth/auth_model');
		
	}

	public function dashboard()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('dashboard', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function users_list()
	{
		$company_id = $this->session->userdata('company_id');

		// var_dump($company_id);die();
		$where = 'company_id ='.$company_id;
		$table = 'user';

		$order = 'user_name';
		$order_method = 'ASC';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'secuirty-agents/agents-list';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->users_model->get_all_users($table, $where, $config["per_page"], $page, $order, $order_method);
		// var_dump($query);die();
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'User Lists';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		// $v_data['all_companies'] = $this->companies_model->all_companies();
		$v_data['page'] = $page;


		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('users_list', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

	public function user_detail($user_id)
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('user_detail', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Add a new company
	*
	*/
	public function add_user() 
	{
		//form validation rules
		$this->form_validation->set_rules('user_name', 'User Name', 'required|xss_clean');
		$this->form_validation->set_rules('user_number', 'Contact name', 'xss_clean');
		$this->form_validation->set_rules('user_phone', 'User Phone', 'xss_clean');
		$this->form_validation->set_rules('user_email', 'User Email', 'xss_clean');
		$this->form_validation->set_rules('user_status', 'Company Status', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			// var_dump($_POST);die();
			if($this->users_model->add_user())
			{
				$this->session->set_userdata('success_message', 'Insurance company added successfully');
				redirect('secuirty-agents/agents-list');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add insurance company. Please try again');
			}
		}
		
		$data['title'] = 'Add user';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('add_user', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Edit an existing company
	*	@param int $company_id
	*
	*/
	public function edit_user($user_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('user_name', 'User Name', 'required|xss_clean');
		$this->form_validation->set_rules('user_number', 'Contact name', 'xss_clean');
		$this->form_validation->set_rules('user_phone', 'User Phone', 'xss_clean');
		$this->form_validation->set_rules('user_email', 'User Email', 'xss_clean');
		$this->form_validation->set_rules('user_status', 'Company Status', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update user
			if($this->users_model->update_user($user_id))
			{
				$this->session->set_userdata('success_message', 'user updated successfully');
				redirect('secuirty-agents/agents-list');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update user. Please try again');
			}
		}
		
		//open the add new user
		$data['title'] = 'Edit user';
		$v_data['title'] = $data['title'];
		
		//select the user from the database
		$query = $this->users_model->get_user($user_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['user_array'] = $query->row();
			
			$data['content'] = $this->load->view('edit_user', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Company does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}


	/*
	*
	*	Delete an existing company
	*	@param int $company_id
	*
	*/
	public function delete_user($user_id)
	{
		$this->users_model->delete_user($user_id);
		$this->session->set_userdata('success_message', 'user has been deleted');
		redirect('secuirty-agents/agents-list');
	}
    
	/*
	*
	*	Activate an existing user
	*	@param int $user_id
	*
	*/
	public function activate_user($user_id)
	{
		$this->users_model->activate_user($user_id);
		$this->session->set_userdata('success_message', 'user activated successfully');
		redirect('secuirty-agents/agents-list');
	}
    
	/*
	*
	*	Deactivate an existing user
	*	@param int $user_id
	*
	*/
	public function deactivate_user($user_id)
	{
		$this->users_model->deactivate_user($user_id);
		$this->session->set_userdata('success_message', 'Company disabled successfully');
		redirect('secuirty-agents/agents-list');
	}

	public function reset_password($user_id)
	{
		$array['user_password'] = md5(123456);
		$this->db->where('user_id',$user_id);
		$this->db->get('user',$array);

		$this->session->set_userdata('success_message', 'Successfully reset password');
		redirect('secuirty-agents/agents-list');
	}
}
?>