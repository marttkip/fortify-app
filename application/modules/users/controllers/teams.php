<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL);
class Teams extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();

		$this->load->model('site/site_model');
		$this->load->model('users/teams_model');

		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('reception/reception_model');
		// $this->load->model('dental/dental_model');
		$this->load->model('reception/database');
		$this->load->model('messaging/messaging_model');
		$this->load->model('admin/email_model');

		$this->load->model('auth/auth_model');
		
	}

	public function dashboard()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('dashboard', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function teams_list()
	{
		$company_id = $this->session->userdata('company_id');

		// var_dump($company_id);die();
		$where = 'company_id ='.$company_id;
		$table = 'team';

		$order = 'team_name';
		$order_method = 'ASC';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'users/teams';
		$config['total_rows'] = $this->teams_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->teams_model->get_all_teams($table, $where, $config["per_page"], $page, $order, $order_method);
		// var_dump($query);die();
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Team Lists';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		// $v_data['all_companies'] = $this->companies_model->all_companies();
		$v_data['page'] = $page;


		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('teams_list', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

	public function team_detail($team_id)
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('team_detail', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Add a new company
	*
	*/
	public function add_team() 
	{
		//form validation rules
		$this->form_validation->set_rules('team_name', 'User Name', 'required|xss_clean');
		$this->form_validation->set_rules('team_status', 'Company Status', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			// var_dump($_POST);die();
			if($this->teams_model->add_team())
			{
				$this->session->set_userdata('success_message', 'Insurance company added successfully');
				redirect('users/teams');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add insurance company. Please try again');
			}
		}
		
		$data['title'] = 'Add team';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('add_team', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Edit an existing company
	*	@param int $company_id
	*
	*/
	public function edit_team($team_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('team_name', 'Team Name', 'required|xss_clean');
		$this->form_validation->set_rules('team_status', 'Team Status', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update user
			if($this->teams_model->update_team($team_id))
			{
				$this->session->set_userdata('success_message', 'user updated successfully');
				redirect('users/teams');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update user. Please try again');
			}
		}
		
		//open the add new user
		$data['title'] = 'Edit team';
		$v_data['title'] = $data['title'];
		
		//select the user from the database
		$query = $this->teams_model->get_user($team_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['team_array'] = $query->row();
			
			$data['content'] = $this->load->view('edit_team', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Team does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}


	/*
	*
	*	Delete an existing company
	*	@param int $company_id
	*
	*/
	public function delete_team($team_id)
	{
		$this->teams_model->delete_team($team_id);
		$this->session->set_userdata('success_message', 'user has been deleted');
		redirect('users/teams');
	}
    
	/*
	*
	*	Activate an existing user
	*	@param int $team_id
	*
	*/
	public function activate_team($team_id)
	{
		$this->teams_model->activate_team($team_id);
		$this->session->set_userdata('success_message', 'user activated successfully');
		redirect('users/teams');
	}
    
	/*
	*
	*	Deactivate an existing user
	*	@param int $team_id
	*
	*/
	public function deactivate_team($team_id)
	{
		$this->teams_model->deactivate_team($team_id);
		$this->session->set_userdata('success_message', 'Company disabled successfully');
		redirect('users/teams');
	}

	public function allocate_team($team_id)
	{

		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$v_data['team_id'] = $team_id;
		$data['content'] = $this->load->view('team_detail', $v_data, true);
		// $data['content'] = $this->load->view('profile_page', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function add_team_member($team_id)
	{

		$this->form_validation->set_rules('user_id', 'User', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$array['user_id'] = $this->input->post('user_id');
			$array['team_id'] = $team_id;
			$array['company_id'] =$this->session->userdata('company_id');
			$array['created'] = date('Y-m-d');
			$array['created_by'] =$this->session->userdata('personnel_id');

			//update user
			if($this->db->insert('team_user',$array))
			{
				$this->session->set_userdata('success_message', 'User added successfully');
				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add user. Please try again');
			}
		}

		redirect('allocate-team/'.$team_id);
		

	}

	public function update_team_location($team_id)
	{

		$this->form_validation->set_rules('location', 'Location', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			// update other 
			$company_id =$this->session->userdata('company_id');
			$array_checked['team_location_deleted'] = 1;
			$this->db->where('team_id = '.$team_id.' AND company_id = '.$company_id);
			$this->db->insert('team_location',$array_checked);

			$start_location = explode(',', $this->input->post('location')); 

			$client_latitude = $start_location[0];
			$client_longitude = $start_location[1];

			$array['team_location_latitude'] = $this->input->post('user_id');
			$array['team_id'] = $team_id;
			$array['company_id'] = $this->session->userdata('company_id');
			$array['team_location_longitude'] =$client_longitude;
			$array['team_location_latitude'] =$client_latitude;
			$array['created'] = date('Y-m-d');
			$array['created_by'] =$this->session->userdata('personnel_id');

			//update user
			if($this->db->insert('team_location',$array))
			{
				$this->session->set_userdata('success_message', 'Location successfully updated');
				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update location. Please try again');
			}
		}

		redirect('allocate-team/'.$team_id);

	}

}
?>