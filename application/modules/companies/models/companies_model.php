<?php

class Companies_model extends CI_Model 
{	
	/*
	*	Retrieve all companies
	*
	*/
	public function all_companies()
	{
		$this->db->where('company_status = 1');
		$this->db->order_by('company_name');
		$query = $this->db->get('company');
		
		return $query;
	}
	
	/*
	*	Retrieve all companies
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_companies($table, $where, $per_page, $page, $order = 'company_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Add a new company
	*	@param string $image_name
	*
	*/
	public function add_company()
	{
		$start_location = explode(',', $this->input->post('location')); 

		$client_latitude = $start_location[0];
		$client_longitude = $start_location[1];

		// var_dump($_POST);die();
		$data = array(
				'company_name'=>$this->input->post('company_name'),
				'company_contact_person_name'=>$this->input->post('company_contact_person_name'),
				'company_contact_person_phone1'=>$this->input->post('company_contact_person_phone1'),
				'company_contact_person_phone2'=>$this->input->post('company_contact_person_phone2'),
				'company_contact_person_email1'=>$this->input->post('company_contact_person_email1'),
				'company_contact_person_email2'=>$this->input->post('company_contact_person_email2'),
				'company_description'=>$this->input->post('company_description'),
				'company_status'=>$this->input->post('company_status'),
				'admin_password'=>md5(123456),
				'admin_username'=>$this->input->post('admin_username'),
				'created'=>date('Y-m-d H:i:s'),
				'company_latitude'=>$client_latitude,
				'company_longitude'=>$client_longitude,
				'created_by'=>$this->session->userdata('personnel_id'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('company', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing company
	*	@param string $image_name
	*	@param int $company_id
	*
	*/
	public function update_company($company_id)
	{
		$start_location = explode(',', $this->input->post('location')); 

		$client_latitude = $start_location[0];
		$client_longitude = $start_location[1];

		$data = array(
				'company_name'=>$this->input->post('company_name'),
				'company_contact_person_name'=>$this->input->post('company_contact_person_name'),
				'company_contact_person_phone1'=>$this->input->post('company_contact_person_phone1'),
				'company_contact_person_phone2'=>$this->input->post('company_contact_person_phone2'),
				'company_contact_person_email1'=>$this->input->post('company_contact_person_email1'),
				'company_contact_person_email2'=>$this->input->post('company_contact_person_email2'),
				'company_description'=>$this->input->post('company_description'),
				'company_status'=>$this->input->post('company_status'),
				'modified_by'=>$this->session->userdata('personnel_id'),
				'company_latitude'=>$company_latitude,
				'company_longitude'=>$company_longitude
			);
			
		$this->db->where('company_id', $company_id);
		if($this->db->update('company', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single company's details
	*	@param int $company_id
	*
	*/
	public function get_company($company_id)
	{
		//retrieve all users
		$this->db->from('company');
		$this->db->select('*');
		$this->db->where('company_id = '.$company_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing company
	*	@param int $company_id
	*
	*/
	public function delete_company($company_id)
	{
		if($this->db->delete('company', array('company_id' => $company_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated company
	*	@param int $company_id
	*
	*/
	public function activate_company($company_id)
	{
		$data = array(
				'company_status' => 1
			);
		$this->db->where('company_id', $company_id);
		
		if($this->db->update('company', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated company
	*	@param int $company_id
	*
	*/
	public function deactivate_company($company_id)
	{
		$data = array(
				'company_status' => 0
			);
		$this->db->where('company_id', $company_id);
		
		if($this->db->update('company', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
?>