<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyA4aMmzYaIn4DiPAA4dWbueAtu4wMaVwJ8" type="text/javascript"></script>   <<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul>
            </div>            
            <div class="col-lg-6 col-md-4 col-sm-12 text-right">
                
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo $title;?></h2>
                    <ul class="header-dropdown">
                        <li><a href="<?php echo site_url();?>administration/companies" class="btn btn-info pull-right">Back to companies</a></li>

                    </ul>
                </div>
                <div class="body">
    	
                    <?php
                    if(isset($error)){
                        echo '<div class="alert alert-danger"> Oh snap! '.$error.'. </div>';
                    }
                    
                    $validation_errors = validation_errors();
                    
                    if(!empty($validation_errors))
                    {
                        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                    }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                    <div class="row">
                    	<div class="col-sm-6">
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Company Name</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="<?php echo set_value('company_name');?>">
                                </div>
                            </div>
                            
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Concact Person Name</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_contact_person_name" placeholder="Concact Person Name" value="<?php echo set_value('company_contact_person_name');?>">
                                </div>
                            </div>
                            
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Contact Person Phone 1</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_contact_person_phone1" placeholder="Contact Person Phone 1" value="<?php echo set_value('company_contact_person_phone1');?>">
                                </div>
                            </div>
                            
                             <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Contact Person email 1</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_contact_person_email1" placeholder="Contact Person email 1" value="<?php echo set_value('company_contact_person_email1');?>">
                                </div>
                            </div>
                            
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Company Description</label>
                                <div class="col-lg-8">
                                    <textarea name="company_description" class="form-control" rows="5" placeholder="Company Description"><?php echo set_value('company_description');?></textarea>
                                </div>
                            </div>
                            <!-- Activate checkbox -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Activate Company?</label>
                                <div class="col-lg-8">
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios1" type="radio" checked value="1" name="company_status">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" value="0" name="company_status">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    	<div class="col-sm-6">
                            <div id="map_1" style=" height:500px;"></div>
                           <input type="hidden" id="location" name="location"  >
                           
                        </div>
                    </div>
                    
                    
                    <div class="form-actions center-align">
                        <button class="submit btn btn-primary" type="submit">
                            Add company
                        </button>
                    </div>
                    <br />


                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    var map = new GMap2(document.getElementById("map_1"));
    //var start = new GLatLng(65,25);
    map.setCenter(new GLatLng(-1.265385,36.816444), 12);
    map.addControl(new GMapTypeControl(1));
    map.addControl(new GLargeMapControl());

    map.enableContinuousZoom();
    map.enableDoubleClickZoom();



    // "tiny" marker icon
    var icon = new GIcon();
    icon.image = "http://labs.google.com/ridefinder/images/mm_20_red.png";
    icon.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
    icon.iconSize = new GSize(12, 20);
    icon.shadowSize = new GSize(22, 20);
    icon.iconAnchor = new GPoint(6, 20);
    icon.infoWindowAnchor = new GPoint(5, 1);



    /////Draggable markers




    var point = new GLatLng(-1.265385,36.816444);
    var markerD2 = new GMarker(point, {icon:G_DEFAULT_ICON, draggable: true}); 
    map.addOverlay(markerD2);

    markerD2.enableDragging();

    GEvent.addListener(markerD2, "drag", function(){
    document.getElementById("location").value=markerD2.getPoint().toUrlValue();
    });





    ////Mouse pointer

    GEvent.addListener(map, "mousemove", function(point){
    document.getElementById("mouse").value=point.toUrlValue();
    });

    // second map items

</script>