<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul>
            </div>            
            <div class="col-lg-6 col-md-4 col-sm-12 text-right">
                
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo $title;?></h2>
                    <ul class="header-dropdown">
                        <li><a href="<?php echo site_url();?>administration/companies" class="btn btn-info pull-right">Back to companies</a></li>

                    </ul>
                </div>
                <div class="body">
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
			$row = $company_array;
			//the company details
			$company_id = $row->company_id;
			$company_name = $row->company_name;
			$company_contact_person_name = $row->company_contact_person_name;
			$company_contact_person_phone1 = $row->company_contact_person_phone1;
			$company_contact_person_phone2 = $row->company_contact_person_phone2;
			$company_contact_person_email1 = $row->company_contact_person_email1;
			$company_contact_person_email2 = $row->company_contact_person_email2;
			$company_description = $row->company_description;
			$company_status = $row->company_status;
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
				$company_id = set_value('$row->company_id');
				$company_name = set_value('$row->company_name');
				$company_contact_person_name = set_value('$row->company_contact_person_name');
				$company_contact_person_phone1 = set_value('$row->company_contact_person_phone1');
				$company_contact_person_phone2 = set_value('$row->company_contact_person_phone2');
				$company_contact_person_email1 = set_value('$row->company_contact_person_email1');
				$company_contact_person_email2 = set_value('$row->company_contact_person_email2');
				$company_description = set_value('$row->company_description');
				$company_status = set_value('$row->company_status');
				
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
            <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
            <div class="row">
                    	<div class="col-sm-6">
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Company Name</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="<?php echo $company_name;?>">
                                </div>
                            </div>
                            
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Concact Person Name</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_contact_person_name" placeholder="Concact Person Name" value="<?php echo $company_contact_person_name;?>">
                                </div>
                            </div>
                            
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Contact Person Phone 1</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_contact_person_phone1" placeholder="Contact Person Phone 1" value="<?php echo $company_contact_person_phone1;?>">
                                </div>
                            </div>
                            
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Contact Person Phone 2</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_contact_person_phone2" placeholder="Contact Person Phone 2" value="<?php echo $company_contact_person_phone2;?>">
                                </div>
                            </div>
                        </div>
                        
                    	<div class="col-sm-6">
                            
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Contact Person email 1</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_contact_person_email1" placeholder="Contact Person email 1" value="<?php echo $company_contact_person_email1;?>">
                                </div>
                            </div>
                            
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Contact Person email 2</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="company_contact_person_email2" placeholder="Contact Person email 2" value="<?php echo $company_contact_person_email2;?>">
                                </div>
                            </div>
                            
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Insurance Company Description</label>
                                <div class="col-lg-8">
                                	<textarea name="company_description" class="form-control" rows="5" placeholder="Insurance Company Description"><?php echo $company_description;?></textarea>
                                </div>
                            </div>
                            <!-- Activate checkbox -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Activate Company?</label>
                                <div class="col-lg-8">
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios1" type="radio" checked value="1" name="company_status">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" value="0" name="company_status">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row" style="margin-bottom: 10px" >
                        <div class="col-sm-12 center-align">
                            <!-- Company Name -->
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Admin Username</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" name="admin_username" placeholder="Username" value="<?php echo $admin_username;?>" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions center-align">
                        <button class="submit btn btn-primary" type="submit">
                            Edit company
                        </button>
                    </div>
                    <br />
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
</div>