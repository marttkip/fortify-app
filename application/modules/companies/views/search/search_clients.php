<div class="row">

	<section class="panel panel-primary">
	    <header class="panel-heading">
	        <h2 class="panel-title">Search </h2>
	    </header>
	    <!-- Widget content -->
	         <div class="panel-body">
	    	<div class="padd">
				<?php
				
				
				echo form_open("reception/search_general_queue", array("class" => "form-horizontal"));
				
	            
	            ?>
	            <div class="row">
	                <div class="col-md-4">                   
	                    
	                    <div class="form-group">
	                        <label class="col-md-4 control-label">Client Name: </label>
	                        
	                        <div class="col-md-8">
	                            <input type="text" class="form-control" name="patient_number" placeholder="Patient number">
	                        </div>
	                    </div>
	                </div>
	                
	                <div class="col-md-4">
	                    
	                    <div class="form-group">
	                        <label class="col-md-4 control-label">Client Number: </label>
	                        
	                        <div class="col-md-8">
	                            <input type="text" class="form-control" name="surname" placeholder="Patient name">
	                        </div>
	                    </div>
	                    
	                </div>
	                <div class="col-md-4">
	                	<div class="form-group">
	                        <div class="col-md-8 col-md-offset-4">
	                            <div class="center-align">
	                                <button type="submit" class="btn btn-info">Search</button>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <?php
	            echo form_close();
	            ?>
	    	</div>
	</section>
	
</div>