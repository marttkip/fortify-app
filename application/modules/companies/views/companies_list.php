<?php //echo $this->load->view('search/search_clients', '', TRUE);?>

<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Company name</th>
						<th>Contact person</th>
						<th>Primary phone</th>
						<th>Primary email</th>
						<th>Users</th>
						<th>Cases Handled</th>
						<th>Status</th>
						<th colspan="5">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->personnel_model->retrieve_personnel();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$company_id = $row->company_id;
				$company_name = $row->company_name;
				$company_contact_person_name = $row->company_contact_person_name;
				$company_contact_person_phone1 = $row->company_contact_person_phone1;
				$company_contact_person_phone2 = $row->company_contact_person_phone2;
				$company_contact_person_email1 = $row->company_contact_person_email1;
				$company_contact_person_email2 = $row->company_contact_person_email2;
				$company_description = $row->company_description;
				$company_status = $row->company_status;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$users = 0;
				
				//create deactivated status display
				if($company_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-sm btn-info" href="'.site_url().'activate-company/'.$company_id.'" onclick="return confirm(\'Do you want to activate '.$company_name.'?\');" title="Activate '.$company_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($company_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-sm btn-default" href="'.site_url().'deactivate-company/'.$company_id.'" onclick="return confirm(\'Do you want to deactivate '.$company_name.'?\');" title="Deactivate '.$company_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
				
				//creators & editors
				if($admins != NULL)
				{
					foreach($admins as $adm)
					{
						$user_id = $adm->personnel_id;
						
						if($user_id == $created_by)
						{
							$created_by = $adm->personnel_fname;
						}
						
						if($user_id == $modified_by)
						{
							$modified_by = $adm->personnel_fname;
						}
					}
				}
				
				else
				{
				}
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$company_name.'</td>
						<td>'.$company_contact_person_name.'</td>
						<td>'.$company_contact_person_phone1.'</td>
						<td>'.$company_contact_person_email1.'</td>
						<td>'.$users.'</td>
						<td>'.$users.'</td>
						<td>'.$status.'</td>
						<td>
							<a href="'.site_url().'companies/company-detail/'.$company_id.'" class="btn btn-sm btn-primary"><i class="fa fa-folder-open"></i> Profile</a>
							<a href="'.site_url().'edit-company/'.$company_id.'" class="btn btn-sm btn-success" title="Edit '.$company_name.'"><i class="fa fa-pencil"></i></a>
							'.$button.'
							<a href="'.site_url().'delete-company/'.$company_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$company_name.'?\');" title="Delete '.$company_name.'"><i class="fa fa-trash"></i></a></td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no insurance companies";
		}
?>
<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo $title?></h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url().'dashboard'?>"><i class="icon-home"></i></a></li>      
                    <li class="breadcrumb-item active"><?php echo $title;?></li>
                </ul>
            </div>            
            <div class="col-lg-6 col-md-4 col-sm-12 text-right">
                <div class="bh_chart hidden-xs">
                    <div class="float-left m-r-15">
                        <small>Visitors</small>
                        <h6 class="mb-0 mt-1"><i class="icon-user"></i> 1,784</h6>
                    </div>
                    <span class="bh_visitors float-right">2,5,1,8,3,6,7,5</span>
                </div>
                <div class="bh_chart hidden-sm">
                    <div class="float-left m-r-15">
                        <small>Visits</small>
                        <h6 class="mb-0 mt-1"><i class="icon-globe"></i> 325</h6>
                    </div>
                    <span class="bh_visits float-right">10,8,9,3,5,8,5</span>
                </div>
                <div class="bh_chart hidden-sm">
                    <div class="float-left m-r-15">
                        <small>Chats</small>
                        <h6 class="mb-0 mt-1"><i class="icon-bubbles"></i> 13</h6>
                    </div>
                    <span class="bh_chats float-right">1,8,5,6,2,4,3,2</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo $title;?></h2>
                    <ul class="header-dropdown">
                        <li><a href="<?php echo site_url();?>add-company" class="btn btn-success  btn-xs"> Add Company</a></li>

                    </ul>
                </div>
                <div class="body">
					<div class="table-responsive">
						<?php echo $result;?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
