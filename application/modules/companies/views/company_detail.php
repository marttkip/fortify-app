<?php
$row = $company_array;
//the company details
$company_id = $row->company_id;
$company_name = $row->company_name;
$company_contact_person_name = $row->company_contact_person_name;
$company_contact_person_phone1 = $row->company_contact_person_phone1;
$company_contact_person_phone2 = $row->company_contact_person_phone2;
$company_contact_person_email1 = $row->company_contact_person_email1;
$company_contact_person_email2 = $row->company_contact_person_email2;
$company_description = $row->company_description;
$company_status = $row->company_status;

?>
<div class="row" style="margin-top: 10px;">
	<div class="col-md-2">
		<section class="card">
			<div class="card-body">
				<div class="thumb-info mb-3">
					<img src="http://placehold.it/250x200" class="rounded img-fluid" alt="John Doe">
					<div class="thumb-info-title">
						<span class="thumb-info-inner"><?php echo $company_name?></span>
						<span class="thumb-info-type"><?php echo 'CUST0001';?></span>
					</div>
				</div>

				<div class="widget-toggle-expand mb-3">
					
					<div class="widget-content-expanded">
						<ul class="mt-3" style="list-style: none;padding: 0px !important;">
							<li><strong>Phone: </strong><br> <?php echo $company_contact_person_phone1;?></li>
							<li><strong>Email: </strong><br><?php echo $company_contact_person_email1;?></li>
							<li><strong>Reg Date: </strong><br> <?php echo date('jS M Y',strtotime(date('Y-m-d')));?></li>

						</ul>
					</div>
				</div>


			</div>
		</section>

		<hr class="dotted short">
		<h4 class="mb-3 mt-0">Personal Stats</h4>
		<ul class="simple-card-list mb-3">
			<li class="primary">
				<h4><?php echo '0'?></h4>
				<p class="text-light">Purcahses.</p>
			</li>
			<li class="warning">
				<h4>Kes. <?php echo number_format(0,2);?></h4>
				<p class="text-light">Invoice</p>
			</li>
			<li class="info">
				<h4>Kes. <?php echo number_format(0,2);?></h4>
				<p class="text-light">Payments</p>
			</li>
			<li class="danger">
				<h3>Kes. <?php echo number_format(0,2);?></h3>
				<p class="text-light">Total Revenue</p>
			</li>
		</ul>
	</div>
	<div class="col-md-10">

		<div class="row">
	
			<div class="col-md-12 col-lg-12 col-xl-12">
				<div class="row">
					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-primary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-primary">
											<i class="fa fa-life-ring"></i>
										</div>
									</div>
									<div class="widget-summary-col">
										<div class="summary">
											<h4 class="title">Total Staff</h4>
											<div class="info">
												<strong class="amount">0</strong>
												<!-- <span class="text-primary">(14 unread)</span> -->
											</div>
										</div>
										<div class="summary-footer">
											<!-- <a class="text-muted text-uppercase">(view all)</a> -->
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-secondary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-secondary">
											<i class="fa fa-bell"></i>
										</div>
									</div>
									<div class="widget-summary-col">
										<div class="summary">
											<h4 class="title">Alarms Requested</h4>
											<div class="info">
												<strong class="amount"> 0</strong>
											</div>
										</div>
										<div class="summary-footer">
											<!-- <a class="text-muted text-uppercase">(list)</a> -->
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-tertiary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-tertiary">
											<i class="fa fa-shopping-cart"></i>
										</div>
									</div>
									<div class="widget-summary-col">
										<div class="summary">
											<h4 class="title"> Responses</h4>
											<div class="info">
												<strong class="amount">0</strong>
											</div>
										</div>
										<div class="summary-footer">
											<!-- <a class="text-muted text-uppercase">(list)</a> -->
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
					<div class="col-md-12 col-lg-3 col-xl-3">
						<section class="panel panel-featured-left panel-featured-quartenary">
							<div class="panel-body">
								<div class="widget-summary">
									<div class="widget-summary-col widget-summary-col-icon">
										<div class="summary-icon bg-quartenary">
											<i class="fa fa-thumb-up"></i>
										</div>
									</div>
									<div class="widget-summary-col">
										<div class="summary">
											<h4 class="title">Rating</h4>
											<div class="info">
												<strong class="amount">5</strong>
											</div>
										</div>
										<div class="summary-footer">
											<a class="text-muted text-uppercase">(report)</a>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a href="<?php echo site_url().'administration/companies'?>" class="btn btn-sm btn-warning"><i class="fa fa-arrow-left"></i> Back to companies </a>
				</div>
				
			</div>
		</div>
		<div class="tabs">
			<ul class="nav nav-tabs tabs-primary">
				<li class="nav-item active">
					<a class="nav-link" href="#visit_info" data-toggle="tab" >General Information</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="#staff-member" data-toggle="tab">Staff Members</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="#staff-groups" data-toggle="tab">Groups</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="#overview" data-toggle="tab">Alarms Responded</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="#reviews" data-toggle="tab">Clients Reponses & Reviews</a>
				</li>
			</ul>
			<div class="tab-content">

				
				<div id="visit_info" class="tab-pane active">
					<div class="padd">
						
						<div id="visit-information"></div>
						
					</div>
					

				</div>
				<div id="overview" class="tab-pane >">
					<div class="padd">
						
						<div id="general-detail"></div>
						
					</div>
					

				</div>
				<div id="reviews" class="tab-pane ">
					<div class="padd">
						
						<div id="general-detail"></div>
						
					</div>
					

				</div>
				
			</div>
		</div>
	</div>

</div>