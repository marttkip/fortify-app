<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Sizes extends MX_Controller {
	var $sizes_path;
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('admin/users_model');
		$this->load->model('sizes_model');
		$this->load->model('admin/file_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('site/site_model');
		$this->load->model('administration/personnel_model');
		//path to image directory
	}
    
	/*
	*
	*	Default action is to show all the sizes
	*
	*/

	public function index() 
	{
		//$where = 'created_by IN (0, '.$this->session->userdata('vendor_id').')';
		//$where = 'branch_code = "'.$this->session->userdata('branch_code').'"';
		$where = "size_id > 0";
		$table = 'size';

		$size_search = $this->session->userdata('size_search');
		
		if(!empty($size_search))
		{
			$where .= $size_search;
		}
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'product-sizes';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->sizes_model->get_all_sizes($table, $where, $config["per_page"], $page);
	
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['title'] = 'All sizes';
		$data['content'] = $this->load->view('sizes/all_sizes', $v_data, true);
		
		$data['title'] = 'All sizes';
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Add a new size
	*
	*/
	public function add_size() 
	{
		//form validation rules
		$this->form_validation->set_rules('size_name', 'size Name', 'required|xss_clean');
		$this->form_validation->set_rules('size_parent', 'size Parent', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//upload product's gallery images
			
			if($this->sizes_model->add_size())
			{
				$this->session->set_userdata('success_message', 'size added successfully');
				redirect('inventory/product-sizes');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add size. Please try again');
			}
		}
		
		//open the add new size
		$data['title'] = 'Add New size';
		$v_data['title'] = 'Add New size';
		$v_data['all_sizes'] = $this->sizes_model->all_parent_sizes();
		$data['content'] = $this->load->view('sizes/add_size', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Edit an existing size
	*	@param int $size_id
	*
	*/
	public function edit_size($size_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('size_name', 'size Name', 'required|xss_clean');
		$this->form_validation->set_rules('size_status', 'size Status', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->sizes_model->update_size($size_id))
			{
				$this->session->set_userdata('success_message', 'size updated successfully');
				redirect('inventory/product-sizes');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update size. Please try again');
			}
		}
		
		//open the add new size
		$data['title'] = 'Edit size';
		$v_data['title'] = 'Edit size';
		
		//select the size from the database
		$query = $this->sizes_model->get_size($size_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['size'] = $query->result();
			$v_data['all_sizes'] = $this->sizes_model->all_parent_sizes();
			
			$data['content'] = $this->load->view('sizes/edit_size', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'size does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing size
	*	@param int $size_id
	*
	*/
	public function delete_size($size_id)
	{
		//delete size image
		$query = $this->sizes_model->get_size($size_id);
		
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			$image = $result[0]->size_image_name;
			
			$this->load->model('file_model');
			//delete image
			$this->file_model->delete_file($this->sizes_path."/images/".$image, $this->sizes_path);
			//delete thumbnail
			$this->file_model->delete_file($this->sizes_path."/thumbs/".$image, $this->sizes_path);
		}
		$this->sizes_model->delete_size($size_id);
		$this->session->set_userdata('success_message', 'size has been deleted');
		redirect('inventory/product-sizes');
	}
    
	/*
	*
	*	Activate an existing size
	*	@param int $size_id
	*
	*/
	public function activate_size($size_id)
	{
		$this->sizes_model->activate_size($size_id);
		$this->session->set_userdata('success_message', 'size activated successfully');
		redirect('inventory/product-sizes');
	}
    
	/*
	*
	*	Deactivate an existing size
	*	@param int $size_id
	*
	*/
	public function deactivate_size($size_id)
	{
		$this->sizes_model->deactivate_size($size_id);
		$this->session->set_userdata('success_message', 'size disabled successfully');
		redirect('inventory/product-sizes');
	}
	public function search_sizes()
	{

		$size_name = $this->input->post('size_name');


		if(!empty($size_name))
		{
			$size_name = ' AND size.size_name LIKE \'%'.mysql_real_escape_string($size_name).'%\' ';
		}
		
		
		$search = $size_name;
		$this->session->set_userdata('size_search', $search);
		
		$this->index();
		
	}
	public function close_sizes_search()
	{
		$this->session->unset_userdata('size_search');
		redirect('inventory/product-sizes');
	}
}
?>