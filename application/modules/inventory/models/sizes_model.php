<?php

class Sizes_model extends CI_Model 
{	
	/*
	*	Retrieve all sizes
	*
	*/
	public function all_sizes()
	{
		$this->db->where('size_status = 1');
		$this->db->order_by('size_name');
		$query = $this->db->get('size');
		
		return $query;
	}
	/*
	*	Retrieve latest size
	*
	*/
	public function latest_size()
	{
		$this->db->limit(1);
		$this->db->order_by('created', 'DESC');
		$query = $this->db->get('size');
		
		return $query;
	}
	/*
	*	Retrieve all parent sizesget_all_sections
	*
	*/
	public function all_parent_sizes()
	{
		$this->db->where('size_status = 1 AND size_parent = 0 AND branch_code = "'.$this->session->userdata('branch_code').'"');
		$this->db->order_by('size_name', 'ASC');
		$query = $this->db->get('size');
		
		return $query;
	}
	/*
	*	Retrieve all children sizes
	*
	*/
	public function all_child_sizes()
	{
		$this->db->where('size_status = 1 AND size_parent > 0');
		$this->db->order_by('size_name', 'ASC');
		$query = $this->db->get('size');
		
		return $query;
	}
	
	/*
	*	Retrieve all sizes
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_sizes($table, $where, $per_page, $page)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('size_name, size_parent');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Add a new size
	*	@param string $image_name
	*
	*/
	public function add_size()
	{
		
		$charset = str_replace(" ", "", $this->input->post('size_name'));
		$first_char = substr($charset, 0,1);
		$prefix = substr(str_shuffle($charset), 0, 2);

		$size_prefix = strtoupper(strtolower($first_char.$prefix));
		// var_dump($first_char);die();

		$data = array(
				'size_name'=>ucwords(strtolower($this->input->post('size_name'))),
				'size_parent'=>$this->input->post('size_parent'),
				'size_status'=>$this->input->post('size_status'),
				'size_preffix'=>$size_prefix,
				'created'=>date('Y-m-d H:i:s'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'modified_by'=>$this->session->userdata('personnel_id'),
				'branch_code'=>$this->session->userdata('branch_code')
			);
			
		if($this->db->insert('size', $data))
		{
			return TRUE;
		}

		
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing size
	*	@param string $image_name
	*	@param int $size_id
	*
	*/
	public function update_size($size_id)
	{
		$data = array(
				'size_name'=>ucwords(strtolower($this->input->post('size_name'))),
				'size_parent'=>$this->input->post('size_parent'),
				'size_status'=>$this->input->post('size_status'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		$this->db->where('branch_code = "'.$this->session->userdata('branch_code').'"AND size_id = '.$size_id);
		if($this->db->update('size', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single size's children
	*	@param int $size_id
	*
	*/
	public function get_sub_sizes($size_id)
	{
		//retrieve all users
		$this->db->from('size');
		$this->db->select('*');
		$this->db->where('size_parent = '.$size_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single size's details
	*	@param int $size_id
	*
	*/
	public function get_size($size_id)
	{
		//retrieve all users
		$this->db->from('size');
		$this->db->select('*');
		$this->db->where('size_id = '.$size_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single size's details
	*	@param int $size_id
	*
	*/
	public function get_size_by_name($size_name)
	{
		//retrieve all users
		$this->db->from('size');
		$this->db->select('*');
		$this->db->where('size_name = \''.$size_name.'\'');
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing size
	*	@param int $size_id
	*
	*/
	public function delete_size($size_id)
	{
		if($this->db->delete('size', array('size_id' => $size_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated size
	*	@param int $size_id
	*
	*/
	public function activate_size($size_id)
	{
		$data = array(
				'size_status' => 1
			);
		$this->db->where('size_id', $size_id);
		
		if($this->db->update('size', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated size
	*	@param int $size_id
	*
	*/
	public function deactivate_size($size_id)
	{
		$data = array(
				'size_status' => 0
			);
		$this->db->where('size_id', $size_id);
		
		if($this->db->update('size', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
?>