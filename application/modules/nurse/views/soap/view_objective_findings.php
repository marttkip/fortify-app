<?php

$rs2 = $this->nurse_model->get_visit_objective_findings($visit_id);
$num_rows2 = count($rs2);

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] = $this->nurse_model->get_notes(4, $visit_id);

if(!isset($mobile_personnel_id))
{
  $mobile_personnel_id = NULL;
}
$v_data['mobile_personnel_id'] = $mobile_personnel_id;

$notes = $this->load->view('nurse/patients/notes', $v_data, TRUE);

?>
  <div class='row' style="margin-bottom: 10px;">
		<button type='button' class='btn btn-success pull-right' data-toggle='modal' data-target='#add_objective_findings'>Add Past Medical History</button>
	</div>
    <div class="modal fade bs-example-modal-lg" id="add_objective_findings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Past Medical History</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                      <div class='col-md-12'>
                            <textarea class='cleditor' id='visit_objective_findings' ></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a class='btn btn-info btn-sm' type='submit' onclick='save_objective_findings(<?php echo $visit_id;?>)'>Add Past Medical History</a>
                </div>
            </div>
        </div>
    </div>
  <div class='row'>
        <div class="col-md-8">
            <div id="objective_findings_section"><?php echo $notes;?></div>
        </div>
    </div>