<?php

class Mpesa_model extends CI_Model
{

  /*
  *	Count all items from a table
  *	@param string $table
  * 	@param string $where
  *
  */
  public function count_items($table, $where, $limit = NULL)
  {
    if($limit != NULL)
    {
      $this->db->limit($limit);
    }
    $this->db->from($table);
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  /*
  *	Retrieve all tenants
  *	@param string $table
  * 	@param string $where
  *
  */
  public function get_unreconcilled_payments($table, $where, $per_page=null, $page=null, $order=null, $order_method = 'ASC')
  {
    //retrieve all tenants
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }



	public function get_cancel_actions()
	{
		$this->db->where('cancel_action_status', 1);
		$this->db->order_by('cancel_action_name');

		return $this->db->get('cancel_action');
	}

  public function cancel_mpesa_payment($mpesa_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->session->userdata('personnel_id'),
			"cancelled_date" => date("Y-m-d"),
			"mpesa_status" => 1
		);

		$this->db->where('mpesa_id', $mpesa_id);
		if($this->db->update('mpesa_transactions', $data))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}



  	public function payment_item_add($lease_id)
  	{
  		$amount = $this->input->post('amount');
  		$invoice_type_id=$this->input->post('invoice_type_id');
  		// $invoice_id=$this->input->post('invoice_id');
  		$payment_month=$this->input->post('payment_month');
  		$payment_year=$this->input->post('payment_year');
  		$lease_number=$this->input->post('lease_number');
  		$rental_unit_id=$this->input->post('rental_unit_id');
  		$tenant_id=$this->input->post('tenant_id');
  		// insert for service charge

  		$this->db->where('lease_invoice_id',$invoice_id);
  		$query = $this->db->get('lease_invoice');

  		$row = $query->row();

  		$invoice_date = $row->invoice_date;


  		$this->db->where('invoice_type_id',$invoice_type_id);
  		$query_type = $this->db->get('invoice_type');
  		$row_two = $query_type->row();
  		$invoice_type_name = $row_two->invoice_type_name;
  		$concate = $payment_year.'-'.$payment_month;
  		$date = date('M Y',strtotime($concate));

  		$remarks = 'Payment for '.$date.' '.$invoice_type_name;

  		$service = array(
  							'payment_id'=>0,
  							'amount_paid'=> $amount,
  							'payment_month'=> $payment_month,
  							'rental_unit_id'=> $rental_unit_id,
  							'tenant_id'=> $tenant_id,
  							'lease_number'=> $lease_number,
  							'payment_year'=> $payment_year,
  							'invoice_type_id' => $invoice_type_id,
  							'payment_item_status' => 0,
  							'lease_id' => $lease_id,
  							'mpesa_id' => $this->input->post('mpesa_id'),
  							'personnel_id' => $this->session->userdata('personnel_id'),
  							'payment_item_created' => date('Y-m-d'),
  							'remarks'=>$remarks
  						);

  		// var_dump($service); die();
  		$this->db->insert('payment_item',$service);
  		return TRUE;

  	}


  	public function confirm_payment($lease_id,$personnel_id = NULL)
  	{
  		$amount = $this->input->post('total_amount');
  		$payment_method=$this->input->post('payment_method');
  		$type_of_account=$this->input->post('type_of_account');
  		$rental_unit_id = $this->input->post('rental_unit_id');
      $lease_number=$this->input->post('lease_number');
      $tenant_id=$this->input->post('tenant_id');
  		if($payment_method == 2 || $payment_method == 3 || $payment_method == 5)
  		{
  			$transaction_code = $this->input->post('reference_number');
  			$bank_id = $this->input->post('bank_id');
  		}
  		else
  		{
  			$transaction_code = '';
  			$bank_id = 1;
  		}

  		// calculate the points to get
  		$payment_date = $this->input->post('payment_date');

  		$date_check = explode('-', $payment_date);
  		$month = $date_check[1];
  		$year = $date_check[0];


  		$receipt_number = $this->accounts_model->create_receipt_number();

  		$data = array(
  			'payment_method_id'=>$payment_method,
  			'bank_id'=>$bank_id,
  			'amount_paid'=>$amount,
  			'personnel_id'=>$this->session->userdata("personnel_id"),
  			'transaction_code'=>$transaction_code,
  			'payment_date'=>$this->input->post('payment_date'),
  			'receipt_number'=>$transaction_code,
  			'document_number'=>$receipt_number,
  			'paid_by'=>$this->input->post('paid_by'),
  			'payment_created'=>date("Y-m-d"),
  			'rental_unit_id'=>$rental_unit_id,
  			'year'=>$year,
  			'month'=>$month,
        'account_id'=>$lease_number,
        'tenant_id'=>$tenant_id,
  			'confirm_number'=> $receipt_number,
  			'payment_created_by'=>$this->session->userdata("personnel_id"),
  			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
  		);

  		if($type_of_account == 1)
  		{
  			$data['lease_id'] = $lease_id;

  			if($this->db->insert('payments', $data))
  			{
  				$payment_id = $this->db->insert_id();
  				$service = array(
  									'payment_id'=>$payment_id,
  									'payment_item_created' =>$this->input->post('payment_date'),
  									'payment_item_status'=>1,
  									'rental_unit_id'=>$rental_unit_id,
                    'lease_number'=>$lease_number,
                    'tenant_id'=>$tenant_id,
  									'lease_id'=>$lease_id,
  									'document_no'=>$receipt_number
  								);
  				$this->db->where('payment_item_status = 0 AND lease_id = '.$lease_id.' and personnel_id = '.$this->session->userdata('personnel_id'));
  				$this->db->update('payment_item',$service);


  				return TRUE;
  			}
  			else{
  				return FALSE;
  			}
  		}
  		else if($type_of_account == 0)
  		{
  			return FALSE;

  		}
  	}
    public function send_money($phone,$amount,$short_loan_id = NULL)
    {
        $consumer_key = 'CNAoVjUxPmmc0V75oozTzKI7tcXg8gng';
        $consumer_secret = '70umWGjjtUYCbaYI';
        $headers = ['Content-Type:application/json; charset=utf8'];


        $access_token_url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init($access_token_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($curl, CURLOPT_HEADER, FALSE);

        curl_setopt($curl, CURLOPT_USERPWD, $consumer_key.':'.$consumer_secret);

        $result = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $result = json_decode($result);

        $access_token = $result->access_token;

        curl_close($curl);


        $url = 'https://api.safaricom.co.ke/mpesa/b2c/v1/paymentrequest';
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$access_token)); //setting custom header
        
        
        $curl_post_data = array(

          'InitiatorName' => 'BOBBYAPP',
          'SecurityCredential' => 'aktE00NGAd5berqE7Wx3Uhh0xbkBzsF1sjVZrlOFPPEMnzKf/USdu+F1Zvn+GwRkWUVC6APMmHY2WKD0wpyVb20sEn7bwquM1ea4uzgbUFW5QINvAKF3DXdl45ZsKVNE+j59d4f5hohuwBxObksi5zN90GMdI9E/AcrhsK23BYyDkrrb/0Q/b5IIhyNs3fsBZDESS1dW2qHA3D/ETI1ZtyI08M44Cm4VJsh1WS0a5MaIa9RMhiBC5fcHgfI4C/7oMlHnL7FKyBMxPCVBrg8chGJG6J5Bqq+tifaFN9N7qFOKnKJaeFWIjYh1tHE5j20ugWmRKU5sBPHiGM0lvMOXpg==',
          'CommandID' => 'SalaryPayment',
          'Amount' => '10',
          'PartyA' => '3012483',
          'PartyB' => $phone,
          'Remarks' => 'Salary',
          'QueueTimeOutURL' => 'https://simplex-financials.com/transaction/QueueTimeOutURL.php',
          'ResultURL' => 'https://simplex-financials.com/transaction/ResultURL.php',
          'Occasion' => 'SalaryPayment'

        );
        
        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response2 = curl_exec($curl);

        
        curl_close($curl);

        $curl_message = json_decode($curl_response2,true);

        $ResponseCode = $curl_message['ResponseCode'];
        $ResponseCode = $curl_message['ResponseCode'];
        $update_array['transaction_code'] = '';
        $update_array['short_loan_status'] = 1;
        $update_array['mpesa_response'] = $curl_response2;


        $update_array['loans_plan_id'] = 6;
        $json_array = json_decode($curl_response2,true);

        // $update_array['conversation_id'] = $json_array['ConversationID'];
        // $update_array['originator_conversation_id'] = $json_array['OriginatorConversationID'];

        $this->db->where('short_loan_id',$short_loan_id);
        $this->db->update('short_loan',$update_array);
        // return $curl_message;

    }


}
?>
